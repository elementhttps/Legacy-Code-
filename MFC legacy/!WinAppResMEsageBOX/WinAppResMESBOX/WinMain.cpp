#include <windows.h>

//---------------------------------------------------------------------------

LPCTSTR Caption = "Application Programming Interface";

INT WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance,
                   LPSTR lpCmdLine, int nCmdShow)
{
    MessageBox( NULL,
                "Welcome to Win32 Application Development\n"
                "You will learn about functions, classes, "
                "communication, and other cool stuff\n"
                "Are you ready to rumble!!!!!!!!!!!!!!",
                Caption,
                MB_ABORTRETRYIGNORE | MB_ICONWARNING);

    return 0;
}
/*
  MB_OK	
  MB_OKCANCEL	 
  MB_ABORTRETRYIGNORE	  
  MB_YESNOCANCEL	  
  MB_YESNO	 
  MB_RETRYCANCEL	 
  MB_CANCELTRYCONTINUE	  
  MB_HELP 
  
MB_ICONEXCLAMATION
MB_ICONWARNING	 	
MB_ICONINFORMATION
MB_ICONASTERISK	 	
MB_ICONQUESTION	 	
MB_ICONSTOP
MB_ICONERROR
MB_ICONHAND	
*/ 