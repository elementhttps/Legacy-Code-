// Atmosfera.cpp : implementation file
//

#include "stdafx.h"
#include "KreatorTrajektorijaDV.h"
#include "Atmosfera.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CAtmosfera dialog


CAtmosfera::CAtmosfera(CWnd* pParent /*=NULL*/)
	: CDialog(CAtmosfera::IDD, pParent)
{
	//{{AFX_DATA_INIT(CAtmosfera)
	m_PAG = -1;
	m_TAG = -1;
	m_tropos = 0;
	m_troposp = 0;
	m_mezos = 0;
	m_stratos = 0;
	m_PadT = 0.0;
	m_To = 0.0;
	m_PTG = -1;
	//}}AFX_DATA_INIT
}


void CAtmosfera::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CAtmosfera)
	DDX_Radio(pDX, IDC_PRIKAZGA_DA, m_PAG);
	DDX_Radio(pDX, IDC_TEKSTA_DA, m_TAG);
	DDX_Text(pDX, IDC_TROPOS, m_tropos);
	DDX_Text(pDX, IDC_TROPOP, m_troposp);
	DDX_Text(pDX, IDC_MEZOSFERA, m_mezos);
	DDX_Text(pDX, IDC_STRATOSFERA, m_stratos);
	DDX_Text(pDX, IDC_PADT, m_PadT);
	DDX_Text(pDX, IDC_PTO, m_To);
	DDX_Radio(pDX, IDC_TEMP_DA, m_PTG);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CAtmosfera, CDialog)
	//{{AFX_MSG_MAP(CAtmosfera)
		// NOTE: the ClassWizard will add message map macros here
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CAtmosfera message handlers
