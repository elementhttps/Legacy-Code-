// AutorDV.cpp : implementation file
//

#include "stdafx.h"
#include "KreatorTrajektorijaDV.h"
#include "AutorDV.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CAutorDV dialog


CAutorDV::CAutorDV(CWnd* pParent /*=NULL*/)
	: CDialog(CAutorDV::IDD, pParent)
{
	//{{AFX_DATA_INIT(CAutorDV)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void CAutorDV::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CAutorDV)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CAutorDV, CDialog)
	//{{AFX_MSG_MAP(CAutorDV)
		// NOTE: the ClassWizard will add message map macros here
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CAutorDV message handlers
