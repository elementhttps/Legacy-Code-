// Korak.cpp : implementation file
//

#include "stdafx.h"
#include "KreatorTrajektorijaDV.h"
#include "Korak.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CKorak dialog


CKorak::CKorak(CWnd* pParent /*=NULL*/)
	: CDialog(CKorak::IDD, pParent)
{
	//{{AFX_DATA_INIT(CKorak)
	m_BrojTacaka = 0.0;
	m_IDev = 0;
	m_Korak = 0.0;
	//}}AFX_DATA_INIT
}


void CKorak::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CKorak)
	DDX_Text(pDX, IDC_BROJTACAKA, m_BrojTacaka);
	DDX_Text(pDX, IDC_LOOP, m_IDev);
	DDX_Text(pDX, IDC_KORAK, m_Korak);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CKorak, CDialog)
	//{{AFX_MSG_MAP(CKorak)
	ON_BN_CLICKED(IDC_IZRACUNAJ, OnIzracunaj)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CKorak message handlers


void CKorak::OnIzracunaj() 
{
	UpdateData(true);

	m_BrojTacaka =m_IDev/m_Korak;
	UpdateData(false);
	
}
