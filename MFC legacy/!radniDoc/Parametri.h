#if !defined(AFX_PARAMETRI_H__48ACA1AC_30EE_4000_9633_0DD03D1D13DB__INCLUDED_)
#define AFX_PARAMETRI_H__48ACA1AC_30EE_4000_9633_0DD03D1D13DB__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// Parametri.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CParametri dialog

class CParametri : public CDialog
{
// Construction
public:
	CParametri(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CParametri)
	enum { IDD = IDD_PARAMETRI };
	int		m_Brzina_f3;
	int		m_Ugao_f1;
	int		m_Ugao_f2;
	int		m_Ugao_f3;
	double	m_PI;
	double	m_Grav;
	int		m_Mf1;
	int		m_Mf2;
	int		m_Mf3;
	double	m_Brzina_f1;
	double	m_Brzina_f2;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CParametri)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CParametri)
		// NOTE: the ClassWizard will add member functions here
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_PARAMETRI_H__48ACA1AC_30EE_4000_9633_0DD03D1D13DB__INCLUDED_)
