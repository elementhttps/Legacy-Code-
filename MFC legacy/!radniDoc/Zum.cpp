// Zum.cpp : implementation file
//

#include "stdafx.h"
#include "KreatorTrajektorijaDV.h"
#include "Zum.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CZum dialog


CZum::CZum(CWnd* pParent /*=NULL*/)
	: CDialog(CZum::IDD, pParent)
{
	//{{AFX_DATA_INIT(CZum)
	m_Zumx = 0;
	m_Zumy = 0;
	//}}AFX_DATA_INIT
}


void CZum::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CZum)
	DDX_Text(pDX, IDC_ZUM, m_Zumx);
	DDX_Text(pDX, IDC_ZUMY, m_Zumy);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CZum, CDialog)
	//{{AFX_MSG_MAP(CZum)
		// NOTE: the ClassWizard will add message map macros here
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CZum message handlers
