//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by KreatorTrajektorijaDV.rc
//
#define IDD_ABOUTBOX                    100
#define IDR_MAINFRAME                   128
#define IDR_KREATOTYPE                  129
#define IDD_PARAMETRI                   130
#define IDD_MREZA                       132
#define IDD_KORAK                       133
#define IDD_UPUTSTVO                    134
#define IDD_AUTORDV                     135
#define IDB_BITMAP1                     136
#define IDD_DGRAFIK                     137
#define IDD_ITABLA                      138
#define IDD_ZUM                         139
#define IDD_ATMOSFERA                   140
#define IDB_BITMAP2                     141
#define IDC_UGAO_F1                     1000
#define IDC_BRZINA_F1                   1001
#define IDC_MREZA_DA                    1001
#define IDC_UGAO_F2                     1002
#define IDC_MREZA_NE                    1002
#define IDC_KORAK                       1002
#define IDC_BRZINA_F2                   1003
#define IDC_LOOP                        1003
#define IDC_UGAO_F3                     1004
#define IDC_BROJTACAKA                  1004
#define IDC_BRZINA_F3                   1005
#define IDC_IZRACUNAJ                   1005
#define IDC_DGRAF_DA                    1006
#define IDC_DGRAF_NE                    1007
#define IDC_GMREZA                      1008
#define IDC_TABLA_DA                    1009
#define IDC_TABLA_NE                    1010
#define IDC_PI                          1011
#define IDC_GRAV                        1012
#define IDC_ZUM                         1012
#define IDC_MF1_DA                      1013
#define IDC_ZUMY                        1013
#define IDC_MF1_NE                      1014
#define IDC_MF2_DA                      1015
#define IDC_MF2_NE                      1016
#define IDC_MF3_DA                      1017
#define IDC_MF3_NE                      1018
#define IDC_TROPOS                      1019
#define IDC_TROPOP                      1020
#define IDC_STRATOSFERA                 1021
#define IDC_MEZOSFERA                   1022
#define IDC_PRIKAZGA_DA                 1023
#define IDC_PRIKAZAG_NE                 1024
#define IDC_TEKSTA_DA                   1025
#define IDC_TEKSTA_NE                   1026
#define IDC_PTO                         1027
#define IDC_PADT                        1028
#define IDC_TEMP_DA                     1029
#define IDC_TEMP_NE                     1030
#define IDM_PARAMETRI                   32771
#define IDM_MREZA                       32772
#define IDM_DGRAFIK                     32773
#define IDM_AUTOR                       32774
#define IDM_UPUTSTVO                    32775
#define IDM_KORAK                       32776
#define IDM_TABLA                       32777
#define IDM_ZUM                         32778
#define IDM_ATMOSFERA                   32779

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        142
#define _APS_NEXT_COMMAND_VALUE         32780
#define _APS_NEXT_CONTROL_VALUE         1031
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
