// BitTest.h : main header file for the BITTEST application
//

#if !defined(AFX_BITTEST_H__43B69E35_A258_4DEB_AAA8_EBC767AB954E__INCLUDED_)
#define AFX_BITTEST_H__43B69E35_A258_4DEB_AAA8_EBC767AB954E__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"       // main symbols

/////////////////////////////////////////////////////////////////////////////
// CBitTestApp:
// See BitTest.cpp for the implementation of this class
//

class CBitTestApp : public CWinApp
{
public:
	CBitTestApp();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CBitTestApp)
	public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL

// Implementation
	//{{AFX_MSG(CBitTestApp)
	afx_msg void OnAppAbout();
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_BITTEST_H__43B69E35_A258_4DEB_AAA8_EBC767AB954E__INCLUDED_)
