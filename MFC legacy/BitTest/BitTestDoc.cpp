// BitTestDoc.cpp : implementation of the CBitTestDoc class
//

#include "stdafx.h"
#include "BitTest.h"

#include "BitTestDoc.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CBitTestDoc

IMPLEMENT_DYNCREATE(CBitTestDoc, CDocument)

BEGIN_MESSAGE_MAP(CBitTestDoc, CDocument)
	//{{AFX_MSG_MAP(CBitTestDoc)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CBitTestDoc construction/destruction

CBitTestDoc::CBitTestDoc()
{
	// TODO: add one-time construction code here

}

CBitTestDoc::~CBitTestDoc()
{
}

BOOL CBitTestDoc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;

	// TODO: add reinitialization code here
	// (SDI documents will reuse this document)

	return TRUE;
}



/////////////////////////////////////////////////////////////////////////////
// CBitTestDoc serialization

void CBitTestDoc::Serialize(CArchive& ar)
{
	if (ar.IsStoring())
	{
		// TODO: add storing code here
	}
	else
	{
		// TODO: add loading code here
	}
}

/////////////////////////////////////////////////////////////////////////////
// CBitTestDoc diagnostics

#ifdef _DEBUG
void CBitTestDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CBitTestDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CBitTestDoc commands
