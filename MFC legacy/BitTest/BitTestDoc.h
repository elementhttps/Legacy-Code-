// BitTestDoc.h : interface of the CBitTestDoc class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_BITTESTDOC_H__E26CB65D_B8C6_4714_9C24_CCC25724B4D0__INCLUDED_)
#define AFX_BITTESTDOC_H__E26CB65D_B8C6_4714_9C24_CCC25724B4D0__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


class CBitTestDoc : public CDocument
{
protected: // create from serialization only
	CBitTestDoc();
	DECLARE_DYNCREATE(CBitTestDoc)

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CBitTestDoc)
	public:
	virtual BOOL OnNewDocument();
	virtual void Serialize(CArchive& ar);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CBitTestDoc();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	//{{AFX_MSG(CBitTestDoc)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_BITTESTDOC_H__E26CB65D_B8C6_4714_9C24_CCC25724B4D0__INCLUDED_)
