// BitTestView.h : interface of the CBitTestView class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_BITTESTVIEW_H__325E3D15_9BA8_4653_A0F2_319A844655B5__INCLUDED_)
#define AFX_BITTESTVIEW_H__325E3D15_9BA8_4653_A0F2_319A844655B5__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


class CBitTestView : public CView
{
protected: // create from serialization only
	CBitTestView();
	DECLARE_DYNCREATE(CBitTestView)

// Attributes
public:
	CBitTestDoc* GetDocument();

// Operations
public:
CDC m_MemDC;
CBitmap m_bmpView;
int m_nBmpWidth,m_nBmpHeight;
CPoint pt;


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CBitTestView)
	public:
	virtual void OnDraw(CDC* pDC);  // overridden to draw this view
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	virtual void OnInitialUpdate();
	virtual void OnPrepareDC(CDC* pDC, CPrintInfo* pInfo = NULL);
	protected:
	virtual BOOL OnPreparePrinting(CPrintInfo* pInfo);
	virtual void OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo);
	virtual void OnEndPrinting(CDC* pDC, CPrintInfo* pInfo);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CBitTestView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	//{{AFX_MSG(CBitTestView)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

#ifndef _DEBUG  // debug version in BitTestView.cpp
inline CBitTestDoc* CBitTestView::GetDocument()
   { return (CBitTestDoc*)m_pDocument; }
#endif

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_BITTESTVIEW_H__325E3D15_9BA8_4653_A0F2_319A844655B5__INCLUDED_)
