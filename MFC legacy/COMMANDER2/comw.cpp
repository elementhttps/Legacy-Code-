//#include "windows.h"
#include <stdio.h>
#include <io.h>
#include <conio.h>
#include <stdlib.h>
#include <iostream>
#include <stdio.h>
#include <string>             // for STL string class
#include <windows.h>          // for HANDLE
#include <process.h>          // for _beginthread()


#define maxBytes 64//128  //256 5 sekundi //interesantno za 8   //procitaj char do 256
#define MAX 32//115200//4096//2048//1024//utice znatno na brzinu npr 8  //max broj kolona u fajlu da procita
       
	HANDLE hSerial;
	DCB dcb;
	COMMTIMEOUTS timeouts;
	DWORD dwBytesWrite = MAX;
	int i;
	char szBuff[maxBytes];
	char stemp[MAX];
	FILE *fp;
    OVERLAPPED osWrite = {0};
    char * lpBuf;

 
class Thread
{
  
public:

  static unsigned __stdcall ThreadStaticEntryPoint(void * pThis)//POCETAK THREDA 
  {
      Thread * pthX = (Thread*)pThis;   // the tricky cast //pointer na threadX pthX =ThreadStaticEntryPoint na void funkciju
      pthX->ThreadEntryPoint(); // now call the true entry-point-function //pozivam iz klase funkciju
     

      return 1;          // the thread exit code              //KRAJ THREDA 
  }

  void ThreadEntryPoint()
  {


//	LocalFree(szBuff);


	    
    //printf( "%s thread terminating\n" );
  }
};





int main()
{



	//opening the serial port
	hSerial = CreateFile(
		"COM2",// Pointer to the name of the port
		GENERIC_WRITE,//GENERIC_READ | GENERIC_WRITE,  // Access (read/write) mode
		0,           // Share mode
		NULL,// Pointer to the security attribute
		OPEN_EXISTING,   // How to open the serial port
	   	0,//FILE_ATTRIBUTE_NORMAL,
		NULL); //0

	if(hSerial==INVALID_HANDLE_VALUE)
	{
		if(GetLastError()==ERROR_FILE_NOT_FOUND)
		{
			printf("Serial port does not exist\n");
		}
		printf("Other errors\n");
	}

	//setting parameters
	dcb.DCBlength = sizeof (dcb);

	//GetCommState is to retrieves the current control settings for a specific communications device.
	if (!GetCommState(hSerial, &dcb))
	{
		printf("Not GetCommState, not able to retrieves the current control.\n");
	}
   
/*
iz MTTTY programa
char * szBaud[] = {
	    "110", "300", "600", "1200", "2400", 
	    "4800", "9600", "14400", "19200",
	    "38400", "56000", "57600", "115200","120000", 
	    "128000","230400","240000","300000","921600","1000000", "2457600", "3000000","4000000","6000000"
	};

DWORD   BaudTable[] =  {
	    CBR_110, CBR_300, CBR_600, CBR_1200, CBR_2400,
	    CBR_4800, CBR_9600, CBR_14400, CBR_19200, CBR_38400,
	    CBR_56000, CBR_57600, CBR_115200,120000, CBR_128000,230400,240000,300000, 921600,1000000,2457600,3000000, 4000000,6000000
	} ;


 //pri brzini od 6.000.000 braud slanje je trajalo 48 sekundi COMMANDERU2 bez tredova  //brzia je oko 153000 baud
  //pri brzini od 6.000.000 braud slanje je trajalo 7 sekundi u MTTTY sa tredovima 

EKSPERIMENT 2

!WriteFile(
			hSerial, // Port handle  
			lpBuf,//szBuff, 
			dwBytesWrite,  // Number of bytes to write 
			&dwBytesWrite, // Pointer to the number of bytes written
			&osWrite))// Must be NULL for Windows CE


  ubaceno je 
  umesto  char szBuff[maxBytes]; stavljeno je char * lpBuf;
  umesto maxBytes    ----------- DWORD dwBytesWrite = MAX;
  umesto NULL        ----------- &osWrite

*/	
	
	
	dcb.BaudRate =6000000;//4000000;  //2457600 9s za 2000 redova;//CBR_128000;//CBR_256000;//6000000;//CBR_9600;//CBR_256000;//CBR_115200;
	dcb.ByteSize = 8;
	dcb.StopBits = ONESTOPBIT;
	dcb.Parity = NOPARITY;
	dcb.fBinary = TRUE;// Binary mode; no EOF check
	dcb.fParity = TRUE; // Enable parity checking
    dcb.fDtrControl     = DTR_CONTROL_ENABLE;// DTR flow control type
    dcb.fRtsControl     = RTS_CONTROL_ENABLE;// RTS flow control
    dcb.fOutxCtsFlow    = FALSE;// No CTS output flow control
    dcb.fOutxDsrFlow    = FALSE;// No DSR output flow control
    dcb.fDsrSensitivity = FALSE;// DSR sensitivity
    dcb.fOutX           = FALSE;//No XON/XOFF out flow control
    dcb.fInX            = FALSE;//No XON/XOFF in flow control
    dcb.fTXContinueOnXoff = TRUE;// XOFF continues Tx
	dcb.fNull = FALSE; // Disable null stripping.
	dcb.fErrorChar = FALSE; // Disable error replacement.
    dcb.fAbortOnError = FALSE; // Do not abort reads/writes on error
  

	//SetCommState configures a communications device according to the specifications
	//in DCB. The function reinitializes all hardware control settings but it does not
	//empty output or input queues
	if (!SetCommState(hSerial, &dcb))
	{
		printf("Not SetCommState, cannot configures serial port according to DCB specifications set.\n");
	}
    GetCommTimeouts(hSerial, &timeouts);
	//setting timeouts
	timeouts.ReadIntervalTimeout = MAXDWORD;
	timeouts.ReadTotalTimeoutConstant = 0;
	timeouts.ReadTotalTimeoutMultiplier = 0;
	timeouts.WriteTotalTimeoutConstant = 10;
	timeouts.WriteTotalTimeoutMultiplier = 1000;

	//SetCommTimeouts set the time out parameters for all reand and write operation
	if (!SetCommTimeouts(hSerial, &timeouts))
	{
		printf("Not SetCommTimeouts, cannot set the timeout parameters to serial port.\n");
	}

	//Writting data
	//WriteFile write data from the specified file or i/o devices.

//-------------------------------------------------------------------------------------
Thread * o1 = new Thread();

    HANDLE   hth1;// handle na thread 1 
    unsigned  uiThread1ID; //unsigned uzmi samo pozitivne vrednosti

	hth1 = (HANDLE)_beginthreadex( NULL,         // security
                                   0,            // stack size
								   Thread::ThreadStaticEntryPoint,
                                   o1,           // arg list
                                   CREATE_SUSPENDED,  // so we can later call ResumeThread()
                                   &uiThread1ID );

    if ( hth1 == 0 )//ako je handle ekvivalentno nuli
        printf("Failed to create thread 1\n");

    DWORD   dwExitCode;

    GetExitCodeThread( hth1, &dwExitCode );  // should be STILL_ACTIVE = 0x00000103 = 259 // ubaci u exit code vrednost
    printf( "initial thread 1 exit code = %u\n", dwExitCode );

	

 
//-------------------------------------------------------------------------------------
//if (!WriteFile(hFile, lpBuf, dwBufLen, &dwWritten, NULL))
    printf("The content inside the file: \n\n");
	fp = fopen("1.txt", "r");
	while ((fgets(stemp, MAX, fp)) != NULL)
	{
		i = sprintf(szBuff, "%s", stemp);
		if (!WriteFile(
			hSerial, // Port handle  
			lpBuf,//szBuff, 
			dwBytesWrite,  // Number of bytes to write 
			&dwBytesWrite, // Pointer to the number of bytes written
			&osWrite))// Must be NULL for Windows CE
		{


  
			printf("Serial port cannot write file.\n");
		}
		else
		{
			//bez printf 45 sekundi pri 6.000.000 braud

				printf("%s", szBuff);
				LocalFree(szBuff);
		}
	}

   
    ResumeThread( hth1 );   
    Sleep(10);//10
	WaitForSingleObject( hth1, INFINITE );
    CloseHandle( hth1 );
    delete o1;//cisti thread1
    o1 = NULL;
	fclose(fp);
	CloseHandle(hSerial);
	printf("Primary thread terminating.\n");








}

/*

	while ((fgets(stemp, MAX, fp)) != NULL)
	{
		i = sprintf(szBuff, "%s", stemp);
		if (!WriteFile(
			hSerial, // Port handle  
			szBuff, 
			maxBytes,  // Number of bytes to write 
			&dwBytesWrite, // Pointer to the number of bytes written
			NULL))// Must be NULL for Windows CE
		{


  
			printf("Serial port cannot write file.\n");
		}
		else
		{
			//bez printf 45 sekundi pri 6.000.000 braud

				printf("%s", szBuff);
				LocalFree(szBuff);
		}
	}







	while ((fgets(stemp, MAX, fp)) != NULL)
	{
		i = sprintf(szBuff, "%s", stemp);
		if (!WriteFile(
			hSerial, // Port handle  
			szBuff, 
			maxBytes,  // Number of bytes to write 
			&dwBytesWrite, // Pointer to the number of bytes written
			NULL))// Must be NULL for Windows CE
		{


  
			printf("Serial port cannot write file.\n");
		}
		else
		{
			//bez printf 45 sekundi pri 6.000.000 braud

				printf("%s", szBuff);
				LocalFree(szBuff);
		}
	}
	*/