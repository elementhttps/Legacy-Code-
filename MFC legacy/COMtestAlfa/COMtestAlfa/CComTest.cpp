// CComTest.cpp : implementation file
//

#include "stdafx.h"
#include "COMtestAlfa.h"
#include "CComTest.h"

#using <System.dll>// project//imeXXX properties //configuration properties//general//common langugage runtime support//common langugage runtime support /clr
using namespace System;
using namespace System::IO::Ports;
using namespace System::ComponentModel;
// CCComTest dialog

IMPLEMENT_DYNAMIC(CCComTest, CDialog)

CCComTest::CCComTest(CWnd* pParent /*=NULL*/)
	: CDialog(CCComTest::IDD, pParent)
	, m_status(_T(""))//string
	, m_COMn(_T(""))//string
	, m_BaudRate(0)
	, m_Parity(_T(""))
	, m_StopBits(_T(""))
	, m_ByteSize(_T(""))
{

}

CCComTest::~CCComTest()
{
		m_COMn = "COM2";
		m_BaudRate = 9600;
	    //m_Parity = "NOPAR";
		//m_StopBits ="1";
		//m_ByteSize ="8"; 


}

void CCComTest::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDIT2, m_status);
	DDX_CBString(pDX, IDC_COMBO1, m_COMn);
	DDX_CBIndex(pDX, IDC_COMBO2, m_BaudRate);
	DDX_CBString(pDX, IDC_COMBO3, m_Parity);
	DDX_CBString(pDX, IDC_COMBO4, m_StopBits);
	DDX_CBString(pDX, IDC_COMBO5, m_ByteSize);
}


BEGIN_MESSAGE_MAP(CCComTest, CDialog)
	
	ON_BN_CLICKED(IDC_BUTTON1, &CCComTest::OnBnClickedButton1)
	ON_BN_CLICKED(IDC_BUTTON2, &CCComTest::OnBnClickedButton2)
	ON_BN_CLICKED(IDC_BUTTON3, &CCComTest::OnBnClickedButton3)
	ON_BN_CLICKED(IDC_BUTTON4, &CCComTest::OnBnClickedButton4)
	ON_BN_CLICKED(IDC_BUTTON5, &CCComTest::OnBnClickedButton5)
END_MESSAGE_MAP()


// CCComTest message handlers

char * szParity[] =   {   "None", "Even", "Odd", "Mark", "Space" };

DWORD   ParityTable[] = {  NOPARITY, EVENPARITY, ODDPARITY, MARKPARITY, SPACEPARITY  } ;

char * szStopBits[] =  {  "1", "1.5", "2"  };

//DWORD   StopBitsTable[] =  { ONESTOPBIT, ONE5STOPBITS, TWOSTOPBITS } ;
//DWORD  ONESTOPBIT, ONE5STOPBITS, TWOSTOPBITS  ;

 


void CCComTest::OnBnClickedButton1()
{   
 UpdateData(true);

DeviceHandle = CreateFile 
(  m_COMn,                           //_T("\\\\.\\COM2") ,//otvori  portove _T("\\\\.\\COMn")
  GENERIC_READ|GENERIC_WRITE, 
  FILE_SHARE_READ|FILE_SHARE_WRITE, 
  (LPSECURITY_ATTRIBUTES)NULL, 
  OPEN_EXISTING, 
  0, 
  NULL);

 if (GetCommState(DeviceHandle,&config_) == 0)
    {
      
	  AfxMessageBox(_T("Port nije otvoren\nMoguci problemi:\n - port nije fizicki prikljucen  \n - drajver nije instaliran (npr USB to rs232 converter )\n - GetCommState(DeviceHandle,&config_) == 0 \n - kod nevalja :)  "));
    
    }
  if (GetCommState(DeviceHandle,&config_) != 0)
    {
      AfxMessageBox(_T("PORT otvoren"),MB_ICONINFORMATION);
      SetDlgItemText(IDC_EDIT2,_T("OTVOREN"));
    }
	
	
	

}



void CCComTest::OnBnClickedButton3()
{
	ShellExecute(this->m_hWnd,_T("open"),_T("C:\\WINDOWS\\system32\\devmgmt.msc"),	_T(""),_T(""),SW_SHOW );

}
/*
  BOOL statusPort_;                    // port's status.
	HANDLE handlePort_;                  // the object that is a instace of port. 
  DCB config_;                         // configuation of serial communication.  








  */
void CCComTest::OnBnClickedButton4()
{
	CONSOLE_FULLSCREEN ;
	AllocConsole();
	
	array<String^>^ serialPorts = nullptr;
    try
    {
        // Get a list of serial port names.
        serialPorts = SerialPort::GetPortNames();
    }
    catch (Win32Exception^ ex)
    {
        Console::WriteLine(ex->Message);
    }

    Console::WriteLine("Nadjeni su sledeci portovi:");
    //SetDlgItemText(IDC_EDIT1,_T("The following serial ports were found:"));
    // Display each port name to the console.
    for each(String^ port in serialPorts)
	{   
		
        Console::WriteLine(port);
     
	
    }
	//::KEYEVENTF_KEYUP
	//::MOUSEEVENTF_RIGHTDOWN
	//Sleep(3000);
	
}

void CCComTest::OnBnClickedButton5()
{
/*

110 
300
600
1200
2400
4800
9600
14400
19200  
38400 
56000 
57600
115200 


	#define NOPARITY            0
#define ODDPARITY           1
#define EVENPARITY          2
#define MARKPARITY          3
#define SPACEPARITY         4

#define ONESTOPBIT          0
#define ONE5STOPBITS        1
#define TWOSTOPBITS         2
      */


	
    //config_.BaudRate = 9600;    // Specify buad rate of communicaiton.
   // config_.StopBits = ONESTOPBIT;    // 0,1,2 = 1, 1.5, 2
	//config_.Parity = NOPARITY;
   // config_.ByteSize = 8;    // Specify  byte of size of communication.
	config_.fBinary = TRUE; // Binary mode; no EOF check
    config_.fErrorChar = FALSE; // Disable error replacement.
    config_.fNull = FALSE; // Disable null stripping.
	config_.fAbortOnError = FALSE;// Do not abort reads/writes on error
	
	
	
	/*
PortDCB.BaudRate = 9600;  // Current baud rate
PortDCB.fBinary = TRUE; // Binary mode; no EOF check
PortDCB.fParity = TRUE; // Enable parity checking.
PortDCB.fOutxCtsFlow = FALSE; // No CTS output flow control
PortDCB.fOutxDsrFlow = FALSE; // No DSR output flow control
PortDCB.fDtrControl = DTR_CONTROL_ENABLE; // DTR flow control type
PortDCB.fDsrSensitivity = FALSE; // DSR sensitivity
PortDCB.fTXContinueOnXoff = TRUE;    // XOFF continues Tx
PortDCB.fOutX = FALSE; // No XON/XOFF out flow control
PortDCB.fInX = FALSE; // No XON/XOFF in flow control
PortDCB.fErrorChar = FALSE; // Disable error replacement.
PortDCB.fNull = FALSE; // Disable null stripping.
PortDCB.fRtsControl = RTS_CONTROL_ENABLE;  // RTS flow control
PortDCB.fAbortOnError = FALSE; // Do not abort reads/writes on error
PortDCB.ByteSize = 8;  // Number of bits/bytes, 4-8
PortDCB.Parity = NOPARITY; // 0-4=no,odd,even,mark,space
PortDCB.StopBits = ONESTOPBIT; // 0,1,2 = 1, 1.5, 2


*/
	


	UpdateData(TRUE);
	Invalidate();
    if (SetCommState(DeviceHandle,&config_) == 0)
    {
       AfxMessageBox(_T("Otvori port prvo"),MB_ICONWARNING);
       
    }
/*
COM STATUS 
ako je otvoren port i [podesi com] je pritisnuto onda prilikom odabira prikazi poruku
vezanu za konfiguraciju

*/
	if(SetCommState(DeviceHandle,&config_) != 0)
{
	   
	//PARITY-------------------------------------------------------------
	//PARITY KONFIGURISI  - konf parity i prikazi poruku ako je port otvoren
	//VREDNOSTI - NOPARITY, EVENPARITY, ODDPARITY, MARKPARITY, SPACEPARITY 
	UpdateData(TRUE);
    GetCommState(DeviceHandle,&config_);
	if (m_Parity == "NOPAR")
    {
		config_.Parity=NOPARITY;UpdateData(TRUE);
		AfxMessageBox(_T("NOPARITY "),MB_ICONINFORMATION);
	}
	if (m_Parity == "NEPAR")
	{
		config_.Parity=ODDPARITY;UpdateData(TRUE);
		AfxMessageBox(_T("NEPAR "),MB_ICONINFORMATION);
	}
    if (m_Parity == "PAR")
	{
		config_.Parity=EVENPARITY;UpdateData(TRUE);
		AfxMessageBox(_T("PAR "),MB_ICONINFORMATION);
	}
     if (m_Parity == "MARK")
	{
		config_.Parity=MARKPARITY;UpdateData(TRUE);
		AfxMessageBox(_T("MARK "),MB_ICONINFORMATION);
	}
	  if (m_Parity == "SPACE")
	{
		config_.Parity=SPACEPARITY;UpdateData(TRUE);
		AfxMessageBox(_T("SPACE "),MB_ICONINFORMATION);
	}
	UpdateData(TRUE);
	if (GetCommState(DeviceHandle,&config_) == 0)
    {
      AfxMessageBox(_T("Get configuration port has problem."));
    
    }
   //PARITY konfigurisan i upadate data
   //PARITY-------------------------------------------------------------
   
   //STOPBITS-------------------------------------------------------------
   //STOPBITS 
   //VREDNOSTI - ONESTOPBIT, ONE5STOPBITS, TWOSTOPBITS
   UpdateData(TRUE);
   GetCommState(DeviceHandle,&config_);
	if (m_StopBits == "1")
    {
		config_.StopBits=ONESTOPBIT;UpdateData(TRUE);
		AfxMessageBox(_T("ONESTOPBIT "),MB_ICONINFORMATION);
	}
	if (m_StopBits == "1.5")
	{
		config_.StopBits=ONE5STOPBITS;UpdateData(TRUE);
		AfxMessageBox(_T("ONE5STOPBITS "),MB_ICONINFORMATION);
	}
    if (m_StopBits == "2")
	{
		config_.StopBits=TWOSTOPBITS;UpdateData(TRUE);
		AfxMessageBox(_T("TWOSTOPBITS "),MB_ICONINFORMATION);
	}
    UpdateData(TRUE);
    if (GetCommState(DeviceHandle,&config_) == 0)
    {
      AfxMessageBox(_T("Get configuration port has problem."));
    
    }
	//STOPBITS konfigurisan
    //STOPBITS-------------------------------------------------------------
    
	//BYTESIZE-------------------------------------------------------------
    //BYTESIZE 
	//VREDNOSTI  - 4 5 6 7 8
     UpdateData(TRUE);
    GetCommState(DeviceHandle,&config_);
	if (m_ByteSize == "4")
    {
		config_.ByteSize=4;UpdateData(TRUE);
		AfxMessageBox(_T("4 "),MB_ICONINFORMATION);
	}
	if (m_ByteSize == "5")
	{
		config_.ByteSize=5;UpdateData(TRUE);
		AfxMessageBox(_T("5 "),MB_ICONINFORMATION);
	}
    if (m_ByteSize == "6")
	{
		config_.StopBits=6;UpdateData(TRUE);
		AfxMessageBox(_T(" "),MB_ICONINFORMATION);
	}
	if (m_ByteSize == "7")
	{
		config_.ByteSize=7;UpdateData(TRUE);
		AfxMessageBox(_T("7 "),MB_ICONINFORMATION);
	}
    if (m_ByteSize == "8")
	{
		config_.ByteSize=8;UpdateData(TRUE);
		AfxMessageBox(_T("8 "),MB_ICONINFORMATION);
	}
    UpdateData(TRUE);
    if (GetCommState(DeviceHandle,&config_) == 0)
    {
      AfxMessageBox(_T("Get configuration port has problem."));
    
    }
    //BYTESIZE konfigurisan
    //BYTESIZE-------------------------------------------------------------
    

   //COMMTIMEOUT-------------------------------------------------------------
   //COMMTIMEOUT konfiguracija 

	GetCommTimeouts(DeviceHandle,&comtime);	// Retrieve the time-out parameters for all read and write operations on the port.
	
	    comtime.ReadIntervalTimeout = MAXDWORD;// Maximum time between read chars
		comtime.ReadTotalTimeoutConstant = 0;//Constant in milliseconds
		comtime.ReadTotalTimeoutMultiplier = 0;//Multiplier of characters
		comtime.WriteTotalTimeoutConstant =10;//Constant in milliseconds
		comtime.WriteTotalTimeoutMultiplier =1000;	//Multiplier of characters
	
/*
    DWORD ReadIntervalTimeout;          /* Maximum time between read chars. 
    DWORD ReadTotalTimeoutMultiplier;   /* Multiplier of characters.        
    DWORD ReadTotalTimeoutConstant;     /* Constant in milliseconds.        
    DWORD WriteTotalTimeoutMultiplier;  /* Multiplier of characters.        
    DWORD WriteTotalTimeoutConstant;    /*         
*/

   SetCommTimeouts(DeviceHandle,&comtime);//update 
   
   //COMMTIMEOUT konfiguracija 
   //COMMTIMEOUT-------------------------------------------------------------	

	AfxMessageBox(_T("Port podesen "),MB_ICONINFORMATION);//na kraju kazi da je port podesen
}







}

void CCComTest::OnBnClickedButton2()
{   
	  
   if (GetCommState(DeviceHandle,&config_) != 0)
    {
		   
	Sleep(1000);
    CloseHandle(DeviceHandle);
	UpdateData(TRUE);
	SetDlgItemText(IDC_EDIT2,_T("ZATVOREN"));
    }
   else 
   {
	  AfxMessageBox(_T("Port je vec bio zatvoren \n Port nije prikljucen"));
   }
	

}










/*
switch (m_BaudRate)
		 {
  case 110:
	
    config_.BaudRate = 110;
	
	 
    break;
  case 300:
	  	UpdateData(TRUE);
	 
    config_.BaudRate = 300;
     AfxMessageBox(_T("Error"),MB_ICONWARNING);

    break;
  case 600:
    config_.BaudRate = CBR_600;
    break;
  case 1200:
    config_.BaudRate = CBR_1200;
    break;
  case 2400:
    config_.BaudRate = CBR_2400;
    break;
  case 4800:
    config_.BaudRate = CBR_4800;
    break;
  case 9600:
	  
    config_.BaudRate = CBR_9600;
    break;
  case 14400:
    config_.BaudRate = CBR_14400;
    break;
  case 19200:
    config_.BaudRate = CBR_19200;
    break;
  case 38400:
    config_.BaudRate = CBR_38400;
    break;
  case 56000:
    config_.BaudRate = CBR_56000;
    break;
  case 57600:
    config_.BaudRate = CBR_57600;
    break;
  case 115200 :
    config_.BaudRate = CBR_115200;
    break;
  case 128000:
    config_.BaudRate = CBR_128000;
    break;
  case 256000:
    config_.BaudRate = CBR_256000;
    break;
  default:
	  config_.BaudRate = 9600;
	   AfxMessageBox(_T("Error2"),MB_ICONWARNING);
    break;
  }
  */