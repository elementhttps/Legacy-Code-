#pragma once


// CCComTest dialog

class CCComTest : public CDialog
{
	DECLARE_DYNAMIC(CCComTest)

public:
	CCComTest(CWnd* pParent = NULL);   // standard constructor
	virtual ~CCComTest();

// Dialog Data
	enum { IDD = IDD_DIALOG1 };

public:
 DCB config_;  // DCB CONFIG NEOPHODAN JE ZA DOBIJANJE STATUSA PORTA DA LI JE OTVOREN ILI NE ... 
 HANDLE DeviceHandle; // HANDLE ZA DEVICE U OVOM SLUCAJU ZA USB TO RS232 KONEKTOR
 COMMTIMEOUTS comtime; // Retrieve the time-out parameters for all read and write operations on the port.


protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:

public:
	afx_msg void OnBnClickedButton1();
public:
	CString m_status;
public:
	afx_msg void OnBnClickedButton2();
public:
	afx_msg void OnBnClickedButton3();
public:
	afx_msg void OnBnClickedButton4();
public:
	afx_msg void OnBnClickedButton5();
public:
	
	CString m_COMn;// ime porta
/*
public:
	
	CString m_Parity;// even odd mark space noparity

public:
	
	CString m_StopBits;// 0,1,2 = 1, 1.5, 2
public:
	
	CString m_ByteSize;// Number of bits/bytes, 4;5;6;7;8 
	*/

public:
	int m_BaudRate;
public:
	CString m_Parity;
public:
	CString m_StopBits;
public:
	CString m_ByteSize;
};
