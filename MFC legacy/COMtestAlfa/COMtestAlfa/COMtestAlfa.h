// COMtestAlfa.h : main header file for the COMtestAlfa application
//
#pragma once

#ifndef __AFXWIN_H__
	#error "include 'stdafx.h' before including this file for PCH"
#endif

#include "resource.h"       // main symbols


// CCOMtestAlfaApp:
// See COMtestAlfa.cpp for the implementation of this class
//

class CCOMtestAlfaApp : public CWinApp
{
public:
	CCOMtestAlfaApp();


// Overrides
public:
	virtual BOOL InitInstance();

// Implementation
	afx_msg void OnAppAbout();
	DECLARE_MESSAGE_MAP()
};

extern CCOMtestAlfaApp theApp;