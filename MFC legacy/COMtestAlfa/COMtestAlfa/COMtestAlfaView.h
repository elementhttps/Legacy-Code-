// COMtestAlfaView.h : interface of the CCOMtestAlfaView class
//


#pragma once


class CCOMtestAlfaView : public CView
{
protected: // create from serialization only
	CCOMtestAlfaView();
	DECLARE_DYNCREATE(CCOMtestAlfaView)

// Attributes
public:
	CCOMtestAlfaDoc* GetDocument() const;

// Operations
public:

// Overrides
public:
	virtual void OnDraw(CDC* pDC);  // overridden to draw this view
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
protected:

// Implementation
public:
	virtual ~CCOMtestAlfaView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
public:
		CString m_status;
protected:
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnComtest();
};

#ifndef _DEBUG  // debug version in COMtestAlfaView.cpp
inline CCOMtestAlfaDoc* CCOMtestAlfaView::GetDocument() const
   { return reinterpret_cast<CCOMtestAlfaDoc*>(m_pDocument); }
#endif

