//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by COMtestAlfa.rc
//
#define IDD_ABOUTBOX                    100
#define IDR_MAINFRAME                   128
#define IDR_COMtestAlfaTYPE             129
#define IDD_DIALOG1                     130
#define IDB_BITMAP1                     132
#define IDC_EDIT1                       1000
#define IDC_BUTTON1                     1001
#define IDC_BUTTON2                     1002
#define IDC_EDIT2                       1003
#define IDC_BUTTON3                     1004
#define IDC_BUTTON4                     1005
#define IDC_COMBO1                      1007
#define IDC_BUTTON5                     1008
#define IDC_COMBO2                      1011
#define IDC_COMBO3                      1012
#define IDC_COMBO4                      1013
#define IDC_COMBO5                      1014
#define IDC_COMBO6                      1015
#define ID_COMTEST                      32771

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        133
#define _APS_NEXT_COMMAND_VALUE         32772
#define _APS_NEXT_CONTROL_VALUE         1016
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
