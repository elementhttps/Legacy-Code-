// Circles.h : main header file for the CIRCLES application
//

#if !defined(AFX_CIRCLES_H__6BEE00D0_717A_461A_A917_FF5333AB307C__INCLUDED_)
#define AFX_CIRCLES_H__6BEE00D0_717A_461A_A917_FF5333AB307C__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"       // main symbols

/////////////////////////////////////////////////////////////////////////////
// CCirclesApp:
// See Circles.cpp for the implementation of this class
//

class CCirclesApp : public CWinApp
{
public:
	CCirclesApp();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCirclesApp)
	public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL

// Implementation
	//{{AFX_MSG(CCirclesApp)
	afx_msg void OnAppAbout();
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CIRCLES_H__6BEE00D0_717A_461A_A917_FF5333AB307C__INCLUDED_)
