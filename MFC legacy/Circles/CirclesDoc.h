// CirclesDoc.h : interface of the CCirclesDoc class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_CIRCLESDOC_H__382C9B3A_4F12_4310_98F4_FCC77274ADE3__INCLUDED_)
#define AFX_CIRCLESDOC_H__382C9B3A_4F12_4310_98F4_FCC77274ADE3__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


class CCirclesDoc : public CDocument
{
protected: // create from serialization only
	CCirclesDoc();
	DECLARE_DYNCREATE(CCirclesDoc)

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCirclesDoc)
	public:
	virtual BOOL OnNewDocument();
	virtual void Serialize(CArchive& ar);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CCirclesDoc();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	//{{AFX_MSG(CCirclesDoc)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CIRCLESDOC_H__382C9B3A_4F12_4310_98F4_FCC77274ADE3__INCLUDED_)
