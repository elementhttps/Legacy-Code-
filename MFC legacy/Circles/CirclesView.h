// CirclesView.h : interface of the CCirclesView class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_CIRCLESVIEW_H__0387835A_DBA8_48C5_AD69_955D0F3540FA__INCLUDED_)
#define AFX_CIRCLESVIEW_H__0387835A_DBA8_48C5_AD69_955D0F3540FA__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


class CCirclesView : public CView
{
protected: // create from serialization only
	CCirclesView();
	DECLARE_DYNCREATE(CCirclesView)

// Attributes
public:
	CCirclesDoc* GetDocument();

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCirclesView)
	public:
	virtual void OnDraw(CDC* pDC);  // overridden to draw this view
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	protected:
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CCirclesView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	//{{AFX_MSG(CCirclesView)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

#ifndef _DEBUG  // debug version in CirclesView.cpp
inline CCirclesDoc* CCirclesView::GetDocument()
   { return (CCirclesDoc*)m_pDocument; }
#endif

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CIRCLESVIEW_H__0387835A_DBA8_48C5_AD69_955D0F3540FA__INCLUDED_)
