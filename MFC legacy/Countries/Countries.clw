; CLW file contains information for the MFC ClassWizard

[General Info]
Version=1
LastClass=CCountriesDlg
LastTemplate=CDialog
NewFileInclude1=#include "stdafx.h"
NewFileInclude2=#include "Countries.h"

ClassCount=3
Class1=CCountriesApp
Class2=CCountriesDlg
Class3=CAboutDlg

ResourceCount=3
Resource1=IDD_ABOUTBOX
Resource2=IDR_MAINFRAME
Resource3=IDD_COUNTRIES_DIALOG

[CLS:CCountriesApp]
Type=0
HeaderFile=Countries.h
ImplementationFile=Countries.cpp
Filter=N

[CLS:CCountriesDlg]
Type=0
HeaderFile=CountriesDlg.h
ImplementationFile=CountriesDlg.cpp
Filter=D
BaseClass=CDialog
VirtualFilter=dWC
LastObject=IDC_ADD_COUNTRY

[CLS:CAboutDlg]
Type=0
HeaderFile=CountriesDlg.h
ImplementationFile=CountriesDlg.cpp
Filter=D

[DLG:IDD_ABOUTBOX]
Type=1
Class=CAboutDlg
ControlCount=4
Control1=IDC_STATIC,static,1342177283
Control2=IDC_STATIC,static,1342308480
Control3=IDC_STATIC,static,1342308352
Control4=IDOK,button,1342373889

[DLG:IDD_COUNTRIES_DIALOG]
Type=1
Class=CCountriesDlg
ControlCount=5
Control1=IDOK,button,1342242817
Control2=IDCANCEL,button,1342242816
Control3=IDC_NEWCOUNTRY,edit,1350631552
Control4=IDC_ADD_COUNTRY,button,1342242816
Control5=IDC_COUNTRIES_LIST,listbox,1352728835

