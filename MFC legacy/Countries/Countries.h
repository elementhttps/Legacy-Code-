// Countries.h : main header file for the COUNTRIES application
//

#if !defined(AFX_COUNTRIES_H__2E21BB3A_AC8C_46A2_8484_C3E7B4DF95F2__INCLUDED_)
#define AFX_COUNTRIES_H__2E21BB3A_AC8C_46A2_8484_C3E7B4DF95F2__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"		// main symbols

/////////////////////////////////////////////////////////////////////////////
// CCountriesApp:
// See Countries.cpp for the implementation of this class
//

class CCountriesApp : public CWinApp
{
public:
	CCountriesApp();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCountriesApp)
	public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL

// Implementation

	//{{AFX_MSG(CCountriesApp)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_COUNTRIES_H__2E21BB3A_AC8C_46A2_8484_C3E7B4DF95F2__INCLUDED_)
