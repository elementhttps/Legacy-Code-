// CountriesDlg.h : header file
//

#if !defined(AFX_COUNTRIESDLG_H__AF821941_408E_41E2_9895_82D4518DFFC4__INCLUDED_)
#define AFX_COUNTRIESDLG_H__AF821941_408E_41E2_9895_82D4518DFFC4__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

/////////////////////////////////////////////////////////////////////////////
// CCountriesDlg dialog

class CCountriesDlg : public CDialog
{
// Construction
public:
	CStringList ListOfCountries;
	
	CCountriesDlg(CWnd* pParent = NULL);	// standard constructor

// Dialog Data
	//{{AFX_DATA(CCountriesDlg)
	enum { IDD = IDD_COUNTRIES_DIALOG };
	CEdit	m_ctlNewCountry;
	CListBox	m_CountriesList;
	CString	m_NewCountry;
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCountriesDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	//{{AFX_MSG(CCountriesDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	afx_msg void OnAddCountry();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_COUNTRIESDLG_H__AF821941_408E_41E2_9895_82D4518DFFC4__INCLUDED_)
