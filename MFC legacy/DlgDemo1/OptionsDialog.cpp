// OptionsDialog.cpp : implementation file
//

#include "stdafx.h"
#include "DlgDemo1.h"
#include "OptionsDialog.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// OptionsDialog dialog


OptionsDialog::OptionsDialog(CWnd* pParent /*=NULL*/)
	: CDialog(OptionsDialog::IDD, pParent)
{
	//{{AFX_DATA_INIT(OptionsDialog)
	m_nUnits = -1;
	m_nHeight = 0;
	m_Ugao = 0;
	//}}AFX_DATA_INIT
}


void OptionsDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(OptionsDialog)
	DDX_Radio(pDX, IDC_INCHES, m_nUnits);
	DDX_Text(pDX, IDC_HEIGHT, m_nHeight);
	DDV_MinMaxInt(pDX, m_nHeight, 1, 128);
	DDX_Text(pDX, IDC_WIDTH, m_Ugao);
	DDV_MinMaxInt(pDX, m_Ugao, 0, 90);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(OptionsDialog, CDialog)
	//{{AFX_MSG_MAP(OptionsDialog)
	ON_BN_CLICKED(IDC_RESET, OnReset)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// OptionsDialog message handlers

void OptionsDialog::OnReset() 
{
	// TODO: Add your control notification handler code here
	m_Ugao = 4;
    m_nHeight = 2;
    m_nUnits = 0;
    UpdateData (FALSE);   
}
