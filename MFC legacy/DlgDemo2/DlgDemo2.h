// DlgDemo2.h : main header file for the DLGDEMO2 application
//

#if !defined(AFX_DLGDEMO2_H__706664D8_5D30_4214_BE26_8871C5EFEF2B__INCLUDED_)
#define AFX_DLGDEMO2_H__706664D8_5D30_4214_BE26_8871C5EFEF2B__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"       // main symbols

/////////////////////////////////////////////////////////////////////////////
// CDlgDemo2App:
// See DlgDemo2.cpp for the implementation of this class
//

class CDlgDemo2App : public CWinApp
{
public:
	CDlgDemo2App();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDlgDemo2App)
	public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL

// Implementation

public:
	//{{AFX_MSG(CDlgDemo2App)
	afx_msg void OnAppAbout();
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DLGDEMO2_H__706664D8_5D30_4214_BE26_8871C5EFEF2B__INCLUDED_)
