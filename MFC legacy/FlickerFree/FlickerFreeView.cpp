// FlickerFreeView.cpp : implementation of the CFlickerFreeView class
//

#include "stdafx.h"
#include "FlickerFree.h"

#include "memdc.h"
#include "windows.h"

#include "FlickerFreeDoc.h"
#include "FlickerFreeView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CFlickerFreeView

IMPLEMENT_DYNCREATE(CFlickerFreeView, CView)

BEGIN_MESSAGE_MAP(CFlickerFreeView, CView)
	//{{AFX_MSG_MAP(CFlickerFreeView)
	ON_WM_TIMER()
	ON_WM_ERASEBKGND()
	ON_WM_MOUSEMOVE()
	ON_WM_KEYDOWN()
	ON_WM_KEYUP()
	//}}AFX_MSG_MAP
	// Standard printing commands
	ON_COMMAND(ID_FILE_PRINT, CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_DIRECT, CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_PREVIEW, CView::OnFilePrintPreview)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CFlickerFreeView construction/destruction

CFlickerFreeView::CFlickerFreeView()
{
	x = 0;
	y = 100;
	w = 100;
	h = 150;
	MovingRight = TRUE;
	//point.x =0;

	m_Mreza = 1;
	m_DGrafik = 0;
	m_Zumx= 20;
	m_Zumy= 20;
	m_Korak = 10;
	m_IDev = 100000;
	m_GMreza = 5000;

	m_viewx = 1;
	m_viewy = -1;

	m_Orgfx =90;
	m_Orgfy =620;

	dx = 1;
	timera =24;




	m_pp1 = 1;

   nWidth =0;
   nHeight =0;
   Px = 0; //nema promene u pocetnom trenutku P=0
   Py = 0;
   P = 0;
   dxzum =0; 
   dyzum =0; 

   dxs =0;
   dys =0;
   x =0;
   y=0;
  

}

CFlickerFreeView::~CFlickerFreeView()
{
}

BOOL CFlickerFreeView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	return CView::PreCreateWindow(cs);
}

/////////////////////////////////////////////////////////////////////////////
// CFlickerFreeView drawing

void CFlickerFreeView::OnDraw(CDC* dc)
{
    CMemDC pDC(dc);
	CFlickerFreeDoc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);
    if (!pDoc)
		return;

::SetCursor(::LoadCursor(NULL, IDC_CROSS));
	CRect rect;
	CBrush brsBlack, *brsOld;

	GetClientRect(&rect);
	brsBlack.CreateSolidBrush(RGB(255, 255, 255));

	brsOld = dc->SelectObject(&brsBlack);
	dc->Rectangle(rect.left, rect.top, rect.Width(), rect.Height());

	CPen   penRed, *penOld;
	penRed.CreatePen(PS_SOLID, 1, RGB(255, 0, 0));
	penOld = dc->SelectObject(&penRed);

	dc->Rectangle(x, y, x + 100, h);

	dc->SelectObject(penOld);
	dc->SelectObject(brsOld);

int nWidth  =rect.Width();//int MERA SIRINE
 int   nHeight = rect.Height();// int MERA DUZINE


 dc->MoveTo(point.x+10, 0 );//LINIJA ZA PRACENJE X OSE  
 dc->LineTo(point.x+10, point.y+10000);

 dc->MoveTo(0,point.y);//LINIJA ZA PRACENJE Y OSE
 dc->LineTo(point.x+10000, point.y);


		CPen PenBlue2(PS_SOLID, 1, RGB(0, 0, 255));
	dc->SelectObject(PenBlue2);

    dc->MoveTo(nWidth / 2, 0);  //LINIJA ZA PRIKAZ POLOVINE X EKRANA
	dc->LineTo(nWidth / 2, nHeight);
	dc->MoveTo(0, nHeight/ 2);//LINIJA ZA PRIKAZ POLOVINE Y EKRANA
	dc->LineTo(nWidth, nHeight / 2);

	GetCursorPos(&point);


char t1[10];
sprintf(t1, "Pozicija  (%d)",  point.x);
dc->TextOut (nWidth/2,nHeight/2 ,t1);
//----------------------------------------------------------
//POKRETNI TEKST NA POZICIJU MISA X I Y	
//----------------------------------------------------------	
	dc->SetTextColor(RGB(100, 100, 0)); 

    dc->TextOut(point.x-100, point.y-50, 'X');
	dc->TextOut(point.x-4, point.y+50, 'Y');
   //////////////////////////////////////////////////////



dc->SetMapMode(MM_ANISOTROPIC); //orijentacija osa PO ZELJI PROGRAMERA



dc->SetViewportOrg(m_Orgfx, m_Orgfy);


	dc->SetWindowExt(m_Zumx,m_Zumy); //SetWindowExtent(100,100)  SetViewportExtent(200,200)  100 jedinica = 200 pixela

	


	dc->SetViewportExt(m_viewx,m_viewy);//1 -1

//PRETVARANJE POZICIJA MISA U LOGICKE KOORDINATE ZA PRIKAZ NA SEKUNDARNOM KOS
   xLogical= (point2.x-m_Orgfx)/(m_viewx/m_Zumx);
   yLogical =(point2.y-m_Orgfy)/(m_viewy/m_Zumy);

	 int sxLogical=xLogical+dxzum;

    int  syLogical=yLogical+dyzum;




CPen PenGreen2(PS_SOLID, 10, RGB(0,255, 0));
dc->SelectObject(PenGreen2);



     dc->MoveTo(sxLogical, 0 );
     dc->LineTo(sxLogical, syLogical);

     dc->MoveTo(0, syLogical );
     dc->LineTo(sxLogical, syLogical);



   xLogical2= (point3.x-m_Orgfx)/(m_viewx/m_Zumx);
   yLogical2 =(point3.y-m_Orgfy)/(m_viewy/m_Zumy);



   	 int sx2Logical=xLogical2+dxzum;

    int  sy2Logical=yLogical2+dyzum;

dxs =sx2Logical;
dys =sy2Logical;

CPen PenOrange(PS_SOLID, 10, RGB(100,100, 0));
dc->SelectObject(PenOrange);



     dc->MoveTo(dxs, 0 );
     dc->LineTo(dxs, dys);

     dc->MoveTo(0, dys );
     dc->LineTo(dxs, dys);
/*
*/
//----------------------------------------------------------
// OSE
	CPen PenBlack40(PS_SOLID, 50, RGB(0, 0, 0));
	dc->SelectObject(PenBlack40);
	dc->MoveTo(-1000,     0);
	dc->LineTo( 20000000,     0);
	dc->MoveTo(   0, -1000);
	dc->LineTo(   0,  20000000);


//----------------------------------------------------------



//----------------------------------------------------------	
//POZICIJA OZNAKA MREZE LENJIRA
//----------------------------------------------------------	
	
	
    
	
	
	//Lenjiri po x i y osi
	//////////////////////////////////////////////////////	

    CPen PenBlack3(PS_SOLID, 3, RGB(0, 0, 0));
	dc->SelectObject(PenBlack3);
    
	for(int koox = 0; koox < 100000; koox += 1000)
    {
        dc->MoveTo(koox, 0);
        dc->LineTo(koox, -1000);
    }

	for(int kooy = 0; kooy < 100000; kooy += 1000)
    {
        dc->MoveTo(0, kooy);
        dc->LineTo(-1000, kooy);
    }



	dc->SelectObject(PenBlack3);
    for(int koox2 = 0; koox2 < 100000; koox2 += 5000)
    {
        dc->MoveTo(koox2, 0);
        dc->LineTo(koox2, -2000);
    }
	for(int kooy2 = 0; kooy2 < 100000; kooy2 += 5000)
    {
        dc->MoveTo(0, kooy2);
        dc->LineTo(-2000, kooy2);
    }

	for(int koox3 = 0; koox3 < 100000; koox3 += 10000)
    {
        dc->MoveTo(koox3, 0);
        dc->LineTo(koox3, -3000);
    }
	for(int kooy3 = 0; kooy3 < 100000; kooy3 += 10000)
    {
        dc->MoveTo(0, kooy3);
        dc->LineTo(-3000, kooy3);
    }

    dc->SetTextColor(RGB(0, 0, 0));
	for (int kt1= 0; kt1<100000; kt1+=10000) 
	{
        
        CString string;
        string.Format (("%d "), (kt1 ) );
        dc->TextOut (kt1, -3000, string);
    }

	dc->SetTextColor(RGB(0, 0, 0));
	for (int kt2= 1000; kt2<100000; kt2+=10000) 
	{
        
        CString string;
        string.Format (("%d"), (((kt2-1000)) ) );
        dc->TextOut (-6000, kt2, string);
    }

    








}

/////////////////////////////////////////////////////////////////////////////
// CFlickerFreeView printing

BOOL CFlickerFreeView::OnPreparePrinting(CPrintInfo* pInfo)
{
	// default preparation
	return DoPreparePrinting(pInfo);
}

void CFlickerFreeView::OnBeginPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: add extra initialization before printing
}

void CFlickerFreeView::OnEndPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: add cleanup after printing
}

/////////////////////////////////////////////////////////////////////////////
// CFlickerFreeView diagnostics

#ifdef _DEBUG
void CFlickerFreeView::AssertValid() const
{
	CView::AssertValid();
}

void CFlickerFreeView::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}

CFlickerFreeDoc* CFlickerFreeView::GetDocument() // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CFlickerFreeDoc)));
	return (CFlickerFreeDoc*)m_pDocument;
}
#endif //_DEBUG



void CFlickerFreeView::OnMouseMove(UINT nFlags, CPoint point) 
{
	
      
	//InvalidateRect(rect,true);
	//
	//Sleep(0.5);
	//Invalidate();
	//GetCursorPos(&point);
	
	CView::OnMouseMove(nFlags, point);
}	


void CFlickerFreeView::OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags) 
{
		switch (nChar)
 {
		
		case 0x54 ://t
			timera-=10;
		
 	
			Invalidate();
         break;


		case 0x47 ://g
			timera+=10;
		

			Invalidate();
         break;










	case VK_RIGHT :
			m_Orgfx-=50;
			m_pp1 = 0;
			xLogical=+50;
			
			Px=1;
		    dxzum -=50*m_Zumx*Px;
			Px=0;
		;
            //int dyzum =50*m_Zumy*Py; 
   
			
	
			Invalidate();
         break;

	case VK_LEFT :
		m_Orgfx+=50;
			m_pp1 = 0;
			xLogical=-50;
			Px=1;
			dxzum +=50*m_Zumx*Px;
			Px=0;
		
		
	
			Invalidate();
		break;

	case VK_UP :
		m_Orgfy+=50;
			m_pp1 = 0;
			Py=1;
			dyzum -=50*m_Zumy*Py;
			Py=0;


	
	
			Invalidate();
		break;

    case VK_DOWN:
		m_Orgfy-=50;
			m_pp1 = 0;
			Py=1;
			dyzum +=50*m_Zumy*Py;
			Py=0;



		
		Invalidate();
		break;
    
	case VK_HOME:
		m_Orgfx = 90;
		m_Orgfy = 620;
		m_Zumx= 60;
        m_Zumy = 60;
			m_pp1 = 0;
		Invalidate();
		break;

	case VK_F2:
		m_Zumx +=10;
		m_Zumy +=10;
			m_pp1 = 0;
			dxzum =0;
			dyzum =0;
	
		Invalidate();
		break;

	case VK_F1:
		m_Zumx -=10;
		m_Zumy -=10;
		m_pp1 = 0;
		dxzum =0;
		dyzum =0;
	
		if (m_Zumx < 1)
		{
			m_Zumx =1;
			m_Zumy =1;
		}
			
		Invalidate();
		break;

	case VK_F3:
		m_Zumx -=1;
		m_Zumy -=1;
		
		if (m_Zumx < 1)
		{
			m_Zumx =1;
			m_Zumy =1;
		}
			m_pp1 = 0;
		Invalidate();
		break;

	case VK_F4://f4
		m_Zumx +=1;
		m_Zumy +=1;
		m_pp1 = 0;
	
		Invalidate();
		break;



	case 0x51://q
	
		dx +=100;
		m_pp1 = 0;
	
		Invalidate();
		break;

	case 0x57://w
		
		dx -=100;
		m_pp1 = 0;
	
		Invalidate();
		break;
	

}
	
	CView::OnKeyDown(nChar, nRepCnt, nFlags);
}

void CFlickerFreeView::OnKeyUp(UINT nChar, UINT nRepCnt, UINT nFlags) 
{
		switch (nChar)
 {
	case VK_RIGHT :
			
			m_pp1 = 1;
			Px=0;
		    //dxzum -=50*m_Zumx*Px; 
            //int dyzum =50*m_Zumy*Py; 
			
Invalidate();
         break;

	case VK_LEFT :
	m_pp1 = 1;
	Px=0;

	
		
	
			Invalidate();
		break;

	case VK_UP :
	m_pp1 = 1;
	Py=0;
	
	
	
			Invalidate();
		break;

    case VK_DOWN:
		m_pp1 = 1;
		Py=0;
	
		
		Invalidate();
		break;
    
	case VK_HOME:
	m_pp1 = 1;
		Invalidate();
		break;

	case VK_F2:
	m_pp1 = 1;
	
		Invalidate();
		break;

	case VK_F1:
	m_pp1 = 1;
	P=1;
	dxs -=m_Zumx*(point3.x)*P;
    dys -=m_Zumy*(point3.y)*P;
		
	
		break;

	case VK_F3:
     m_pp1 = 1;
		Invalidate();
		break;

	case VK_F4://f4
	m_pp1 = 1;
	
		Invalidate();
		break;



	case 0x51://q
	
	m_pp1 = 1;
	
		Invalidate();
		break;

	case 0x57://w
		
	m_pp1 = 1;
	
		Invalidate();
		break;
	

	


	case 0x41://a
	
			m_viewx -=1;
		

	
		Invalidate();
		break;

	case 0x53://s
		
		m_viewx +=1;
		

	
		Invalidate();
		break;

	case 0x5a://z
	
		m_viewy -=1;

	
		Invalidate();
		break;

	case 0x58://x
	
         m_viewy +=1;
	
		Invalidate();
		break;


	case 0x1b://escape
	
       exit(0);  
	
		Invalidate();
		break;
/*
	case 0x56://v
		
		dugao2-=1;

	
		Invalidate();
		break;
		*/



}	
	CView::OnKeyUp(nChar, nRepCnt, nFlags);
}
/////////////////////////////////////////////////////////////////////////////
// CFlickerFreeView message handlers

void CFlickerFreeView::OnInitialUpdate() 
{
	CView::OnInitialUpdate();
	
	// TODO: Add your specialized code here and/or call the base class
	SetTimer(WM_MOUSEMOVE, timera, NULL);//IDT_OCCUR
}

void CFlickerFreeView::OnTimer(UINT nIDEvent) 
{
/*	CRect rect;
	GetClientRect(&rect);

	if( x < 0 )
	{
		x = 0;
		MovingRight = TRUE;
	}
	if( x > (rect.Width() - 100) )
	{
		x = rect.Width() - 100;
		MovingRight = FALSE;
	}

	

	if( MovingRight == TRUE )
		x++;
	else
		x--;*///
//Invalidate();
	GetCursorPos(&point);
	CView::OnTimer(nIDEvent);
}
/**/
BOOL CFlickerFreeView::OnEraseBkgnd(CDC* pDC) 
{
	// TODO: Add your message handler code here and/or call default
	
	return false;//CView::OnEraseBkgnd(pDC);
}