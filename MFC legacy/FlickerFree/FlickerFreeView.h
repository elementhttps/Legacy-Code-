// FlickerFreeView.h : interface of the CFlickerFreeView class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_FLICKERFREEVIEW_H__4BC72D1A_F271_4635_9D8C_C36D559CBE4C__INCLUDED_)
#define AFX_FLICKERFREEVIEW_H__4BC72D1A_F271_4635_9D8C_C36D559CBE4C__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


class CFlickerFreeView : public CView
{
protected: // create from serialization only
	CFlickerFreeView();
	DECLARE_DYNCREATE(CFlickerFreeView)

// Attributes
public:
	CFlickerFreeDoc* GetDocument();

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CFlickerFreeView)
	public:
	virtual void OnDraw(CDC* pDC);  // overridden to draw this view
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	virtual void OnInitialUpdate();
	protected:
	virtual BOOL OnPreparePrinting(CPrintInfo* pInfo);
	virtual void OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo);
	virtual void OnEndPrinting(CDC* pDC, CPrintInfo* pInfo);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CFlickerFreeView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	//{{AFX_MSG(CFlickerFreeView)
	afx_msg void OnTimer(UINT nIDEvent);
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags);
	afx_msg void OnKeyUp(UINT nChar, UINT nRepCnt, UINT nFlags);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

private:
	int x;
	int y;
	int w;
	int h;
	BOOL MovingRight;
	CPoint point;
	CPoint point2;
	CPoint point3;
	CRect rect;
	double m_Korak;
	int m_Zumx;
	int m_Zumy;
	int m_GMreza;

	int m_IDev;
	int m_Mreza;
	int m_DGrafik;
	CRect Recto;

	int m_Orgfx;
	int m_Orgfy;

	int dx;
	int m_pp1;

	double m_viewx;
	double m_viewy; 
    




	int nWidth;
    int nHeight;

	int xLogical;
    int yLogical;
	int xLogical2;
    int yLogical2;
	int  sxLogical;
	int  syLogical;

	int Px; //faktor promene pravca
	int Py; //faktor promene pravca
	int P;
	int dxzum; 
    int dyzum; 

	int dxs;
	int dys;

	int dxg;
	int dyg;

	int timera;
public:


};

#ifndef _DEBUG  // debug version in FlickerFreeView.cpp
inline CFlickerFreeDoc* CFlickerFreeView::GetDocument()
   { return (CFlickerFreeDoc*)m_pDocument; }
#endif

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_FLICKERFREEVIEW_H__4BC72D1A_F271_4635_9D8C_C36D559CBE4C__INCLUDED_)
