// GRAFIK2.h : main header file for the GRAFIK2 application
//

#if !defined(AFX_GRAFIK2_H__C2E190E2_0ACC_4F50_A034_4C56562AA317__INCLUDED_)
#define AFX_GRAFIK2_H__C2E190E2_0ACC_4F50_A034_4C56562AA317__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"       // main symbols

/////////////////////////////////////////////////////////////////////////////
// CGRAFIK2App:
// See GRAFIK2.cpp for the implementation of this class
//

class CGRAFIK2App : public CWinApp
{
public:
	CGRAFIK2App();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CGRAFIK2App)
	public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL

// Implementation
	//{{AFX_MSG(CGRAFIK2App)
	afx_msg void OnAppAbout();
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_GRAFIK2_H__C2E190E2_0ACC_4F50_A034_4C56562AA317__INCLUDED_)
