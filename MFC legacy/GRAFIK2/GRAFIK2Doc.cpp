// GRAFIK2Doc.cpp : implementation of the CGRAFIK2Doc class
//

#include "stdafx.h"
#include "GRAFIK2.h"

#include "GRAFIK2Doc.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CGRAFIK2Doc

IMPLEMENT_DYNCREATE(CGRAFIK2Doc, CDocument)

BEGIN_MESSAGE_MAP(CGRAFIK2Doc, CDocument)
	//{{AFX_MSG_MAP(CGRAFIK2Doc)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CGRAFIK2Doc construction/destruction

CGRAFIK2Doc::CGRAFIK2Doc()
{
	// TODO: add one-time construction code here

}

CGRAFIK2Doc::~CGRAFIK2Doc()
{
}

BOOL CGRAFIK2Doc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;

	// TODO: add reinitialization code here
	// (SDI documents will reuse this document)

	return TRUE;
}



/////////////////////////////////////////////////////////////////////////////
// CGRAFIK2Doc serialization

void CGRAFIK2Doc::Serialize(CArchive& ar)
{
	if (ar.IsStoring())
	{
		// TODO: add storing code here
	}
	else
	{
		// TODO: add loading code here
	}
}

/////////////////////////////////////////////////////////////////////////////
// CGRAFIK2Doc diagnostics

#ifdef _DEBUG
void CGRAFIK2Doc::AssertValid() const
{
	CDocument::AssertValid();
}

void CGRAFIK2Doc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CGRAFIK2Doc commands
