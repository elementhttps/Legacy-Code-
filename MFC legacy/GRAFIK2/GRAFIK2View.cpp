// GRAFIK2View.cpp : implementation of the CGRAFIK2View class
//

#include "stdafx.h"
#include "GRAFIK2.h"
#include "Math.h"



#include "GRAFIK2Doc.h"
#include "GRAFIK2View.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CGRAFIK2View

IMPLEMENT_DYNCREATE(CGRAFIK2View, CView)

BEGIN_MESSAGE_MAP(CGRAFIK2View, CView)
	//{{AFX_MSG_MAP(CGRAFIK2View)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CGRAFIK2View construction/destruction

CGRAFIK2View::CGRAFIK2View()
{
	m_Mreza = 1;
	m_DGrafik = 0;
	m_Zum= 100000;
	m_Korak = 1;
	m_IDev = 150000;
	m_GMreza = 5000;
}

CGRAFIK2View::~CGRAFIK2View()
{
}

BOOL CGRAFIK2View::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	return CView::PreCreateWindow(cs);
}

/////////////////////////////////////////////////////////////////////////////
// CGRAFIK2View drawing

void CGRAFIK2View::OnDraw(CDC* pDC)
{
	{

	
	
	pDC->SetMapMode(MM_ISOTROPIC);
	pDC->SetViewportOrg(60, 420);
	pDC->SetWindowExt(m_Zum/10,m_Zum/10);
	pDC->SetViewportExt(100,-100);

//MREZA
//////////////////////////////////////////////////////	
	CPen PenBlack4(PS_SOLID, 1, RGB(0, 0, 0));
	pDC->SelectObject(PenBlack4);
	
	switch (m_Mreza)
	
	{
	case 0:
    
	

	break;

	case 1:
		for(int kooxx = 0; kooxx < 1000000; kooxx += m_GMreza)
    {
        pDC->MoveTo(kooxx, 0);
        pDC->LineTo(kooxx, 50000);
    }
	

	for(int kooyy = 0; kooyy < 1000000; kooyy += m_GMreza)
    {
        pDC->MoveTo(0, kooyy);
        pDC->LineTo(150000, kooyy);
    }
		break;
	}
//////////////////////////////////////////////////////	

	

// OSE
//////////////////////////////////////////////////////	
	pDC->MoveTo(-1000,     0);
	pDC->LineTo( 20000000,     0);
	pDC->MoveTo(   0, -1000);
	pDC->LineTo(   0,  20000000);
//////////////////////////////////////////////////////	


//KONSTANTE
//////////////////////////////////////////////////////	
double PI = 3.14159;
double g = 9.81;
//////////////////////////////////////////////////////	



//PROMENE BRZINE I UGLA

//////////////////////////////////////////////////////	
int alfa1 = 10;

int v1 = 800;

	

CString string;

pDC->SetTextColor(RGB(255, 0, 0));
string.Format (("-Funkcija f1 Ugao %d, Brzina %d m/s "), alfa1, v1);

pDC->TextOut (60000,35000 , string);


//////////////////////////////////////////////////////	


//////////////////////////////////////////////////////	
//FUNKCIJE
//////////////////////////////////////////////////////	


//KORAK I MAKSIMUM
//////////////////////////////////////////////////////	

double kor = m_Korak;

double max = m_IDev; //oko milion
//////////////////////////////////////////////////////	





////////////////////////////////////////////////////////////////////////////////////////////////////////////		
//Funkcija f1 CRVENA
////////////////////////////////////////////////////////////////////////////////////////////////////////////	
	for(double i = 1; i < max; i +=kor)
	{ 
        
		double c1 = 1  ;


	    double f =(i*tan((PI*alfa1)/180))-((g*i*i)/(2*v1*v1*cos((PI*alfa1)/180)*cos((PI*alfa1)/180)));
			//
		    // double f =1*i*tan(100*i)*sin(i);


	    pDC->SetPixel(i, f, RGB(255, 0, 0));
	}





//////////////////////////////////////////////////////	
//POZICIJA OZNAKA
//////////////////////////////////////////////////////		
	
	//Koordinate 
	//////////////////////////////////////////////////////	
	pDC->SetTextColor(RGB(100, 100, 0));

    pDC->TextOut(56000, 2000, 'X');
	pDC->TextOut(500, 35000, 'Y');
   //////////////////////////////////////////////////////	
    
	
	
	//Lenjiri po x i y osi
	//////////////////////////////////////////////////////	

    CPen PenBlack3(PS_SOLID, 2, RGB(0, 0, 0));
	pDC->SelectObject(PenBlack3);
    
	for(int koox = 0; koox < 1000000; koox += 1000)
    {
        pDC->MoveTo(koox, 0);
        pDC->LineTo(koox, -1000);
    }

	for(int kooy = 0; kooy < 1000000; kooy += 1000)
    {
        pDC->MoveTo(0, kooy);
        pDC->LineTo(-1000, kooy);
    }



	pDC->SelectObject(PenBlack3);
    for(int koox2 = 0; koox2 < 1000000; koox2 += 5000)
    {
        pDC->MoveTo(koox2, 0);
        pDC->LineTo(koox2, -2000);
    }
	for(int kooy2 = 0; kooy2 < 1000000; kooy2 += 5000)
    {
        pDC->MoveTo(0, kooy2);
        pDC->LineTo(-2000, kooy2);
    }

	for(int koox3 = 0; koox3 < 1000000; koox3 += 10000)
    {
        pDC->MoveTo(koox3, 0);
        pDC->LineTo(koox3, -3000);
    }
	for(int kooy3 = 0; kooy3 < 1000000; kooy3 += 10000)
    {
        pDC->MoveTo(0, kooy3);
        pDC->LineTo(-3000, kooy3);
    }

    pDC->SetTextColor(RGB(0, 0, 0));
	for (int kt1= 0; kt1<1000000; kt1+=10000) 
	{
        
        CString string;
        string.Format (("%d km"), (kt1/1000 ) );
        pDC->TextOut (kt1, -3000, string);
    }

	pDC->SetTextColor(RGB(0, 0, 0));
	for (int kt2= 1000; kt2<1000000; kt2+=10000) 
	{
        
        CString string;
        string.Format (("%dkm"), (((kt2-1000)/1000) ) );
        pDC->TextOut (-6000, kt2, string);
    }
	// TODO: add draw code for native data here
}

/////////////////////////////////////////////////////////////////////////////
// CGRAFIK2View diagnostics

#ifdef _DEBUG
void CGRAFIK2View::AssertValid() const
{
	CView::AssertValid();
}

void CGRAFIK2View::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}

CGRAFIK2Doc* CGRAFIK2View::GetDocument() // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CGRAFIK2Doc)));
	return (CGRAFIK2Doc*)m_pDocument;
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CGRAFIK2View message handlers
