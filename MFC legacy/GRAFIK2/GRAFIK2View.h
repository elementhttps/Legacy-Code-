// GRAFIK2View.h : interface of the CGRAFIK2View class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_GRAFIK2VIEW_H__159941FD_9745_44FB_BEE7_A17D9BBB4DBD__INCLUDED_)
#define AFX_GRAFIK2VIEW_H__159941FD_9745_44FB_BEE7_A17D9BBB4DBD__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


class CGRAFIK2View : public CView
{
protected: // create from serialization only
	CGRAFIK2View();
	DECLARE_DYNCREATE(CGRAFIK2View)

// Attributes
public:
	CGRAFIK2Doc* GetDocument();

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CGRAFIK2View)
	public:
	virtual void OnDraw(CDC* pDC);  // overridden to draw this view
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	protected:
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CGRAFIK2View();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	double m_Korak;
	int m_Zum;
	int m_GMreza;

	int m_IDev;
	int m_Mreza;
	int m_DGrafik;
	//{{AFX_MSG(CGRAFIK2View)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

#ifndef _DEBUG  // debug version in GRAFIK2View.cpp
inline CGRAFIK2Doc* CGRAFIK2View::GetDocument()
   { return (CGRAFIK2Doc*)m_pDocument; }
#endif

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_GRAFIK2VIEW_H__159941FD_9745_44FB_BEE7_A17D9BBB4DBD__INCLUDED_)
