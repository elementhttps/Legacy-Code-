#include "stdafx.h"
#include "GeoML.h"
#include "Math.h"
#include "Performanse_1.h"


//------------------------------
//funkcije get iz konstruktora

double GEOML::GEOMLgetAiz()
{
       return aiz ;
}

double GEOML::GEOMLgetAkr()
{
       return akr ;
}

double GEOML::GEOMLgetAlfa()
{
       return alfa ;
}

double GEOML::GEOMLgetAul()
{
       return aul ;
}

double GEOML::GEOMLgetBeta()
{
       return beta ;
}

double GEOML::GEOMLgetEE()
{
       return EE ;
}

double GEOML::GEOMLgetLdi()
{
       return Ldi ;
}

double GEOML::GEOMLgetLk()
{
       return Lk ;
}

double GEOML::GEOMLgetLko()
{
       return Lko ;
}

double GEOML::GEOMLgetDkr()
{
	return dkr;
}

double GEOML::GEOMLgetDiz()
{
PERF par;
double EE = par.PERFgetEE();

GEOML geo;
double dkr = geo.GEOMLgetDkr();
//A = r^2*PI  r = sqrt(A/PI)



double diz = EE*dkr;



	return diz;
}


double GEOML::GEOMLgetL()
{

GEOML geo;
double diz = geo.GEOMLgetDiz();
double dkr = geo.GEOMLgetDkr();
double alfa = geo.GEOMLgetAlfa();
double PI = 3.14159265359;

	double di = (diz-dkr)/2;

	double L = di/tan((PI*alfa)/180);


	return L; // od kriticnog do izlaznog pp
}




/*
////////////////////////////////////////////////////////////////////////////////////////////////////////////		
//Funkcija f1 CRVENA
////////////////////////////////////////////////////////////////////////////////////////////////////////////	
	for(double i = 1; i < max; i +=kor)
	{ 
        
		PERF par;
	//	double F =par.PERFgetPotisak();

	//	double mpp = par.PERFgetMaseniProtok();
	    double Isp = par.PERFgetIsp();
	
   double G = par.PERFgetG();


   
   CCRM fluid;

double po = fluid.CCRMgetP_kom();
double Rgg = fluid.CCRMgetRgg();
double To=fluid.CCRMgetT_kom();
double dTo = i+To;
GEOML geo;
double Akr = geo.GEOMLgetAkr();
double dAkr = Akr;




double par1 = po*dAkr;
double par2 = Rgg*dTo;
double par3 = sqrt(par2);
double par4 = par1/par3;

double mp= G*par4;


  
        
         double dkr = geo.GEOMLgetDkr();
		 
		 double Fp = mp*(Isp);

	    
	


	    pDC->SetPixel(Fp, dTo, RGB(255, 0, 0));
	}



*/