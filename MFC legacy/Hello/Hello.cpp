#include <afxwin.h>
#include "Hello.h"

CMyApp myApp;

/////////////////////////////////////////////////////////////////////////
// CMyApp member functions

BOOL CMyApp::InitInstance ()
{
    m_pMainWnd = new CMainWindow;
	

   m_pMainWnd->ShowWindow (m_nCmdShow);
    m_pMainWnd->UpdateWindow ();
    return TRUE;
}

/////////////////////////////////////////////////////////////////////////
// CMainWindow message map and member functions

BEGIN_MESSAGE_MAP (CMainWindow, CFrameWnd)
    ON_WM_PAINT ()
	//ON_WM_LBUTTONDOWN()
END_MESSAGE_MAP ()

CMainWindow::CMainWindow ()
{
    Create (NULL, _T ("The Hello Application"),WS_EX_TRANSPARENT  | WS_CAPTION |
    WS_SYSMENU | WS_MINIMIZEBOX | WS_MAXIMIZE |WS_VSCROLL|WS_HSCROLL,CRect (32, 64, 352, 304));
}

void CMainWindow::OnPaint ()
{
    CPaintDC dc (this);
    
    CRect rect;
    GetClientRect (&rect);

    dc.DrawText ("Hello, MFC", -1/*-1 ili broj slova*/, &rect,DT_SINGLELINE|DT_CENTER|DT_VCENTER );
}

/*void CMainWindow::OnLButtonDown (UINT nFlags, CPoint point)
{
    CRect rect;
    GetClientRect (&rect);

    CClientDC dc (this);
    dc.MoveTo (rect.left, rect.top);
    dc.LineTo (rect.right, rect.bottom);
    dc.MoveTo (rect.right, rect.top);
    dc.LineTo (rect.left, rect.bottom);
}
*/

