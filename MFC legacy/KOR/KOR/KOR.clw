; CLW file contains information for the MFC ClassWizard

[General Info]
Version=1
LastClass=CKORView
LastTemplate=CDialog
NewFileInclude1=#include "stdafx.h"
NewFileInclude2=#include "KOR.h"
LastPage=0

ClassCount=7
Class1=CKORApp
Class2=CKORDoc
Class3=CKORView
Class4=CMainFrame

ResourceCount=4
Resource1=IDR_KORTYPE
Resource2=IDD_ABOUTBOX
Class5=CChildFrame
Class6=CAboutDlg
Resource3=IDR_MAINFRAME
Class7=CCAutorDLG
Resource4=IDD_DIALOG1

[CLS:CKORApp]
Type=0
HeaderFile=KOR.h
ImplementationFile=KOR.cpp
Filter=N
LastObject=CKORApp

[CLS:CKORDoc]
Type=0
HeaderFile=KORDoc.h
ImplementationFile=KORDoc.cpp
Filter=N
LastObject=CKORDoc

[CLS:CKORView]
Type=0
HeaderFile=KORView.h
ImplementationFile=KORView.cpp
Filter=C
LastObject=IDM_AUTOR_DLG
BaseClass=CView
VirtualFilter=VWC


[CLS:CMainFrame]
Type=0
HeaderFile=MainFrm.h
ImplementationFile=MainFrm.cpp
Filter=T
LastObject=CMainFrame


[CLS:CChildFrame]
Type=0
HeaderFile=ChildFrm.h
ImplementationFile=ChildFrm.cpp
Filter=M
LastObject=CChildFrame


[CLS:CAboutDlg]
Type=0
HeaderFile=KOR.cpp
ImplementationFile=KOR.cpp
Filter=D

[DLG:IDD_ABOUTBOX]
Type=1
Class=CAboutDlg
ControlCount=4
Control1=IDC_STATIC,static,1342177283
Control2=IDC_STATIC,static,1342308480
Control3=IDC_STATIC,static,1342308352
Control4=IDOK,button,1342373889

[MNU:IDR_MAINFRAME]
Type=1
Class=CMainFrame
Command1=ID_FILE_NEW
Command2=ID_FILE_OPEN
Command3=ID_FILE_MRU_FILE1
Command4=ID_APP_EXIT
Command5=ID_APP_ABOUT
CommandCount=5

[MNU:IDR_KORTYPE]
Type=1
Class=CKORView
Command1=ID_FILE_NEW
Command2=ID_FILE_OPEN
Command3=ID_FILE_CLOSE
Command4=ID_FILE_SAVE
Command5=ID_FILE_SAVE_AS
Command6=ID_FILE_MRU_FILE1
Command7=ID_APP_EXIT
Command8=ID_EDIT_UNDO
Command9=ID_EDIT_CUT
Command10=ID_EDIT_COPY
Command11=ID_EDIT_PASTE
Command12=ID_WINDOW_NEW
Command13=ID_WINDOW_CASCADE
Command14=ID_WINDOW_TILE_HORZ
Command15=ID_WINDOW_ARRANGE
Command16=ID_APP_ABOUT
Command17=IDM_AUTOR_DLG
CommandCount=17

[ACL:IDR_MAINFRAME]
Type=1
Class=CMainFrame
Command1=ID_FILE_NEW
Command2=ID_FILE_OPEN
Command3=ID_FILE_SAVE
Command4=ID_EDIT_UNDO
Command5=ID_EDIT_CUT
Command6=ID_EDIT_COPY
Command7=ID_EDIT_PASTE
Command8=ID_EDIT_UNDO
Command9=ID_EDIT_CUT
Command10=ID_EDIT_COPY
Command11=ID_EDIT_PASTE
Command12=ID_NEXT_PANE
Command13=ID_PREV_PANE
CommandCount=13

[DLG:IDD_DIALOG1]
Type=1
Class=CCAutorDLG
ControlCount=5
Control1=IDOK,button,1342242817
Control2=IDC_STATIC,static,1342312961
Control3=IDC_STATIC,static,1342312961
Control4=IDC_STATIC,static,1342312448
Control5=IDC_STATIC,static,1342177294

[CLS:CCAutorDLG]
Type=0
HeaderFile=CAutorDLG.h
ImplementationFile=CAutorDLG.cpp
BaseClass=CDialog
Filter=D
LastObject=ID_APP_ABOUT

