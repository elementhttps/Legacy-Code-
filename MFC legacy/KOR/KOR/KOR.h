// KOR.h : main header file for the KOR application
//

#if !defined(AFX_KOR_H__887859CB_0D7F_4ACC_AA58_C9B9FB058C07__INCLUDED_)
#define AFX_KOR_H__887859CB_0D7F_4ACC_AA58_C9B9FB058C07__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"       // main symbols

/////////////////////////////////////////////////////////////////////////////
// CKORApp:
// See KOR.cpp for the implementation of this class
//

class CKORApp : public CWinApp
{
public:
	CKORApp();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CKORApp)
	public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL

// Implementation
	//{{AFX_MSG(CKORApp)
	afx_msg void OnAppAbout();
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_KOR_H__887859CB_0D7F_4ACC_AA58_C9B9FB058C07__INCLUDED_)
