// KORDoc.h : interface of the CKORDoc class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_KORDOC_H__0CB75A28_966E_4D0F_A31B_6DA599E40271__INCLUDED_)
#define AFX_KORDOC_H__0CB75A28_966E_4D0F_A31B_6DA599E40271__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


class CKORDoc : public CDocument
{
protected: // create from serialization only
	CKORDoc();
	DECLARE_DYNCREATE(CKORDoc)

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CKORDoc)
	public:
	virtual BOOL OnNewDocument();
	virtual void Serialize(CArchive& ar);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CKORDoc();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	//{{AFX_MSG(CKORDoc)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_KORDOC_H__0CB75A28_966E_4D0F_A31B_6DA599E40271__INCLUDED_)
