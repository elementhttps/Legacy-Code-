#if !defined(AFX_KORAK_H__C072F3D2_F46F_4D97_B9C2_6BC1629D5065__INCLUDED_)
#define AFX_KORAK_H__C072F3D2_F46F_4D97_B9C2_6BC1629D5065__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// Korak.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CKorak dialog

class CKorak : public CDialog
{
// Construction
public:
	CKorak(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CKorak)
	enum { IDD = IDD_KORAK };
	double	m_BrojTacaka;
	int		m_IDev;
	double	m_Korak;
	int		m_StepenP;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CKorak)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CKorak)
	afx_msg void OnIzracunaj();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_KORAK_H__C072F3D2_F46F_4D97_B9C2_6BC1629D5065__INCLUDED_)
