// KreatorTrajektorijaDoc.h : interface of the CKreatorTrajektorijaDoc class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_KREATORTRAJEKTORIJADOC_H__B0936BEE_1770_45C4_A244_E1EB22E22F95__INCLUDED_)
#define AFX_KREATORTRAJEKTORIJADOC_H__B0936BEE_1770_45C4_A244_E1EB22E22F95__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


class CKreatorTrajektorijaDoc : public CDocument
{
protected: // create from serialization only
	CKreatorTrajektorijaDoc();
	DECLARE_DYNCREATE(CKreatorTrajektorijaDoc)

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CKreatorTrajektorijaDoc)
	public:
	virtual BOOL OnNewDocument();
	virtual void Serialize(CArchive& ar);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CKreatorTrajektorijaDoc();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	//{{AFX_MSG(CKreatorTrajektorijaDoc)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_KREATORTRAJEKTORIJADOC_H__B0936BEE_1770_45C4_A244_E1EB22E22F95__INCLUDED_)
