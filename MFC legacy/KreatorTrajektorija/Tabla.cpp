// Tabla.cpp : implementation file
//

#include "stdafx.h"
#include "KreatorTrajektorija.h"
#include "Tabla.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CTabla dialog


CTabla::CTabla(CWnd* pParent /*=NULL*/)
	: CDialog(CTabla::IDD, pParent)
{
	//{{AFX_DATA_INIT(CTabla)
		m_ITabla = -1;
	//}}AFX_DATA_INIT
}


void CTabla::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CTabla)
		DDX_Radio(pDX, IDC_TABLA_DA, m_ITabla);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CTabla, CDialog)
	//{{AFX_MSG_MAP(CTabla)
		// NOTE: the ClassWizard will add message map macros here
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CTabla message handlers
