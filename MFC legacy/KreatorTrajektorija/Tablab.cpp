// Tablab.cpp : implementation file
//

#include "stdafx.h"
#include "KreatorTrajektorija.h"
#include "Tablab.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CTablab dialog


CTablab::CTablab(CWnd* pParent /*=NULL*/)
	: CDialog(CTablab::IDD, pParent)
{
	//{{AFX_DATA_INIT(CTablab)
	m_InfoTabla = _T("");
	//}}AFX_DATA_INIT
}


void CTablab::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CTablab)
	DDX_Text(pDX, IDC_TABLA_B, m_InfoTabla);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CTablab, CDialog)
	//{{AFX_MSG_MAP(CTablab)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CTablab message handlers

void CTablab::OnOK() 
{
	UpdateData(true);

}

void CTablab::OnCancel() 
{
    DestroyWindow ();
}
BOOL CTablab::Create()
{
	return CDialog::Create(CTablab::IDD);
}