#if !defined(AFX_TABLAB_H__C0966B3B_EA57_4BCE_A223_AB3AD2F743DE__INCLUDED_)
#define AFX_TABLAB_H__C0966B3B_EA57_4BCE_A223_AB3AD2F743DE__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// Tablab.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CTablab dialog

class CTablab : public CDialog
{


// Construction	
private:
    CView* m_dlgINFO;
public:
	CTablab(CWnd* pParent = NULL);   // standard constructor
    CTablab(CView* m_dlgINFO);    
    BOOL Create();


// Dialog Data
	//{{AFX_DATA(CTablab)
	enum { IDD = IDD_TABLA_B };
	CString	m_InfoTabla;

	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CTablab)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CTablab)
	virtual void OnOK();
	virtual void OnCancel();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_TABLAB_H__C0966B3B_EA57_4BCE_A223_AB3AD2F743DE__INCLUDED_)
