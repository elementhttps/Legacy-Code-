// KreatorTrajektorijaDV.h : main header file for the KREATORTRAJEKTORIJADV application
//

#if !defined(AFX_KREATORTRAJEKTORIJADV_H__BF59716D_7559_48F6_9320_F20BD35B78F0__INCLUDED_)
#define AFX_KREATORTRAJEKTORIJADV_H__BF59716D_7559_48F6_9320_F20BD35B78F0__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"       // main symbols

/////////////////////////////////////////////////////////////////////////////
// CKreatorTrajektorijaDVApp:
// See KreatorTrajektorijaDV.cpp for the implementation of this class
//

class CKreatorTrajektorijaDVApp : public CWinApp
{
public:
	CKreatorTrajektorijaDVApp();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CKreatorTrajektorijaDVApp)
	public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL

// Implementation
	//{{AFX_MSG(CKreatorTrajektorijaDVApp)
	afx_msg void OnAppAbout();
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_KREATORTRAJEKTORIJADV_H__BF59716D_7559_48F6_9320_F20BD35B78F0__INCLUDED_)
