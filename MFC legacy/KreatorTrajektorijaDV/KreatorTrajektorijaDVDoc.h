// KreatorTrajektorijaDVDoc.h : interface of the CKreatorTrajektorijaDVDoc class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_KREATORTRAJEKTORIJADVDOC_H__A8EE0AB8_537A_4049_BD66_7785BAA1887F__INCLUDED_)
#define AFX_KREATORTRAJEKTORIJADVDOC_H__A8EE0AB8_537A_4049_BD66_7785BAA1887F__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


class CKreatorTrajektorijaDVDoc : public CDocument
{
protected: // create from serialization only
	CKreatorTrajektorijaDVDoc();
	DECLARE_DYNCREATE(CKreatorTrajektorijaDVDoc)

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CKreatorTrajektorijaDVDoc)
	public:
	virtual BOOL OnNewDocument();
	virtual void Serialize(CArchive& ar);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CKreatorTrajektorijaDVDoc();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	//{{AFX_MSG(CKreatorTrajektorijaDVDoc)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_KREATORTRAJEKTORIJADVDOC_H__A8EE0AB8_537A_4049_BD66_7785BAA1887F__INCLUDED_)
