// KreatorTrajektorijaDVView.cpp : implementation of the CKreatorTrajektorijaDVView class
//

#include "stdafx.h"
#include "KreatorTrajektorijaDV.h"

#include "KreatorTrajektorijaDVDoc.h"
#include "KreatorTrajektorijaDVView.h"

#include "Math.h"



//DIJALOZI
#include "AutorDV.h"
#include "Uputstvo.h"
#include "Parametri.h"
#include "Mreza.h"
#include "Korak.h"
#include "DGrafik.h"



#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CKreatorTrajektorijaDVView

IMPLEMENT_DYNCREATE(CKreatorTrajektorijaDVView, CView)

BEGIN_MESSAGE_MAP(CKreatorTrajektorijaDVView, CView)
	//{{AFX_MSG_MAP(CKreatorTrajektorijaDVView)
	ON_COMMAND(IDM_UPUTSTVO, OnUputstvo)
	ON_COMMAND(IDM_PARAMETRI, OnParametri)
	ON_COMMAND(IDM_MREZA, OnMreza)
	ON_COMMAND(IDM_KORAK, OnKorak)
	ON_COMMAND(IDM_DGRAFIK, OnDgrafik)
	ON_COMMAND(IDM_AUTOR, OnAutor)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CKreatorTrajektorijaDVView construction/destruction

CKreatorTrajektorijaDVView::CKreatorTrajektorijaDVView()
{
	// TODO: add construction code here
	m_Brzina_f1 = 0;
	m_Brzina_f2 = 0;
	m_Brzina_f3= 0;
	m_Ugao_f1= 0;
    m_Ugao_f2= 0;
	m_Ugao_f3= 0;
	m_Mreza = 0;
	m_DGrafik = 0;
	m_Zum= 100000;
	m_Korak = 1;
	m_IDev = 150000;
	m_GMreza = 5000;

}

CKreatorTrajektorijaDVView::~CKreatorTrajektorijaDVView()
{
}

BOOL CKreatorTrajektorijaDVView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	return CView::PreCreateWindow(cs);
}

/////////////////////////////////////////////////////////////////////////////
// CKreatorTrajektorijaDVView drawing

void CKreatorTrajektorijaDVView::OnDraw(CDC* pDC)
{
	CKreatorTrajektorijaDVDoc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);
	
	
	pDC->SetMapMode(MM_ISOTROPIC);
	pDC->SetViewportOrg(60, 420);
	pDC->SetWindowExt(m_Zum/10,m_Zum/10);
	pDC->SetViewportExt(100,-100);

//MREZA
//////////////////////////////////////////////////////	
	CPen PenBlack4(PS_SOLID, 1, RGB(0, 0, 0));
	pDC->SelectObject(PenBlack4);
	
	switch (m_Mreza)
	
	{
	case 0:
    
	

	break;

	case 1:
		for(int kooxx = 0; kooxx < 1000000; kooxx += m_GMreza)
    {
        pDC->MoveTo(kooxx, 0);
        pDC->LineTo(kooxx, 50000);
    }
	

	for(int kooyy = 0; kooyy < 1000000; kooyy += m_GMreza)
    {
        pDC->MoveTo(0, kooyy);
        pDC->LineTo(150000, kooyy);
    }
		break;
	}
//////////////////////////////////////////////////////	

	

// OSE
//////////////////////////////////////////////////////	
	pDC->MoveTo(-1000,     0);
	pDC->LineTo( 20000000,     0);
	pDC->MoveTo(   0, -1000);
	pDC->LineTo(   0,  20000000);
//////////////////////////////////////////////////////	


//KONSTANTE
//////////////////////////////////////////////////////	
double PI = 3.14159;
double g = 9.81;
//////////////////////////////////////////////////////	



//PROMENE BRZINE I UGLA

//////////////////////////////////////////////////////	
int alfa1 = m_Ugao_f1;
int alfa2 = m_Ugao_f2;
int alfa3 = m_Ugao_f3;

int v1 = m_Brzina_f1;
int v2 = m_Brzina_f2;
int v3 = m_Brzina_f3;
	

CString string;

pDC->SetTextColor(RGB(255, 0, 0));
string.Format (("-Funkcija f1 Ugao %d, Brzina %d m/s "), alfa1, v1);

pDC->TextOut (60000,35000 , string);

pDC->SetTextColor(RGB(0, 0, 255));
string.Format (("-Funkcija f1 Ugao %d, Brzina %d m/s "), alfa2, v2);
pDC->TextOut (60000,33000 , string);

pDC->SetTextColor(RGB(0, 255, 0));
string.Format (("-Funkcija f1 Ugao %d, Brzina %d m/s "), alfa3, v3);
pDC->TextOut (60000,31000 , string);
//////////////////////////////////////////////////////	


//////////////////////////////////////////////////////	
//FUNKCIJE
//////////////////////////////////////////////////////	


//KORAK I MAKSIMUM
//////////////////////////////////////////////////////	

double kor = m_Korak;

double max = m_IDev; //oko milion
//////////////////////////////////////////////////////	





////////////////////////////////////////////////////////////////////////////////////////////////////////////		
//Funkcija f1 CRVENA
////////////////////////////////////////////////////////////////////////////////////////////////////////////	
	for(double i = 1; i < max; i +=kor)
	{ 
        
		double c1 = 1  ;


	    double f =(i*tan((PI*alfa1)/180))-((g*i*i)/(2*v1*v1*cos((PI*alfa1)/180)*cos((PI*alfa1)/180)));
			//
		    // double f =1*i*tan(100*i)*sin(i);


	    pDC->SetPixel(i, f, RGB(255, 0, 0));
	}
////////////////////////////////////////////////////////////////////////////////////////////////////////////	
//Funkcija f2 PLAVA
////////////////////////////////////////////////////////////////////////////////////////////////////////////	
	for(double i2 = 1; i2 < max; i2 +=kor)
	{
		int c2 = 0;
	    double f2 = (i2*tan((PI*alfa2)/180))-((g*i2*i2)/(2*v2*v2*cos((PI*alfa2)/180)*cos((PI*alfa2)/180)));
			//tan(i2)*i2;
	    pDC->SetPixel(i2, f2, RGB(0, 0, 255));

	}
////////////////////////////////////////////////////////////////////////////////////////////////////////////		
//Funkcija f3 ZELENA
////////////////////////////////////////////////////////////////////////////////////////////////////////////	
    for(double i3 = 1; i3 < max; i3 +=kor)
	{
		int c3 = 0;
	    double f3 = (i3*tan((PI*alfa3)/180))-((g*i3*i3)/(2*v3*v3*cos((PI*alfa3)/180)*cos((PI*alfa3)/180)));;
	    pDC->SetPixel(i3, f3, RGB(0, 255, 0));
	}



//GRAFIK DONJI PRIKAZ    
//////////////////////////////////////////////////////		
	
	CBrush brush (RGB (255, 255, 255));
	pDC->SelectObject(brush);


	switch (m_DGrafik)
	{
	case 0:
		
	      pDC->Rectangle(-1000000,-1,2000000,-10000000);
	break;



	case 1:
		break;
	}
//////////////////////////////////////////////////////	
	




//////////////////////////////////////////////////////	
//POZICIJA OZNAKA
//////////////////////////////////////////////////////		
	
	//Koordinate 
	//////////////////////////////////////////////////////	
	pDC->SetTextColor(RGB(100, 100, 0));

    pDC->TextOut(56000, 2000, 'X');
	pDC->TextOut(500, 35000, 'Y');
   //////////////////////////////////////////////////////	
    
	
	
	//Lenjiri po x i y osi
	//////////////////////////////////////////////////////	

    CPen PenBlack3(PS_SOLID, 2, RGB(0, 0, 0));
	pDC->SelectObject(PenBlack3);
    
	for(int koox = 0; koox < 1000000; koox += 1000)
    {
        pDC->MoveTo(koox, 0);
        pDC->LineTo(koox, -1000);
    }

	for(int kooy = 0; kooy < 1000000; kooy += 1000)
    {
        pDC->MoveTo(0, kooy);
        pDC->LineTo(-1000, kooy);
    }



	pDC->SelectObject(PenBlack3);
    for(int koox2 = 0; koox2 < 1000000; koox2 += 5000)
    {
        pDC->MoveTo(koox2, 0);
        pDC->LineTo(koox2, -2000);
    }
	for(int kooy2 = 0; kooy2 < 1000000; kooy2 += 5000)
    {
        pDC->MoveTo(0, kooy2);
        pDC->LineTo(-2000, kooy2);
    }

	for(int koox3 = 0; koox3 < 1000000; koox3 += 10000)
    {
        pDC->MoveTo(koox3, 0);
        pDC->LineTo(koox3, -3000);
    }
	for(int kooy3 = 0; kooy3 < 1000000; kooy3 += 10000)
    {
        pDC->MoveTo(0, kooy3);
        pDC->LineTo(-3000, kooy3);
    }

    pDC->SetTextColor(RGB(0, 0, 0));
	for (int kt1= 0; kt1<1000000; kt1+=10000) 
	{
        
        CString string;
        string.Format (("%d km"), (kt1/1000 ) );
        pDC->TextOut (kt1, -3000, string);
    }

	pDC->SetTextColor(RGB(0, 0, 0));
	for (int kt2= 1000; kt2<1000000; kt2+=10000) 
	{
        
        CString string;
        string.Format (("%dkm"), (((kt2-1000)/1000) ) );
        pDC->TextOut (-6000, kt2, string);
    }
}

/////////////////////////////////////////////////////////////////////////////
// CKreatorTrajektorijaDVView diagnostics

#ifdef _DEBUG
void CKreatorTrajektorijaDVView::AssertValid() const
{
	CView::AssertValid();
}

void CKreatorTrajektorijaDVView::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}

CKreatorTrajektorijaDVDoc* CKreatorTrajektorijaDVView::GetDocument() // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CKreatorTrajektorijaDVDoc)));
	return (CKreatorTrajektorijaDVDoc*)m_pDocument;
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CKreatorTrajektorijaDVView message handlers

void CKreatorTrajektorijaDVView::OnUputstvo() 
{
	CUputstvo dlg;
	dlg.DoModal();
	
}

void CKreatorTrajektorijaDVView::OnParametri() 
{
	CParametri dlg;

    dlg.m_Brzina_f1 =m_Brzina_f1;
	dlg.m_Brzina_f2 =m_Brzina_f2;
	dlg.m_Brzina_f3 =m_Brzina_f3;

	dlg.m_Ugao_f1 =m_Ugao_f1;
	dlg.m_Ugao_f2 =m_Ugao_f2;
	dlg.m_Ugao_f3 =m_Ugao_f3;


    if (dlg.DoModal () == IDOK)
	{
        m_Brzina_f1 = dlg.m_Brzina_f1;
		m_Brzina_f2 = dlg.m_Brzina_f2;
		m_Brzina_f3 = dlg.m_Brzina_f3;

		m_Ugao_f1 = dlg.m_Ugao_f1;
		m_Ugao_f2 = dlg.m_Ugao_f2;
		m_Ugao_f3 = dlg.m_Ugao_f3;
             
        Invalidate ();
    }    
	
}

void CKreatorTrajektorijaDVView::OnMreza() 
{
	CMreza dlg;
	dlg.m_Mreza = m_Mreza;
	dlg.m_GMreza = m_GMreza;
	
	if (dlg.DoModal () == IDOK)
	{
		m_Mreza= dlg.m_Mreza;
		m_GMreza= dlg.m_GMreza;
		Invalidate ();
	}
	
}

void CKreatorTrajektorijaDVView::OnKorak() 
{
	CKorak dlg;
	dlg.m_Korak = m_Korak;
	dlg.m_IDev = m_IDev;

	if(dlg.DoModal() == IDOK)
	{
	m_Korak = dlg.m_Korak;
	m_IDev = dlg.m_IDev;
	Invalidate();
	}
	
}

void CKreatorTrajektorijaDVView::OnDgrafik() 
{
	CDGrafik dlg;
	dlg.m_DGrafik = m_DGrafik;
	
	if (dlg.DoModal () == IDOK)
	{
		m_DGrafik= dlg.m_DGrafik;
		Invalidate ();
	}
	
}

void CKreatorTrajektorijaDVView::OnAutor() 
{
	CAutorDV dlg;
	dlg.DoModal();
	
}
