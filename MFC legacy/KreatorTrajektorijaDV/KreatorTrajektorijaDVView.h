// KreatorTrajektorijaDVView.h : interface of the CKreatorTrajektorijaDVView class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_KREATORTRAJEKTORIJADVVIEW_H__BC90AF10_38BC_4A70_AC6A_D47EC8666A14__INCLUDED_)
#define AFX_KREATORTRAJEKTORIJADVVIEW_H__BC90AF10_38BC_4A70_AC6A_D47EC8666A14__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


class CKreatorTrajektorijaDVView : public CView
{
protected: // create from serialization only
	CKreatorTrajektorijaDVView();
	DECLARE_DYNCREATE(CKreatorTrajektorijaDVView)

// Attributes
public:
	CKreatorTrajektorijaDVDoc* GetDocument();

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CKreatorTrajektorijaDVView)
	public:
	virtual void OnDraw(CDC* pDC);  // overridden to draw this view
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	protected:
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CKreatorTrajektorijaDVView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	int m_GMreza;
	

	double m_Korak;
	int m_Zum;

	int m_IDev;
	int m_Mreza;
	int m_DGrafik;


	int m_Brzina_f3;
	int m_Ugao_f3;

	int m_Brzina_f2;
	int m_Ugao_f2;

	int m_Brzina_f1;
	int m_Ugao_f1;



	//{{AFX_MSG(CKreatorTrajektorijaDVView)
	afx_msg void OnUputstvo();
	afx_msg void OnParametri();
	afx_msg void OnMreza();
	afx_msg void OnKorak();
	afx_msg void OnDgrafik();
	afx_msg void OnAutor();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

#ifndef _DEBUG  // debug version in KreatorTrajektorijaDVView.cpp
inline CKreatorTrajektorijaDVDoc* CKreatorTrajektorijaDVView::GetDocument()
   { return (CKreatorTrajektorijaDVDoc*)m_pDocument; }
#endif

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_KREATORTRAJEKTORIJADVVIEW_H__BC90AF10_38BC_4A70_AC6A_D47EC8666A14__INCLUDED_)
