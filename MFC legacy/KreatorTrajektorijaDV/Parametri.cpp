// Parametri.cpp : implementation file
//

#include "stdafx.h"
#include "KreatorTrajektorijaDV.h"
#include "Parametri.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CParametri dialog


CParametri::CParametri(CWnd* pParent /*=NULL*/)
	: CDialog(CParametri::IDD, pParent)
{
	//{{AFX_DATA_INIT(CParametri)
	m_Brzina_f1 = 0;
	m_Brzina_f2 = 0;
	m_Brzina_f3 = 0;
	m_Ugao_f1 = 0;
	m_Ugao_f2 = 0;
	m_Ugao_f3 = 0;
	//}}AFX_DATA_INIT
}


void CParametri::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CParametri)
	DDX_Text(pDX, IDC_BRZINA_F1, m_Brzina_f1);
	DDX_Text(pDX, IDC_BRZINA_F2, m_Brzina_f2);
	DDX_Text(pDX, IDC_BRZINA_F3, m_Brzina_f3);
	DDX_Text(pDX, IDC_UGAO_F1, m_Ugao_f1);
	DDX_Text(pDX, IDC_UGAO_F2, m_Ugao_f2);
	DDX_Text(pDX, IDC_UGAO_F3, m_Ugao_f3);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CParametri, CDialog)
	//{{AFX_MSG_MAP(CParametri)
		// NOTE: the ClassWizard will add message map macros here
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CParametri message handlers
