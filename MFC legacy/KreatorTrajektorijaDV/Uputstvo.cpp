// Uputstvo.cpp : implementation file
//

#include "stdafx.h"
#include "KreatorTrajektorijaDV.h"
#include "Uputstvo.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CUputstvo dialog


CUputstvo::CUputstvo(CWnd* pParent /*=NULL*/)
	: CDialog(CUputstvo::IDD, pParent)
{
	//{{AFX_DATA_INIT(CUputstvo)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void CUputstvo::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CUputstvo)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CUputstvo, CDialog)
	//{{AFX_MSG_MAP(CUputstvo)
		// NOTE: the ClassWizard will add message map macros here
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CUputstvo message handlers
