//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by KreatorTrajektorijaDV.rc
//
#define IDD_ABOUTBOX                    100
#define IDR_MAINFRAME                   128
#define IDR_KREATOTYPE                  129
#define IDD_PARAMETRI                   130
#define IDD_MREZA                       132
#define IDD_KORAK                       133
#define IDD_UPUTSTVO                    134
#define IDD_AUTORDV                     135
#define IDB_BITMAP1                     136
#define IDD_DGRAFIK                     137
#define IDC_UGAO_F1                     1000
#define IDC_BRZINA_F1                   1001
#define IDC_MREZA_DA                    1001
#define IDC_UGAO_F2                     1002
#define IDC_MREZA_NE                    1002
#define IDC_KORAK                       1002
#define IDC_BRZINA_F2                   1003
#define IDC_LOOP                        1003
#define IDC_UGAO_F3                     1004
#define IDC_BROJTACAKA                  1004
#define IDC_BRZINA_F3                   1005
#define IDC_IZRACUNAJ                   1005
#define IDC_DGRAF_DA                    1006
#define IDC_DGRAF_NE                    1007
#define IDC_GMREZA                      1008
#define IDM_PARAMETRI                   32771
#define IDM_MREZA                       32772
#define IDM_DGRAFIK                     32773
#define IDM_AUTOR                       32774
#define IDM_UPUTSTVO                    32775
#define IDM_KORAK                       32776

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        138
#define _APS_NEXT_COMMAND_VALUE         32777
#define _APS_NEXT_CONTROL_VALUE         1009
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
