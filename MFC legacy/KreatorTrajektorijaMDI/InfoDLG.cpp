// InfoDLG.cpp : implementation file
//

#include "stdafx.h"
#include "KreatorTrajektorijaMDI.h"
#include "InfoDLG.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CInfoDLG dialog


CInfoDLG::CInfoDLG(CWnd* pParent /*=NULL*/)
	: CDialog(CInfoDLG::IDD, pParent)
{
	//{{AFX_DATA_INIT(CInfoDLG)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void CInfoDLG::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CInfoDLG)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CInfoDLG, CDialog)
	//{{AFX_MSG_MAP(CInfoDLG)
		// NOTE: the ClassWizard will add message map macros here
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CInfoDLG message handlers
