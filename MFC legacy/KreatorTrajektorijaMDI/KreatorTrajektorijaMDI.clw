; CLW file contains information for the MFC ClassWizard

[General Info]
Version=1
LastClass=CInfoDLG
LastTemplate=CDialog
NewFileInclude1=#include "stdafx.h"
NewFileInclude2=#include "KreatorTrajektorijaMDI.h"
LastPage=0

ClassCount=8
Class1=CKreatorTrajektorijaMDIApp
Class2=CKreatorTrajektorijaMDIDoc
Class3=CKreatorTrajektorijaMDIView
Class4=CMainFrame

ResourceCount=5
Resource1=IDR_MAINFRAME
Resource2=IDD_ABOUTBOX
Class5=CChildFrame
Class6=CAboutDlg
Resource3=IDR_KREATOTYPE
Class7=CUputstvoDLG
Resource4=IDD_TABLA
Class8=CInfoDLG
Resource5=IDD_UPUTSTVO

[CLS:CKreatorTrajektorijaMDIApp]
Type=0
HeaderFile=KreatorTrajektorijaMDI.h
ImplementationFile=KreatorTrajektorijaMDI.cpp
Filter=N

[CLS:CKreatorTrajektorijaMDIDoc]
Type=0
HeaderFile=KreatorTrajektorijaMDIDoc.h
ImplementationFile=KreatorTrajektorijaMDIDoc.cpp
Filter=N

[CLS:CKreatorTrajektorijaMDIView]
Type=0
HeaderFile=KreatorTrajektorijaMDIView.h
ImplementationFile=KreatorTrajektorijaMDIView.cpp
Filter=C
LastObject=CKreatorTrajektorijaMDIView
BaseClass=CView
VirtualFilter=VWC


[CLS:CMainFrame]
Type=0
HeaderFile=MainFrm.h
ImplementationFile=MainFrm.cpp
Filter=T
LastObject=CMainFrame


[CLS:CChildFrame]
Type=0
HeaderFile=ChildFrm.h
ImplementationFile=ChildFrm.cpp
Filter=M
LastObject=CChildFrame


[CLS:CAboutDlg]
Type=0
HeaderFile=KreatorTrajektorijaMDI.cpp
ImplementationFile=KreatorTrajektorijaMDI.cpp
Filter=D
LastObject=IDM_PARAMETRI

[DLG:IDD_ABOUTBOX]
Type=1
Class=CAboutDlg
ControlCount=4
Control1=IDC_STATIC,static,1342177283
Control2=IDC_STATIC,static,1342308480
Control3=IDC_STATIC,static,1342308352
Control4=IDOK,button,1342373889

[MNU:IDR_MAINFRAME]
Type=1
Class=CMainFrame
Command1=ID_FILE_NEW
Command2=ID_FILE_OPEN
Command3=ID_FILE_MRU_FILE1
Command4=ID_APP_EXIT
Command5=ID_APP_ABOUT
CommandCount=5

[MNU:IDR_KREATOTYPE]
Type=1
Class=CKreatorTrajektorijaMDIView
Command1=ID_FILE_NEW
Command2=ID_FILE_OPEN
Command3=ID_FILE_CLOSE
Command4=ID_FILE_SAVE
Command5=ID_FILE_SAVE_AS
Command6=ID_FILE_MRU_FILE1
Command7=ID_APP_EXIT
Command8=ID_EDIT_UNDO
Command9=ID_EDIT_CUT
Command10=ID_EDIT_COPY
Command11=ID_EDIT_PASTE
Command12=ID_WINDOW_NEW
Command13=ID_WINDOW_CASCADE
Command14=ID_WINDOW_TILE_HORZ
Command15=ID_WINDOW_ARRANGE
Command16=ID_APP_ABOUT
Command17=IDM_UPUTSTVO
CommandCount=17

[ACL:IDR_MAINFRAME]
Type=1
Class=CMainFrame
Command1=ID_FILE_NEW
Command2=ID_FILE_OPEN
Command3=ID_FILE_SAVE
Command4=ID_EDIT_UNDO
Command5=ID_EDIT_CUT
Command6=ID_EDIT_COPY
Command7=ID_EDIT_PASTE
Command8=ID_EDIT_UNDO
Command9=ID_EDIT_CUT
Command10=ID_EDIT_COPY
Command11=ID_EDIT_PASTE
Command12=ID_NEXT_PANE
Command13=ID_PREV_PANE
CommandCount=13

[CLS:CUputstvoDLG]
Type=0
HeaderFile=UputstvoDLG.h
ImplementationFile=UputstvoDLG.cpp
BaseClass=CDialog
Filter=D
LastObject=CUputstvoDLG

[DLG:IDD_UPUTSTVO]
Type=1
Class=CUputstvoDLG
ControlCount=2
Control1=IDOK,button,1342242817
Control2=IDC_STATIC,static,1342308352

[CLS:CInfoDLG]
Type=0
HeaderFile=InfoDLG.h
ImplementationFile=InfoDLG.cpp
BaseClass=CDialog
Filter=D
LastObject=ID_EDIT_UNDO

[DLG:IDD_TABLA]
Type=1
Class=CInfoDLG
ControlCount=5
Control1=IDOK,button,1342242817
Control2=IDCANCEL,button,1342242816
Control3=IDC_NE,button,1342308361
Control4=IDC_DA,button,1342177289
Control5=IDC_STATIC,button,1342177287

