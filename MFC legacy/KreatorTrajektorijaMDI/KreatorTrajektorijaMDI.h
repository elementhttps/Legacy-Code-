// KreatorTrajektorijaMDI.h : main header file for the KREATORTRAJEKTORIJAMDI application
//

#if !defined(AFX_KREATORTRAJEKTORIJAMDI_H__AEEFF0B6_0069_48F2_8C8C_9289687FF7FC__INCLUDED_)
#define AFX_KREATORTRAJEKTORIJAMDI_H__AEEFF0B6_0069_48F2_8C8C_9289687FF7FC__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"       // main symbols

/////////////////////////////////////////////////////////////////////////////
// CKreatorTrajektorijaMDIApp:
// See KreatorTrajektorijaMDI.cpp for the implementation of this class
//

class CKreatorTrajektorijaMDIApp : public CWinApp
{
public:
	CKreatorTrajektorijaMDIApp();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CKreatorTrajektorijaMDIApp)
	public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL

// Implementation
	//{{AFX_MSG(CKreatorTrajektorijaMDIApp)
	afx_msg void OnAppAbout();
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_KREATORTRAJEKTORIJAMDI_H__AEEFF0B6_0069_48F2_8C8C_9289687FF7FC__INCLUDED_)
