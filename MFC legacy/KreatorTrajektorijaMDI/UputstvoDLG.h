#if !defined(AFX_UPUTSTVODLG_H__AFAFDB5A_04A8_486D_94A9_D27CE7574B9E__INCLUDED_)
#define AFX_UPUTSTVODLG_H__AFAFDB5A_04A8_486D_94A9_D27CE7574B9E__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// UputstvoDLG.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CUputstvoDLG dialog

class CUputstvoDLG : public CDialog
{
// Construction
public:
	CUputstvoDLG(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CUputstvoDLG)
	enum { IDD = IDD_UPUTSTVO };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CUputstvoDLG)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CUputstvoDLG)
		// NOTE: the ClassWizard will add member functions here
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_UPUTSTVODLG_H__AFAFDB5A_04A8_486D_94A9_D27CE7574B9E__INCLUDED_)
