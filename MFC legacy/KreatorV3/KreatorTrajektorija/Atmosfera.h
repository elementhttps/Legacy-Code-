#if !defined(AFX_ATMOSFERA_H__B49BCAC4_06FA_46C9_9084_930BEF44D11C__INCLUDED_)
#define AFX_ATMOSFERA_H__B49BCAC4_06FA_46C9_9084_930BEF44D11C__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// Atmosfera.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CAtmosfera dialog

class CAtmosfera : public CDialog
{
// Construction
public:
	CAtmosfera(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CAtmosfera)
	enum { IDD = IDD_ATMOSFERA };
	int		m_PAG;
	int		m_TAG;
	int		m_tropos;
	int		m_troposp;
	int		m_mezos;
	int		m_stratos;
	double	m_PadT;
	double	m_To;
	int		m_PTG;
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CAtmosfera)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CAtmosfera)
		// NOTE: the ClassWizard will add member functions here
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_ATMOSFERA_H__B49BCAC4_06FA_46C9_9084_930BEF44D11C__INCLUDED_)
