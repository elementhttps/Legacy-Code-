// FODT.cpp : implementation file
//

#include "stdafx.h"
#include "KreatorTrajektorija.h"
#include "FODT.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CFODT dialog


CFODT::CFODT(CWnd* pParent /*=NULL*/)
	: CDialog(CFODT::IDD, pParent)
{
	//{{AFX_DATA_INIT(CFODT)
	m_FodT = _T("");
	//}}AFX_DATA_INIT
}


void CFODT::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CFODT)
	DDX_Text(pDX, IDC_TEMP, m_FodT);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CFODT, CDialog)
	//{{AFX_MSG_MAP(CFODT)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CFODT message handlers

void CFODT::OnOK() 
{
	UpdateData(TRUE);
}

void CFODT::OnCancel() 
{
	DestroyWindow ();
}

BOOL CFODT::Create()
{
	return CDialog::Create(CFODT::IDD);
}
