; CLW file contains information for the MFC ClassWizard

[General Info]
Version=1
LastClass=CMreza
LastTemplate=CDialog
NewFileInclude1=#include "stdafx.h"
NewFileInclude2=#include "KreatorTrajektorija.h"
LastPage=0

ClassCount=16
Class1=CKreatorTrajektorijaApp
Class2=CKreatorTrajektorijaDoc
Class3=CKreatorTrajektorijaView
Class4=CMainFrame

ResourceCount=13
Resource1=IDR_KREATOTYPE
Resource2=IDR_MAINFRAME
Resource3=IDD_ATMOSFERA
Class5=CChildFrame
Class6=CAboutDlg
Class7=CAutorDV
Resource4=IDD_ZUM
Class8=CKorak
Resource5=IDD_MREZA
Class9=CParametri
Resource6=IDD_AUTORDV
Class10=CMreza
Resource7=IDD_UPUTSTVO
Class11=CDGrafik
Resource8=IDD_DGRAFIK
Class12=CTabla
Resource9=IDD_ITABLA
Class13=CZum
Resource10=IDD_ABOUTBOX
Resource11=IDD_PARAMETRI
Class14=CUputstvo
Class15=CAtmosfera
Resource12=IDD_KORAK
Class16=CFODT
Resource13=IDD_FODT

[CLS:CKreatorTrajektorijaApp]
Type=0
HeaderFile=KreatorTrajektorija.h
ImplementationFile=KreatorTrajektorija.cpp
Filter=N

[CLS:CKreatorTrajektorijaDoc]
Type=0
HeaderFile=KreatorTrajektorijaDoc.h
ImplementationFile=KreatorTrajektorijaDoc.cpp
Filter=N
LastObject=CKreatorTrajektorijaDoc

[CLS:CKreatorTrajektorijaView]
Type=0
HeaderFile=KreatorTrajektorijaView.h
ImplementationFile=KreatorTrajektorijaView.cpp
Filter=C
LastObject=CKreatorTrajektorijaView
BaseClass=CView
VirtualFilter=VWC


[CLS:CMainFrame]
Type=0
HeaderFile=MainFrm.h
ImplementationFile=MainFrm.cpp
Filter=T
BaseClass=CMDIFrameWnd
VirtualFilter=fWC
LastObject=IDM_UPUTSTVO


[CLS:CChildFrame]
Type=0
HeaderFile=ChildFrm.h
ImplementationFile=ChildFrm.cpp
Filter=M


[CLS:CAboutDlg]
Type=0
HeaderFile=KreatorTrajektorija.cpp
ImplementationFile=KreatorTrajektorija.cpp
Filter=D
LastObject=CAboutDlg

[DLG:IDD_ABOUTBOX]
Type=1
Class=CAboutDlg
ControlCount=4
Control1=IDC_STATIC,static,1342177283
Control2=IDC_STATIC,static,1342308480
Control3=IDC_STATIC,static,1342308352
Control4=IDOK,button,1342373889

[MNU:IDR_MAINFRAME]
Type=1
Class=CMainFrame
Command1=ID_FILE_NEW
Command2=ID_FILE_OPEN
Command3=ID_FILE_MRU_FILE1
Command4=ID_APP_EXIT
Command5=ID_APP_ABOUT
Command6=IDM_AUTOR
Command7=IDM_UPUTSTVO
CommandCount=7

[MNU:IDR_KREATOTYPE]
Type=1
Class=CKreatorTrajektorijaView
Command1=ID_FILE_NEW
Command2=ID_FILE_OPEN
Command3=ID_FILE_CLOSE
Command4=ID_FILE_SAVE
Command5=ID_FILE_SAVE_AS
Command6=ID_FILE_MRU_FILE1
Command7=ID_APP_EXIT
Command8=ID_EDIT_UNDO
Command9=ID_EDIT_CUT
Command10=ID_EDIT_COPY
Command11=ID_EDIT_PASTE
Command12=ID_WINDOW_NEW
Command13=ID_WINDOW_CASCADE
Command14=ID_WINDOW_TILE_HORZ
Command15=ID_WINDOW_ARRANGE
Command16=IDM_PARAMETRI
Command17=IDM_MREZA
Command18=IDM_TABLA
Command19=IDM_DGRAFIK
Command20=IDM_ATMOSFERA
Command21=IDM_FODT
Command22=IDM_KORAK
Command23=IDM_ZUM
Command24=ID_APP_ABOUT
Command25=IDM_AUTOR
Command26=IDM_UPUTSTVO
CommandCount=26

[ACL:IDR_MAINFRAME]
Type=1
Class=CMainFrame
Command1=ID_FILE_NEW
Command2=ID_FILE_OPEN
Command3=ID_FILE_SAVE
Command4=ID_EDIT_UNDO
Command5=ID_EDIT_CUT
Command6=ID_EDIT_COPY
Command7=ID_EDIT_PASTE
Command8=ID_EDIT_UNDO
Command9=ID_EDIT_CUT
Command10=ID_EDIT_COPY
Command11=ID_EDIT_PASTE
Command12=ID_NEXT_PANE
Command13=ID_PREV_PANE
CommandCount=13

[DLG:IDD_AUTORDV]
Type=1
Class=CAutorDV
ControlCount=6
Control1=IDOK,button,1342242817
Control2=IDC_STATIC,static,1342177294
Control3=IDC_STATIC,static,1342308352
Control4=IDC_STATIC,static,1342308352
Control5=IDC_STATIC,static,1342308865
Control6=IDC_STATIC,static,1342308352

[CLS:CAutorDV]
Type=0
HeaderFile=AutorDV.h
ImplementationFile=AutorDV.cpp
BaseClass=CDialog
Filter=D
LastObject=CAutorDV

[DLG:IDD_KORAK]
Type=1
Class=CKorak
ControlCount=11
Control1=IDOK,button,1342242817
Control2=IDCANCEL,button,1342242816
Control3=IDC_KORAK,edit,1350631552
Control4=IDC_LOOP,edit,1350631552
Control5=IDC_BROJTACAKA,edit,1350631552
Control6=IDC_STATIC,static,1342308352
Control7=IDC_STATIC,static,1342308352
Control8=IDC_STATIC,static,1342308352
Control9=IDC_STATIC,button,1342177287
Control10=IDC_STATIC,button,1342177287
Control11=IDC_IZRACUNAJ,button,1342242816

[CLS:CKorak]
Type=0
HeaderFile=Korak.h
ImplementationFile=Korak.cpp
BaseClass=CDialog
Filter=D
LastObject=CKorak

[DLG:IDD_PARAMETRI]
Type=1
Class=CParametri
ControlCount=32
Control1=IDOK,button,1342242817
Control2=IDCANCEL,button,1342242816
Control3=IDC_STATIC,button,1342177287
Control4=IDC_MF1_DA,button,1342308361
Control5=IDC_MF1_NE,button,1342177289
Control6=IDC_MF2_DA,button,1342308361
Control7=IDC_MF2_NE,button,1342177289
Control8=IDC_MF3_DA,button,1342308361
Control9=IDC_MF3_NE,button,1342177289
Control10=IDC_UGAO_F1,edit,1350631552
Control11=IDC_BRZINA_F1,edit,1350631552
Control12=IDC_STATIC,button,1342177287
Control13=IDC_STATIC,static,1342308352
Control14=IDC_STATIC,static,1342308352
Control15=IDC_STATIC,button,1342177287
Control16=IDC_STATIC,button,1342177287
Control17=IDC_STATIC,button,1342177287
Control18=IDC_UGAO_F2,edit,1350631552
Control19=IDC_BRZINA_F2,edit,1350631552
Control20=IDC_STATIC,button,1342177287
Control21=IDC_UGAO_F3,edit,1350631552
Control22=IDC_BRZINA_F3,edit,1350631552
Control23=IDC_STATIC,button,1342177287
Control24=IDC_PI,edit,1350631552
Control25=IDC_GRAV,edit,1350631552
Control26=IDC_STATIC,static,1342308352
Control27=IDC_STATIC,static,1342308352
Control28=IDC_STATIC,static,1342308352
Control29=IDC_STATIC,static,1342308352
Control30=IDC_STATIC,static,1342308352
Control31=IDC_STATIC,static,1342308352
Control32=IDC_STATIC,button,1342177287

[CLS:CParametri]
Type=0
HeaderFile=Parametri.h
ImplementationFile=Parametri.cpp
BaseClass=CDialog
Filter=D
LastObject=ID_WINDOW_CASCADE

[DLG:IDD_MREZA]
Type=1
Class=CMreza
ControlCount=7
Control1=IDOK,button,1342242817
Control2=IDCANCEL,button,1342242816
Control3=IDC_MREZA_DA,button,1342308361
Control4=IDC_MREZA_NE,button,1342177289
Control5=IDC_GMREZA,edit,1350631552
Control6=IDC_STATIC,button,1342177287
Control7=IDC_STATIC,button,1342177287

[CLS:CMreza]
Type=0
HeaderFile=Mreza.h
ImplementationFile=Mreza.cpp
BaseClass=CDialog
Filter=D
LastObject=IDOK

[DLG:IDD_DGRAFIK]
Type=1
Class=CDGrafik
ControlCount=5
Control1=IDOK,button,1342242817
Control2=IDCANCEL,button,1342242816
Control3=IDC_DGRAF_DA,button,1342308361
Control4=IDC_DGRAF_NE,button,1342177289
Control5=IDC_STATIC,button,1342177287

[CLS:CDGrafik]
Type=0
HeaderFile=DGrafik.h
ImplementationFile=DGrafik.cpp
BaseClass=CDialog
Filter=D

[DLG:IDD_ITABLA]
Type=1
Class=CTabla
ControlCount=5
Control1=IDOK,button,1342242817
Control2=IDCANCEL,button,1342242816
Control3=IDC_TABLA_DA,button,1342308361
Control4=IDC_TABLA_NE,button,1342177289
Control5=IDC_STATIC,button,1342177287

[CLS:CTabla]
Type=0
HeaderFile=Tabla.h
ImplementationFile=Tabla.cpp
BaseClass=CDialog
Filter=D

[DLG:IDD_ZUM]
Type=1
Class=CZum
ControlCount=6
Control1=IDOK,button,1342242817
Control2=IDCANCEL,button,1342242816
Control3=IDC_ZUM,edit,1350631552
Control4=IDC_ZUMY,edit,1350631552
Control5=IDC_STATIC,static,1342308352
Control6=IDC_STATIC,static,1342308352

[CLS:CZum]
Type=0
HeaderFile=Zum.h
ImplementationFile=Zum.cpp
BaseClass=CDialog
Filter=D

[DLG:IDD_UPUTSTVO]
Type=1
Class=CUputstvo
ControlCount=2
Control1=IDOK,button,1342242817
Control2=IDC_STATIC,static,1342308352

[CLS:CUputstvo]
Type=0
HeaderFile=Uputstvo.h
ImplementationFile=Uputstvo.cpp
BaseClass=CDialog
Filter=D

[DLG:IDD_ATMOSFERA]
Type=1
Class=CAtmosfera
ControlCount=30
Control1=IDOK,button,1342242817
Control2=IDCANCEL,button,1342242816
Control3=IDC_TROPOS,edit,1350631552
Control4=IDC_TROPOP,edit,1350631552
Control5=IDC_STRATOSFERA,edit,1350631552
Control6=IDC_MEZOSFERA,edit,1350631552
Control7=IDC_PTO,edit,1350631552
Control8=IDC_PADT,edit,1350631552
Control9=IDC_PRIKAZGA_DA,button,1342308361
Control10=IDC_PRIKAZGA_NE,button,1342177289
Control11=IDC_TEKSTA_DA,button,1342308361
Control12=IDC_TEKSTA_NE,button,1342177289
Control13=IDC_TEMP_DA,button,1342308361
Control14=IDC_TEMP_NE,button,1342177289
Control15=IDC_STATIC,button,1342177287
Control16=IDC_STATIC,button,1342177287
Control17=IDC_STATIC,button,1342177287
Control18=IDC_STATIC,button,1342177287
Control19=IDC_STATIC,button,1342177287
Control20=IDC_STATIC,static,1342308352
Control21=IDC_STATIC,static,1342308352
Control22=IDC_STATIC,static,1342308352
Control23=IDC_STATIC,static,1342308352
Control24=IDC_STATIC,static,1342308352
Control25=IDC_STATIC,static,1342308352
Control26=IDC_STATIC,static,1342308352
Control27=IDC_STATIC,static,1342308352
Control28=IDC_STATIC,static,1342308352
Control29=IDC_STATIC,static,1342308352
Control30=IDC_STATIC,static,1350565902

[CLS:CAtmosfera]
Type=0
HeaderFile=Atmosfera.h
ImplementationFile=Atmosfera.cpp
BaseClass=CDialog
Filter=D

[DLG:IDD_FODT]
Type=1
Class=CFODT
ControlCount=3
Control1=IDOK,button,1342242817
Control2=IDCANCEL,button,1342242816
Control3=IDC_TEMP,edit,1353781252

[CLS:CFODT]
Type=0
HeaderFile=FODT.h
ImplementationFile=FODT.cpp
BaseClass=CDialog
Filter=D
VirtualFilter=dWC
LastObject=IDCANCEL

