// KreatorTrajektorijaView.h : interface of the CKreatorTrajektorijaView class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_KREATORTRAJEKTORIJAVIEW_H__B677E3A2_2929_4341_A61B_21C72372499D__INCLUDED_)
#define AFX_KREATORTRAJEKTORIJAVIEW_H__B677E3A2_2929_4341_A61B_21C72372499D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class CFODT;

class CKreatorTrajektorijaView : public CView
{
protected: // create from serialization only
	CKreatorTrajektorijaView();
	DECLARE_DYNCREATE(CKreatorTrajektorijaView)

// Attributes
public:
	CKreatorTrajektorijaDoc* GetDocument();
	CFODT* m_pDlgFODT;// clasa dijaloga FODT i kreiranje naziva

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CKreatorTrajektorijaView)
	public:
	virtual void OnDraw(CDC* pDC);  // overridden to draw this view
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	protected:
	//}}AFX_VIRTUAL

// Implementation
public:
	double m_Brzina_f1;
	virtual ~CKreatorTrajektorijaView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	int m_PTG;
	double m_PadT;
	double m_To;




	int m_Zumy;
	int m_TAG;
	int m_PAG;
	
	int m_mezos;
	int m_stratos;
	int m_troposp;
	int m_tropos;
	int m_Mf3;
	int m_Mf2;
	int m_Mf1;
	double m_Grav;
	double m_PI;
	int m_ITabla;
	int m_GMreza;



	

	double m_Korak;
	int m_Zumx;

	int m_IDev;
	int m_Mreza;
	int m_DGrafik;


	int m_Brzina_f3;
	int m_Ugao_f3;

	double m_Brzina_f2;
	int m_Ugao_f2;
	

	
	int m_Ugao_f1;
	
	int m_Orgfx;
	int m_Orgfy;


	CString m_FodT;
	//{{AFX_MSG(CKreatorTrajektorijaView)
	afx_msg void OnUputstvo();
	afx_msg void OnZum();
	afx_msg void OnParametri();
	afx_msg void OnMreza();
	afx_msg void OnKorak();
	afx_msg void OnDgrafik();
	afx_msg void OnTabla();
	afx_msg void OnAutor();
	afx_msg void OnAtmosfera();
	afx_msg void OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags);
	afx_msg void OnFodt();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

#ifndef _DEBUG  // debug version in KreatorTrajektorijaView.cpp
inline CKreatorTrajektorijaDoc* CKreatorTrajektorijaView::GetDocument()
   { return (CKreatorTrajektorijaDoc*)m_pDocument; }
#endif

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_KREATORTRAJEKTORIJAVIEW_H__B677E3A2_2929_4341_A61B_21C72372499D__INCLUDED_)
