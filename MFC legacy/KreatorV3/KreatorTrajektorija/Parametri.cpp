// Parametri.cpp : implementation file
//

#include "stdafx.h"
#include "KreatorTrajektorija.h"
#include "Parametri.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CParametri dialog


CParametri::CParametri(CWnd* pParent /*=NULL*/)
	: CDialog(CParametri::IDD, pParent)
{
	//{{AFX_DATA_INIT(CParametri)
		m_Brzina_f3 = 0;
	m_Ugao_f1 = 0;
	m_Ugao_f2 = 0;
	m_Ugao_f3 = 0;
	m_PI = 0.0;
	m_Grav = 0.0;
	m_Mf1 = -1;
	m_Mf2 = -1;
	m_Mf3 = -1;
	m_Brzina_f1 = 0.0;
	m_Brzina_f2 = 0.0;
	//}}AFX_DATA_INIT
}


void CParametri::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CParametri)
		DDX_Text(pDX, IDC_BRZINA_F3, m_Brzina_f3);
	DDX_Text(pDX, IDC_UGAO_F1, m_Ugao_f1);
	DDX_Text(pDX, IDC_UGAO_F2, m_Ugao_f2);
	DDX_Text(pDX, IDC_UGAO_F3, m_Ugao_f3);
	DDX_Text(pDX, IDC_PI, m_PI);
	DDX_Text(pDX, IDC_GRAV, m_Grav);
	DDX_Radio(pDX, IDC_MF1_DA, m_Mf1);
	DDX_Radio(pDX, IDC_MF2_DA, m_Mf2);
	DDX_Radio(pDX, IDC_MF3_DA, m_Mf3);
	DDX_Text(pDX, IDC_BRZINA_F1, m_Brzina_f1);
	DDX_Text(pDX, IDC_BRZINA_F2, m_Brzina_f2);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CParametri, CDialog)
	//{{AFX_MSG_MAP(CParametri)
		// NOTE: the ClassWizard will add message map macros here
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CParametri message handlers
