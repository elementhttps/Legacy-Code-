#include <afxwin.h>
#include "Pita.h"
#include <math.h>
#define PI 3.1415926

CMyApp myApp;

/////////////////////////////////////////////////////////////////////////
// CMyApp member functions

BOOL CMyApp::InitInstance ()
{
    m_pMainWnd = new CMainWindow;
	

   m_pMainWnd->ShowWindow (m_nCmdShow);
    m_pMainWnd->UpdateWindow ();
    return TRUE;
}

/////////////////////////////////////////////////////////////////////////
// CMainWindow message map and member functions

BEGIN_MESSAGE_MAP (CMainWindow, CFrameWnd)
    ON_WM_PAINT ()
END_MESSAGE_MAP ()

CMainWindow::CMainWindow ()
{
    Create (NULL,  ("Kruzni Grafik"));
}

void CMainWindow::OnPaint ()
{
    CPaintDC dc (this);
	
    int nRevenues[4] = { 10, 1, 3, 1 };

    CRect rect;
    GetClientRect (&rect);
    dc.SetViewportOrg (rect.Width () / 2, rect.Height () /2);  //koordinatni centar

    int nTotal = 0;
    for (int i=0; i<4; i++)
	{
        nTotal += nRevenues[i];
	}   


    int x1 = 0;
    int y1 = -1000;
    int nSum = 0;

    for (i=0; i<4; i++) 
	{
        nSum += nRevenues[i];

        double rad = ((double) (nSum * 2 * PI) / (double) nTotal) + PI;

        int x2 = (int) (sin (rad) * 1000);
        int y2 = (int) (cos (rad) * 1000 * 3) / 5;

        dc.Pie (-200, -150, 200, 150, x1, y1, x2, y2);

        x1 = x2;
        y1 = y2;
    }
	dc.TextOut (10, 100, CString (("PITA SA SLJIVAMA")));
	
	
	
}
