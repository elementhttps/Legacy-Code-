// MFCSampleDlg.h : header file
//

#if !defined(AFX_MFCSAMPLEDLG_H__98275535_D612_457E_B401_ADA08E8D8B51__INCLUDED_)
#define AFX_MFCSAMPLEDLG_H__98275535_D612_457E_B401_ADA08E8D8B51__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

/////////////////////////////////////////////////////////////////////////////
// CMFCSampleDlg dialog

class CMFCSampleDlg : public CDialog
{
// Construction
public:
	CMFCSampleDlg(CWnd* pParent = NULL);	// standard constructor

// Dialog Data
	//{{AFX_DATA(CMFCSampleDlg)
	enum { IDD = IDD_MFCSAMPLE_DIALOG };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMFCSampleDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	//{{AFX_MSG(CMFCSampleDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	afx_msg void OnPbfile();
	afx_msg void OnPbFile();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_MFCSAMPLEDLG_H__98275535_D612_457E_B401_ADA08E8D8B51__INCLUDED_)
