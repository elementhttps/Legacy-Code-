#include <afxwin.h>
#include "resource.h"
class CMainFrame : public CFrameWnd //............1......._Klasa_WINDOW
{
public:
	CMainFrame ();//deklaracija CMainFrame funkcije /--DEK--/

	
//DEKLARACIJE FUNKCIJA U KLASI CMainFrame
protected:
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);// potrebni parametri za OnCreate (int) /--DEK--/
	afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);// deklaracija funkcije OnShowWindow OnActivate /--DEK--/
	afx_msg void OnActivate(UINT nState, CWnd* pWndOther, BOOL bMinimized);//deklaracija funkcije /--DEK--/
	afx_msg void OnPaint();//deklaracija funkcije OnPaint /--DEK--/
	afx_msg void OnMove(int x, int y);///--DEK--/
	afx_msg void OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags);///--DEK--/
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);///--DEK--/

	DECLARE_MESSAGE_MAP()//macro za BEGIN_MESSAGE_MAP(CMainFrame, CFrameWnd) /--DEK--/
};

CMainFrame::CMainFrame()//definicija i pristup funkciji u klasi CMainFrame
{
	HCURSOR hCursor;
    HICON hIcon;
  
    hCursor = AfxGetApp()->LoadCursor(IDC_APP_CURS);//paziti postiji LoadCursor(Ime) i LoadStandardCursor(IDC_SIZEALL)
    hIcon   = AfxGetApp()->LoadStandardIcon(IDI_EXCLAMATION);

    const char *RWC = AfxRegisterWndClass(CS_VREDRAW | CS_HREDRAW,
	                                      hCursor,
				                          (HBRUSH)GetStockObject(BLACK_BRUSH),
                                          hIcon);
	Create( RWC, //lpszClassName naziv klase OVDE JE ULAZ ZA RESURSE u vidu pointera  RWC-a
		    "Windows Application", // prvobitan naziv prozora
		   WS_OVERLAPPEDWINDOW ,// stil prikazivanja WS_VISIBLE | WS_SYSMENU | WS_MINIMIZEBOX )WS_OVERLAPPEDWINDOW
	        CRect(120, 100, 700, 480),// pozicija prikaza na ekranu
			NULL,
		    MAKEINTRESOURCE(IDR_MENU1));//Meni DA
}
/*
BOOL Create(    LPCTSTR lpszClassName,
				LPCTSTR lpszWindowName,
				DWORD dwStyle = WS_OVERLAPPEDWINDOW,
				const RECT& rect = rectDefault,
				CWnd* pParentWnd = NULL,        // != NULL for popups
				LPCTSTR lpszMenuName = NULL,
				DWORD dwExStyle = 0,
				CCreateContext* pContext = NULL);
*/
class CExerciseApp: public CWinApp //.............2........_Klasa_APP
{
public:
	BOOL InitInstance();
};

BEGIN_MESSAGE_MAP(CMainFrame, CFrameWnd)
	ON_WM_CREATE()
	ON_WM_SHOWWINDOW()
	ON_WM_ACTIVATE()
	ON_WM_PAINT()
	ON_WM_MOVE()
	ON_WM_KEYDOWN()
	ON_WM_LBUTTONDOWN()
END_MESSAGE_MAP()



//DEFINICIJE FUNKCUJA


int CMainFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)   // /--DEFINICIJA--/ za OnCreate (nije potrebna jer je vec definisana)
// MOZE I BEZ OVE DEFINICIJE AKO SE UKLONI afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
{	
	
	if (CFrameWnd::OnCreate(lpCreateStruct) == 0)// Call the base class to create the window
	{
		MessageBox("The window has been created!!!");// If the window was successfully created, let the user know
		return 0;
	}
		
	return -1;// Otherwise, return -1
}

void CMainFrame::OnShowWindow(BOOL bShow, UINT nStatus) // /--DEFINICIJA--/ pocetni prikaz prozora MAXISIZE MINISIZE...
{
	CFrameWnd::OnShowWindow(bShow, nStatus);
	ShowWindow(SW_SHOWMAXIMIZED );//handler code  ShowWindow(SW_SHOW);SW_NORMAL SW_SHOWMINIMIZED
	
	 
	
}

void CMainFrame::OnActivate(UINT nState, CWnd* pWndOther, BOOL bMinimized) // /--DEFINICIJA--/ 
//  nakon sto je prikazan uraditi funkciju prilikom aktiviranja prozora npr ALT+TAB , mis ...
{
	CFrameWnd::OnActivate(nState, pWndOther, bMinimized);
	
	// TODO: Add your message handler code here
	switch( nState )
	{
	case WA_ACTIVE:
		SetWindowText("This window has been activated, without the mouse!");
		break;
	case WA_INACTIVE:
		SetWindowText("Deaktiviran prozor!!");
		break;
	case WA_CLICKACTIVE:
		SetWindowText("This window has been activated using the mouse!!!");
		break;
	}	
}

void CMainFrame::OnPaint() // /--DEFINICIJA--/
{
	CFrameWnd::OnPaint();

	SetWindowText("The window has been painted");
}
void CMainFrame::OnMove(int x, int y) // /--DEFINICIJA--/
{
	CFrameWnd::OnMove(x, y);
	SetWindowText("Prozor je pomeren misem");

}
void CMainFrame::OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags) // /--DEFINICIJA--/
{
	switch(nChar)
	{
	case VK_RETURN:
		SetWindowText("You pressed Enter");
		break;
	case VK_F1:
		SetWindowText("Help is not available at the moment");
		break;
	case VK_DELETE:
		SetWindowText("Can't Delete This");
		break;
	default:
		SetWindowText("Whatever");
	}
}
void CMainFrame::OnLButtonDown(UINT nFlags, CPoint point) 
{
	
	SetWindowText("Klik na prozor");
}

BOOL CExerciseApp::InitInstance()
{
	m_pMainWnd = new CMainFrame ;
	m_pMainWnd->ShowWindow(SW_SHOW);//SW_MAX, SW_SHOW
	m_pMainWnd->UpdateWindow();

	return TRUE;
}

CExerciseApp theApp;