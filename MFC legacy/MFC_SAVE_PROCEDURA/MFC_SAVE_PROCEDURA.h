// MFC_SAVE_PROCEDURA.h : main header file for the MFC_SAVE_PROCEDURA application
//

#if !defined(AFX_MFC_SAVE_PROCEDURA_H__B60B7D76_3ED7_469A_87F9_9FDBF898DEE6__INCLUDED_)
#define AFX_MFC_SAVE_PROCEDURA_H__B60B7D76_3ED7_469A_87F9_9FDBF898DEE6__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"       // main symbols

/////////////////////////////////////////////////////////////////////////////
// CMFC_SAVE_PROCEDURAApp:
// See MFC_SAVE_PROCEDURA.cpp for the implementation of this class
//

class CMFC_SAVE_PROCEDURAApp : public CWinApp
{
public:
	CMFC_SAVE_PROCEDURAApp();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMFC_SAVE_PROCEDURAApp)
	public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL

// Implementation
	//{{AFX_MSG(CMFC_SAVE_PROCEDURAApp)
	afx_msg void OnAppAbout();
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_MFC_SAVE_PROCEDURA_H__B60B7D76_3ED7_469A_87F9_9FDBF898DEE6__INCLUDED_)
