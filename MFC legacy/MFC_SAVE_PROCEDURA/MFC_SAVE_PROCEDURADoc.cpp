// MFC_SAVE_PROCEDURADoc.cpp : implementation of the CMFC_SAVE_PROCEDURADoc class
//

#include "stdafx.h"
#include "MFC_SAVE_PROCEDURA.h"

#include "MFC_SAVE_PROCEDURADoc.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CMFC_SAVE_PROCEDURADoc

IMPLEMENT_DYNCREATE(CMFC_SAVE_PROCEDURADoc, CDocument)

BEGIN_MESSAGE_MAP(CMFC_SAVE_PROCEDURADoc, CDocument)
	//{{AFX_MSG_MAP(CMFC_SAVE_PROCEDURADoc)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CMFC_SAVE_PROCEDURADoc construction/destruction

CMFC_SAVE_PROCEDURADoc::CMFC_SAVE_PROCEDURADoc()
{
	// TODO: add one-time construction code here

}

CMFC_SAVE_PROCEDURADoc::~CMFC_SAVE_PROCEDURADoc()
{
}

BOOL CMFC_SAVE_PROCEDURADoc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;

	// TODO: add reinitialization code here
	// (SDI documents will reuse this document)

	return TRUE;
}



/////////////////////////////////////////////////////////////////////////////
// CMFC_SAVE_PROCEDURADoc serialization

void CMFC_SAVE_PROCEDURADoc::Serialize(CArchive& ar)
{
	if (ar.IsStoring())
	{
		// TODO: add storing code here
	}
	else
	{
		// TODO: add loading code here
	}
}

/////////////////////////////////////////////////////////////////////////////
// CMFC_SAVE_PROCEDURADoc diagnostics

#ifdef _DEBUG
void CMFC_SAVE_PROCEDURADoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CMFC_SAVE_PROCEDURADoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CMFC_SAVE_PROCEDURADoc commands
