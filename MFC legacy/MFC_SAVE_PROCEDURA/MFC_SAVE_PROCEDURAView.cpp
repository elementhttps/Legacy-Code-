// MFC_SAVE_PROCEDURAView.cpp : implementation of the CMFC_SAVE_PROCEDURAView class
//

#include "stdafx.h"
#include "MFC_SAVE_PROCEDURA.h"

#include "MFC_SAVE_PROCEDURADoc.h"
#include "MFC_SAVE_PROCEDURAView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CMFC_SAVE_PROCEDURAView

IMPLEMENT_DYNCREATE(CMFC_SAVE_PROCEDURAView, CView)

BEGIN_MESSAGE_MAP(CMFC_SAVE_PROCEDURAView, CView)
	//{{AFX_MSG_MAP(CMFC_SAVE_PROCEDURAView)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CMFC_SAVE_PROCEDURAView construction/destruction

CMFC_SAVE_PROCEDURAView::CMFC_SAVE_PROCEDURAView()
{
	// TODO: add construction code here

}

CMFC_SAVE_PROCEDURAView::~CMFC_SAVE_PROCEDURAView()
{
}

BOOL CMFC_SAVE_PROCEDURAView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	return CView::PreCreateWindow(cs);
}

/////////////////////////////////////////////////////////////////////////////
// CMFC_SAVE_PROCEDURAView drawing

void CMFC_SAVE_PROCEDURAView::OnDraw(CDC* pDC)
{
	CMFC_SAVE_PROCEDURADoc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);
	// TODO: add draw code for native data here
}

/////////////////////////////////////////////////////////////////////////////
// CMFC_SAVE_PROCEDURAView diagnostics

#ifdef _DEBUG
void CMFC_SAVE_PROCEDURAView::AssertValid() const
{
	CView::AssertValid();
}

void CMFC_SAVE_PROCEDURAView::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}

CMFC_SAVE_PROCEDURADoc* CMFC_SAVE_PROCEDURAView::GetDocument() // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CMFC_SAVE_PROCEDURADoc)));
	return (CMFC_SAVE_PROCEDURADoc*)m_pDocument;
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CMFC_SAVE_PROCEDURAView message handlers
