// MFC_SAVE_PROCEDURAView.h : interface of the CMFC_SAVE_PROCEDURAView class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_MFC_SAVE_PROCEDURAVIEW_H__BF4786E9_B9D0_485E_A648_EAF40ECAA8C0__INCLUDED_)
#define AFX_MFC_SAVE_PROCEDURAVIEW_H__BF4786E9_B9D0_485E_A648_EAF40ECAA8C0__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


class CMFC_SAVE_PROCEDURAView : public CView
{
protected: // create from serialization only
	CMFC_SAVE_PROCEDURAView();
	DECLARE_DYNCREATE(CMFC_SAVE_PROCEDURAView)

// Attributes
public:
	CMFC_SAVE_PROCEDURADoc* GetDocument();

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMFC_SAVE_PROCEDURAView)
	public:
	virtual void OnDraw(CDC* pDC);  // overridden to draw this view
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	protected:
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CMFC_SAVE_PROCEDURAView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	//{{AFX_MSG(CMFC_SAVE_PROCEDURAView)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

#ifndef _DEBUG  // debug version in MFC_SAVE_PROCEDURAView.cpp
inline CMFC_SAVE_PROCEDURADoc* CMFC_SAVE_PROCEDURAView::GetDocument()
   { return (CMFC_SAVE_PROCEDURADoc*)m_pDocument; }
#endif

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_MFC_SAVE_PROCEDURAVIEW_H__BF4786E9_B9D0_485E_A648_EAF40ECAA8C0__INCLUDED_)
