#include <afxwin.h>

class CMainFrame : public CFrameWnd
{
public:
	CMainFrame ();

protected:
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnShowWindow(BOOL bShow, UINT nStatus );//su vrednosti u void funkciji koja uzima konstanta nStatus 
	                                                     // SW_MINIMIZE,SW_MAXIMIZE, SW_PARENTCLOSING,SW_PARENTOPENING,0
	DECLARE_MESSAGE_MAP()
};

CMainFrame::CMainFrame()
{
	// Create the window's frame
	Create(NULL, "Windows Application", WS_OVERLAPPEDWINDOW,
	       CRect(120, 100, 700, 480), NULL);
}

class CExerciseApp: public CWinApp
{
public:
	BOOL InitInstance();
};

BEGIN_MESSAGE_MAP(CMainFrame, CFrameWnd)
	ON_WM_CREATE()
	ON_WM_SHOWWINDOW()
END_MESSAGE_MAP()
//kao vrednost za lpCreateStruct uzima se int vrednost koja moze biti:


//----------------------------------
//                  1
//----------------------------------

int CMainFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	// Call the base class to create the window
	if( CFrameWnd::OnCreate(lpCreateStruct) == 0)
	{
		// If the window was successfully created, let the user know
		MessageBox("The window has been created!!!");
                  // Since the window was successfully created, return 0
		return 0;
	}
	// Otherwise, return -1
	return -1;
}


//----------------------------------
//                   2
//----------------------------------
/*int CMainFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	// Call the base class to create the window
	if (CFrameWnd::OnCreate(lpCreateStruct) == -1)
		return -1;
	return 0;
}
*/
void CMainFrame::OnShowWindow(BOOL bShow, UINT nStatus) 
{
	CFrameWnd::OnShowWindow(bShow, nStatus);
	
	// TODO: Add your message handler code here
	ShowWindow(SW_MINIMIZE); //unos vrednosti konstanti// ili SW_MINIMIZE,SW_MAXIMIZE, SW_PARENTCLOSING,SW_PARENTOPENING,0
}

BOOL CExerciseApp::InitInstance()
{
	m_pMainWnd = new CMainFrame ;
	m_pMainWnd->ShowWindow(SW_SHOW);
	m_pMainWnd->UpdateWindow();

	return TRUE;
}

CExerciseApp theApp;