// MFC_TESTDVDDoc.h : interface of the CMFC_TESTDVDDoc class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_MFC_TESTDVDDOC_H__F5209747_A752_4E10_93AB_54B49728DC7F__INCLUDED_)
#define AFX_MFC_TESTDVDDOC_H__F5209747_A752_4E10_93AB_54B49728DC7F__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


class CMFC_TESTDVDDoc : public CDocument
{
protected: // create from serialization only
	CMFC_TESTDVDDoc();
	DECLARE_DYNCREATE(CMFC_TESTDVDDoc)

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMFC_TESTDVDDoc)
	public:
	virtual BOOL OnNewDocument();
	virtual void Serialize(CArchive& ar);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CMFC_TESTDVDDoc();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	//{{AFX_MSG(CMFC_TESTDVDDoc)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_MFC_TESTDVDDOC_H__F5209747_A752_4E10_93AB_54B49728DC7F__INCLUDED_)
