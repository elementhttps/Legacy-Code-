// MFC_TESTDVDView.h : interface of the CMFC_TESTDVDView class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_MFC_TESTDVDVIEW_H__FBCA1E98_1BC5_4AB9_A726_46CB094036D9__INCLUDED_)
#define AFX_MFC_TESTDVDVIEW_H__FBCA1E98_1BC5_4AB9_A726_46CB094036D9__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


class CMFC_TESTDVDView : public CView
{
protected: // create from serialization only
	CMFC_TESTDVDView();
	DECLARE_DYNCREATE(CMFC_TESTDVDView)

// Attributes
public:
	CMFC_TESTDVDDoc* GetDocument();

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMFC_TESTDVDView)
	public:
	virtual void OnDraw(CDC* pDC);  // overridden to draw this view
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	protected:
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CMFC_TESTDVDView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	int m_Linija;
	//{{AFX_MSG(CMFC_TESTDVDView)
	afx_msg void OnOpcije();
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg BOOL OnMouseWheel(UINT nFlags, short zDelta, CPoint pt);
	afx_msg void OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

#ifndef _DEBUG  // debug version in MFC_TESTDVDView.cpp
inline CMFC_TESTDVDDoc* CMFC_TESTDVDView::GetDocument()
   { return (CMFC_TESTDVDDoc*)m_pDocument; }
#endif

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_MFC_TESTDVDVIEW_H__FBCA1E98_1BC5_4AB9_A726_46CB094036D9__INCLUDED_)
