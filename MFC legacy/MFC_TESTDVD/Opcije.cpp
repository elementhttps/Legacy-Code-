// Opcije.cpp : implementation file
//

#include "stdafx.h"
#include "MFC_TESTDVD.h"
#include "Opcije.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// COpcije dialog


COpcije::COpcije(CWnd* pParent /*=NULL*/)
	: CDialog(COpcije::IDD, pParent)
{
	//{{AFX_DATA_INIT(COpcije)
	m_Linija = 0;
	//}}AFX_DATA_INIT
}


void COpcije::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(COpcije)
	DDX_Text(pDX, IDC_LINIJA, m_Linija);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(COpcije, CDialog)
	//{{AFX_MSG_MAP(COpcije)
		// NOTE: the ClassWizard will add message map macros here
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// COpcije message handlers
