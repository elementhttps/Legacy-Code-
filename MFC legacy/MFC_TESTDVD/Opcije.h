#if !defined(AFX_OPCIJE_H__6B448646_783D_44E2_B78D_59FDE00EEF90__INCLUDED_)
#define AFX_OPCIJE_H__6B448646_783D_44E2_B78D_59FDE00EEF90__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// Opcije.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// COpcije dialog

class COpcije : public CDialog
{
// Construction
public:
	COpcije(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(COpcije)
	enum { IDD = IDD_OPCIJE };
	int		m_Linija;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(COpcije)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(COpcije)
		// NOTE: the ClassWizard will add member functions here
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_OPCIJE_H__6B448646_783D_44E2_B78D_59FDE00EEF90__INCLUDED_)
