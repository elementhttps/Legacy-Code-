// MFC_TVer3.h : main header file for the MFC_TVER3 application
//

#if !defined(AFX_MFC_TVER3_H__DE0787A5_65EF_4193_9BAC_B59EFB750E97__INCLUDED_)
#define AFX_MFC_TVER3_H__DE0787A5_65EF_4193_9BAC_B59EFB750E97__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"       // main symbols

/////////////////////////////////////////////////////////////////////////////
// CMFC_TVer3App:
// See MFC_TVer3.cpp for the implementation of this class
//

class CMFC_TVer3App : public CWinApp
{
public:
	CMFC_TVer3App();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMFC_TVer3App)
	public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL

// Implementation
	//{{AFX_MSG(CMFC_TVer3App)
	afx_msg void OnAppAbout();
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_MFC_TVER3_H__DE0787A5_65EF_4193_9BAC_B59EFB750E97__INCLUDED_)
