// MFC_TVer3View.cpp : implementation of the CMFC_TVer3View class
//

#include "stdafx.h"
#include "MFC_TVer3.h"

#include "Math.h"

#include "MFC_TVer3Doc.h"
#include "MFC_TVer3View.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CMFC_TVer3View

IMPLEMENT_DYNCREATE(CMFC_TVer3View, CView)

BEGIN_MESSAGE_MAP(CMFC_TVer3View, CView)
	//{{AFX_MSG_MAP(CMFC_TVer3View)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CMFC_TVer3View construction/destruction

CMFC_TVer3View::CMFC_TVer3View()
{
	// TODO: add construction code here

}

CMFC_TVer3View::~CMFC_TVer3View()
{
}

BOOL CMFC_TVer3View::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	return CView::PreCreateWindow(cs);
}

/////////////////////////////////////////////////////////////////////////////
// CMFC_TVer3View drawing

void CMFC_TVer3View::OnDraw(CDC* pDC)
{
	CMFC_TVer3Doc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);


    
    
    //
    // Initialize the device context.
    //
    pDC->SetMapMode(MM_ISOTROPIC);
	pDC->SetViewportOrg(60, 420);
	pDC->SetWindowExt(10000,10000);
	pDC->SetViewportExt(100,-100);
	

	CPen PenBlack(PS_SOLID, 200, RGB(0, 0, 0));
	pDC->SelectObject(PenBlack);

	// OSE
	pDC->MoveTo(-1000,     0);
	pDC->LineTo( 20000000,     0);
	pDC->MoveTo(   0, -1000);
	pDC->LineTo(   0,  20000000);

	
;   
	
	
	
//KONSTANTE

double PI = 3.14159;
double g = 9.81;

//PROMENE BRZINE I UGLA
int alfa1 = 46;
int alfa2 = 44;
int alfa3 = 45;

long int v1 = 870;
int v2 = 880;
int v3 = 876;
	
//KORAK I MAKSIMUM
CString string;

pDC->SetTextColor(RGB(255, 0, 0));
string.Format (("-Funkcija f1 Ugao %d, Brzina %d m/s "), alfa1, v1);

pDC->TextOut (60000,35000 , string);

pDC->SetTextColor(RGB(0, 0, 255));
string.Format (("-Funkcija f1 Ugao %d, Brzina %d m/s "), alfa2, v2);
pDC->TextOut (60000,33000 , string);

pDC->SetTextColor(RGB(0, 255, 0));
string.Format (("-Funkcija f1 Ugao %d, Brzina %d m/s "), alfa3, v3);
pDC->TextOut (60000,31000 , string);

double kor = 1;

double max = 150000; //oko milion


// Multiplikator koordinata pozicije teksta
	double kox = 1500;
	double koy = 1000;
/*
    pDC->SetTextColor(RGB(255, 0, 0)); 
	pDC->TextOut(50*kox, 40*koy, "Funkcija  f1" , 12);
	pDC->SetTextColor(RGB(0, 0, 255));
	pDC->TextOut(50*kox, 35*koy, "Funkcija  f2" , 12);
	pDC->SetTextColor(RGB(0, 255, 0));
	pDC->TextOut(50*kox, 30*koy, "Funkcija  f3" , 12);
*/

//Funkcija f1 CRVENA

	for(double i = 1; i < max; i +=kor)
	{ 
        
		double c1 = 1  ;


	    double f = (i*tan((PI*alfa1)/180))-((g*i*i)/(2*v1*v1*cos((PI*alfa1)/180)*cos((PI*alfa1)/180)));
	    pDC->SetPixel(i, f, RGB(255, 0, 0));
	}

//Funkcija f2 PLAVA

	for(double i2 = 1; i2 < max; i2 +=kor)
	{
		int c2 = 0;
	    double f2 = (i2*tan((PI*alfa2)/180))-((g*i2*i2)/(2*v2*v2*cos((PI*alfa2)/180)*cos((PI*alfa2)/180)));
			//tan(i2)*i2;
	    pDC->SetPixel(i2, f2, RGB(0, 0, 255));
	}

//Funkcija f3 ZELENA

    for(double i3 = 1; i3 < max; i3 +=kor)
	{
		int c3 = 0;
	    double f3 = (i3*tan((PI*alfa3)/180))-((g*i3*i3)/(2*v3*v3*cos((PI*alfa3)/180)*cos((PI*alfa3)/180)));;
	    pDC->SetPixel(i3, f3, RGB(0, 255, 0));
	}



//POZICIJA OZNAKA
	
	
	//Koordinate 
	pDC->SetTextColor(RGB(100, 100, 0));

    pDC->TextOut(56000, 2000, 'X');
	pDC->TextOut(500, 35000, 'Y');
   
    //Lenjiri po x i y osi
    CPen PenBlack3(PS_SOLID, 100, RGB(0, 0, 0));
	pDC->SelectObject(PenBlack3);
    
	for(int koox = 0; koox < 1000000; koox += 1000)
    {
        pDC->MoveTo(koox, 0);
        pDC->LineTo(koox, -1000);
    }

	for(int kooy = 0; kooy < 1000000; kooy += 1000)
    {
        pDC->MoveTo(0, kooy);
        pDC->LineTo(-1000, kooy);
    }



	pDC->SelectObject(PenBlack3);
    for(int koox2 = 0; koox2 < 1000000; koox2 += 5000)
    {
        pDC->MoveTo(koox2, 0);
        pDC->LineTo(koox2, -2000);
    }
	for(int kooy2 = 0; kooy2 < 1000000; kooy2 += 5000)
    {
        pDC->MoveTo(0, kooy2);
        pDC->LineTo(-2000, kooy2);
    }

	for(int koox3 = 0; koox3 < 1000000; koox3 += 10000)
    {
        pDC->MoveTo(koox3, 0);
        pDC->LineTo(koox3, -3000);
    }
	for(int kooy3 = 0; kooy3 < 1000000; kooy3 += 10000)
    {
        pDC->MoveTo(0, kooy3);
        pDC->LineTo(-3000, kooy3);
    }

    pDC->SetTextColor(RGB(0, 0, 0));
	for (int kt1= 0; kt1<1000000; kt1+=10000) 
	{
        
        CString string;
        string.Format (("%d km"), (kt1/1000 ) );
        pDC->TextOut (kt1, -3000, string);
    }

	pDC->SetTextColor(RGB(0, 0, 0));
	for (int kt2= 1000; kt2<1000000; kt2+=10000) 
	{
        
        CString string;
        string.Format (("%dkm"), (((kt2-1000)/1000) ) );
        pDC->TextOut (-6000, kt2, string);
    }

	





	// TODO: add draw code for native data here
}

/////////////////////////////////////////////////////////////////////////////
// CMFC_TVer3View diagnostics

#ifdef _DEBUG
void CMFC_TVer3View::AssertValid() const
{
	CView::AssertValid();
}

void CMFC_TVer3View::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}

CMFC_TVer3Doc* CMFC_TVer3View::GetDocument() // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CMFC_TVer3Doc)));
	return (CMFC_TVer3Doc*)m_pDocument;
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CMFC_TVer3View message handlers
