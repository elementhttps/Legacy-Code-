// CPodaci.cpp : implementation file
//

#include "stdafx.h"
#include "MFC_TestN7.h"
#include "CPodaci.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CCPodaci dialog


CCPodaci::CCPodaci(CWnd* pParent /*=NULL*/)
	: CDialog(CCPodaci::IDD, pParent)
{
	//{{AFX_DATA_INIT(CCPodaci)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void CCPodaci::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CCPodaci)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CCPodaci, CDialog)
	//{{AFX_MSG_MAP(CCPodaci)
		// NOTE: the ClassWizard will add message map macros here
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CCPodaci message handlers
