#if !defined(AFX_CPODACI_H__C667C677_ED9B_4256_95E2_8FA8A7147657__INCLUDED_)
#define AFX_CPODACI_H__C667C677_ED9B_4256_95E2_8FA8A7147657__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// CPodaci.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CCPodaci dialog

class CCPodaci : public CDialog
{
// Construction
public:
	CCPodaci(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CCPodaci)
	enum { IDD = IDD_PODACI_DLG };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCPodaci)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CCPodaci)
		// NOTE: the ClassWizard will add member functions here
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CPODACI_H__C667C677_ED9B_4256_95E2_8FA8A7147657__INCLUDED_)
