// MFC_TestN7Doc.cpp : implementation of the CMFC_TestN7Doc class
//

#include "stdafx.h"
#include "MFC_TestN7.h"
#include "CPodaci.h"
#include "PANDA.h"
#include "MFC_TestN7Doc.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CMFC_TestN7Doc

IMPLEMENT_DYNCREATE(CMFC_TestN7Doc, CDocument)

BEGIN_MESSAGE_MAP(CMFC_TestN7Doc, CDocument)
	//{{AFX_MSG_MAP(CMFC_TestN7Doc)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CMFC_TestN7Doc construction/destruction

CMFC_TestN7Doc::CMFC_TestN7Doc()
{
	// TODO: add one-time construction code here

}

CMFC_TestN7Doc::~CMFC_TestN7Doc()
{
}

BOOL CMFC_TestN7Doc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;

	// TODO: add reinitialization code here
	// (SDI documents will reuse this document)

	return TRUE;
}



/////////////////////////////////////////////////////////////////////////////
// CMFC_TestN7Doc serialization

void CMFC_TestN7Doc::Serialize(CArchive& ar)
{
	if (ar.IsStoring())
	{
		// TODO: add storing code here
	}
	else
	{
		// TODO: add loading code here
	}
}

/////////////////////////////////////////////////////////////////////////////
// CMFC_TestN7Doc diagnostics

#ifdef _DEBUG
void CMFC_TestN7Doc::AssertValid() const
{
	CDocument::AssertValid();
}

void CMFC_TestN7Doc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CMFC_TestN7Doc commands
