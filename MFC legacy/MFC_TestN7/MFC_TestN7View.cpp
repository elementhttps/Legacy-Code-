// MFC_TestN7View.cpp : implementation of the CMFC_TestN7View class
//

#include "stdafx.h"
#include "MFC_TestN7.h"
#include "CPodaci.h"
#include "MFC_TestN7Doc.h"
#include "MFC_TestN7View.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CMFC_TestN7View

IMPLEMENT_DYNCREATE(CMFC_TestN7View, CView)

BEGIN_MESSAGE_MAP(CMFC_TestN7View, CView)
	//{{AFX_MSG_MAP(CMFC_TestN7View)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG_MAP
	// Standard printing commands
	ON_COMMAND(ID_FILE_PRINT, CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_DIRECT, CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_PREVIEW, CView::OnFilePrintPreview)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CMFC_TestN7View construction/destruction

CMFC_TestN7View::CMFC_TestN7View()
{
	// TODO: add construction code here

}

CMFC_TestN7View::~CMFC_TestN7View()
{
}

BOOL CMFC_TestN7View::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	return CView::PreCreateWindow(cs);
}

/////////////////////////////////////////////////////////////////////////////
// CMFC_TestN7View drawing

void CMFC_TestN7View::OnDraw(CDC* pDC)
{
	CMFC_TestN7Doc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);
	// TODO: add draw code for native data here
}

/////////////////////////////////////////////////////////////////////////////
// CMFC_TestN7View printing

BOOL CMFC_TestN7View::OnPreparePrinting(CPrintInfo* pInfo)
{
	// default preparation
	return DoPreparePrinting(pInfo);
}

void CMFC_TestN7View::OnBeginPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: add extra initialization before printing
}

void CMFC_TestN7View::OnEndPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: add cleanup after printing
}

/////////////////////////////////////////////////////////////////////////////
// CMFC_TestN7View diagnostics

#ifdef _DEBUG
void CMFC_TestN7View::AssertValid() const
{
	CView::AssertValid();
}

void CMFC_TestN7View::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}

CMFC_TestN7Doc* CMFC_TestN7View::GetDocument() // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CMFC_TestN7Doc)));
	return (CMFC_TestN7Doc*)m_pDocument;
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CMFC_TestN7View message handlers
