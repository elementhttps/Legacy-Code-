#if !defined(AFX_PANDA_H__562CEF02_628F_449D_B94D_577A82AC31C5__INCLUDED_)
#define AFX_PANDA_H__562CEF02_628F_449D_B94D_577A82AC31C5__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// PANDA.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CPANDA dialog

class CPANDA : public CDialog
{
// Construction
public:
	CPANDA(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CPANDA)
	enum { IDD = IDD_DIALOG1 };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CPANDA)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CPANDA)
		// NOTE: the ClassWizard will add member functions here
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_PANDA_H__562CEF02_628F_449D_B94D_577A82AC31C5__INCLUDED_)
