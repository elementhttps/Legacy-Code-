// MFC_ZOOM_TEST.h : main header file for the MFC_ZOOM_TEST application
//

#if !defined(AFX_MFC_ZOOM_TEST_H__08287213_4B2C_4394_BB00_56A7798B032B__INCLUDED_)
#define AFX_MFC_ZOOM_TEST_H__08287213_4B2C_4394_BB00_56A7798B032B__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"       // main symbols

/////////////////////////////////////////////////////////////////////////////
// CMFC_ZOOM_TESTApp:
// See MFC_ZOOM_TEST.cpp for the implementation of this class
//

class CMFC_ZOOM_TESTApp : public CWinApp
{
public:
	CMFC_ZOOM_TESTApp();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMFC_ZOOM_TESTApp)
	public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL

// Implementation
	//{{AFX_MSG(CMFC_ZOOM_TESTApp)
	afx_msg void OnAppAbout();
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_MFC_ZOOM_TEST_H__08287213_4B2C_4394_BB00_56A7798B032B__INCLUDED_)
