// MFC_ZOOM_TESTDoc.cpp : implementation of the CMFC_ZOOM_TESTDoc class
//

#include "stdafx.h"
#include "MFC_ZOOM_TEST.h"

#include "MFC_ZOOM_TESTDoc.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CMFC_ZOOM_TESTDoc

IMPLEMENT_DYNCREATE(CMFC_ZOOM_TESTDoc, CDocument)

BEGIN_MESSAGE_MAP(CMFC_ZOOM_TESTDoc, CDocument)
	//{{AFX_MSG_MAP(CMFC_ZOOM_TESTDoc)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CMFC_ZOOM_TESTDoc construction/destruction

CMFC_ZOOM_TESTDoc::CMFC_ZOOM_TESTDoc()
{
	// TODO: add one-time construction code here

}

CMFC_ZOOM_TESTDoc::~CMFC_ZOOM_TESTDoc()
{
}

BOOL CMFC_ZOOM_TESTDoc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;

	// TODO: add reinitialization code here
	// (SDI documents will reuse this document)

	return TRUE;
}



/////////////////////////////////////////////////////////////////////////////
// CMFC_ZOOM_TESTDoc serialization

void CMFC_ZOOM_TESTDoc::Serialize(CArchive& ar)
{
	if (ar.IsStoring())
	{
		// TODO: add storing code here
	}
	else
	{
		// TODO: add loading code here
	}
}

/////////////////////////////////////////////////////////////////////////////
// CMFC_ZOOM_TESTDoc diagnostics

#ifdef _DEBUG
void CMFC_ZOOM_TESTDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CMFC_ZOOM_TESTDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CMFC_ZOOM_TESTDoc commands
