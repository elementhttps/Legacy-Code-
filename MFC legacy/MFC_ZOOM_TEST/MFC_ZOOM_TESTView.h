// MFC_ZOOM_TESTView.h : interface of the CMFC_ZOOM_TESTView class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_MFC_ZOOM_TESTVIEW_H__93107FCF_A877_4A9E_86CC_FC3EB73BBD43__INCLUDED_)
#define AFX_MFC_ZOOM_TESTVIEW_H__93107FCF_A877_4A9E_86CC_FC3EB73BBD43__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


class CMFC_ZOOM_TESTView : public CView
{
protected: // create from serialization only
	CMFC_ZOOM_TESTView();
	DECLARE_DYNCREATE(CMFC_ZOOM_TESTView)

// Attributes
public:
	CMFC_ZOOM_TESTDoc* GetDocument();
	BOOL m_bSelectMode;
	CRect m_rubberBand;
	CPoint m_ptStart;


// Operations
public:

	
// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMFC_ZOOM_TESTView)
	public:
	virtual void OnDraw(CDC* pDC);  // overridden to draw this view
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	protected:
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CMFC_ZOOM_TESTView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	//{{AFX_MSG(CMFC_ZOOM_TESTView)
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

#ifndef _DEBUG  // debug version in MFC_ZOOM_TESTView.cpp
inline CMFC_ZOOM_TESTDoc* CMFC_ZOOM_TESTView::GetDocument()
   { return (CMFC_ZOOM_TESTDoc*)m_pDocument; }
#endif

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_MFC_ZOOM_TESTVIEW_H__93107FCF_A877_4A9E_86CC_FC3EB73BBD43__INCLUDED_)
