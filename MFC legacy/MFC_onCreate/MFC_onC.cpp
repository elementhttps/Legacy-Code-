
#include <afxwin.h>

class CMainFrame : public CFrameWnd
{
public:
	CMainFrame (); 

protected:
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);//message identified as ON_WM_CREATE. Its syntax is this afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);This message calls the CFrameWnd::Create() method to create a window
	DECLARE_MESSAGE_MAP()                               //OnCreate
};

CMainFrame::CMainFrame()
{
	// Create the window's frame
	Create(NULL, "Aplikacija", WS_OVERLAPPEDWINDOW,// naziv na tabli Aplikacija
	       CRect(120, 100, 700, 480), NULL);// dimenzije
}

class C_App2: public CWinApp      // naziv C_App2
{
public:
	BOOL InitInstance();
};






BEGIN_MESSAGE_MAP(CMainFrame, CFrameWnd)
	ON_WM_CREATE()
END_MESSAGE_MAP()

int CMainFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)   //CreateStruct //OnCreate
{
	// Call the base class to create the window
	if( CFrameWnd::OnCreate(lpCreateStruct) == 0)   //CreateStruct //OnCreate
	{
		// If the window was successfully created, let the user know
		MessageBox("Mnogo malo prozorce za win aplikaciju!!!");
                  // Since the window was successfully created, return 0
		return 0;
	}
	// Otherwise, return -1
	return -1;
}

BOOL C_App2::InitInstance()// uslov BOOL C_App2
{
	m_pMainWnd = new CMainFrame ;
	m_pMainWnd->ShowWindow(SW_SHOW);
	m_pMainWnd->UpdateWindow();

	return TRUE;
}



C_App2 theApp;  // naziv C_App2
