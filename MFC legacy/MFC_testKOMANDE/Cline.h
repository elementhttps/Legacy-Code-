#if !defined(AFX_CLINE_H__CD74D64A_16CE_48E7_8C43_EA752E8EF343__INCLUDED_)
#define AFX_CLINE_H__CD74D64A_16CE_48E7_8C43_EA752E8EF343__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// Cline.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CCline view

class CCline : public CView
{
protected:
	CCline();           // protected constructor used by dynamic creation
	DECLARE_DYNCREATE(CCline)

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCline)
	protected:
	virtual void OnDraw(CDC* pDC);      // overridden to draw this view
	//}}AFX_VIRTUAL

// Implementation
protected:
	virtual ~CCline();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

	// Generated message map functions
protected:
	//{{AFX_MSG(CCline)
		// NOTE - the ClassWizard will add and remove member functions here.
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CLINE_H__CD74D64A_16CE_48E7_8C43_EA752E8EF343__INCLUDED_)
