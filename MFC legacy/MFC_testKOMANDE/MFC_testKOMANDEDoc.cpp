// MFC_testKOMANDEDoc.cpp : implementation of the CMFC_testKOMANDEDoc class
//

#include "stdafx.h"
#include "MFC_testKOMANDE.h"

#include "MFC_testKOMANDEDoc.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CMFC_testKOMANDEDoc

IMPLEMENT_DYNCREATE(CMFC_testKOMANDEDoc, CDocument)

BEGIN_MESSAGE_MAP(CMFC_testKOMANDEDoc, CDocument)
	//{{AFX_MSG_MAP(CMFC_testKOMANDEDoc)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CMFC_testKOMANDEDoc construction/destruction

CMFC_testKOMANDEDoc::CMFC_testKOMANDEDoc()
{
	// TODO: add one-time construction code here

}

CMFC_testKOMANDEDoc::~CMFC_testKOMANDEDoc()
{
}

BOOL CMFC_testKOMANDEDoc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;

	// TODO: add reinitialization code here
	// (SDI documents will reuse this document)

	return TRUE;
}



/////////////////////////////////////////////////////////////////////////////
// CMFC_testKOMANDEDoc serialization

void CMFC_testKOMANDEDoc::Serialize(CArchive& ar)
{
	if (ar.IsStoring())
	{
		// TODO: add storing code here
	}
	else
	{
		// TODO: add loading code here
	}
}

/////////////////////////////////////////////////////////////////////////////
// CMFC_testKOMANDEDoc diagnostics

#ifdef _DEBUG
void CMFC_testKOMANDEDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CMFC_testKOMANDEDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CMFC_testKOMANDEDoc commands
