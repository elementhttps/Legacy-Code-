// MFC_testKOMANDEDoc.h : interface of the CMFC_testKOMANDEDoc class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_MFC_TESTKOMANDEDOC_H__2F6A62DC_7973_4FEB_8F87_8CC3F7641CB0__INCLUDED_)
#define AFX_MFC_TESTKOMANDEDOC_H__2F6A62DC_7973_4FEB_8F87_8CC3F7641CB0__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


class CMFC_testKOMANDEDoc : public CDocument
{
protected: // create from serialization only
	CMFC_testKOMANDEDoc();
	DECLARE_DYNCREATE(CMFC_testKOMANDEDoc)

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMFC_testKOMANDEDoc)
	public:
	virtual BOOL OnNewDocument();
	virtual void Serialize(CArchive& ar);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CMFC_testKOMANDEDoc();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	//{{AFX_MSG(CMFC_testKOMANDEDoc)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_MFC_TESTKOMANDEDOC_H__2F6A62DC_7973_4FEB_8F87_8CC3F7641CB0__INCLUDED_)
