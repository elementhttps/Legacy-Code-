// MFC_testN8.h : main header file for the MFC_TESTN8 application
//

#if !defined(AFX_MFC_TESTN8_H__CB170CB6_F63E_4754_9B84_1FC846CFF858__INCLUDED_)
#define AFX_MFC_TESTN8_H__CB170CB6_F63E_4754_9B84_1FC846CFF858__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"       // main symbols

/////////////////////////////////////////////////////////////////////////////
// CMFC_testN8App:
// See MFC_testN8.cpp for the implementation of this class
//

class CMFC_testN8App : public CWinApp
{
public:
	CMFC_testN8App();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMFC_testN8App)
	public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL

// Implementation
	//{{AFX_MSG(CMFC_testN8App)
	afx_msg void OnAppAbout();
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_MFC_TESTN8_H__CB170CB6_F63E_4754_9B84_1FC846CFF858__INCLUDED_)
