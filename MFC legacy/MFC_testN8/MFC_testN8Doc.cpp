// MFC_testN8Doc.cpp : implementation of the CMFC_testN8Doc class
//

#include "stdafx.h"
#include "MFC_testN8.h"

#include "MFC_testN8Doc.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CMFC_testN8Doc

IMPLEMENT_DYNCREATE(CMFC_testN8Doc, CDocument)

BEGIN_MESSAGE_MAP(CMFC_testN8Doc, CDocument)
	//{{AFX_MSG_MAP(CMFC_testN8Doc)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CMFC_testN8Doc construction/destruction

CMFC_testN8Doc::CMFC_testN8Doc()
{
	// TODO: add one-time construction code here

}

CMFC_testN8Doc::~CMFC_testN8Doc()
{
}

BOOL CMFC_testN8Doc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;

	// TODO: add reinitialization code here
	// (SDI documents will reuse this document)

	return TRUE;
}



/////////////////////////////////////////////////////////////////////////////
// CMFC_testN8Doc serialization

void CMFC_testN8Doc::Serialize(CArchive& ar)
{
	if (ar.IsStoring())
	{
		// TODO: add storing code here
	}
	else
	{
		// TODO: add loading code here
	}
}

/////////////////////////////////////////////////////////////////////////////
// CMFC_testN8Doc diagnostics

#ifdef _DEBUG
void CMFC_testN8Doc::AssertValid() const
{
	CDocument::AssertValid();
}

void CMFC_testN8Doc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CMFC_testN8Doc commands
