// MFC_testN8Doc.h : interface of the CMFC_testN8Doc class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_MFC_TESTN8DOC_H__0593F4D9_9843_40FA_B0A3_E9A5C785979B__INCLUDED_)
#define AFX_MFC_TESTN8DOC_H__0593F4D9_9843_40FA_B0A3_E9A5C785979B__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


class CMFC_testN8Doc : public CDocument
{
protected: // create from serialization only
	CMFC_testN8Doc();
	DECLARE_DYNCREATE(CMFC_testN8Doc)

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMFC_testN8Doc)
	public:
	virtual BOOL OnNewDocument();
	virtual void Serialize(CArchive& ar);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CMFC_testN8Doc();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	//{{AFX_MSG(CMFC_testN8Doc)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_MFC_TESTN8DOC_H__0593F4D9_9843_40FA_B0A3_E9A5C785979B__INCLUDED_)
