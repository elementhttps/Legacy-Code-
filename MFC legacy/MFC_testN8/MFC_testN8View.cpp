// MFC_testN8View.cpp : implementation of the CMFC_testN8View class
//

#include "stdafx.h"
#include "MFC_testN8.h"

#include "MFC_testN8Doc.h"
#include "MFC_testN8View.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CMFC_testN8View

IMPLEMENT_DYNCREATE(CMFC_testN8View, CView)

BEGIN_MESSAGE_MAP(CMFC_testN8View, CView)
	//{{AFX_MSG_MAP(CMFC_testN8View)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CMFC_testN8View construction/destruction

CMFC_testN8View::CMFC_testN8View()
{
	// TODO: add construction code here

}

CMFC_testN8View::~CMFC_testN8View()
{
}

BOOL CMFC_testN8View::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	return CView::PreCreateWindow(cs);
}

/////////////////////////////////////////////////////////////////////////////
// CMFC_testN8View drawing

void CMFC_testN8View::OnDraw(CDC* pDC)
{
	CMFC_testN8Doc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);
	// TODO: add draw code for native data here
}

/////////////////////////////////////////////////////////////////////////////
// CMFC_testN8View diagnostics

#ifdef _DEBUG
void CMFC_testN8View::AssertValid() const
{
	CView::AssertValid();
}

void CMFC_testN8View::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}

CMFC_testN8Doc* CMFC_testN8View::GetDocument() // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CMFC_testN8Doc)));
	return (CMFC_testN8Doc*)m_pDocument;
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CMFC_testN8View message handlers
