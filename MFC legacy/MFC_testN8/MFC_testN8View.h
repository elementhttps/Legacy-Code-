// MFC_testN8View.h : interface of the CMFC_testN8View class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_MFC_TESTN8VIEW_H__519F7920_BDCA_4E4B_8C36_2026C5A00591__INCLUDED_)
#define AFX_MFC_TESTN8VIEW_H__519F7920_BDCA_4E4B_8C36_2026C5A00591__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


class CMFC_testN8View : public CView
{
protected: // create from serialization only
	CMFC_testN8View();
	DECLARE_DYNCREATE(CMFC_testN8View)

// Attributes
public:
	CMFC_testN8Doc* GetDocument();

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMFC_testN8View)
	public:
	virtual void OnDraw(CDC* pDC);  // overridden to draw this view
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	protected:
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CMFC_testN8View();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	//{{AFX_MSG(CMFC_testN8View)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

#ifndef _DEBUG  // debug version in MFC_testN8View.cpp
inline CMFC_testN8Doc* CMFC_testN8View::GetDocument()
   { return (CMFC_testN8Doc*)m_pDocument; }
#endif

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_MFC_TESTN8VIEW_H__519F7920_BDCA_4E4B_8C36_2026C5A00591__INCLUDED_)
