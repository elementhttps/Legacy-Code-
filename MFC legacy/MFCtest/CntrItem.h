// CntrItem.h : interface of the CMFCtestCntrItem class
//

#if !defined(AFX_CNTRITEM_H__69EA61A2_B754_404C_8AD9_755CE2857520__INCLUDED_)
#define AFX_CNTRITEM_H__69EA61A2_B754_404C_8AD9_755CE2857520__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class CMFCtestDoc;
class CMFCtestView;

class CMFCtestCntrItem : public COleClientItem
{
	DECLARE_SERIAL(CMFCtestCntrItem)

// Constructors
public:
	CMFCtestCntrItem(CMFCtestDoc* pContainer = NULL);
		// Note: pContainer is allowed to be NULL to enable IMPLEMENT_SERIALIZE.
		//  IMPLEMENT_SERIALIZE requires the class have a constructor with
		//  zero arguments.  Normally, OLE items are constructed with a
		//  non-NULL document pointer.

// Attributes
public:
	CMFCtestDoc* GetDocument()
		{ return (CMFCtestDoc*)COleClientItem::GetDocument(); }
	CMFCtestView* GetActiveView()
		{ return (CMFCtestView*)COleClientItem::GetActiveView(); }

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMFCtestCntrItem)
	public:
	virtual void OnChange(OLE_NOTIFICATION wNotification, DWORD dwParam);
	virtual void OnActivate();
	protected:
	virtual void OnGetItemPosition(CRect& rPosition);
	virtual void OnDeactivateUI(BOOL bUndoable);
	virtual BOOL OnChangeItemPosition(const CRect& rectPos);
	//}}AFX_VIRTUAL

// Implementation
public:
	~CMFCtestCntrItem();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif
	virtual void Serialize(CArchive& ar);
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CNTRITEM_H__69EA61A2_B754_404C_8AD9_755CE2857520__INCLUDED_)
