@echo off
REM -- First make map file from Microsoft Visual C++ generated resource.h
echo // MAKEHELP.BAT generated Help Map file.  Used by MFCTEST.HPJ. >"hlp\MFCtest.hm"
echo. >>"hlp\MFCtest.hm"
echo // Commands (ID_* and IDM_*) >>"hlp\MFCtest.hm"
makehm ID_,HID_,0x10000 IDM_,HIDM_,0x10000 resource.h >>"hlp\MFCtest.hm"
echo. >>"hlp\MFCtest.hm"
echo // Prompts (IDP_*) >>"hlp\MFCtest.hm"
makehm IDP_,HIDP_,0x30000 resource.h >>"hlp\MFCtest.hm"
echo. >>"hlp\MFCtest.hm"
echo // Resources (IDR_*) >>"hlp\MFCtest.hm"
makehm IDR_,HIDR_,0x20000 resource.h >>"hlp\MFCtest.hm"
echo. >>"hlp\MFCtest.hm"
echo // Dialogs (IDD_*) >>"hlp\MFCtest.hm"
makehm IDD_,HIDD_,0x20000 resource.h >>"hlp\MFCtest.hm"
echo. >>"hlp\MFCtest.hm"
echo // Frame Controls (IDW_*) >>"hlp\MFCtest.hm"
makehm IDW_,HIDW_,0x50000 resource.h >>"hlp\MFCtest.hm"
REM -- Make help for Project MFCTEST


echo Building Win32 Help files
start /wait hcw /C /E /M "hlp\MFCtest.hpj"
if errorlevel 1 goto :Error
if not exist "hlp\MFCtest.hlp" goto :Error
if not exist "hlp\MFCtest.cnt" goto :Error
echo.
if exist Debug\nul copy "hlp\MFCtest.hlp" Debug
if exist Debug\nul copy "hlp\MFCtest.cnt" Debug
if exist Release\nul copy "hlp\MFCtest.hlp" Release
if exist Release\nul copy "hlp\MFCtest.cnt" Release
echo.
goto :done

:Error
echo hlp\MFCtest.hpj(1) : error: Problem encountered creating help file

:done
echo.
