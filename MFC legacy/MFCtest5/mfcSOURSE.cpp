
#include <afxwin.h>

class C_App : public CWinApp              //Klasa bilo kog naziva (C_App)
{
public:
	virtual BOOL InitInstance();          // Konstruktor virtual
};





class C_osnovna_klasa : public CFrameWnd        // Klasa bilo kog naziva (C_osnovna_klasa)
{
public:
	C_osnovna_klasa();                          //Funkcija u C_osnovna_klas -i
};



C_osnovna_klasa::C_osnovna_klasa()              // poziv funkcije u osnovnoj klasi
{
	Create(NULL, "Osnove MFC");
}

BOOL C_App::InitInstance()
{
	m_pMainWnd = new C_osnovna_klasa;
	m_pMainWnd->ShowWindow(SW_NORMAL);

	return TRUE;
}

C_App theApp;

/*

 1 Start Microsoft Visual C++ or Visual Studio

 2 On the main menu, click either File -> New... or File -> New Project...

 3 In the New dialog box, click Projects or, in the New Project dialog box, click Visual C++ Projects

 4 Click either Win32 Application or Win32 Project

 5 Type a name for the application in the Name edit box. An example would be mfcfundamentals1

 6 Click OK

 7 Specify that you want to create a Windows Application as an Empty Project and click Finish

 8 To use MFC,

  in MSVC 6, click Project -> Settings...

  In MSVC 7, in the Solutions Explorer property page, right-click the project name (mfcfundamentals1) and click Properties

 9 In the Microsoft Foundation Classes combo box or in the Use of MFC combo box, select Use MFC In A Shared DLL

 10 Click OK

 11 To add a file to create the application, on the main menu of MSVC 6, click File -> New... or, for MSVC 7, on the main menu, click Project -> Add New Item...

 12 Click C++ (Source) File and, in the Name edit box, type a name for the file. An example would be Exercise

 13 Click OK or Open

 14 In the empty file, type the above code:

----------------------------------------------------------------------------------------------------------------------------------
The fundamental classes of MFC are declared in the afxwin.h header file
----------------------------------------------------------------------------------------------------------------------------------



----------------------------------------------------------------------------------------------------------------------------------
After creating the application, to make it available to other parts of the program, 
you must declare a global variable of your class. 
It is usually called theApp but you can call it anything you want.

class CExerciseApp : public CWinApp
----------------------------------------------------------------------------------------------------------------------------------



----------------------------------------------------------------------------------------------------------------------------------
To create a frame, the MFC library provides various classes. 
One of these is called CFrameWnd and it is the most commonly used frame class. 
To use a frame, you can derive your own class from CFrameWnd as follows:

  
class CApplicationFrame : public CFrameWnd
{
};
----------------------------------------------------------------------------------------------------------------------------------



----------------------------------------------------------------------------------------------------------------------------------
For now, a minimum frame can be created by simply passing the class name as NULL 
and the window name with a null-terminated string. Here is an example:

  
	
	  
class CMainFrame : public CFrameWnd
{
public:
	CMainFrame();
};

CMainFrame::CMainFrame()
{
	Create(NULL, "MFC Fundamentals");
}
------------------------------------------------------------------------------------------------------------------------------------



------------------------------------------------------------------------------------------------------------------------------------
CWinThread is the base class of CWinApp and therefore makes m_pMainWnd available to any CWinThread 
derived class such as CFrameWnd. Based on this, to create a thread for the main window to display, 
you can assign a pointer of your frame class to m_pMainWnd. 
After this assignment, m_pMainWnd can be used as the window object to display the frame, 
which is usually done by calling the ShowWindow() method. This would be done as follows:

  
	
	  
BOOL CExerciseApp::InitInstance()
{
	m_pMainWnd = new CMainFrame;
	m_pMainWnd->ShowWindow(SW_NORMAL);

	return TRUE;
}

----------------------------------------------------------------------------------------------------------------------------------




*/


