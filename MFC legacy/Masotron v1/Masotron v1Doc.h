// Masotron v1Doc.h : interface of the CMasotronv1Doc class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_MASOTRONV1DOC_H__300C9C23_DC4F_448C_8665_0E959E7D3035__INCLUDED_)
#define AFX_MASOTRONV1DOC_H__300C9C23_DC4F_448C_8665_0E959E7D3035__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


class CMasotronv1Doc : public CDocument
{
protected: // create from serialization only
	CMasotronv1Doc();
	DECLARE_DYNCREATE(CMasotronv1Doc)

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMasotronv1Doc)
	public:
	virtual BOOL OnNewDocument();
	virtual void Serialize(CArchive& ar);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CMasotronv1Doc();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	//{{AFX_MSG(CMasotronv1Doc)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_MASOTRONV1DOC_H__300C9C23_DC4F_448C_8665_0E959E7D3035__INCLUDED_)
