// Masotron v1View.cpp : implementation of the CMasotronv1View class
//

#include "stdafx.h"
#include "Masotron v1.h"
#include "Math.h"

#include "Masotron v1Doc.h"
#include "Masotron v1View.h"




#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CMasotronv1View

IMPLEMENT_DYNCREATE(CMasotronv1View, CView)

BEGIN_MESSAGE_MAP(CMasotronv1View, CView)
	//{{AFX_MSG_MAP(CMasotronv1View)
	ON_WM_KEYDOWN()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CMasotronv1View construction/destruction

CMasotronv1View::CMasotronv1View()
{

	m_Zumx = 1000;
	m_Zumy = 1000;
	alfa1 = 10;
	dugao = 90;
	dugao2 =45;
	dugao3 =45;


}

CMasotronv1View::~CMasotronv1View()
{
}

BOOL CMasotronv1View::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	return CView::PreCreateWindow(cs);
}

/////////////////////////////////////////////////////////////////////////////
// CMasotronv1View drawing

void CMasotronv1View::OnDraw(CDC* pDC)
{
	//CMasotronv1Doc* pDoc = GetDocument();
	//ASSERT_VALID(pDoc);
	


	CClientDC;
	pDC->SetMapMode(MM_ANISOTROPIC);
	pDC->SetViewportOrg(90, 620); //90 -620
	pDC->SetWindowExt(m_Zumx,m_Zumy); //SetWindowExtent(100,100)  SetViewportExtent(200,200)  100 jedinica = 200 pixela
	                                                                                             //1 m = 2 pixela
	
	pDC->SetViewportExt(1000,-1000);
	




//------------------------------------------------------------------------------------------------------------
//OSE 2D KOS
//------------------------------------------------------------------------------------------------------------


CPen PenBlack1(PS_SOLID,1,RGB(0,0,0));
pDC->SelectObject(PenBlack1);

	pDC->MoveTo(-1000, 0);
	pDC->LineTo( 200000,0);

	pDC->MoveTo(   0, -1000);
	pDC->LineTo(   0,  200000);

//------------------------------------------------------------------------------------------------------------
//UKLESTENJE
//------------------------------------------------------------------------------------------------------------


//---------------
//PRAVE
CPen PenBlack4(PS_SOLID,4,RGB(0,0,0));
pDC->SelectObject(PenBlack4);
/*

    y 0sa
   |
   |
   | *---*
   | x1  x2    * y2   
   |           |
   |           * y1
   |        
   *------------- x 0sa

   
	VERTIKALA X= const


    HORIZONTALA Y= const



*/


int x1 = 0;    int y1 = 200;  // ako x = y = 0 => koordinatni pocetak    

int x2 = 0;    int y2 = 100+y1;//visina ukljestenja  y1 = 100 y2 = 200


    pDC->MoveTo( x1, y1);  //tacka 1. na x = ...,y = ... (x,y);
	pDC->LineTo( x2, y2);   //tacka 2.  na x1 i y1        (x1,y1);

	  // duzina x+x1 pri y=y1=0

//---------------------
//KOSE LINIJE
//------------------------------------------------------------------------------------------------------------



//------------------------------------------------------------------------------------------------------------
/*

  y osa
  |
  |
  |        *        x = const
  |       /|
  |       /|
  |        *
  |  
  | 
  *------------- x osa






// bitan uslov !!!!
//  yk2 = xk1 + xk2
*/
//------------------------------------------------------------------------------------------------------------

pDC->SelectObject(PenBlack1);
int xk1 = 0;     int yk1 = y1;

int xk2 = -50;     int yk2 = yk1 + xk2;


for (int i = 0; i < 120; i +=20)
{
    pDC->MoveTo( xk1, yk1+i);  
	pDC->LineTo( xk2, yk2+i);


}

//-------------------
//centar
//------------------------------------------------------------------------------------------------------------
CPen PenBlue1(PS_SOLID,2,RGB(0,0,255));
CPen PenRed1(PS_SOLID,1,RGB(255,0,0));
pDC->SelectObject(PenRed1);
CBrush BrushRed (RGB (255, 0, 0));
CBrush BrushBlue (RGB (0, 0, 255));

/*
int x1 = 100;   
int y1 = 200;
     
int x2 = 100;   
int y2 = 300;


  x1 = x2 = 100; 

  (y1+y2)/2 = (200+300)/2 = 250


  1. tacka = 100 = n
  2. tacka = 250 = k

na osnovu pravougaonika i nivelacije mera sledi

  x1 = n-2  y1 = k-3
  x2 = n+3  y2 = k+2


*/
//pDC->Rectangle(98,247,103,252); 

//pDC->Ellipse(x1-2,((y1+y2)/2)-3,x1+3,((y1+y2)/2)+2); uvek na kraju

//------------------------------------------------------------------------------------------------------------

double duzg1 = 250;  // 300
double duzg2 = 300;   //200      za vrednosti 500 , 500   umanjenje -50  dpx
double duzg3 = 300; //100

//---------------
//POMOCNE LINIJE
//------------------------------------------------------------------------------------------------------------
pDC->SelectObject(PenBlack1);
 pDC->MoveTo( 0,(y1+y2)/2 );  
 pDC->LineTo( x2,(y1+y2)/2 );


//------------------------------------------------------------------------------------------------------------
//KRUG ROTACIJE
//------------------------------------------------------------------------------------------------------------


int c1 = 100;

//Ellipse(int x1, int y1, int x2, int y2);
pDC->SelectObject(PenBlack1);
pDC->Ellipse(duzg1-duzg2,(((y1+y2)/2)-duzg2),(duzg2+duzg1),((y1+y2)/2)+duzg2); 
//pDC->Ellipse(0,-50,400,350);


//
//y1 = 100 y2= 200   
//(y1+y2)/2 = 300/2 = 150
//duzg2 = 200
//duzg2/2 = 200/2 = 100


//200-200,150-200,200+200, 150 +


//------------------------------------------------------------------------------------------------------------
//GRAFICKI PRIKAZ LINIJE [ GREDA 1.]
//------------------------------------------------------------------------------------------------------------

CPen PenBlack3(PS_SOLID,3,RGB(0,0,0));

pDC->SelectObject(PenBlack3);

pDC->MoveTo( x2,(y1+y2)/2 );  
pDC->LineTo( duzg1,(y1+y2)/2 );


//------------------------------------------------------------------------------------------------------------
//GRAFICKI PRIKAZ LINIJE [ GREDA 2.]
//------------------------------------------------------------------------------------------------------------

pDC->SelectObject(PenBlack3);
//*
//double alfa1 = 0;
double PI = 3.14159265359;

//alfa1 = 90;

//sin 0 = 0
//cos 0 = 1

//pDC->MoveTo( duzg1,(y1+y2)/2 );  
//pDC->LineTo( duzg2+duzg1,(y1+y2)/2 );

pDC->MoveTo( duzg1,(y1+y2)/2 );  
pDC->LineTo( duzg1+(duzg2*cos((PI*alfa1)/180)),((y1+y2)/2)+(duzg2*sin((PI*alfa1)/180)));

//------------------------------------------------------------------------------------------------------------
//SPOJNA GREDA 1. I 2. GREDE
//------------------------------------------------------------------------------------------------------------
pDC->MoveTo(duzg1/2,(y1+y2)/2);
pDC->LineTo((duzg1+(duzg2*cos((PI*alfa1)/180))/2),((y1+y2)/2)+(duzg2*sin((PI*alfa1)/180)/2));


//------------------------------------------------------------------------------------------------------------
//PRIKAZ MREZE X Y
//------------------------------------------------------------------------------------------------------------
pDC->SelectObject(PenBlack1);
for(int koox2 = 0; koox2 < 1000; koox2 += 50)
    {
        pDC->MoveTo(koox2, 0);
        pDC->LineTo(koox2, 5000);
	

    }

for(int kooy2 = 0; kooy2 < 1000; kooy2 += 50)
    {
        pDC->MoveTo(0, kooy2);
        pDC->LineTo(5000, kooy2);
	
    }


//-----------------
//CENTRI MASA GREDA GRAFIKA BRUSH RED
//------------------------------------------------------------------------------------------------------------
pDC->SelectObject(BrushRed);

CPen PenRed3(PS_SOLID,3,RGB(255,0,0));
pDC->SelectObject(PenRed3);


//------------------------------------------------------------------------------------------------------------
//UKLJESTENJE PRIKAZ CENTRA
//------------------------------------------------------------------------------------------------------------
pDC->Ellipse(x1-2,((y1+y2)/2)-3,x1+3,((y1+y2)/2)+2);


//------------------------------------------------------------------------------------------------------------
//centar mase grede 1 duzine duzg1  //EEEEEEEEEE// x1 i y1
//------------------------------------------------------------------------------------------------------------


double xcg1;
double ycg1;

xcg1 = ((x2+duzg1)/2);
ycg1 = ((y1+y2)/2);


pDC->Ellipse(xcg1-2,ycg1-3,xcg1+3,ycg1+2);


//------------------------------------------------------------------------------------------------------------
//centar mase grede 2 duzine duzg2 i kretanje u zavisnosti od koordinata   - //EEEEEEEEEE// x2 i y2
//------------------------------------------------------------------------------------------------------------


double xcg2;
double ycg2;

xcg2 = (duzg1+(duzg2*cos((PI*alfa1)/180))/2);

ycg2 = ((y1+y2)/2)+(duzg2*sin((PI*alfa1)/180)/2);


pDC->Ellipse( xcg2-2,ycg2-3,xcg2+3,ycg2+2);


//------------------------------------------------------------------------------------------------------------
//CENTAR I JEDNACINA KRETANJA TACKE SPOJNE GREDE
//------------------------------------------------------------------------------------------------------------

double dsin;
double dcos;


dsin =sin((PI*alfa1)/180);
dcos =cos((PI*alfa1)/180);


/*
//1 GREDA CENTAR MASE
//   X     ((x2+duzg1)/2)
//   Y     ((y1+y2)/2)

//2 GREDA CENTAR MASE
//   X     (duzg1+(duzg2*cos((PI*alfa1)/180))/2)
//   Y     ((y1+y2)/2)+(duzg2*sin((PI*alfa1)/180)/2)


//1 GREDA KRAJNJA TACKA
//  X = duzg1
//  Y = (y1+y2)/2

*/

//------------------------------------------------------------------------------------------------------------
//GREDA 3
//------------------------------------------------------------------------------------------------------------

double dsing;
double dcosg;

double dxg3;
double dyg3;
double dgama;
dgama = dugao-alfa1;

double ktx2 =(duzg1+(duzg2*cos((PI*alfa1)/180))) ;
double kty2 =((y1+y2)/2)+(duzg2*sin((PI*alfa1)/180));


dsing =sin((PI*dgama)/180);
dcosg =cos((PI*dgama)/180);


//duzg3 = 200;

dxg3 = ktx2-(dcosg*duzg3);
dyg3 = kty2+(dsing*duzg3);

pDC->SelectObject(PenBlack3);

pDC->MoveTo(ktx2,kty2);
pDC->LineTo(dxg3,dyg3);



//CENTAR MASE GREDE 3
//------------------------------------------------------------------------------------------------------------
double xcg3;
double ycg3;

xcg3 = dxg3+(duzg3*cos((PI*dgama)/180)/2);
ycg3 = dyg3-(duzg3*sin((PI*dgama)/180)/2);

pDC->SelectObject(PenRed3);
pDC->Ellipse(xcg3-2,ycg3-3,xcg3+3,ycg3+2);

//------------------------------------------------------------------------------------------------------------
//GREDA 4
//------------------------------------------------------------------------------------------------------------
pDC->SelectObject(PenBlack3);
double duzg4;
double dsing4;
double dcosg4;
int dfi;
double dxg4;
double dyg4;

dfi = dugao3+dgama;

dsing4 =sin((PI*dfi)/180);
dcosg4=cos((PI*dfi)/180);

duzg4 = 100;

dxg4 = dxg3-(dcosg4*duzg4);
dyg4 = dyg3+(dsing4*duzg4);

pDC->MoveTo(dxg3,dyg3);
pDC->LineTo(dxg4,dyg4);


//CENTAR MASE GREDE 4
//------------------------------------------------------------------------------------------------------------

double xcg4;
double ycg4;

xcg4 = dxg4+(duzg4*cos((PI*dfi)/180)/2);
ycg4 = dyg4-(duzg4*sin((PI*dfi)/180)/2);

pDC->SelectObject(PenRed3);
pDC->Ellipse(xcg4-2,ycg4-3,xcg4+3,ycg4+2);






//------------------------------------------------------------------------------------------------------------
//GREDE 5 I 6
//------------------------------------------------------------------------------------------------------------
pDC->SelectObject(PenBlack3);
double duzg5;
double dsing5;
double dcosg5;
double dteta1;
double dxg5;
double dyg5;



dteta1 = 270+dugao2+dfi;//315 u nominalnom polozaju dteta1 =315             dugao2= 45

dsing5 =sin((PI*dteta1)/180);
dcosg5 =cos((PI*dteta1)/180);

duzg5 = 50; //ISTO STO I duzg6

dxg5 = dxg4-(dcosg5*duzg5);
dyg5 = dyg4+(dsing5*duzg5);

pDC->MoveTo(dxg4,dyg4);
pDC->LineTo(dxg5,dyg5);



double dsing6;
double dcosg6;
int dteta2;
double dxg6;
double dyg6;

dteta2 = 90-dugao2+dfi; // 90-...za zahvat 

dsing6 =sin((PI*dteta2)/180);
dcosg6 =cos((PI*dteta2)/180);



dxg6 = dxg4-(dcosg6*duzg5);
dyg6 = dyg4+(dsing6*duzg5);

pDC->MoveTo(dxg4,dyg4);
pDC->LineTo(dxg6,dyg6);


//CENTAR MASE GREDE 5 i 6
//------------------------------------------------------------------------------------------------------------

double xcg5;
double ycg5;

xcg5 = dxg4-(duzg5*cos((PI*dteta1)/180)/2);
ycg5 = dyg4+(duzg5*sin((PI*dteta1)/180)/2);

pDC->SelectObject(PenRed3);
pDC->Ellipse(xcg5-2,ycg5-3,xcg5+3,ycg5+2);

double xcg6;
double ycg6;

xcg6 = dxg4-(duzg5*cos((PI*dteta2)/180)/2);
ycg6 = dyg4+(duzg5*sin((PI*dteta2)/180)/2);

pDC->SelectObject(PenRed3);
pDC->Ellipse(xcg6-2,ycg6-3,xcg6+3,ycg6+2);




//------------------------------------------------------------------------------------------------------------
//GREDA 7 I 8
//------------------------------------------------------------------------------------------------------------
pDC->SelectObject(PenBlack3);
double duzg7;
double dsing7;
double dcosg7;
int domega1;
double dxg7;
double dyg7;



domega1 = 90+dteta1;//315 u nominalnom polozaju dteta1 =315             dugao2= 45

dsing7 =sin((PI*domega1)/180);
dcosg7 =cos((PI*domega1)/180);

duzg7 = 30; //ISTO STO I duzg6

dxg7 = dxg5-(dcosg7*duzg7);
dyg7 = dyg5+(dsing7*duzg7);

pDC->MoveTo(dxg5,dyg5);
pDC->LineTo(dxg7,dyg7);


//*
double dsing8;
double dcosg8;
int domega2;
double dxg8;
double dyg8;

domega2 = 270+dteta2; // 90-...za zahvat 

dsing8 =sin((PI*domega2)/180);
dcosg8 =cos((PI*domega2)/180);



dxg8 = dxg6-(dcosg8*duzg7);
dyg8 = dyg6+(dsing8*duzg7);

pDC->MoveTo(dxg6,dyg6);
pDC->LineTo(dxg8,dyg8);




//CENTAR MASE GREDE 7 I 8
//------------------------------------------------------------------------------------------------------------

double xcg7;
double ycg7;

xcg7 = dxg5-(duzg7*cos((PI*domega1)/180)/2);
ycg7 = dyg5+(duzg7*sin((PI*domega1)/180)/2);

pDC->SelectObject(PenRed3);
pDC->Ellipse(xcg7-2,ycg7-3,xcg7+3,ycg7+2);

double xcg8;
double ycg8;

xcg8 = dxg6-(duzg7*cos((PI*domega2)/180)/2);
ycg8 = dyg6+(duzg7*sin((PI*domega2)/180)/2);

pDC->SelectObject(PenRed3);
pDC->Ellipse(xcg8-2,ycg8-3,xcg8+3,ycg8+2);






//------------------------------------------------------------------------------------------------------------
//GRAFICKI PRIKAZ TEKSTA
//------------------------------------------------------------------------------------------------------------




double TXTalfa1;
TXTalfa1 = alfa1;


if(TXTalfa1 > 360)
{
	TXTalfa1-=360;
	
		
}

char xa1[10];		 
sprintf(xa1,"x:    %2f", (duzg2+duzg1)*cos((PI*alfa1)/180));
pDC->TextOut (690,200 ,xa1);


char xa2[10];		 
sprintf(xa2,"y:    %2f", ((y1+y2)/2)*sin((PI*alfa1)/180));
pDC->TextOut (690,100 ,xa2);

char xa3[10];		 
sprintf(xa3,"ugao:    %2f",TXTalfa1);
pDC->TextOut (690,50 ,xa3);

CPen PenRed2(PS_SOLID,2,RGB(255,0,0));
pDC->SelectObject(PenRed2);

//------------------------------------------------------------------------------------------------------------
//POCETNI KOEFICIJENTI  ZA POMOCNE LINIJE
//------------------------------------------------------------------------------------------------------------
double dpx; //300/2 = 150
double dpy; // 200+300  /2 = 250

dpx = 75+(duzg1/4);//duzg1/2;pocetno 150  duzg1= 0  dxp = 75   //200  125//300 150  // 400  175  //500     200   // 300   za 500   - 50
dpy =((y1+y2)/2);


//od x ose ka liniji VERTIKALA BLIZA Y OSI
pDC->MoveTo(duzg1/2,0);
pDC->LineTo(duzg1/2,((y1+y2)/20)+(duzg2*sin((PI*alfa1)/180)/2));


//od y ose ka tacki HORIZONTALA GORNJA
pDC->MoveTo(0,((y1+y2)/2)+(duzg2*sin((PI*alfa1)/180)/2));
pDC->LineTo((duzg1+(duzg2*cos((PI*alfa1)/180))/2),((y1+y2)/2)+(duzg2*sin((PI*alfa1)/180)/2));


//VERTIKALA DALJA OD Y OSE
pDC->MoveTo((duzg1+(duzg2*cos((PI*alfa1)/180))/2),0);
pDC->LineTo((duzg1+(duzg2*cos((PI*alfa1)/180))/2),((y1+y2)/2)+(duzg2*sin((PI*alfa1)/180)/2));


//od lezista do linije HORIZONTALA DONJA BLIZA X OSI
pDC->MoveTo(0,dpy);
pDC->LineTo((duzg1+(duzg2*cos((PI*alfa1)/180))/2),dpy);



//------------------------------------------------------------------------------------------------------------
//GRAFICKI PRIKAZ TACAKA
//------------------------------------------------------------------------------------------------------------
pDC->SelectObject(PenRed1);



double Vosa;//RAZLIKA VERTIKALNIH POKRETNIH OSA
double Hosa;//RAZLIKA HORIZONTALNIH POKRETNIH OSA


Hosa = (((y1+y2)/2)+(duzg2*sin((PI*alfa1)/180)/2)-dpy)/2;
Vosa = ((duzg1+(duzg2*cos((PI*alfa1)/180))/2)-150)/2;


pDC->MoveTo(0,Hosa+((y1+y2)/2));
pDC->LineTo((duzg1+(duzg2*cos((PI*alfa1)/180))/2),Hosa+((y1+y2)/2));

pDC->MoveTo(Vosa+dpx,0);
pDC->LineTo(Vosa+dpx,((y1+y2)/2)+(duzg2*sin((PI*alfa1)/180)/2));


pDC->Ellipse(Vosa+dpx-2,Hosa+dpy-3,Vosa+dpx+3,Hosa+dpy+2);

//KRAJNJA TACKA GREDE 1
pDC->SelectObject(PenBlue1);
pDC->SelectObject(BrushBlue);
pDC->Ellipse(duzg1-2,((y1+y2)/2)-3,duzg1+3,((y1+y2)/2)+2);

//KRETANJE KRAJNJE TACKE GREDE 2.
//pDC->Ellipse(((duzg2+duzg1))-2),(((y1+y2)/2))-3),((duzg2+duzg1))+3),(((y1+y2)/2))+2));

pDC->Ellipse( (duzg1+(duzg2*cos((PI*alfa1)/180)))-2,((y1+y2)/2)+(duzg2*sin((PI*alfa1)/180))-3,(duzg1+(duzg2*cos((PI*alfa1)/180)))+3,((y1+y2)/2)+(duzg2*sin((PI*alfa1)/180))+2);


//KRETANJE TACKE NA KRAJU GREDE 3
pDC->Ellipse(dxg3-2,dyg3-3,dxg3+3,dyg3+2);

//KRETANJE TACKE NA KRAJU GREDE 4
pDC->Ellipse(dxg4-2,dyg4-3,dxg4+3,dyg4+2);

//KRETANJE TACKE NA KRAJU GREDE 5 i 6
pDC->Ellipse(dxg5-2,dyg5-3,dxg5+3,dyg5+2);

pDC->Ellipse(dxg6-2,dyg6-3,dxg6+3,dyg6+2);

//------------------------------------------------------------------------------------------------------------	
//CENTAR MASE SISTEMA !!!
//------------------------------------------------------------------------------------------------------------

double M;
double Ms1;
double Ms2;
double Ms3;
double Ms4;
double Ms5;
double Ms6;
double Ms7;
double Ms8;
double Ms9;
double dxmc;   //HAMMER TIME !!!! genije
double dymc;
double SumaX;
double SumaY;


double Xcmg1;
double Ycmg1;

double Xcmg2;
double Ycmg2;

double Xcmg3;
double Ycmg3;

double Xcmg4;
double Ycmg4;

double Xcmg5;
double Ycmg5;

double Xcmg6;
double Ycmg6;

double Xcmg7;
double Ycmg7;

double Xcmg8;
double Ycmg8;

double Xcmg9;
double Ycmg9;

Xcmg1=((x2+duzg1)/2);
Ycmg1=((y1+y2)/2);

Xcmg2=(duzg1+(duzg2*cos((PI*alfa1)/180))/2);
Ycmg2=((y1+y2)/2)+(duzg2*sin((PI*alfa1)/180)/2);

Xcmg3=xcg3;
Ycmg3=ycg3;

Xcmg4=xcg4;
Ycmg4=ycg4;

Xcmg5=xcg5;
Ycmg5=ycg5;

Xcmg6=xcg6;
Ycmg6=ycg6;

Xcmg7=xcg7;
Ycmg7=ycg7;

Xcmg8=xcg8;
Ycmg8=ycg8;

Xcmg9 = Vosa+dpx;
Ycmg9 = Hosa+dpy;

Ms1 = 200;
Ms2 = 100;
Ms3 = 100;
Ms4 = 200;
Ms5 = 200;
Ms6= 200;
Ms7= 50000;
Ms8= 50000;
Ms9= 500;


SumaX = (Ms1*Xcmg2)+(Ms2*Xcmg1)+(Ms9*Xcmg9)+(Ms3*Xcmg3)+(Ms4*Xcmg4)+(Ms5*Xcmg5)+(Ms6*Xcmg6)+(Ms7*Xcmg7)+(Ms8*Xcmg8);
SumaY = (Ms1*Ycmg2)+(Ms2*Ycmg1)+(Ms9*Ycmg9)+(Ms3*Ycmg3)+(Ms4*Ycmg4)+(Ms5*Ycmg5)+(Ms6*Ycmg6)+(Ms7*Ycmg7)+(Ms8*Ycmg8);

M=Ms1+Ms2+Ms3+Ms4+Ms5+Ms6+Ms7+Ms8+Ms9;
dxmc = SumaX/M;
dymc = SumaY/M;

CPen PenGreen3(PS_SOLID,3,RGB(0,255,0));
pDC->SelectObject(PenGreen3);
//------------------------------------------------------------------------------------------------------------
//GRAFICKI PRIKAZ CENTRA MASE
//------------------------------------------------------------------------------------------------------------
pDC->Ellipse(dxmc-2,dymc-3,dxmc+2,dymc+3);

CPen PenGreen1(PS_SOLID,1,RGB(0,255,0));
pDC->SelectObject(PenGreen1);


//------------------------------------------------------------------------------------------------------------
//POMOCNE LINIJE GRAFICKI PRIKAZ za centar mase
//------------------------------------------------------------------------------------------------------------



//Vertikala pomocna
pDC->MoveTo(dxmc,0);
pDC->LineTo(dxmc,dymc);


//Horizontala
pDC->MoveTo(0,dymc);
pDC->LineTo(dxmc,dymc);




//-------------------------------------------------------------------------------------------------------------
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                               - BRZINE - 
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
//-------------------------------------------------------------------------------------------------------------



double v_alfa1;
double v_duzg2;
//-------------------------------------------------------------------------------------------------------------
//INTENZITETI MOMENATA GREDA 1 I 2
//-------------------------------------------------------------------------------------------------------------

double M_duzg1; 
double M_duzg2; 
double M_duzg3;
double M_duzg4;
double M_duzg5;
double M_duzg6;


double sumMO1;

/*//---------------------

(M~)= (F~) x (r~)




(F~) = m*(a~)

a~ = ( a1*i~+a2*j~+a3*k~)

g~ = -9.802 j~


*///-------------------

double g;
g = 9.802;



double cos_duzg2;// PROJEKCIJA NA OSU X
double cos_alfa1;


cos_alfa1 =cos((PI*alfa1)/180);
cos_duzg2 =cos_alfa1*duzg2;
//duzg1 = 250
//duzg2 = 300
//Ms1 = 200
//Ms2 = 100

double cos_duzg3;
double cos_dgama;

cos_dgama = cos((PI*dgama)/180);
cos_duzg3 = cos_dgama*duzg3;

double cos_duzg4;
double cos_dfi;

cos_dfi = cos((PI*dfi)/180);
cos_duzg4 = cos_dfi*duzg4;

double cos_duzg5;
double cos_dteta1;

cos_dteta1 = cos((PI*dteta1)/180);
cos_duzg5 = cos_dteta1*duzg5;

double cos_duzg6;
double cos_dteta2;

cos_dteta2 = cos((PI*dteta2)/180);
cos_duzg6 = cos_dteta2*duzg5;

M_duzg1 = (duzg1/100)*(-g)*(Ms1/1000);
M_duzg2 = (cos_duzg2/100)*(-g)*(Ms2/1000);
M_duzg3 = -(cos_duzg3/100)*(-g)*(Ms3/1000);
M_duzg4 = -(cos_duzg4/100)*(-g)*(Ms4/1000);
M_duzg5 = -(cos_duzg5/100)*(-g)*(Ms5/1000);
M_duzg6 = -(cos_duzg6/100)*(-g)*(Ms6/1000);



sumMO1 = M_duzg1+M_duzg2+M_duzg3+M_duzg4+M_duzg5+M_duzg6;

char ma1[10];		 
sprintf(ma1,"Moment M1:    %2fNm", M_duzg1);
pDC->TextOut (990,100 ,ma1);

char ma2[10];		 
sprintf(ma2,"Moment M2:    %2fNm", M_duzg2);
pDC->TextOut (990,150 ,ma2);


char ma3[10];		 
sprintf(ma3,"Moment M3:    %2fNm", M_duzg3);
pDC->TextOut (990,200 ,ma3);


char ma4[10];		 
sprintf(ma4,"Moment M4:    %2fNm", M_duzg4);
pDC->TextOut (990,250 ,ma4);

char ma5[10];		 
sprintf(ma5,"Moment M5:    %2fNm", M_duzg5);
pDC->TextOut (990,300 ,ma5);

char ma6[10];		 
sprintf(ma6,"Moment M6:    %2fNm", M_duzg6);
pDC->TextOut (990,350 ,ma6);

char masum[10];		 
sprintf(masum,"Suma Mo:    %2fNm",sumMO1 );
pDC->TextOut (990,50 ,masum);





















ReleaseDC(pDC);


}

/////////////////////////////////////////////////////////////////////////////
// CMasotronv1View diagnostics

#ifdef _DEBUG
void CMasotronv1View::AssertValid() const
{
	CView::AssertValid();
}

void CMasotronv1View::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}

CMasotronv1Doc* CMasotronv1View::GetDocument() // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CMasotronv1Doc)));
	return (CMasotronv1Doc*)m_pDocument;
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CMasotronv1View message handlers


void CMasotronv1View::OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags) 
{
	switch (nChar)
 {
	case VK_F1:
		m_Zumx +=10;
		m_Zumy +=10;
	
		Invalidate();
		break;

	case VK_F2:
		m_Zumx -=10;
		m_Zumy -=10;
	
		if (m_Zumx < 10)
		{
			m_Zumx =10;
			m_Zumy =10;
		}
		Invalidate();
		break;

	case 0x51://q
	
		alfa1+=1;

	
		Invalidate();
		break;

	case 0x57://w
		
		alfa1-=1;

	
		Invalidate();
		break;


	case 0x41://a
	
		dugao-=1;

	
		Invalidate();
		break;

	case 0x53://s
		
		dugao+=1;

	
		Invalidate();
		break;

	case 0x5a://z
	
		dugao3-=1;

	
		Invalidate();
		break;

	case 0x58://x
		
		dugao3+=1;

	
		Invalidate();
		break;
		
    case VK_SPACE:
		
		dugao2+=1;

	
		Invalidate();
		break;
	
   	case 0x56://v
		
		dugao2-=1;

	
		Invalidate();
		break;
	


}
	
	CView::OnKeyDown(nChar, nRepCnt, nFlags);
}
