// Masotron v1View.h : interface of the CMasotronv1View class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_MASOTRONV1VIEW_H__AEB0B19B_3C7F_4509_8DAB_F39DF2F8655A__INCLUDED_)
#define AFX_MASOTRONV1VIEW_H__AEB0B19B_3C7F_4509_8DAB_F39DF2F8655A__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


class CMasotronv1View : public CView
{
protected: // create from serialization only
	CMasotronv1View();
	DECLARE_DYNCREATE(CMasotronv1View)

// Attributes
public:
	CMasotronv1Doc* GetDocument();

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMasotronv1View)
	public:
	virtual void OnDraw(CDC* pDC);  // overridden to draw this view
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	protected:
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CMasotronv1View();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	int m_Zumx;
	int m_Zumy;
	int alfa1 ;
	int dugao;
	int dugao2;
	int dugao3;



	//{{AFX_MSG(CMasotronv1View)
	afx_msg void OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

#ifndef _DEBUG  // debug version in Masotron v1View.cpp
inline CMasotronv1Doc* CMasotronv1View::GetDocument()
   { return (CMasotronv1Doc*)m_pDocument; }
#endif

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_MASOTRONV1VIEW_H__AEB0B19B_3C7F_4509_8DAB_F39DF2F8655A__INCLUDED_)
