// ModelessDlg.cpp : implementation file
//

#include "stdafx.h"
#include "Modeless.h"
#include "ModelessDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CModelessDlg dialog


CModelessDlg::CModelessDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CModelessDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CModelessDlg)
	m_strMessage = _T("");
	//}}AFX_DATA_INIT
	m_pView = NULL;
}
CModelessDlg::CModelessDlg(CView* pView)
{
	m_pView = pView;
}



void CModelessDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CModelessDlg)
	DDX_Text(pDX, IDC_MESSAGE, m_strMessage);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CModelessDlg, CDialog)
	//{{AFX_MSG_MAP(CModelessDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CModelessDlg message handlers

void CModelessDlg::OnCancel() 
{
	if (m_pView != NULL) {
		// modeless case -- do not call base class OnOK
		m_pView->PostMessage(WM_GOODBYE, IDCANCEL);
	}
	else 
	CDialog::OnCancel(); // modal case

	
	//CDialog::OnCancel();
}

void CModelessDlg::OnOK() 
{
	if (m_pView != NULL) {
		// modeless case -- do not call base class OnOK
		UpdateData(TRUE);
		m_pView->PostMessage(WM_GOODBYE, IDOK);
	}
	else 
		CDialog::OnOK(); // modal case

	
	//CDialog::OnOK();
}

BOOL CModelessDlg::Create()
{
	return CDialog::Create(CModelessDlg::IDD);
}
