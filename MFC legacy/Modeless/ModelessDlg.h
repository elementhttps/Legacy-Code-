#if !defined(AFX_MODELESSDLG_H__29982855_383F_45E8_92BC_1B812C25552C__INCLUDED_)
#define AFX_MODELESSDLG_H__29982855_383F_45E8_92BC_1B812C25552C__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ModelessDlg.h : header file
//
#define WM_GOODBYE WM_USER +1
/////////////////////////////////////////////////////////////////////////////
// CModelessDlg dialog

class CModelessDlg : public CDialog
{
// Construction
private:
	CView* m_pView;

public:
	CModelessDlg(CWnd* pParent = NULL);   // standard constructor
    CModelessDlg(CView* pView);    
    BOOL Create();

// Dialog Data
	//{{AFX_DATA(CModelessDlg)
	enum { IDD = IDD_MODELESS };
	CString	m_strMessage;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CModelessDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CModelessDlg)
	virtual void OnCancel();
	virtual void OnOK();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_MODELESSDLG_H__29982855_383F_45E8_92BC_1B812C25552C__INCLUDED_)
