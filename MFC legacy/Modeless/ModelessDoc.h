// ModelessDoc.h : interface of the CModelessDoc class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_MODELESSDOC_H__EEC1D22D_B8D2_4D5B_B8C7_B9671E7E3D39__INCLUDED_)
#define AFX_MODELESSDOC_H__EEC1D22D_B8D2_4D5B_B8C7_B9671E7E3D39__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


class CModelessDoc : public CDocument
{
protected: // create from serialization only
	CModelessDoc();
	DECLARE_DYNCREATE(CModelessDoc)

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CModelessDoc)
	public:
	virtual BOOL OnNewDocument();
	virtual void Serialize(CArchive& ar);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CModelessDoc();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	//{{AFX_MSG(CModelessDoc)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_MODELESSDOC_H__EEC1D22D_B8D2_4D5B_B8C7_B9671E7E3D39__INCLUDED_)
