// ModelessView.cpp : implementation of the CModelessView class
//

#include "stdafx.h"
#include "Modeless.h"

#include "ModelessDoc.h"
#include "ModelessView.h"

#include "ModelessDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CModelessView

IMPLEMENT_DYNCREATE(CModelessView, CView)

BEGIN_MESSAGE_MAP(CModelessView, CView)
    ON_MESSAGE(WM_GOODBYE, OnGoodbye)
	//{{AFX_MSG_MAP(CModelessView)
	ON_WM_LBUTTONDOWN()
	ON_WM_RBUTTONDOWN()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CModelessView construction/destruction

CModelessView::CModelessView()
{
	// TODO: add construction code here
	m_pDlg = new CModelessDlg(this);

}

CModelessView::~CModelessView()
{
	 delete m_pDlg; // destroys window if not already destroyed 
}

BOOL CModelessView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	return CView::PreCreateWindow(cs);
}

/////////////////////////////////////////////////////////////////////////////
// CModelessView drawing

void CModelessView::OnDraw(CDC* pDC)
{
	CModelessDoc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);

	pDC->TextOut(0, 0, "Press the left mouse button here.");
}

/////////////////////////////////////////////////////////////////////////////
// CModelessView diagnostics

#ifdef _DEBUG
void CModelessView::AssertValid() const
{
	CView::AssertValid();
}

void CModelessView::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}

CModelessDoc* CModelessView::GetDocument() // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CModelessDoc)));
	return (CModelessDoc*)m_pDocument;
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CModelessView message handlers

void CModelessView::OnLButtonDown(UINT nFlags, CPoint point) 
{
	//creates the dialog if not already created
	if (m_pDlg->GetSafeHwnd() == 0) {
		m_pDlg->Create(); // displays the dialog window
	}

}

void CModelessView::OnRButtonDown(UINT nFlags, CPoint point) 
{
     m_pDlg->DestroyWindow();
	// no problem if window was destroyed already

}

LRESULT CModelessView::OnGoodbye(WPARAM wParam, LPARAM lParam)
{
	// message received in response to modeless dialog OK
	// and Cancel buttons
	TRACE("ModelessView::OnGoodbye %x, %lx\n", wParam, lParam);
	TRACE("Dialog edit1 contents = %s\n",
		  (const char*) m_pDlg->m_strMessage );
	if (wParam == IDCANCEL)
		m_pDlg->DestroyWindow();
	return 0L;
}
