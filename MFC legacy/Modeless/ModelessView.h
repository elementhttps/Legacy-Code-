// ModelessView.h : interface of the CModelessView class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_MODELESSVIEW_H__E6D3B354_582F_43A9_A9D5_91FD8B604081__INCLUDED_)
#define AFX_MODELESSVIEW_H__E6D3B354_582F_43A9_A9D5_91FD8B604081__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class CModelessDlg; // added by user

class CModelessView : public CView
{
protected: // create from serialization only
	CModelessView();
	DECLARE_DYNCREATE(CModelessView)
	CModelessDlg* m_pDlg; // added by user

// Attributes
public:
	CModelessDoc* GetDocument();

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CModelessView)
	public:
	virtual void OnDraw(CDC* pDC);  // overridden to draw this view
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	protected:
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CModelessView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	//{{AFX_MSG(CModelessView)
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnRButtonDown(UINT nFlags, CPoint point);
	//}}AFX_MSG
	afx_msg LRESULT OnGoodbye(WPARAM wParam, LPARAM lParam);
	DECLARE_MESSAGE_MAP()
};

#ifndef _DEBUG  // debug version in ModelessView.cpp
inline CModelessDoc* CModelessView::GetDocument()
   { return (CModelessDoc*)m_pDocument; }
#endif

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_MODELESSVIEW_H__E6D3B354_582F_43A9_A9D5_91FD8B604081__INCLUDED_)
