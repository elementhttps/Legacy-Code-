// Mymfc6Doc.cpp : implementation of the CMymfc6Doc class
//

#include "stdafx.h"
#include "Mymfc6.h"

#include "Mymfc6Doc.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CMymfc6Doc

IMPLEMENT_DYNCREATE(CMymfc6Doc, CDocument)

BEGIN_MESSAGE_MAP(CMymfc6Doc, CDocument)
	//{{AFX_MSG_MAP(CMymfc6Doc)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CMymfc6Doc construction/destruction

CMymfc6Doc::CMymfc6Doc()
{
	// TODO: add one-time construction code here

}

CMymfc6Doc::~CMymfc6Doc()
{
}

BOOL CMymfc6Doc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;

	// TODO: add reinitialization code here
	// (SDI documents will reuse this document)

	return TRUE;
}



/////////////////////////////////////////////////////////////////////////////
// CMymfc6Doc serialization

void CMymfc6Doc::Serialize(CArchive& ar)
{
	if (ar.IsStoring())
	{
		// TODO: add storing code here
	}
	else
	{
		// TODO: add loading code here
	}
}

/////////////////////////////////////////////////////////////////////////////
// CMymfc6Doc diagnostics

#ifdef _DEBUG
void CMymfc6Doc::AssertValid() const
{
	CDocument::AssertValid();
}

void CMymfc6Doc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CMymfc6Doc commands
