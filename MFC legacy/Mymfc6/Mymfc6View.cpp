// Mymfc6View.cpp : implementation of the CMymfc6View class
//

#include "stdafx.h"
#include "Mymfc6.h"

#include "Mymfc6Doc.h"
#include "Mymfc6View.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CMymfc6View

IMPLEMENT_DYNCREATE(CMymfc6View, CScrollView)

BEGIN_MESSAGE_MAP(CMymfc6View, CScrollView)
	//{{AFX_MSG_MAP(CMymfc6View)
	ON_WM_LBUTTONDOWN()
	ON_WM_LBUTTONUP()
	ON_WM_MOUSEMOVE()
	//}}AFX_MSG_MAP
	// Standard printing commands
	ON_COMMAND(ID_FILE_PRINT, CScrollView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_DIRECT, CScrollView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_PREVIEW, CScrollView::OnFilePrintPreview)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CMymfc6View construction/destruction

CMymfc6View::CMymfc6View(): m_sizeEllipse(100, -100), 
                           m_pointTopLeft(0, 0), 
                           m_sizeOffset(0, 0) 
{
	m_bCaptured = FALSE; 

}

CMymfc6View::~CMymfc6View()
{
}

BOOL CMymfc6View::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	return CScrollView::PreCreateWindow(cs);
}

/////////////////////////////////////////////////////////////////////////////
// CMymfc6View drawing

void CMymfc6View::OnDraw(CDC* pDC)
{
	CBrush brushHatch(BS_MONOPATTERN, RGB(0, 0, 0)); //popunjavanje objekta sa bojom i srafurama
    // logical (0, 0) 
    //CPoint point(0, 0); //logicka tacka
  
    // In device coordinates, align the brush with the window origin 
    //pDC->LPtoDP(&point); 
    //pDC->SetBrushOrg(point); 
    CPen PenW(PS_SOLID,1,RGB(0,0,255));
	pDC->SelectObject(PenW);
    pDC->SelectObject(brushHatch); 
	 pDC->Rectangle(CRect(100, -100, 200, -200)); // Test invalid rect 
    pDC->Ellipse(CRect(m_pointTopLeft, m_sizeEllipse)); 
    //pDC->SelectStockObject(BLACK_BRUSH); // Deselect brushHatch

   
}

void CMymfc6View::OnInitialUpdate()
{
	CScrollView::OnInitialUpdate(); 
  
    CSize sizeTotal(8000, 8000); // velicina skrolovanja    8-by-10.5 inches 
    CSize sizePage(sizeTotal.cx / 20, sizeTotal.cy / 20); 
    CSize sizeLine(sizeTotal.cx / 50, sizeTotal.cy / 50); 
    SetScrollSizes(MM_LOENGLISH, sizeTotal, sizePage, sizeLine); 
}

/////////////////////////////////////////////////////////////////////////////
// CMymfc6View printing

BOOL CMymfc6View::OnPreparePrinting(CPrintInfo* pInfo)
{
	// default preparation
	return DoPreparePrinting(pInfo);
}

void CMymfc6View::OnBeginPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: add extra initialization before printing
}

void CMymfc6View::OnEndPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: add cleanup after printing
}

/////////////////////////////////////////////////////////////////////////////
// CMymfc6View diagnostics

#ifdef _DEBUG
void CMymfc6View::AssertValid() const
{
	CScrollView::AssertValid();
}

void CMymfc6View::Dump(CDumpContext& dc) const
{
	CScrollView::Dump(dc);
}

CMymfc6Doc* CMymfc6View::GetDocument() // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CMymfc6Doc)));
	return (CMymfc6Doc*)m_pDocument;
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CMymfc6View message handlers


void CMymfc6View::OnLButtonDown(UINT nFlags, CPoint point) 
{
CRect rectEllipse(m_pointTopLeft, m_sizeEllipse); // still logical 
    CRgn  circle; 
  
    CClientDC dc(this); 
    OnPrepareDC(&dc); 
    dc.LPtoDP(rectEllipse); // Now it's in device coordinates 
    circle.CreateEllipticRgnIndirect(rectEllipse); 
	

    if (circle.PtInRegion(point))
	{ 
        // Capturing the mouse ensures subsequent LButtonUp message 
        SetCapture(); 
        m_bCaptured = TRUE; 
        CPoint pointTopLeft(m_pointTopLeft); 
        dc.LPtoDP(&pointTopLeft); 
        m_sizeOffset = point - pointTopLeft; // koordinate kursora prilikom pomeranja device coordinates 
        // New mouse cursor is active while mouse is captured 
        ::SetCursor(::LoadCursor(NULL, IDC_CROSS)); 
    } 	

	   
}




void CMymfc6View::OnMouseMove(UINT nFlags, CPoint point) 
{
   if (m_bCaptured) 
    { 
        CClientDC dc(this); 
        OnPrepareDC(&dc); 
        CRect rectOld(m_pointTopLeft, m_sizeEllipse); 
        dc.LPtoDP(rectOld); 
        InvalidateRect(rectOld, TRUE); 
        m_pointTopLeft = point - m_sizeOffset; 
        dc.DPtoLP(&m_pointTopLeft); 
        CRect rectNew(m_pointTopLeft, m_sizeEllipse); 
        dc.LPtoDP(rectNew); 
        InvalidateRect(rectNew, TRUE); 
	

    } 
}

void CMymfc6View::OnLButtonUp(UINT nFlags, CPoint point) 
{
	if (m_bCaptured) 
    { 
        ::ReleaseCapture(); 
        m_bCaptured = FALSE; 
    } 
	

}
/*
	 CPen PenB(PS_SOLID,1,RGB(255,0,0));
		dc.SelectObject(PenB);
		dc.MoveTo(100,100);
    	dc.LineTo(point);
		*/