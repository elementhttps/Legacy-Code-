// Mymfc6View.h : interface of the CMymfc6View class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_MYMFC6VIEW_H__C04DB418_0FC6_4335_AA2C_5578054F4E7C__INCLUDED_)
#define AFX_MYMFC6VIEW_H__C04DB418_0FC6_4335_AA2C_5578054F4E7C__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


class CMymfc6View : public CScrollView
{
protected: // create from serialization only
	CMymfc6View();
	DECLARE_DYNCREATE(CMymfc6View)

// Attributes
public:
	CMymfc6Doc* GetDocument();

private: 
    const CSize m_sizeEllipse; 
    CPoint m_pointTopLeft; // logical, top left of ellipse rectangle 
    CSize  m_sizeOffset;   // device, from rect top left  
                        
    // to capture point 
    BOOL   m_bCaptured; 
  

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMymfc6View)
	public:
	virtual void OnDraw(CDC* pDC);  // overridden to draw this view
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	protected:
	virtual void OnInitialUpdate(); // called first time after construct
	virtual BOOL OnPreparePrinting(CPrintInfo* pInfo);
	virtual void OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo);
	virtual void OnEndPrinting(CDC* pDC, CPrintInfo* pInfo);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CMymfc6View();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	//{{AFX_MSG(CMymfc6View)
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

#ifndef _DEBUG  // debug version in Mymfc6View.cpp
inline CMymfc6Doc* CMymfc6View::GetDocument()
   { return (CMymfc6Doc*)m_pDocument; }
#endif

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_MYMFC6VIEW_H__C04DB418_0FC6_4335_AA2C_5578054F4E7C__INCLUDED_)
