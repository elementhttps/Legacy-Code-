/* Win32 GUI app skeleton 
#include <windows.h>
/*
-------------------------------------------------------------------------------------------------------------------------------
This statement includes the standard Windows library needed to create a Windows program. 
The Windows API contains many other libraries, but this file is the main one. 
Just remember that windows.h must be included in every Windows program.
-------------------------------------------------------------------------------------------------------------------------------
*/
/*
LRESULT CALLBACK WndProc( HWND hWnd, UINT messg,
                                        WPARAM wParam, LPARAM lParam );


char szProgName[] = "Hello Win32"; // name of application 
//message to be printed in client area 
char message[] = "Hello from Win32"; 
                                        
int WINAPI WinMain(//Win32 entry-point routine 
                         HINSTANCE hInst,
                         HINSTANCE hPreInst, 
                         LPSTR lpszCmdLine, 
                         int nCmdShow )
/*
-------------------------------------------------------------------------------------------------------------------------------
Just like a C++ program always has a main() function, a Win32 program needs a central function call WinMain. 
The syntax of that function is:

INT WINAPI WinMain(HINSTANCE hInstance,     1)
				   HINSTANCE hPrevInstance, 2)
                   LPSTR lpCmdLine,         3)
				   int nCmdShow );          4)

Unlike the C++ main() function, the arguments of the WinMain() function are not optional. 
Your program will need them to communicate with the operating system.
*********************************************************************************************************************
1) hInst is a handle to your application. A handle is a lot like a pointer or a reference and keeps track of various 
Windows elements, such as your application, your application�s program windows, and other things. 
Inst stands for instance. If two copies of your application are running at the same time, 
each one will be a different instance of the same application. hInst is a handle to this particular instance 
of the application. It can be useful for calling some of the Windows API functions.

hInstance, is a handle to the instance of the program you are writing
***********************************
2) hPreInst is a handle to the previous instance of your application. This is outdated and no longer has use, so just ignore it.
The only reason that it is still here is for backward compatibility. Backward compatibility occurs when a new version of 
some software is still compatible with older versions. This way, people using older versions won�t have to buy the newer version.

hPrevInstance, is used if your program had any previous instance.If not,this argument can be ignored,
which will always be the case.
***********************************
3) lpszCmdLine is just a null-terminated string that stores the command-line arguments. 
If you go to the Start menu and select Run, you can type anything you want after the program name. 
For example, if your program is named Hello.exe, you can type Hello.exe Goodbye. 
Then you can design the program to use this command-line argument to display whatever message is supplied, in this case Goodbye.
Notice that the name of the program, Hello.exe, is not part of lpszCmdLine.

lpCmdLine, is a string that represents all items used on the command line to compile the application.
***********************************
4) nCmdShow is an integer specifying how the window will display. You can treat this as a suggestion from Windows as to 
how your window should look. Like most suggestions, this one can be ignored. This argument has too many possible values 
to list here, but Table 12.1 lists the ones generally needed.

nCmdShow, controls how the window you are building will be displayed.
***********************************




-------------------------------------------------------------------------------------------------------------------------------
*/
//
/*{
     HWND hWnd;
     MSG lpMsg;
     WNDCLASS wc;
     if( !hPreInst ) //set up window class and register it 
     {
          wc.lpszClassName = szProgName;
          wc.hInstance = hInst;
          wc.lpfnWndProc = WndProc;
          wc.hCursor = LoadCursor( NULL, IDC_ARROW );
          wc.hIcon = LoadIcon( NULL, IDI_APPLICATION );
          wc.lpszMenuName = NULL;
          wc.hbrBackground = (HBRUSH)
               GetStockObject( WHITE_BRUSH );
          wc.style = 0;
          wc.cbClsExtra = 0;
          wc.cbWndExtra = 0;
          if( !RegisterClass( &wc ) )
               return FALSE;
     }
hWnd = CreateWindow(// now create the window 
          szProgName,
          "CodeWarrior Win32 stationery",
          WS_OVERLAPPEDWINDOW,
          CW_USEDEFAULT,
          CW_USEDEFAULT,
          CW_USEDEFAULT,
          CW_USEDEFAULT,
          (HWND)NULL,
          (HMENU)NULL,
          (HINSTANCE)hInst,
          (LPSTR)NULL
     );
     ShowWindow(hWnd, nCmdShow );
     UpdateWindow( hWnd );
     
     // begin the message loop 
     while( GetMessage( &lpMsg, NULL, 0, 0 ) )
     {
          TranslateMessage( &lpMsg );
          DispatchMessage( &lpMsg );
     }
     return( lpMsg.wParam);
}
//callback procedure 
LRESULT CALLBACK WndProc( HWND hWnd, UINT messg, 
                                        WPARAM wParam, LPARAM lParam )
{
     HDC hdc; // handle to the device context 
     PAINTSTRUCT pstruct; //struct for the call to BeginPaint 
     
     switch(messg)
     {
          case WM_PAINT:
          // prepare window for painting
          hdc = BeginPaint(hWnd, &pstruct ); 
          //print hello at upper left corner 
          TextOut( hdc, 0, 0, message, 
               ( sizeof(message) - 1 ) ); 
          // stop painting 
          EndPaint(hWnd, &pstruct ); 
          break;
          
          case WM_DESTROY:
          PostQuitMessage( 0 );
          break;
     
          default:
          return( DefWindowProc( hWnd, messg, 
               wParam, lParam ) );
     }
     return( 0L );
}
*/
//_____________________________________________________________________________________________________________________________
//////////////
//PROGRAM V2//
//////////////
#include <windows.h>
/*
-------------------------------------------------------------------------------------------------------------------------------
#include <windows.h>

This statement includes the standard Windows library needed to create a Windows program. 
The Windows API contains many other libraries, but this file is the main one. 
Just remember that windows.h must be included in every Windows program.
-------------------------------------------------------------------------------------------------------------------------------
*/

LPCTSTR ClsName = "BasicApp";
LPCTSTR WndName = "A Simple Window";

LRESULT CALLBACK WndProcedure(HWND hWnd, UINT uMsg,
			      WPARAM wParam, LPARAM lParam);

INT WINAPI WinMain(HINSTANCE hInstance, 
				   HINSTANCE hPrevInstance,
                   LPSTR lpCmdLine, 
			       int nCmdShow)
/*
-------------------------------------------------------------------------------------------------------------------------------
Just like a C++ program always has a main() function, a Win32 program needs a central function call WinMain. 
The syntax of that function is:

INT WINAPI WinMain(HINSTANCE hInstance,     1)
				   HINSTANCE hPrevInstance, 2)
                   LPSTR lpCmdLine,         3)
				   int nCmdShow );          4)

Unlike the C++ main() function, the arguments of the WinMain() function are not optional. 
Your program will need them to communicate with the operating system.


1)HINSTANCE hInstance
Handle to the programs executable module (the .exe file in memory) 

2)HINSTANCE hPrevInstance
Always NULL for Win32 programs.
 
3)LPSTR lpCmdLine
The command line arguments as a single string. NOT including the program name. 

4)int nCmdShow
An integer value which may be passed to ShowWindow()

LP prefix stands for Long Pointer

LPCSTR indicates a pointer to a const string, one that can not or will not be modified

LPSTR on the other hand is not const and may be changed.

***********************************
1) hInst is a handle to your application. A handle is a lot like a pointer or a reference and keeps track of various 
Windows elements, such as your application, your application�s program windows, and other things. 
Inst stands for instance. If two copies of your application are running at the same time, 
each one will be a different instance of the same application. hInst is a handle to this particular instance 
of the application. It can be useful for calling some of the Windows API functions.

hInstance, is a handle to the instance of the program you are writing

***********************************
2) hPreInst is a handle to the previous instance of your application. This is outdated and no longer has use, so just ignore it.
The only reason that it is still here is for backward compatibility. Backward compatibility occurs when a new version of 
some software is still compatible with older versions. This way, people using older versions won�t have to buy the newer version.

hPrevInstance, is used if your program had any previous instance.If not,this argument can be ignored,
which will always be the case.

***********************************
3) lpszCmdLine is just a null-terminated string that stores the command-line arguments. 
If you go to the Start menu and select Run, you can type anything you want after the program name. 
For example, if your program is named Hello.exe, you can type Hello.exe Goodbye. 
Then you can design the program to use this command-line argument to display whatever message is supplied, in this case Goodbye.
Notice that the name of the program, Hello.exe, is not part of lpszCmdLine.

lpCmdLine, is a string that represents all items used on the command line to compile the application.

***********************************
4) nCmdShow is an integer specifying how the window will display. You can treat this as a suggestion from Windows as to 
how your window should look. Like most suggestions, this one can be ignored. This argument has too many possible values 
to list here, but Table 12.1 lists the ones generally needed.

nCmdShow, controls how the window you are building will be displayed.
***********************************
-------------------------------------------------------------------------------------------------------------------------------
*/
{
	MSG        Msg;
	HWND       hWnd;
	WNDCLASSEX WndClsEx;
/*
-------------------------------------------------------------------------------------------------------------------------------

The Win32 library provides two classes for creating the main window and you can use any one of them. 
They are WNDCLASS and WNDCLASSEX. The second adds only a slight feature to the first. 
Therefore, we will mostly use the WNDCLASSEX structure

The WNDCLASS and the WNDCLASSEX classes are defined as follows:  
***********************************
WNDCLASS
***********************************   
typedef struct _WNDCLASS { 
    UINT       style; 
    WNDPROC    lpfnWndProc; 
    int        cbClsExtra; 
    int        cbWndExtra; 
    HINSTANCE  hInstance; 
    HICON      hIcon; 
    HCURSOR    hCursor; 
    HBRUSH     hbrBackground; 
    LPCTSTR    lpszMenuName; 
    LPCTSTR    lpszClassName; 
} WNDCLASS, *PW

***********************************
WNDCLASSEX
***********************************
typedef struct _WNDCLASSEX { 
    UINT       cbSize; 
    UINT       style; 
    WNDPROC    lpfnWndProc; 
    int        cbClsExtra; 
    int        cbWndExtra; 
    HINSTANCE  hInstance; 
    HICON      hIcon; 
    HCURSOR    hCursor; 
    HBRUSH     hbrBackground; 
    LPCTSTR    lpszMenuName; 
    LPCTSTR    lpszClassName; 
    HICON      hIconSm; 
} WNDCLASSEX, *PWNDCLASSEX;
-------------------------------------------------------------------------------------------------------------------------------
The first thing you must do in order to create an application is to declare a variable of either WNDCLASS or WNDCLASSEX type.
 Here is an example of a WNDCLASSEX variable:



INT WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance,
               LPSTR lpCmdLine, int nCmdShow)
{
    WNDCLASSEX WndClsEx;          //WndClsEx ili wc .....

    return 0;
}

-------------------------------------------------------------------------------------------------------------------------------
*/
	// Create the application window
	WndClsEx.cbSize        = sizeof(WNDCLASSEX);                     //1)
	WndClsEx.style         = CS_HREDRAW | CS_VREDRAW;                //2)
	WndClsEx.lpfnWndProc   = WndProcedure;                           //3)
	WndClsEx.cbClsExtra    = 0;                                      //4)
	WndClsEx.cbWndExtra    = 0;                                      //5)
	WndClsEx.hIcon         = LoadIcon(NULL, IDI_APPLICATION);        //6)
	WndClsEx.hCursor       = LoadCursor(NULL, IDC_ARROW);            //7)
	WndClsEx.hbrBackground = (HBRUSH)GetStockObject(WHITE_BRUSH);    //8)
	WndClsEx.lpszMenuName  = NULL;                                   //9)
	WndClsEx.lpszClassName = ClsName;                                //10)
	WndClsEx.hInstance     = hInstance;                              //11)
	WndClsEx.hIconSm       = LoadIcon(NULL, IDI_APPLICATION);        //12)-6)
/*
-------------------------------------------------------------------------------------------------------------------------------
***********************************
1)The Size of the Window Class
***********************************

After declaring a WNDCLASSEX variable, you must specify its size. 
This is done by  initializing your variable with the sizeof operator applied to the window class as follows:

WndClsEx.cbSize = sizeof(WNDCLASSEX);

***********************************
2)The Main Window's Style
***********************************

The style member variable specifies the primary operations applied on the window class. The actual available styles 
are constant values. For example, if a user moves a window or changes its size, you would need the window to be redrawn
 to get its previous characteristics. To redraw the window horizontally, you would apply the CS_HREDRAW. In the same way,
 to redraw the window vertically, you can apply the CS_VREDRAW.

The styles are combined using the bitwise OR (|) operator. The CS_HREDRAW and the CS_VREDRAW styles can be combined and 
assigned to the style member variable as follows:

WndClsEx.style      = CS_HREDRAW | CS_VREDRAW;

  
CS_HREDRAW
 Redraws the entire window if a movement or size adjustment changes the width of the window.
 
CS_VREDRAW
 Redraws the entire window if a movement or size adjustment changes the height of the window.
 
CS_OWNDC
 Makes drawing in the window more convenient.
 
CS_DBLCLKS
 Sends a double-click message when the user double-clicks somewhere within the window.
 
CS_NOCLOSE
 Disables the Close command on the system menu.
 


***********************************
3)Message Processing
***********************************

A computer application is equipped with Windows controls that allow the user to interact with the computer. Each control 
creates messages and sends them to the operating system. To manage these messages, they are handled by a function pointer 
called a Windows procedure. 
This function can appear as follows:

LRESULT CALLBACK MessageProcedure(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam);

This function uses a switch control to list all necessary messages and process each one in turn. This processes only 
the messages that you ask it to. If you have left-over messages, and you will always have un-processed messages, you can
call the DefWindowProc() function at the end to take over.

The most basic message you can process is to make sure a user can close a window after using it. 
This can be done with a function called PostQuitMessage(). 
Its syntax is:

VOID PostQuitMessage(int nExitCode)

This function takes one argument which is the value of the LPARAM argument. To close a window, you can pass the argument 
as WM_QUIT.

Based on this, a simple Windows procedure can be defined as follows:


LRESULT CALLBACK WndProcedure(HWND hWnd, UINT Msg, WPARAM wParam, LPARAM lParam)
{
    switch(Msg)
    {
    case WM_DESTROY:
        PostQuitMessage(WM_QUIT);
        break;
    default:
        return DefWindowProc(hWnd, Msg, wParam, lParam);
    }
    return 0;
}


The name of the window procedure must be assigned to the lpfnWndProc member variable of 
the WNDCLASS or WNDCLASSEX variable. 
This can be defined as follows:

#include <windows.h>

LRESULT WndProcedure(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam);

INT WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance,
               LPSTR lpCmdLine, int nCmdShow)
{
	WNDCLASSEX WndClsEx;

	WndClsEx.cbSize      = sizeof(WNDCLASSEX);
	WndClsEx.style       = CS_HREDRAW | CS_VREDRAW;
	WndClsEx.lpfnWndProc = WndProcedure;
	WndClsEx.cbClsExtra  = 0;
	WndClsEx.cbWndExtra  = 0;
	WndClsEx.hInstance   = hInstance;

	return 0;
}

LRESULT CALLBACK WndProcedure(HWND hWnd, UINT Msg, WPARAM wParam, LPARAM lParam)
{
    switch(Msg)
    {
    case WM_DESTROY:
        PostQuitMessage(WM_QUIT);
        break;
    default:
        return DefWindowProc(hWnd, Msg, wParam, lParam);
    }
    return 0;
}

***********************************
4)Additional Memory Request
***********************************

Upon declaring a WNDCLASSEX variable, the compiler allocates an amount of memory space for it, as it does for all other 
variables. If you think you will need more memory than allocated, assign the number of extra bytes to the cbClsExtra member 
variable. Otherwise, the compiler initializes this variable to 0. If you do not need extra memory for your WNDCLASSEX variable,
initialize this member with 0. Otherwise, you can do it as follows:

WndClsEx.cbClsExtra = 0;


***********************************
5)Window Extra-Memory
***********************************

When an application has been launched and is displaying on the screen, which means an instance of the application 
has been created, the operating system allocates an amount of memory space for that application to use. If you think 
that your application's instance will need more memory than that, you can request that extra memory bytes be allocated to it. 
Otherwise, you can let the operating system handle this instance memory issue and initialize the cbWndExtra 
member variable to 0:

WndClsEx.cbWndExtra = 0;


***********************************
6) and 12)The Application Main Icon
***********************************

An icon can be used to represent an application in My Computer or Windows Explorer. To assign this small picture to 
your application, you can either use an existing icon or design your own. To make your programming a little faster,
Microsoft Windows installs a few icons. The icon is assigned to the hIcon member variable using the LoadIcon() function.
For a Win32 application, the syntax of this function is:


HICON LoadIcon(HINSTANCE hInstance, LPCTSTR lpIconName);

The hInstance argument is a handle to the file in which the icon was created. This file is usually stored in a library (DLL) of
 an executable program. If the icon was created as part of your application, you can use the hInstance of your application. 
 If your are using one of the icons below, set this argument to NULL.

The lpIconName is the name of the icon to be loaded. This name is added to the resource file when you create the icon resource.
It is added automatically if you add the icon as part of your resources; otherwise you can add it manually when creating your 
resource script. Normally, if you had created and designed an icon and gave it an identifier, you can pass it using the
MAKEINTRESOURCE macro.

To make your programming a little faster, Microsoft Windows installs a few icons you can use for your application.
These icons have identification names that you can pass to the LoadIcon() function as the lpIconName argument. 
The icons are:

IDI_APPLICATION	
IDI_INFORMATION	
IDI_ASTERISK	
IDI_QUESTION	
IDI_WARNING	
IDI_EXCLAMATION	
IDI_HAND	
IDI_ERROR

If you designed your own icon (you should make sure you design a 32x32 and a 16x16 versions, even for convenience), 
to use it, specify the hInstance argument of the LoadIcon() function to the instance of your application.
Then use the MAKEINTRESOURCE macro to convert its identifier to a null-terminated string. 
This can be done as follows: 

WndCls.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_STAPLE));

The icon can be specified by its name, which would be a null-terminated string passed as lpszResourceName. If you had 
designed your icon and gave it an ID, you can pass this identifier to the LoadIcon() method.

The LoadIcon() member function returns an HICON object that you can assign to the hIcon member variable of your WNDCLASS object.
Besides the regular (32x32) icon, the WNDCLASSEX structure allows you to specify a small icon (16x16) to use 
in some circumstances. 
You can specify both icons as follows:

WndClsEx.hIcon       = LoadIcon(NULL, IDI_APPLICATION);

WndClsEx.hIconSm     = LoadIcon(NULL, IDI_APPLICATION);

***********************************
7)Introduction to Cursors
***********************************
A cursor is used to locate the position of the mouse pointer on a document or the screen. To use a cursor, call the Win32 
LoadCursor() function. Its syntax is:

HCURSOR LoadCursor(HINSTANCE hInstance, LPCTSTR lpCursorName);

The hInstance argument is a handle to the file in which the cursor was created. This file is usually stored in a library (DLL) 
of an executable program. If the cursor was created as part of your application, you can use the hInstance of your application. 
If your are using one of the below cursors, set this argument to NULL.

When Microsoft Windows installs, it also installs various standard cursors you can use in your program. Each one of these 
cursors is recognized by an ID which is simply a constant integers. 
The available cursors are:

IDC_APPSTARTING	Used to show that something undetermined is going on or the application is not stable
IDC_ARROW		This standard arrow is the most commonly used cursor
IDC_CROSS		The crosshair cursor is used in various circumstances such as drawing
IDC_HAND		The Hand is standard only in Windows 2000. If you are using a previous operating system and need this cursor, you may have to create your own.
IDC_HELP		The combined arrow and question mark cursor is used when providing help on a specific item on a window object
IDC_IBEAM		The I-beam cursor is used on text-based object to show the position of the caret
IDC_ICON		This cursor is not used anymore
IDC_NO		    This cursor can be used to indicate an unstable situation
IDC_SIZE		This cursor is not used anymore
IDC_SIZEALL		The four arrow cursor pointing north, south, east, and west is highly used to indicate that an object is selected or that it is ready to be moved
IDC_SIZENESW	The northeast and southwest arrow cursor can be used when resizing an object on both the length and the height
IDC_SIZENS		The  north - south arrow pointing cursor can be used when shrinking or heightening an object
IDC_SIZENWSE	The northwest - southeast arrow pointing cursor can be used when resizing an object on both the length and the height
IDC_SIZEWE		The west - east arrow pointing cursor can be used when narrowing or enlarging an object
IDC_UPARROW		The vertical arrow cursor can be used to indicate the presence of the mouse or the caret
IDC_WAIT		The Hourglass cursor is usually used to indicate that a window or the application is not ready.

The LoadCursor() member function returns an HCURSOR value. You can assign it to the hCursor member variable of 
your WNDCLASS object.
Here is an example:

WndClsEx.hCursor     = LoadCursor(NULL, IDC_ARROW);
***********************************
8)The Window's Background Color
***********************************
To paint the work area of the window, you must specify what color will be used to fill it. This color is created as 
an HBRUSH and assigned to the hbrBackground member variable of your WNDCLASS or WNDCLASSEX variable. The color you 
are using must be a valid HBRUSH or you can cast a known color to HBRUSH. The Win32 library defines a series of colors 
known as stock objects. To use one of these colors, call the GetStockObject() function. For example, to paint the windows
 background in black, you can pass the BLACK_BRUSH constant to the GetStockObject() function, cast it to HBRUSH and assign
 the result to hbrBackground.

In addition to the stock objects, the Microsoft Windows provides a series of colors for its own internal use. These are the
colors used to paint the borders of frames, buttons, scroll bars, title bars, text, etc. The colors are named (you should 
be able to predict their appearance or role from their name) COLOR_ACTIVEBORDER, COLOR_ACTIVECAPTION, COLOR_APPWORKSPACE,
COLOR_BACKGROUND, COLOR_BTNFACE, COLOR_BTNSHADOW, COLOR_BTNTEXT, COLOR_CAPTIONTEXT, COLOR_GRAYTEXT, COLOR_HIGHLIGHT, 
COLOR_HIGHLIGHTTEXT, COLOR_INACTIVEBORDER, COLOR_INACTIVECAPTION, COLOR_MENU, COLOR_MENUTEXT, COLOR_SCROLLBAR, 
COLOR_WINDOW, COLOR_WINDOWFRAME, and COLOR_WINDOWTEXT. You can use any of these colors to paint the background of your window.
First cast it to HBRUSH and assign it to hbrBackground:

WndClsEx.hbrBackground = GetStockObject(WHITE_BRUSH);


BLACK_BRUSH
 Solid black
 
WHITE_BRUSH
 Solid white
 
GRAY_BRUSH
 Solid gray
 
LTGRAY_BRUSH
 Solid light gray
 
DKGRAY_BRUSH
 Solid dark gray
 
NULL_BRUSH
 Nothing

***********************************
9)The Application's Main Menu
***********************************
If you want the window to display a menu, first create or design the resource menu (we will eventually learn how to do this).
After creating the menu, assign its name to the lpszMenuName name to your WNDCLASS or WNDCLASSEX variable. Otherwise, 
pass this argument as NULL. 
Here is an example:

WndClsEx.lpszMenuName  = NULL;

***********************************
10)The Window's Class Name
***********************************
To create a window, you must provide its name as everything else in the computer has a name. 
The class name of your main window must be provided to the lpszClassName member variable of your WNDCLASS or WNDCLASSEX 
variable. You can provide the name to the variable or declare a global null-terminated string. 
Here is an example:

LPCTSTR ClsName = "BasicApp";

WndClsEx.lpszClassName = ClsName;

***********************************
11)The Application's Instance
***********************************

Creating an application is equivalent to creating an instance for it. To communicate to the WinMain() function that you 
want to create an instance for your application, which is, to make it available as a resource, assign the WinMain()'s 
hInstance argument to your WNDCLASS variable:

WndClsEx.hInstance  = hInstance;


-------------------------------------------------------------------------------------------------------------------------------
*/


	// Register the application
	RegisterClassEx(&WndClsEx);
/*
-------------------------------------------------------------------------------------------------------------------------------
***********************************
Window Registration
***********************************

After initializing the window class, you must make it available to the other controls that will be part of your application. 
This process is referred to as registration. To register the window class, call the RegisterClass() for a WNDCLASS variable.
 If you created your window class using the WNDCLASSEX structure, call the RegisterClassEx() function. 
 Their syntaxes are:

ATOM RegisterClass(CONST WNDCLASS *lpWndClass);
ATOM RegisterClassEx(CONST WNDCLASSEX *lpwcx);

The function simply takes as argument a pointer to a WNDCLASS or WNDCLASSEX. 
This call can be done as follows:

RegisterClassEx(&WndClsEx);


***********************************
Window Creation
***********************************
The WNDLCLASS and the WNDCLASSEX classes are used to initialize the application window class. To display a window, 
that is, to give the user an object to work with, you must create a window object. This window is the object the user 
uses to interact with the computer.

To create a window, you can call either the CreateWindow() or the CreateWindowEx() function. We will come back to these 
functions.

You can simply call this function and specify its arguments after you have registered the window class. 
Here is an example:
	
CreateWindow(. . .);

If you are planning to use the window further in your application, you should retrieve the result of the CreateWindow() 
or the CreateWindowEx() function, which is a handle to the window that is being created. To do this, you can declare 
an HWND variable and initialize it with the create function. 
This can be done as follows:

HWND	hWnd;
.
.
.
hWnd = CreateWindow(. . .);

-------------------------------------------------------------------------------------------------------------------------------
*/

	// Create the window object 
	hWnd = CreateWindow(              // hWnd = CreateWindow(. . .);
		      ClsName,                        //1)name of class
			  WndName,                        //2)name of window "Ura"
			  WS_OVERLAPPEDWINDOW,            //3)style
			  CW_USEDEFAULT,                  //4)distance of window from left side of screen
			  CW_USEDEFAULT,                  //5)distance of window from top of screen
			  CW_USEDEFAULT,                  //6)width of window
			  CW_USEDEFAULT,                  //7)height of window
			  NULL,                           //8)handle to parent of this window
			  NULL,                           //9)handle to menu
			  hInstance,                      //10)handle to instance of application
			  NULL);                          //11)window creation data
/*
-------------------------------------------------------------------------------------------------------------------------------
	
     LPCTSTR lpClassName, // name of class
     LPCTSTR lpWindowName, // name of window
     DWORD dwStyle, //style
     int x, // distance of window from left side of screen
     int y, // distance of window from top of screen
     int nWidth, // width of window
     int nHeight, // height of window
     HWND hWndParent, // handle to parent of this window
     HMENU hMenu, // handle to menu
     HINSTANCE hInstance, // handle to instance of application
     LPVOID lpParam // window creation data
***********************************
3)Style
***********************************
WS_POPUP
 A pop-up window.
 
WS_OVERLAPPED
 An overlapped window that has a title bar and a border.
 
WS_OVERLAPPEDWINDOW
 An overlapped window with the WS_OVERLAPPED, WS_CAPTION, WS_SYSMENU, WS_THICKFRAME, WS_MINIMIZEBOX, and WS_MAXIMIZEBOX styles.
 
WS_VISIBLE
 A window that is initially visible.
 
WS_SYSMENU
 A window that has a window menu on its title bar (when you click the little icon). The WS_CAPTION option must also be specified.
 
WS_BORDER
 A window that has a thin-line border.
 
WS_CAPTION
 A window that has a title bar (the WS_BORDER style is included).
 
WS_MINIMIZE
 A window that is initially minimized.
 
WS_MAXIMIZE
 A window that is initially maximized.
 
-------------------------------------------------------------------------------------------------------------------------------
*/


	// Find out if the window was created
	if( !hWnd ) // If the window was not created,
		return 0; // stop the application

	// Display the window to the user
	ShowWindow(hWnd, SW_SHOWNORMAL);
	UpdateWindow(hWnd);

	// Decode and treat the messages
	// as long as the application is running
	while( GetMessage(&Msg, NULL, 0, 0) )   // ili while(GetMessage(&Msg, NULL, 0, 0) > 0)
	{
             TranslateMessage(&Msg);
             DispatchMessage(&Msg);
	}

	return Msg.wParam;
}
/*



***********************************
GetMessage()
***********************************
GetMessage() gets a message from your application's message queue. Any time the user moves the mouse, types on 
the keyboard, clicks on your window's menu, or does any number of other things, messages are generated by the 
system and entered into your program's message queue. By calling GetMessage() you are requesting the next available 
message to be removed from the queue and returned to you for processing. 
If there is no message, GetMessage() Blocks.

***********************************
TranslateMessage()
***********************************
TranslateMessage() does some additional processing on keyboard events like generating WM_CHAR messages to go along
 with WM_KEYDOWN messages. Finally DispatchMessage() sends the message out to the window that the message was sent to. 
 This could be our main window or it could be another one, or a control, and in some cases a window that was created 
 behind the scenes by the sytem or another program. This isn't something you need to worry about because all we are 
 concerned with is that we get the message and send it out, the system takes care of the rest making sure it gets to 
 the proper window.
*/

LRESULT CALLBACK WndProcedure(HWND hWnd, UINT Msg,
			   WPARAM wParam, LPARAM lParam)
{
    switch(Msg) 
    {
// BEGIN NEW CODE        OSNOVNI DEO PROGRAMA
		        case WM_LBUTTONDOWN:

        {
            char szFileName[MAX_PATH];
            HINSTANCE hInstance = GetModuleHandle(NULL);// GetModuleHandle(NULL) LOKACIJA DOKUMENTA

            GetModuleFileName(hInstance, szFileName, MAX_PATH);
            MessageBox(hWnd, szFileName, "This program is:", MB_OK | MB_ICONINFORMATION);
        }
// END NEW CODE
    // If the user wants to close the application
    case WM_DESTROY:
        // then close it
        PostQuitMessage(WM_QUIT);
        break;
    default:
        // Process the left-over messages
        return DefWindowProc(hWnd, Msg, wParam, lParam);
}      // KRAJ OSNOVNOG DELA PROGRAMA
/* TAJMER 
case WM_CREATE:
     SetTimer (hWnd, 1, 1000, NULL);
     break;
case WM_TIMER:
     switch (wParam)
     {
          case 1:
               //do something
               break;
     }
     break;
case WM_DESTROY:
     KillTimer (hWnd, 1);
     PostQuitMessage(0);
     break;
default:
     return (DefWindowProc(hWnd, messg, wParam, lParam));

*/













    // If something was not done, let it go
    return 0;
} 
/*
***********************************
DefWindowProc
***********************************
We've got a window, but it doesn't do anything except what DefWindowProc() allows it to, 
like be sized, maximised, etc...
*/