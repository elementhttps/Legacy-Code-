#include "stdafx.h"

#include "defineProracuni.h"

#include "GeoML.h"
#include "Math.h"
#include "Performanse_1.h"
#include <fstream.h>

//------------------------------
//funkcije get iz konstruktora

double GEOML::GEOMLgetAiz()
{
       return aiz ;
}

double GEOML::GEOMLgetAkr()
{
       return akr ;
}

double GEOML::GEOMLgetAlfa()
{
       return alfa ;
}

double GEOML::GEOMLgetAul()
{
       return aul ;
}

double GEOML::GEOMLgetBeta()
{
       return beta ;
}

double GEOML::GEOMLgetEE()
{
       return EE ;
}

double GEOML::GEOMLgetLdi()
{
       return Ldi ;
}

double GEOML::GEOMLgetLk()
{
       return Lk ;
}

double GEOML::GEOMLgetLko()
{
       return Lko ;
}

double GEOML::GEOMLgetDkr()
{
	return dkr;
}
/*
double GEOML::GEOMLgetDiz()
{
PERF par;
double EE = par.PERFgetEE();

GEOML geo;
double dkr = geo.GEOMLgetDkr();
//A = r^2*PI  r = sqrt(A/PI)



double diz = EE*dkr;



	return diz;
}
*/

double GEOML::GEOMLgetL()
{

GEOML geo;

double dkr = geo.GEOMLgetDkr();

PERF per;
double EE = per.PERFgetEE();

double diz =dkr*EE;
double alfa = geo.GEOMLgetAlfa();
double PI = 3.14159265359;

	double di = (diz-dkr)/2;

	double L = di/tan((PI*alfa)/180);


	return L; // od kriticnog do izlaznog pp
}
double GEOML::GEOMLgetddidL()
{

GEOML geo;

double dkr = geo.GEOMLgetDkr();
//double L =geo.GEOMLgetL();






PERF per;
double EE = per.PERFgetEE();
double PI = 3.14159265359;


double diz =dkr*EE;
double alfa = geo.GEOMLgetAlfa();
double di = (diz-dkr)/2;

double L = di/tan((PI*alfa)/180);

fstream myfile2;




fstream myfile;
myfile.open ("dDDpodLmlaznika.txt",ios::out);

myfile2.open ("dDDpodLmlaznika[data].txt",ios::out);
for (double dL =0;dL<=L;dL+=0.1)
{

double dd = (tan((PI*alfa)/180))* dL;
double dDD = dkr+2*dd;

myfile<<"dL ="<<dL<<"   mm  "<<"dkr ="<<2*dkr<<"   mm  "<<"EE ="<<EE<<"     "<<"diz ="<<2*diz<<"   mm  "<<"dd ="<<2*dd<<"   mm "<<"Trenutni precnik ="<<2*dDD<<"    mm  "<<"L ="<<L<<"   mm  "<<endl;

myfile2 <<dL<<"  "<<dDD<<endl;
//plot(L,dd);
}

myfile.close();
myfile2.close();
	return dd;
}