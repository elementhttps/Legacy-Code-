#include "stdafx.h"

#include "defineProracuni.h"

#include "Math.h"
#include "Oxidator.h"
#include "TankOxi.h"// za dobijanje zapremine gasa u tanku 
#include <fstream.h>





double OXI::OXIgetcp_oxi()
{
	return cp_oxi;
}

double OXI::OXIgetcv_oxi()
{
	return cv_oxi;
}

double OXI::OXIgetkapa_oxi()
{
	return kapa_oxi;
}

double OXI::OXIgetR_oxi()
{
	return R_oxi;
}

double OXI::OXIgetT_oxi()
{
	return T_oxi;
}

double OXI::OXIgetP_oxi()
{

	return P_oxi;
}
double OXI::OXIgetROoxi()
{
	return ROoxi;
}


#ifdef SA_MASOM
double OXI::OXIgetP_oxi2()
{
OXI oxi;
double Toxi = oxi.OXIgetT_oxi();
double moxi = oxi.m_oxi;
double Roxi = oxi.OXIgetR_oxi();
   GEOXI tank;

double V_oxi = tank.GEOXIgetV_m_tank();

fstream myfile;
myfile.open ("dT.txt",ios::out);

for (double dT= 0;dT<400;dT+=0.1)
{


 double x4 = dT;//+dx;
 
 P_oxi2 = (moxi*Roxi*(273.15+dT))/(V_oxi*10000);
 myfile <<""<<dT<<"  C    "<<P_oxi2<<"    BAR    "<< m_oxi  <<  "Kg      "  << V_oxi << "m3       " <<Roxi<< "KJ/kgK"<<  endl;

 
}
myfile.close();


P_oxi2 = (moxi*Roxi*Toxi)/V_oxi;


	return P_oxi2;
}
#endif 

double OXI::OXIgetm_oxi()
{   
	OXI oxi;
	
double Toxi = oxi.OXIgetT_oxi();
double Poxi = oxi.OXIgetP_oxi();
double Roxi = oxi.OXIgetR_oxi();
   GEOXI tank;

double V_oxi = tank.GEOXIgetV_m_tank();
//dT = 0  C 

m_oxi = (Poxi*V_oxi)/(Roxi*Toxi); //PV = mRT    m = PV/RT
//D_tank =70  mm  V_tank=  0.00630643 m3   A_tank = 0.00384845    m2  L_tank = 1.63869   m    dT = 0  C   dm = 1.1   Kg   P_tank = 90  BAR 

	return m_oxi;
}

//BRZINA ZVUKA ------------------------------------------------//
double OXI::OXIgetc_oxi()
{

/*
//-------------------------------------------------------------------------------------------------------------------------
*************************************************************************************************************************
dBrzinaZvukaPoToxi.txt      POCETAK
*************************************************************************************************************************
//-------------------------------------------------------------------------------------------------------------------------
*/
OXI oxi;
double kapa = oxi.OXIgetkapa_oxi();
double R_oxi = oxi.OXIgetR_oxi();
double P_oxi = oxi.OXIgetP_oxi();

#ifdef pro_CodT
fstream myfile;
myfile.open ("dBrzinaZvukaPoToxi.txt",ios::out);

for (double dT= 273.15;dT<350;dT+=0.1)
{



 double podkoren2 = kapa*R_oxi*dT;
 double c_oxi = pow(podkoren2,0.5);
 myfile <<"T=  "<<dT-273.15<<"  C    "<<"kapa ="<<kapa<<"     "<<"R=  "<< R_oxi  <<  "  J/KgK      "  <<"Brzina Zvuka u OXI tanku =  "<< c_oxi << "  m/s  " <<  endl;

 
}
myfile.close();
#endif
double podkoren = kapa*R_oxi*T_oxi;

c_oxi = pow(podkoren,0.5);

/*
//-------------------------------------------------------------------------------------------------------------------------
*************************************************************************************************************************
dBrzinaZvukaPoToxi.txt      KRAJ
*************************************************************************************************************************
//-------------------------------------------------------------------------------------------------------------------------
*/


	return c_oxi;


}

double OXI::OXIgetV_tanka_kalkulacije()
{


/*
//-------------------------------------------------------------------------------------------------------------------------
*************************************************************************************************************************
Vtankaoxikalkulacije[u nekom rasponu].txt      POCETAK
*************************************************************************************************************************
//-------------------------------------------------------------------------------------------------------------------------
*/
OXI oxi;
double T_oxi = oxi.OXIgetT_oxi();
double R_oxi = oxi.OXIgetR_oxi();
double P_oxi = oxi.OXIgetP_oxi();
double m_oxi = 1.6;//kg
	GEOXI tank;
double D_tank = tank.GEOXIgetD_tank()/1000;//mm
	double PI = 3.14159265359;

 #ifdef pro_TankDUGOTRAJE

fstream myfile;
myfile.open ("Vtankaoxikalkulacije[Dtank125do150].txt",ios::out);
myfile <<"T_oxi ="<<T_oxi-273.15<<" C  "<<"R_oxi=  "<<R_oxi<<"  J/KgK    " <<"P_oxi = "<< P_oxi/100000<< "    BAR  " <<"m_oxi =  "<<m_oxi<<"   Kg"<<endl;
for (double dD= 125;dD<=150;dD+=1)
{
    for(double dT = 273.15; dT <=373.15; dT+=5)
	{
       for (double dm = 1; dm<=5; dm+=0.1)
	   {
 
		   for(double dp = 101325; dp <=10000000;dp+=100000)
		   {
double A_tank= (((dD*dD)*PI)/4)/1000000;//mm2
V_tanka_kalkulacije = (dm*R_oxi*dT)/dp;
double L_tank = V_tanka_kalkulacije/A_tank;
myfile <<"D_tank ="<<dD<<"  mm  "<<"V_tank=  "<<V_tanka_kalkulacije<<" m3   " <<"A_tank = "<< A_tank<< "    m2  " <<"L_tank = " << L_tank<< "   m    "<<"dT = " <<dT-273.15 <<"  C   "<<"dm = "<<dm<<"   Kg   "<<"P_tank = "<< dp/100000 <<"  BAR   "<<"R_oxi=  "<<R_oxi<<"  J/KgK    "<<endl;
		   }
	   }
	}
}

myfile.close();	
#endif
/*
//-------------------------------------------------------------------------------------------------------------------------
*************************************************************************************************************************
Vtankaoxikalkulacije[u nekom rasponu].txt      KRAJ
*************************************************************************************************************************
//-------------------------------------------------------------------------------------------------------------------------
*/	
	
	
	
	return V_tanka_kalkulacije;
}