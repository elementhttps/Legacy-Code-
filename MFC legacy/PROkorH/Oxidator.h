#include "stdafx.h"

#include "Math.h"




#define SA_MASOM   //ako je masa u tanku poznata (program racuna pritisak u boci P = mRT/V)


#define BEZ_MASE  //ako je masa u tanku nepoznata (program racuna masu u boci m = PV/RT)

class OXI
{
public:	
	double cp_oxi; //
    double cv_oxi;
	double kapa_oxi;
	double R_oxi;
	double T_oxi;
	double P_oxi;
	double P_oxi2;
	double m_oxi;
	double ROoxi;
	double c_oxi;
	double V_tanka_kalkulacije;
	///napisati promenu brzine zvuka u zavisnosti od T  c(n2o) = koren iz [ kapa(n2o)*R(n2o)*T(n2o)] u boci  
   //-10 +70 C (+273.16 za K)

	//kako izmeriti staticki pritisak




    /*
     
	//za strujanja M>1 vazi 
- konvergentni deo mlaznika POVECAVA PRITISAK a SMANUJE BRZINU ( suprotno od venturijeve cevi za strujanja M<1)
- divergentni deo mlaznika SMANJUJE PRITISAK a POVECAVA BRZINU



  VAZNO ako dA -> 0 (aka konstantan poprecan presek )
  samo tamo gde je konstantan poprecni presek mogu da se ostvare M=1 !!!!

*/
public:
	OXI()//KONSTRUKTOR KLASE
	{
		double PI = 3.14159265359;
     cp_oxi = 0.7556; //KJ/kgK //0.88
     cv_oxi = 0.5667;//KJ/kgK//0.69
	 kapa_oxi =cp_oxi/cv_oxi ;
	 R_oxi = (cp_oxi-cv_oxi)*1000;// KJ/kgK
	 T_oxi = 273.15+(25);//K
	 P_oxi = (50)*100000;//Paskali
     ROoxi = 1.799; //   g/L tj kg/m3
	 #ifdef SA_MASOM //ako je masa gasa u tanku poznata staviti vrednost ovde 
	 m_oxi = 5;  //kg
	 #endif



	}

//---------------------------------
//get funkcije u klasi //omogucava pristup vrednostima proracunatim u xxx.ccp YEAH ! fajlu
double OXIgetcp_oxi();
double OXIgetcv_oxi();
double OXIgetkapa_oxi();
double OXIgetR_oxi();
double OXIgetT_oxi();
double OXIgetP_oxi();
double OXIgetm_oxi();
double OXIgetROoxi();
double OXIgetc_oxi();



#ifdef SA_MASOM
double OXIgetP_oxi2();
#endif


double OXIgetV_tanka_kalkulacije();
//double OXIget

};