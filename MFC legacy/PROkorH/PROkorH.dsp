# Microsoft Developer Studio Project File - Name="PROkorH" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Application" 0x0101

CFG=PROkorH - Win32 Debug
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "PROkorH.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "PROkorH.mak" CFG="PROkorH - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "PROkorH - Win32 Release" (based on "Win32 (x86) Application")
!MESSAGE "PROkorH - Win32 Debug" (based on "Win32 (x86) Application")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName ""
# PROP Scc_LocalPath ""
CPP=cl.exe
MTL=midl.exe
RSC=rc.exe

!IF  "$(CFG)" == "PROkorH - Win32 Release"

# PROP BASE Use_MFC 6
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 6
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "Release"
# PROP Intermediate_Dir "Release"
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MD /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_AFXDLL" /Yu"stdafx.h" /FD /c
# ADD CPP /nologo /MD /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_AFXDLL" /D "_MBCS" /Yu"stdafx.h" /FD /c
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x409 /d "NDEBUG" /d "_AFXDLL"
# ADD RSC /l 0x409 /d "NDEBUG" /d "_AFXDLL"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 /nologo /subsystem:windows /machine:I386
# ADD LINK32 /nologo /subsystem:windows /machine:I386

!ELSEIF  "$(CFG)" == "PROkorH - Win32 Debug"

# PROP BASE Use_MFC 6
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 6
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "Debug"
# PROP Intermediate_Dir "Debug"
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MDd /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_AFXDLL" /Yu"stdafx.h" /FD /GZ /c
# ADD CPP /nologo /MDd /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_AFXDLL" /D "_MBCS" /FR /Yu"stdafx.h" /FD /GZ /c
# ADD BASE MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x409 /d "_DEBUG" /d "_AFXDLL"
# ADD RSC /l 0x409 /d "_DEBUG" /d "_AFXDLL"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 /nologo /subsystem:windows /debug /machine:I386 /pdbtype:sept
# ADD LINK32 /nologo /subsystem:windows /debug /machine:I386 /pdbtype:sept

!ENDIF 

# Begin Target

# Name "PROkorH - Win32 Release"
# Name "PROkorH - Win32 Debug"
# Begin Group "Source Files"

# PROP Default_Filter "cpp;c;cxx;rc;def;r;odl;idl;hpj;bat"
# Begin Source File

SOURCE=.\ChildFrm.cpp
# End Source File
# Begin Source File

SOURCE=.\MainFrm.cpp
# End Source File
# Begin Source File

SOURCE=.\PROkorH.cpp
# End Source File
# Begin Source File

SOURCE=.\PROkorH.rc
# End Source File
# Begin Source File

SOURCE=.\PROkorHDoc.cpp
# End Source File
# Begin Source File

SOURCE=.\PROkorHView.cpp
# End Source File
# Begin Source File

SOURCE=.\StdAfx.cpp
# ADD CPP /Yc"stdafx.h"
# End Source File
# End Group
# Begin Group "Header Files"

# PROP Default_Filter "h;hpp;hxx;hm;inl"
# Begin Source File

SOURCE=.\ChildFrm.h
# End Source File
# Begin Source File

SOURCE=.\MainFrm.h
# End Source File
# Begin Source File

SOURCE=.\PROkorH.h
# End Source File
# Begin Source File

SOURCE=.\PROkorHDoc.h
# End Source File
# Begin Source File

SOURCE=.\PROkorHView.h
# End Source File
# Begin Source File

SOURCE=.\Resource.h
# End Source File
# Begin Source File

SOURCE=.\StdAfx.h
# End Source File
# End Group
# Begin Group "Resource Files"

# PROP Default_Filter "ico;cur;bmp;dlg;rc2;rct;bin;rgs;gif;jpg;jpeg;jpe"
# Begin Source File

SOURCE=.\hibmereB.bmp
# End Source File
# Begin Source File

SOURCE=.\res\PROkorH.ico
# End Source File
# Begin Source File

SOURCE=.\res\PROkorH.rc2
# End Source File
# Begin Source File

SOURCE=.\res\PROkorHDoc.ico
# End Source File
# Begin Source File

SOURCE=.\res\Toolbar.bmp
# End Source File
# End Group
# Begin Group "Gometrija"

# PROP Default_Filter ""
# Begin Group "Geometrija_Mlaznika"

# PROP Default_Filter ""
# Begin Source File

SOURCE=.\GeoML.cpp
# End Source File
# Begin Source File

SOURCE=.\GeoML.h
# End Source File
# End Group
# Begin Group "Geometrija_Tank_oxi"

# PROP Default_Filter ""
# Begin Source File

SOURCE=.\TankOxi.cpp
# End Source File
# Begin Source File

SOURCE=.\TankOxi.h
# End Source File
# End Group
# End Group
# Begin Group "Velicine_Stanja_po_PP"

# PROP Default_Filter ""
# Begin Group "Velicine_Stanja_Mlaznik"

# PROP Default_Filter ""
# Begin Source File

SOURCE=.\CRMM.CPP
# End Source File
# Begin Source File

SOURCE=.\CRMM.H
# End Source File
# End Group
# Begin Group "Velicine_Stanja_Tank_oxi"

# PROP Default_Filter ""
# Begin Source File

SOURCE=.\Oxidator.cpp
# End Source File
# Begin Source File

SOURCE=.\Oxidator.h
# End Source File
# End Group
# End Group
# Begin Group "Preseci na HIB"

# PROP Default_Filter ""
# Begin Group "PRESEK_xcevi"

# PROP Default_Filter ""
# Begin Source File

SOURCE=.\xcevi_mp.cpp
# End Source File
# Begin Source File

SOURCE=.\xcevi_mp.h
# End Source File
# End Group
# Begin Group "PRESEK_x1_x2"

# PROP Default_Filter ""
# Begin Source File

SOURCE=.\x1x2.cpp
# End Source File
# Begin Source File

SOURCE=.\x1x2.h
# End Source File
# End Group
# Begin Group "Sagorevanje Modeli xgo2_xgo1"

# PROP Default_Filter ""
# Begin Source File

SOURCE=.\xgo12.cpp
# End Source File
# Begin Source File

SOURCE=.\xgo12.h
# End Source File
# End Group
# End Group
# Begin Group "Performanse_HIB"

# PROP Default_Filter ""
# Begin Source File

SOURCE=.\Performanse_HIB.cpp
# End Source File
# Begin Source File

SOURCE=.\Performanse_HIB.h
# End Source File
# End Group
# Begin Group "Dinamika_Leta_v1"

# PROP Default_Filter ""
# Begin Source File

SOURCE=.\DLetav1.cpp
# End Source File
# Begin Source File

SOURCE=.\DLetav1.h
# End Source File
# End Group
# Begin Group "definisati koje proracune"

# PROP Default_Filter ""
# Begin Source File

SOURCE=.\defineProracuni.h
# End Source File
# Begin Source File

SOURCE=.\KONTROLA.CPP
# End Source File
# Begin Source File

SOURCE=.\KONTROLA.H
# End Source File
# End Group
# Begin Source File

SOURCE=.\PROkorH.reg
# End Source File
# Begin Source File

SOURCE=.\ReadMe.txt
# End Source File
# End Target
# End Project
