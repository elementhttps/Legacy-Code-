// PROkorH.h : main header file for the PROKORH application
//

#if !defined(AFX_PROKORH_H__1A255656_1D77_4689_9AC2_4AC686F9FC64__INCLUDED_)
#define AFX_PROKORH_H__1A255656_1D77_4689_9AC2_4AC686F9FC64__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"       // main symbols

/////////////////////////////////////////////////////////////////////////////
// CPROkorHApp:
// See PROkorH.cpp for the implementation of this class
//

class CPROkorHApp : public CWinApp
{
public:
	CPROkorHApp();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CPROkorHApp)
	public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL

// Implementation
	//{{AFX_MSG(CPROkorHApp)
	afx_msg void OnAppAbout();
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_PROKORH_H__1A255656_1D77_4689_9AC2_4AC686F9FC64__INCLUDED_)
