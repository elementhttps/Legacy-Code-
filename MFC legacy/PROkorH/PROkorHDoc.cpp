// PROkorHDoc.cpp : implementation of the CPROkorHDoc class
//

#include "stdafx.h"
#include "PROkorH.h"

#include "PROkorHDoc.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CPROkorHDoc

IMPLEMENT_DYNCREATE(CPROkorHDoc, CDocument)

BEGIN_MESSAGE_MAP(CPROkorHDoc, CDocument)
	//{{AFX_MSG_MAP(CPROkorHDoc)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CPROkorHDoc construction/destruction

CPROkorHDoc::CPROkorHDoc()
{
	// TODO: add one-time construction code here

}

CPROkorHDoc::~CPROkorHDoc()
{
}

BOOL CPROkorHDoc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;

	// TODO: add reinitialization code here
	// (SDI documents will reuse this document)

	return TRUE;
}



/////////////////////////////////////////////////////////////////////////////
// CPROkorHDoc serialization

void CPROkorHDoc::Serialize(CArchive& ar)
{
	if (ar.IsStoring())
	{
		// TODO: add storing code here
	}
	else
	{
		// TODO: add loading code here
	}
}

/////////////////////////////////////////////////////////////////////////////
// CPROkorHDoc diagnostics

#ifdef _DEBUG
void CPROkorHDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CPROkorHDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CPROkorHDoc commands
