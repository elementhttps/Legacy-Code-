// PROkorHView.h : interface of the CPROkorHView class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_PROKORHVIEW_H__885D197A_DD87_4BC8_81D5_266248440EB1__INCLUDED_)
#define AFX_PROKORHVIEW_H__885D197A_DD87_4BC8_81D5_266248440EB1__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


class CPROkorHView : public CView
{
protected: // create from serialization only
	CPROkorHView();
	DECLARE_DYNCREATE(CPROkorHView)

// Attributes
public:
	CPROkorHDoc* GetDocument();

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CPROkorHView)
	public:
	virtual void OnDraw(CDC* pDC);  // overridden to draw this view
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	protected:
	virtual BOOL OnPreparePrinting(CPrintInfo* pInfo);
	virtual void OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo);
	virtual void OnEndPrinting(CDC* pDC, CPrintInfo* pInfo);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CPROkorHView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

	int m_Zumx;
	int m_Zumy;
// Generated message map functions
protected:
	//{{AFX_MSG(CPROkorHView)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

#ifndef _DEBUG  // debug version in PROkorHView.cpp
inline CPROkorHDoc* CPROkorHView::GetDocument()
   { return (CPROkorHDoc*)m_pDocument; }
#endif

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_PROKORHVIEW_H__885D197A_DD87_4BC8_81D5_266248440EB1__INCLUDED_)
