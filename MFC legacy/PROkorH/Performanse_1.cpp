#include "stdafx.h"
#include "Math.h"
#include "Performanse_1.h"
#include "GeoML.h"
#include "CRMM.h"
#include <fstream.h>// data stream
//-----------------------------------------------------------

//-----------------------------------------------------------
double PERF::PERFgetEE()
{

CCRM fluid;
double po = fluid.CCRMgetP_kom();
double pa = fluid.CCRMgetP_amb();
double kapa =fluid.CCRMgetKapa();

GEOML geom;
double dkr = geom.GEOMLgetAkr();
double diz = geom.GEOMLgetAiz();



//1. clan
double par1 =(2/(kapa+1));
double par2 =(1/(kapa-1));
double par3 = pow(par1,par2);

//2.clan
double par4 = (kapa-1)/(kapa+1);
double par5 = sqrt(par4);

//3clan
double par6 = po/pa;
double par7 = 1/kapa;
double par8 = pow(par6,par7);

//4. clan
double par9 = (1-kapa)/kapa;
double par10 =pow(par6,par9);
double par11 = 1-par10;
double par12 = sqrt(par11);
double par13 = 1/par12;

double EE = par3*par5*par8*par13;


return EE;

}

//-----------------------------------------------------------

//-----------------------------------------------------------
double PERF::PERFgetdP()

{
double po = fluid.CCRMgetP_kom();
double pi = fluid.CCRMgetP_izl();

	double dP = pi/po;

    return dP;



}

//-----------------------------------------------------------

//-----------------------------------------------------------
double PERF::PERFgetG() //parametar G funkcija od kape
{
CCRM fluid;
double kapa = fluid.CCRMgetKapa();


double par1 = sqrt(kapa);
double par2 = 2/(kapa+1);
double par3 =(kapa+1)/(2*(kapa-1));
double par4 = pow(par2,par3);
double G =par1*par4;

return G;
}


//-----------------------------------------------------------

//-----------------------------------------------------------
double PERF::PERFgetCZZ()
{
CCRM fluid;
double R = fluid.CCRMgetRgg();
double To=fluid.CCRMgetT_kom(); // T U KOMORI = To

PERF par;
double G = par.PERFgetG();


double par1 = R*To;
double par2 = sqrt(par1);
double CZZ =par2/G;

return CZZ;
}

//-----------------------------------------------------------

//-----------------------------------------------------------
double PERF::PERFgetCf_Adaptiran()
{
CCRM fluid;
double kapa = fluid.CCRMgetKapa();
double po = fluid.CCRMgetP_kom();
double pi = fluid.CCRMgetP_izl();

PERF par;
double G = par.PERFgetG();

double par1 = (2*kapa)/(kapa-1); //
double par2 = (kapa-1)/kapa;   //
double par3 = pi/po;   //NAPOMENA MORA PRITISAK DA SE IZ PASKALA PRETVORI U BARE (1*100000) JER C 1/4500000 RACUNA KAO 0
double par4 = pow(par3,par2);
double par5 =(1-par4);
double par6 = par1*par5;
double par7 = sqrt(par6);
//
double Cf_Adaptiran = G*par7;  // OK 


return Cf_Adaptiran;
}



double PERF::PERFgetIsp()
{



	PERF par;
	double CZZ = par.PERFgetCZZ();
	double Cf =par.PERFgetCf_Neadaptiran();
	
	double Isp = CZZ*Cf;
	
	
	return Isp;

}


double PERF::PERFgetVi()
{

CCRM fluid;
double kapa = fluid.CCRMgetKapa();
double Rgg = fluid.CCRMgetRgg();
double To=fluid.CCRMgetT_kom();
double Pi = fluid.CCRMgetP_izl();
double Pkom =fluid.CCRMgetP_kom();

double par1 = (2*kapa)/(kapa-1);
double par2 = Rgg*To;
double par3 =(kapa-1)/kapa;
double par4 = Pi/Pkom;
double par5 =pow(par4,par3);
double par6 =1-par5;
double par7 = par1*par2*par6;

double par8 = sqrt(par7);



return par8,par1;
}

double PERF::PERFgetVkr()
{
CCRM fluid;
double kapa = fluid.CCRMgetKapa();
double R = fluid.CCRMgetRgg();
double To=fluid.CCRMgetT_kom();



double par1 =(2*kapa)/(kapa+1);
double par2 =R*To;
double par3 =par1*par2;
double Vkr =sqrt(par3);


return Vkr;
}







double PERF::PERFgetPkr()
{

CCRM fluid;
double kapa = fluid.CCRMgetKapa();
double po = fluid.CCRMgetP_kom();

double par1 = 2/(kapa+1);
double par2 =kapa/(kapa-1);
double par3 = pow(par1,par2);
double Pkr = par3*po;


return Pkr;
}



double PERF::PERFgetTi()
{
CCRM fluid;
double kapa = fluid.CCRMgetKapa();
double po = fluid.CCRMgetP_kom();
double Pi = fluid.CCRMgetP_izl();
double To=fluid.CCRMgetT_kom();


double par1 = Pi/po; 
double par2 =(kapa-1)/kapa;
double par3 = pow(par1,par2);
double Ti = par3*To;


return Ti;
}

double PERF::PERFgetTkr()
{
CCRM fluid;
double kapa = fluid.CCRMgetKapa();
double To=fluid.CCRMgetT_kom();

double Tkr = (2*To)/(kapa+1);

return Tkr;
}



double PERF::PERFgetMaseniProtok()
{

   PERF par;
   double G = par.PERFgetG();
   
   CCRM fluid;

double po = fluid.CCRMgetP_kom();
double Rgg = fluid.CCRMgetRgg();
double To=fluid.CCRMgetT_kom();

GEOML geo;
double Akr = geo.GEOMLgetAkr();




double par1 = po*Akr;
double par2 = Rgg*To;
double par3 = sqrt(par2);
double par4 = par1/par3;

double mp= G*par4;
return mp;

}

double PERF::PERFgetVremeRadaRM()
{
	return n_tr;
}


double PERF::PERFgetPotisak()
{

	PERF par;
    double mpp = par.PERFgetMaseniProtok();
	double Isp = par.PERFgetIsp();

	double Fp; //intenzitet sile potiska


	Fp = mpp*Isp;

	return Fp;


}


double PERF::PERFgetFi()
{

GEOML geo;
double alfa = geo.GEOMLgetAlfa();

double PI = 3.14159265359;

double par1 = cos((alfa*PI)/180);
double fi = (1+par1)/2;

return fi;

}

double PERF::PERFgetPotisakII()
{


 CCRM fluid;

double pi = fluid.CCRMgetP_izl();
double pa = fluid.CCRMgetP_amb();


fstream myfile;	
myfile.open ("data5.txt",ios::out);

	
PERF par;

double fi = par.PERFgetFi();
double mpp = par.PERFgetMaseniProtok();
double vi = par.PERFgetVi();
double EE = par.PERFgetEE();

GEOML geo;
double Akr = geo.GEOMLgetAkr();

for(int i = 1; i<1000;i++)
{

double Aiz = Akr*EE;
myfile <<"Akr  "<<Akr+i<<endl;
double par1 =fi*mpp*vi;

double par2 = (pi-pa)*Aiz;

double FpII;
myfile << par1+par2 <<endl;

}	myfile.close();
double Aiz = Akr*EE;

double par1 =fi*mpp*vi;

double par2 = (pi-pa)*Aiz;

double FpII = par1+par2;







return FpII;



}


double PERF::PERFgetCf_Neadaptiran()
{

PERF par;
double cf_a = par.PERFgetCf_Adaptiran();
double EE = par.PERFgetEE();

CCRM fluid;

double pi = fluid.CCRMgetP_izl();
double pa = fluid.CCRMgetP_amb();
double po = fluid.CCRMgetP_kom();



double par1 = pi/po;
double par2 = pa/po;

double par3 = EE*(par1-par2);

double cf = cf_a+par3;



return cf;
}


/**/