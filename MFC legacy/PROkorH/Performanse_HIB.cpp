#include "stdafx.h"
#include "Math.h"

#include "defineProracuni.h"

#include "Performanse_HIB.h"
#include "GeoML.h"
#include "CRMM.h"
#include <fstream.h>// data stream
//-----------------------------------------------------------

//-----------------------------------------------------------
double PERF::PERFgetEE()
{

CCRM fluid;
double po = fluid.CCRMgetP_kom();
double pa = fluid.CCRMgetP_amb();
double kapa =fluid.CCRMgetKapa();





//1. clan
double par1 =(2/(kapa+1));
double par2 =(1/(kapa-1));
double par3 = pow(par1,par2);

//2.clan
double par4 = (kapa-1)/(kapa+1);
double par5 = sqrt(par4);

//3clan
double par6 = po/pa;
double par7 = 1/kapa;
double par8 = pow(par6,par7);

//4. clan
double par9 = (1-kapa)/kapa;
double par10 =pow(par6,par9);
double par11 = 1-par10;
double par12 = sqrt(par11);
double par13 = 1/par12;

double EE = par3*par5*par8*par13;


return EE;

}

//-----------------------------------------------------------

//-----------------------------------------------------------
double PERF::PERFgetdP()

{
CCRM fluid;
double po = fluid.CCRMgetP_kom();
double pi = fluid.CCRMgetP_amb();

double dP = pi/po;
    return dP;



}

//-----------------------------------------------------------

//-----------------------------------------------------------
double PERF::PERFgetG() //parametar G funkcija od kape
{
CCRM fluid;
double kapa = fluid.CCRMgetKapa();


double par1 = sqrt(kapa);
double par2 = 2/(kapa+1);
double par3 =(kapa+1)/(2*(kapa-1));
double par4 = pow(par2,par3);
double G =par1*par4;

return G;
}


//-----------------------------------------------------------

//-----------------------------------------------------------
double PERF::PERFgetCZZ()
{
CCRM fluid;
double R = fluid.CCRMgetRgg();
double To=fluid.CCRMgetT_kom(); // T U KOMORI = To
double kapa = fluid.CCRMgetKapa();


double korenizkapa = sqrt(kapa);
double pod_kv = 2/(kapa+1);
double kv =(kapa+1)/(2*(kapa-1));
double kvadrat = pow(pod_kv,kv);
double G =korenizkapa*kvadrat;


double RTo = R*To;
double KorenIzRTo = sqrt(RTo);
double CZZ =KorenIzRTo/G;

return CZZ;
}

//-----------------------------------------------------------

//-----------------------------------------------------------
double PERF::PERFgetCf_Adaptiran()
{
CCRM fluid;
double kapa = fluid.CCRMgetKapa();
double po = fluid.CCRMgetP_kom();
double pi = fluid.CCRMgetP_izl();

double korenizkapa = sqrt(kapa);
double pod_kv = 2/(kapa+1);
double kv =(kapa+1)/(2*(kapa-1));
double kvadrat = pow(pod_kv,kv);
double G =korenizkapa*kvadrat;


double par1 = (2*kapa)/(kapa-1); //
double par2 = (kapa-1)/kapa;   //
double par3 = pi/po;   //NAPOMENA MORA PRITISAK DA SE IZ PASKALA PRETVORI U BARE (1*100000) JER C 1/4500000 RACUNA KAO 0
double par4 = pow(par3,par2);
double par5 =(1-par4);
double par6 = par1*par5;
double par7 = sqrt(par6);
//
double Cf_Adaptiran = G*par7;  // OK 


return Cf_Adaptiran;
}



double PERF::PERFgetIsp()
{

CCRM fluid;
double kapa = fluid.CCRMgetKapa();
double po = fluid.CCRMgetP_kom();
double pi = fluid.CCRMgetP_izl();
double R = fluid.CCRMgetRgg();
double To=fluid.CCRMgetT_kom(); // T U KOMORI = To

//---------------------------------------------------
//PRORACUN KOEFICIJENTA G
double korenizkapa = sqrt(kapa);
double pod_kv = 2/(kapa+1);
double kv =(kapa+1)/(2*(kapa-1));
double kvadrat = pow(pod_kv,kv);
double G =korenizkapa*kvadrat;
//----------------------------------------------------

//----------------------------------------------------
//PRORACUN ZA CF_ADAPTIRAN Pe = Pamb
double par1 = (2*kapa)/(kapa-1); //
double par2 = (kapa-1)/kapa;   //
double par3 = pi/po;   //NAPOMENA MORA PRITISAK DA SE IZ PASKALA PRETVORI U BARE (1*100000) JER C 1/4500000 RACUNA KAO 0
double par4 = pow(par3,par2);
double par5 =(1-par4);
double par6 = par1*par5;
double par7 = sqrt(par6);

double Cf_Adaptiran = G*par7;  // OK 
//----------------------------------------------------	

//----------------------------------------------------
//PRORACUN ZA CZZ
double RTo = R*To;
double KorenIzRTo = sqrt(RTo);
double CZZ =KorenIzRTo/G;
//----------------------------------------------------



	double Isp = CZZ*Cf_Adaptiran;
	
	
	return Isp;

}


double PERF::PERFgetVi()
{

CCRM fluid;
double kapa = fluid.CCRMgetKapa();
double Rgg = fluid.CCRMgetRgg();
double To=fluid.CCRMgetT_kom();
double Pi = fluid.CCRMgetP_izl();
double Pkom =fluid.CCRMgetP_kom();

double par1 = (2*kapa)/(kapa-1);
double par2 = Rgg*To;
double par3 =(kapa-1)/kapa;
double par4 = Pi/Pkom;
double par5 =pow(par4,par3);
double par6 =1-par5;
double par7 = par1*par2*par6;

double par8 = sqrt(par7);



return par8;
}

double PERF::PERFgetVkr()
{
CCRM fluid;
double kapa = fluid.CCRMgetKapa();
double R = fluid.CCRMgetRgg();
double To=fluid.CCRMgetT_kom();



double par1 =(2*kapa)/(kapa+1);
double par2 =R*To;
double par3 =par1*par2;
double Vkr =sqrt(par3);


return Vkr;
}







double PERF::PERFgetPkr()
{

CCRM fluid;
double kapa = fluid.CCRMgetKapa();
double po = fluid.CCRMgetP_kom();

double par1 = 2/(kapa+1);
double par2 =kapa/(kapa-1);
double par3 = pow(par1,par2);
double Pkr = par3*po;


return Pkr;
}



double PERF::PERFgetTi()
{
CCRM fluid;
double kapa = fluid.CCRMgetKapa();
double po = fluid.CCRMgetP_kom();
double Pi = fluid.CCRMgetP_izl();
double To=fluid.CCRMgetT_kom();


double par1 = Pi/po; 
double par2 =(kapa-1)/kapa;
double par3 = pow(par1,par2);
double Ti = par3*To;


return Ti;
}

double PERF::PERFgetTkr()
{
CCRM fluid;
double kapa = fluid.CCRMgetKapa();
double To=fluid.CCRMgetT_kom();

double Tkr = (2*To)/(kapa+1);

return Tkr;
}



double PERF::PERFgetMaseniProtok()
{
 
CCRM fluid;
double kapa = fluid.CCRMgetKapa();
double po = fluid.CCRMgetP_kom();
double pi = fluid.CCRMgetP_izl();
double To=fluid.CCRMgetT_kom(); // T U KOMORI = To

double Rgg = fluid.CCRMgetRgg();


double korenizkapa = sqrt(kapa);
double pod_kv = 2/(kapa+1);
double kv =(kapa+1)/(2*(kapa-1));
double kvadrat = pow(pod_kv,kv);
double G =korenizkapa*kvadrat;


GEOML geo;
double Akr = geo.GEOMLgetAkr();




double poAkr = po*Akr;
double RTo = Rgg*To;
double korenRTo = sqrt(RTo);
double koef = poAkr/korenRTo;

double mp= G*koef;
return mp;

}



double PERF::PERFgetPotisak()
{

//PRVO DEFINISATI DA LI TREBA DA SE IZRACUNAJU VREDNOSTI U .h FAJLU !!!
#ifdef proPOTISAKvariacije
/*
//-------------------------------------------------------------------------------------------------------------------------
*************************************************************************************************************************
Potisak[varijacije].txt      POCETAK
*************************************************************************************************************************
//-------------------------------------------------------------------------------------------------------------------------
*/
fstream Potisak;
Potisak.open ("Potisak[varijacije].txt",ios::out);


CCRM fluid;
double kapa = fluid.CCRMgetKapa();

double pi = fluid.CCRMgetP_izl();
double Rgg = fluid.CCRMgetRgg();
double To=fluid.CCRMgetT_kom();
double pa = fluid.CCRMgetP_amb();

GEOML geo;
double Akr2 = geo.GEOMLgetAkr();
double dkr = geo.GEOMLgetDkr();//u mm
double Dkr = 2*dkr;// U mm !!!

Potisak <<"kapa=  "<<kapa<<"      "<<endl;//"A_cevi=  "<<A_cevi2*1000000<<"    mm2  "  <<"MaseniProtokKr ="<<MaseniProtokKr3<<" Kg/s    "<<"T_oxi=   " <<T_oxi-273.15<<"   C  "<<"kapa=  "<<kapa<<"      "<<"R_oxi=  "<<R_oxi<<"  J/KgK    "<<"P_oxi=   "<<P_oxi/100000<<"    BAR   " << endl;
Potisak <<"pi=  "<<pi/100000<<" BAR     "<<endl;
Potisak <<"Rgg=  "<<Rgg<<"  J/KgK    "<<endl;

for (double dpo= 1500000;dpo<=7000000;dpo+=100000)
{
	for (double dTo= 473.15;dTo<=2000;dTo+=10)//
	{
     	for (double ddkr= 0;ddkr<=50;ddkr+=0.5)//0.5
		{
	
double PI = 3.14159265359;

//double po = dpo;//fluid.CCRMgetP_kom();

	double dkr_na_kv = pow(ddkr/1000,2);
double dAkr2 = dkr_na_kv*PI; 


double korenizkapa = sqrt(kapa);
double pod_kv = 2/(kapa+1);
double kv =(kapa+1)/(2*(kapa-1));
double kvadrat = pow(pod_kv,kv);
double G =korenizkapa*kvadrat;



double poAkr = dpo*dAkr2;
double RTo = Rgg*dTo;
double korenRTo = sqrt(RTo);
double koef = poAkr/korenRTo;

double mpp= G*koef;


//----------------------------------------------------
//PRORACUN ZA CF_ADAPTIRAN Pe = Pamb
double par1 = (2*kapa)/(kapa-1); //
double par2 = (kapa-1)/kapa;   //
double par3 = pi/dpo;   //NAPOMENA MORA PRITISAK DA SE IZ PASKALA PRETVORI U BARE (1*100000) JER C 1/4500000 RACUNA KAO 0
double par4 = pow(par3,par2);
double par5 =(1-par4);
double par6 = par1*par5;
double par7 = sqrt(par6);

double Cf_Adaptiran = G*par7;  // OK 
//----------------------------------------------------	

//----------------------------------------------------
//PRORACUN ZA CZZ
double RTo1 = Rgg*dTo;
double KorenIzRTo = sqrt(RTo1);
double CZZ =KorenIzRTo/G;
//----------------------------------------------------

double Isp = CZZ*Cf_Adaptiran;



double Fp; //intenzitet sile potiska


	Fp = mpp*Isp;


double po = fluid.CCRMgetP_kom();
	
//1. clan
double par11 =(2/(kapa+1));
double par12 =(1/(kapa-1));
double par13 = pow(par11,par12);

//2.clan
double par14 = (kapa-1)/(kapa+1);
double par15 = sqrt(par14);

//3clan
double par16 = po/pa;
double par17 = 1/kapa;
double par18 = pow(par16,par17);

//4. clan
double par19 = (1-kapa)/kapa;
double par110 =pow(par16,par19);
double par111 = 1-par110;
double par112 = sqrt(par111);
double par113 = 1/par112;

double EE = par13*par15*par18*par113;

double diz=EE*ddkr ;


double alfa = geo.GEOMLgetAlfa();



double par1a = cos((alfa*PI)/180);
double fi = (1+par1a)/2;

double par21 = (2*kapa)/(kapa-1);
double par22 = Rgg*dTo;
double par23 =(kapa-1)/kapa;
double par24 = pi/dpo;
double par25 =pow(par24,par23);
double par26 =1-par25;
double par27 = par21*par22*par26;

double Vi = sqrt(par27);


double Aiz = dAkr2*EE;

double par1b =fi*mpp*Vi;

double par2b = (pi-pa)*Aiz;

double FpII = par1b+par2b;

	


	double di = (diz-ddkr)/2;

	double L_ml = di/tan((PI*alfa)/180);


Potisak << "Fp=" << Fp<<" N  "<< "Fp2=" << FpII<<" N  "<< "fi=" << fi<<" "<<"Vi="<<Vi<<"  m/s "<<"po="<<dpo/100000<<" BAR   " <<"dTo="<<dTo-273.15<<" C  "<<"Dkr_precnik="<<2*ddkr<<"  mm    "<<"EE="<<EE<<"    "<<"Diz_precnik="<<2*diz<<"  mm    "<<"mpp="<<mpp<<"  Kg/s    "<<"Isp="<<Isp<<"  Ns    "<<"alfa= "<<alfa<<"   "<<"L_ml= "<<L_ml<<"  mm "<<endl;
		}
	}
}

Potisak.close();
/*
//-------------------------------------------------------------------------------------------------------------------------
*************************************************************************************************************************
Potisak[varijacije].txt  KRAJ
*************************************************************************************************************************
//-------------------------------------------------------------------------------------------------------------------------
*/


/*
//-------------------------------------------------------------------------------------------------------------------------
*************************************************************************************************************************
Potisak[DATA].txt  POCETAK
*************************************************************************************************************************
//-------------------------------------------------------------------------------------------------------------------------
*/



double po = fluid.CCRMgetP_kom();

double korenizkapa = sqrt(kapa);
double pod_kv = 2/(kapa+1);
double kv =(kapa+1)/(2*(kapa-1));
double kvadrat = pow(pod_kv,kv);
double G =korenizkapa*kvadrat;



double poAkr = dpo*Akr2;
double RTo = Rgg*To;
double korenRTo = sqrt(RTo);
double koef = poAkr/korenRTo;

double mpp= G*koef;


//1. clan
double par11 =(2/(kapa+1));
double par12 =(1/(kapa-1));
double par13 = pow(par11,par12);

//2.clan
double par14 = (kapa-1)/(kapa+1);
double par15 = sqrt(par14);

//3clan
double par16 = po/pa;
double par17 = 1/kapa;
double par18 = pow(par16,par17);

//4. clan
double par19 = (1-kapa)/kapa;
double par110 =pow(par16,par19);
double par111 = 1-par110;
double par112 = sqrt(par111);
double par113 = 1/par112;

double EE = par13*par15*par18*par113;



double alfa = geo.GEOMLgetAlfa();

double PI = 3.14159265359;

double par1a = cos((alfa*PI)/180);
double fi = (1+par1a)/2;

	PERF per;
double 	Vi=       per.PERFgetVi();
double 	Ti=		per.PERFgetTi();
double 	Tkr=		per.PERFgetTkr();
double 	Vkr=		per.PERFgetVkr();




double Aiz = Akr2*EE;

double par1b =fi*mpp*Vi;

double par2b = (pi-pa)*Aiz;

double FpII = par1b+par2b;



double diz=EE*dkr ; 	


	double di = (diz-dkr)/2;

	double L_ml = di/tan((PI*alfa)/180);

double dkr2 = geo.GEOMLgetDkr();//u mm
double vremeR =5;


fstream myfile2;
myfile2.open ("PotisakPritisak_data.txt",ios::out);



for (double dpo2= 100000;dpo2<=20000000;dpo2+=100000)
{
double ddkr=geo.GEOMLgetDkr();
double dTo = fluid.CCRMgetT_kom();
	
double PI = 3.14159265359;

//double po = dpo2;//fluid.CCRMgetP_kom();

	double dkr_na_kv = pow(ddkr/1000,2);
double dAkr2 = dkr_na_kv*PI; 


double korenizkapa = sqrt(kapa);
double pod_kv = 2/(kapa+1);
double kv =(kapa+1)/(2*(kapa-1));
double kvadrat = pow(pod_kv,kv);
double G =korenizkapa*kvadrat;



double poAkr = dpo2*dAkr2;
double RTo = Rgg*dTo;
double korenRTo = sqrt(RTo);
double koef = poAkr/korenRTo;

double mpp= G*koef;


//----------------------------------------------------
//PRORACUN ZA CF_ADAPTIRAN Pe = Pamb
double par1 = (2*kapa)/(kapa-1); //
double par2 = (kapa-1)/kapa;   //
double par3 = pi/dpo2;   //NAPOMENA MORA PRITISAK DA SE IZ PASKALA PRETVORI U BARE (1*100000) JER C 1/4500000 RACUNA KAO 0
double par4 = pow(par3,par2);
double par5 =(1-par4);
double par6 = par1*par5;
double par7 = sqrt(par6);

double Cf_Adaptiran = G*par7;  // OK 
//----------------------------------------------------	

//----------------------------------------------------
//PRORACUN ZA CZZ
double RTo1 = Rgg*dTo;
double KorenIzRTo = sqrt(RTo1);
double CZZ =KorenIzRTo/G;
//----------------------------------------------------

double Isp = CZZ*Cf_Adaptiran;



double Fp; //intenzitet sile potiska


	Fp = mpp*Isp;


double po = fluid.CCRMgetP_kom();
	
//1. clan
double par11 =(2/(kapa+1));
double par12 =(1/(kapa-1));
double par13 = pow(par11,par12);

//2.clan
double par14 = (kapa-1)/(kapa+1);
double par15 = sqrt(par14);

//3clan
double par16 = po/pa;
double par17 = 1/kapa;
double par18 = pow(par16,par17);

//4. clan
double par19 = (1-kapa)/kapa;
double par110 =pow(par16,par19);
double par111 = 1-par110;
double par112 = sqrt(par111);
double par113 = 1/par112;

double EE = par13*par15*par18*par113;

double diz=EE*ddkr ;


double alfa = geo.GEOMLgetAlfa();



double par1a = cos((alfa*PI)/180);
double fi = (1+par1a)/2;

double par21 = (2*kapa)/(kapa-1);
double par22 = Rgg*dTo;
double par23 =(kapa-1)/kapa;
double par24 = pi/dpo2;
double par25 =pow(par24,par23);
double par26 =1-par25;
double par27 = par21*par22*par26;

double Vi = sqrt(par27);


double Aiz = dAkr2*EE;

double par1b =fi*mpp*Vi;

double par2b = (pi-pa)*Aiz;

double FpII = par1b+par2b;


myfile2 <<dpo2/100000<<" "<<Fp<<endl;

}
myfile2.close();
/*
//-------------------------------------------------------------------------------------------------------------------------
*************************************************************************************************************************
Potisak[DATA].txt  POCETAK
*************************************************************************************************************************
//-------------------------------------------------------------------------------------------------------------------------
*/
#endif


	return FpII;
//-----------------------------------------------------------------------------

/*
//OTVORI FAJL
fstream PotisakPROVERA;
PotisakPROVERA.open ("Potisak[KONTROLNIpodaci].txt",ios::out);

PotisakPROVERA <<"kapa=  "<<kapa<<"      "<<endl;//"A_cevi=  "<<A_cevi2*1000000<<"    mm2  "  <<"MaseniProtokKr ="<<MaseniProtokKr3<<" Kg/s    "<<"T_oxi=   " <<T_oxi-273.15<<"   C  "<<"kapa=  "<<kapa<<"      "<<"R_oxi=  "<<R_oxi<<"  J/KgK    "<<"P_oxi=   "<<P_oxi/100000<<"    BAR   " << endl;
PotisakPROVERA <<"po=  "<<po/100000<<" BAR   "<<endl;
PotisakPROVERA <<"pi=  "<<pi/100000<<" BAR     "<<endl;
PotisakPROVERA <<"Rgg=  "<<Rgg<<"  J/KgK    "<<endl;
PotisakPROVERA <<"To=  "<<To<<"  K    "<<endl;
PotisakPROVERA <<"G[korenizkapa*koefKAPA]=  "<<G<<"      "<<endl;
PotisakPROVERA <<"Cf_Adaptiran=  "<<Cf_Adaptiran<<"      "<<endl;
PotisakPROVERA <<"CZZ=  "<<CZZ<<"      "<<endl;
PotisakPROVERA <<"Akr=  "<<Akr2<<"  m2    "<<endl;

PotisakPROVERA <<"Precnik_u_kpp=  "<<2*dkr2<<"  mm    "<<endl;

PotisakPROVERA <<"mpp[G*(poAkr/korenRTo)]=  "<<mpp<<"  Kg/s    "<<endl;

PotisakPROVERA <<endl;
  
PotisakPROVERA <<"Aiz=  "<<Aiz<<"  m2     "<<endl;
PotisakPROVERA <<"Diz_precnik=  "<<2*diz<<"  mm    "<<endl;
PotisakPROVERA <<" alfa=  "<< alfa<<"     "<<endl;
PotisakPROVERA <<" L_ml=  "<< L_ml<<"  mm   "<<endl;

PotisakPROVERA <<endl;


PotisakPROVERA <<"EE=  "<<EE<<"      "<<endl;
PotisakPROVERA <<"Vi=  "<<Vi<<"  m/s    "<<endl;

PotisakPROVERA <<"Vkr=  "<<Vkr<<"  m/s    "<<endl;
PotisakPROVERA <<"Ti=  "<<Ti<<"  K    "<<endl;
PotisakPROVERA <<"Tkr=  "<<Tkr<<"  K    "<<endl;

PotisakPROVERA <<endl;
PotisakPROVERA <<"Isp=  "<<Isp<<"  Ns    "<<endl;
PotisakPROVERA <<"Fp=  "<<Fp<<"  N    "<<endl;
PotisakPROVERA <<"FpII=  "<<FpII<<"  N    "<<endl;
PotisakPROVERA <<endl;
PotisakPROVERA <<"Fp=  "<<Fp/9.8075<<"  Kg    "<<endl;
PotisakPROVERA <<"FpII=  "<<Fp/9.8075<<"  Kg    "<<endl;
PotisakPROVERA <<endl;
PotisakPROVERA <<"Vreme_rada_RM=  "<<vremeR<<"  s    "<<endl;
PotisakPROVERA <<"MasaPM  "<<mpp*vremeR<<"  Kg    "<<endl;


PotisakPROVERA.close();

*/




}

