
#include "stdafx.h"

#include "defineProracuni.h"

#include "TankOxi.h"
#include "Oxidator.h"
#include "Math.h"
#include <fstream.h>

//#include "Performanse_1.h"


//------------------------------
//funkcije get iz konstruktora


double GEOXI::GEOXIgetD_tank()
{
	return  D_tank;
}

double GEOXI::GEOXIgetL_tank()
{
	return  L_tank;
}

double GEOXI::GEOXIgetV_tank()
{
	return  V_tank;
}

double GEOXI::GEOXIgetV_m_tank()
{
	
	return  V_m_tank;
}



double GEOXI::GEOXIgetde_tank_min()
{
	return  de_tank_min;
}

double GEOXI::GEOXIgetm_naponZ()
{
	return m_naponZ;//N/mm2
}

double GEOXI::GEOXIgetstepenS()
{
	return  stepenS;
}

double GEOXI::GEOXIgetROmaterijala()
{
	return  m_ROg;
}



double GEOXI::GEOXIgetMaxP_tank()
{
//PRVO DEFINISATI DA LI TREBA DA SE IZRACUNAJU VREDNOSTI U .h FAJLU !!!



/*
//-------------------------------------------------------------------------------------------------------------------------
*************************************************************************************************************************
MAXP_u_oxi_tanku.txt      POCETAK
*************************************************************************************************************************
//-------------------------------------------------------------------------------------------------------------------------
*/
	GEOXI oxi;

double dzida = oxi.GEOXIgetde_tank_min();//mm
double dzida2 = dzida/1000;
double KonvBar = 100000;//pas u bare 10^5
double Konv2 = 1000000; //mm^2 u m^2
double naponZtank = oxi.GEOXIgetm_naponZ();
double NaponZ = naponZtank*Konv2;// iz N/mm2 u N/m2
double Ppas;
double m_Run = oxi.GEOXIgetD_tank();//mm
double stepenSigurnosti = oxi.GEOXIgetstepenS();
double m_ROg = oxi.GEOXIgetROmaterijala();
#ifdef pro_MAXptank
double Masa_tanka;

double Vcevi;
double Acevi;
double VceviM3;
double Konv = 1000000000; //pretvaranje mm3 u m3
double KvZ; //razlika pod zagradom R^2-r^2
double PI = 3.14159265359;


fstream myfile;
myfile.open ("MAXP_u_oxi_tanku.txt",ios::out);

for (double dd= 0;dd<10;dd+=0.01)// u mm 
{
//PRORACUN MASE TANKA


double L_tank = oxi.GEOXIgetL_tank();
double m_Rsp = (oxi.GEOXIgetD_tank()+(dd+dd))/2;
double m_Run = oxi.GEOXIgetD_tank()/2;

KvZ = (m_Rsp*m_Rsp)-(m_Run*m_Run);
Acevi = KvZ*PI;
Vcevi = Acevi*L_tank;
VceviM3 = Vcevi/Konv;

Masa_tanka = 2*VceviM3*m_ROg;


 double x4 = dd;//+dx;
 KonvBar = 100000;//pas u bare 10^5
 Ppas = ((dd/1000)*(2*NaponZ))/(m_Run/1000);//PASKALI N/m2
 MaxP_tank = (Ppas/KonvBar);
 myfile <<"DD= "<<dd<<"  mm    "<<MaxP_tank<<" BAR    "<<NaponZ/1000000 <<"  N/mm2     "<<"PRECNIK D_tank ="<< m_Run<<"  mm      " <<"SS ="<< stepenSigurnosti<<"   MaxP_tank SSS = "<<  MaxP_tank/stepenSigurnosti<<"    BAR     "<< 2*m_Run<<"  mm    "<<2*m_Rsp<<"  mm      "<<"  "<< Masa_tanka+Masa_tanka<< "  kg    "<<endl;
 //MaxP_tank = (Ppas/KonvBar);// BAR-i




 
}
myfile.close();
#endif

Ppas = (2*dzida2*NaponZ)/(m_Run/1000);//PASKALI N/m2
MaxP_tank = (Ppas/KonvBar)/stepenSigurnosti;// BAR-i


/*
//-------------------------------------------------------------------------------------------------------------------------
*************************************************************************************************************************
MAXP_u_oxi_tanku.txt      KRAJ
*************************************************************************************************************************
//-------------------------------------------------------------------------------------------------------------------------
*/

	return  MaxP_tank;
}

double GEOXI::GEOXIgetL_tankPV()
{

OXI oxi;
double T_oxi = oxi.OXIgetT_oxi();
double R_oxi = oxi.OXIgetR_oxi();
double P_oxi = oxi.OXIgetP_oxi();
double m_oxi = 1.6;//kg
	GEOXI tank;
double D_tank = tank.GEOXIgetD_tank()/1000;//mm

double PI = 3.14159265359;




double A_tank= (((D_tank*D_tank)*PI)/4);//u m2
double V_oxi= (m_oxi*R_oxi*T_oxi)/P_oxi;//PV = mRT    V = mRT/P T raste /V raste  P raste / V opada / L raste 
double L_tankPV = V_oxi/A_tank; //A raste /V opada / L opada 

 

	

	return L_tankPV;

}




































/*
double PI = 3.14159265359;
double Vcevi;
double Acevi;
double VceviM3;
double Konv = 1000000000; //pretvaranje mm3 u m3
double KvZ; //razlika pod zagradom R^2-r^2

KvZ = (m_Rsp*m_Rsp)-(m_Run*m_Run);
Acevi = KvZ*PI;
Vcevi = Acevi*m_Lduz;
VceviM3 = Vcevi/Konv;

m_Umasa = VceviM3*m_ROg;
m_CenaUkupno = m_Umasa*m_CenapoKg;
	
double dzida =(m_Rsp-m_Run)/1000;
double KonvBar = 100000;//pas u bare 10^5
double Konv2 = 1000000; //mm^2 u m^2
double NaponZ = m_naponZ*Konv2;
double Ppas;
Ppas = (2*dzida*NaponZ)/(m_Run/1000);
m_Pmax = Ppas/KonvBar;
*/