#include "stdafx.h"

#include "defineProracuni.h"

#include "xcevi_mp.h"
#include "x1x2.h"
#include "TankOxi.h"
#include "Math.h"
#include "Oxidator.h"
#include "CRMM.h"

#include <fstream.h>




double BRIZGALJKA::BRIZGALJKAgetBroj_otvora()
{
	return Broj_otvora;
}

double BRIZGALJKA::BRIZGALJKAgetD_otvora()
{
	return D_otvora;
}

double BRIZGALJKA::BRIZGALJKAgetA_otvora()
{
	return A_otvora;
}

double BRIZGALJKA::BRIZGALJKAgetAuk_izlazno()
{
BRIZGALJKA bri;
double A2n = bri.BRIZGALJKAgetA_otvora();
double BrojOtvora = bri.BRIZGALJKAgetBroj_otvora();

 Auk_izlazno = A2n*BrojOtvora;

	return Auk_izlazno;
}

double BRIZGALJKA::BRIZGALJKAgetCd()
{
	return Cd;
}



//maseni protok oxidatora kroz brizgaljke 
double BRIZGALJKA::BRIZGALJKAgetMP_oxi()
{

BRIZGALJKA bri;
double Auk_izlazno=  bri.BRIZGALJKAgetAuk_izlazno()/1000000;//
double Cd = bri.BRIZGALJKAgetCd();




XCEVI cev;
double Pkriticno = cev.XCEVIgetPkriticno();
double ROkr = cev.XCEVIgetROkr();


//	MP_oxi = Cd*Abr*koren iz (2 ROxi * (Poxi - Pkomore))

double Pkomore = 2000000; //bara
double dp = Pkriticno-Pkomore;
double podkoren = 2*ROkr*dp;
double koren = pow(podkoren,0.5);
/*
fstream myfileMP;
myfileMP.open ("MaseniProtokKr[dp].txt",ios::out);


for (double dp= P_oxi;dp>0;dp-=1000)
{
 
double clan3_PoAkrsaRTo_2 = (dp*A_cevi)/korenRTo;
double MaseniProtokKr2 = clan1_korenizkapa*clan2_kvadrat*clan3_PoAkrsaRTo_2;

myfileMP <<"dp=  "<<dp/100000<<"  BAR    "<<"MaseniProtokKr ="<<MaseniProtokKr2<<" Kg/s    "<<"T_oxi=   " <<T_oxi-273.15<<"   C  "<<"kapa=  "<<kapa<<"      "<<"R_oxi=  "<<R_oxi<<"  J/KgK    "<<"Acevi= "<<A_cevi<<"   mm2    "<< endl;

}

myfileMP.close();
*/





	MP_oxi = Cd*Auk_izlazno*koren;
	
	return MP_oxi;
}
















/*
double BRIZGALJKA::BRIZGALJKAgetV_otvora()
{


XCEVI cev;
double D_cevi = cev.XCEVIgetD_cevi();
double V_cevi = cev.XCEVIgetVisticanja();//tj V11
double A_cevi = cev.XCEVIgetA_cevi();
BRIZGALJKA bri;
double A2n = bri.BRIZGALJKAgetA_otvora();
double BrojOtvora = bri.BRIZGALJKAgetBroj_otvora();

double PI = 3.14159265359;
double A11 = A_cevi/1000000;// m2


// iz kontinuiteta sledi Brzina na izlazu iz brizgaljke je = Vcevi*A11/ suma svih otvora 


V_otvora = (V_cevi*A11)/(BrojOtvora*A2n/1000000); // m/s
/* NAPOMENA fluid je stisljiv (Ro = const) i nije uzeta u obzir sila trenja i gubici energije u kontrolnoj zapremini kao i 
u cevi ( iz eksperimentalnih uslova aka tus utvrdjeno je da najveca brzina (maseni protok) se nalazi ka centralnim rupama)
kapilarne pojave se zanemaruju i trenje u svakom otvoru




	return V_otvora;
}


*/










