#include "stdafx.h"

#include "defineProracuni.h"

#include "xcevi_mp.h"
#include "TankOxi.h"
#include "Math.h"
#include "Oxidator.h"

#include "CRMM.h"

#include <fstream.h>

double XCEVI::XCEVIgetD_cevi()
{
	return D_cevi;
}

double XCEVI::XCEVIgetA_cevi()
{
XCEVI cev;
	double PI = 3.14159265359;
double D_cevi = cev.XCEVIgetD_cevi();
double A_cevi =  (((D_cevi*D_cevi)*PI)/4);//mm2


	return A_cevi;
}

double XCEVI::XCEVIgetL_cevi()
{
	return L_cevi;
}

double XCEVI::XCEVIgetV_cevi()
{


XCEVI cev;
double L_cevi = cev.XCEVIgetL_cevi();
double A_cevi = cev.XCEVIgetA_cevi();

V_cevi = (A_cevi*L_cevi)/1000000000;
	return V_cevi;
}


double XCEVI::XCEVIgetodnosP()
{
	//ODNOS ZAVSTAVNOG PRITISKA U OXI TANKU I AMBIJENTALNOG PRITISKA 

/*
//-------------------------------------------------------------------------------------------------------------------------
*************************************************************************************************************************
OdnosiPritisaka[Poxi=const].txt      POCETAK
*************************************************************************************************************************
//-------------------------------------------------------------------------------------------------------------------------
*/
OXI oxi;
double P_oxi = oxi.OXIgetP_oxi();

CCRM fluid;
double P_amb = fluid.CCRMgetP_amb();


#ifdef pro_odnosP
fstream myfile;
myfile.open ("OdnosiPritisaka[Poxi=const].txt",ios::out);

for (double dp= 0;dp<P_oxi;dp+=5000)
{

	//staviti dp=0 AKO U TANKU SA OXI JE TOKOM CELOG VREMENA const pritisak ( NEMOGUC SCENARIO)
	double PodT = (P_oxi);//uzeta NASUMICNA PREDPOSTAVKA  da pritisak u boci opada istom razmerom kao i rast pritiska u sistemu !! PREDPOSTAVKA !!
double odnosP2 = dp/PodT;

myfile <<"dp=  "<<dp/100000<<" BAR    "<<"P_oxi ="<<P_oxi/100000<<" BAR     "<<"  PodT= "<<  PodT/100000<<"    BAR  "<<"odnosP2=  "<< odnosP2  <<  "   "  <<"cilj Po/P < 0.528 -> choked flow Mt =1   Po/P > 0.528  izentropsko strujanje  "<<  endl;

if (odnosP2 >= 0.528)
{
myfile <<"dp=  "<<dp/100000<<"  BAR  odnos je veci od 0.528 ------- prelazak u drugi rezim strujanja [IZENTROPSKI]"<<endl;
break;
}


}
myfile.close();
#endif

/*
//-------------------------------------------------------------------------------------------------------------------------
*************************************************************************************************************************
OdnosiPritisaka[Poxi=const].txt       KRAJ
*************************************************************************************************************************
//-------------------------------------------------------------------------------------------------------------------------
*/



/*
//-------------------------------------------------------------------------------------------------------------------------
*************************************************************************************************************************
OdnosiPritisaka[NASUMICE].txt      POCETAK
*************************************************************************************************************************
//-------------------------------------------------------------------------------------------------------------------------
*/

#ifdef pro_odnosPnasumice

fstream myfile2;
myfile2.open ("OdnosiPritisaka[NASUMICE].txt",ios::out);

for (double dpp= 0;dpp<P_oxi;dpp+=5000)
{

	//staviti dp=0 AKO U TANKU SA OXI JE TOKOM CELOG VREMENA const pritisak ( NEMOGUC SCENARIO)
	double PodT = (P_oxi-dpp);//uzeta NASUMICNA PREDPOSTAVKA  da pritisak u boci opada istom razmerom kao i rast pritiska u sistemu !! PREDPOSTAVKA !!
double odnosP3 = dpp/PodT;

myfile2 <<"dp=  "<<dpp/100000<<" BAR    "<<"P_oxi ="<<P_oxi/100000<<" BAR     "<<"  PodT= "<<  PodT/100000<<"    BAR  "<<"odnosP2=  "<< odnosP3  <<  "   "  <<"cilj Po/P < 0.528 -> choked flow Mt =1   Po/P > 0.528  izentropsko strujanje  "<<  endl;

if (odnosP3 >= 0.528)
{
myfile2 <<"dp=  "<<dpp/100000<<"  BAR  odnos je veci od 0.528 ------- prelazak u drugi rezim strujanja [IZENTROPSKI]"<<endl;
break;
}


}
myfile2.close();
#endif
odnosP = P_amb/P_oxi; // ako je odnosP < 0.528 flow is choked !!! Mt =1
                      // ako je odnosP > 0.528 flow is isentropic !!! Q = 0

/*
//-------------------------------------------------------------------------------------------------------------------------
*************************************************************************************************************************
OdnosiPritisaka[NASUMICE].txt      KRAJ
*************************************************************************************************************************
//-------------------------------------------------------------------------------------------------------------------------
*/

	return odnosP;//odnos je racunat u pocetnom trenutku kada je izlazna cev iz tanka prazna (na P_atm)
}




double XCEVI::XCEVIgetUocevi()
{


//BRZINA ISTICANJA SA DOOOSTA PREDPOSTAVKI U POCETNOM t=0 TRENUTKU


/*
//-------------------------------------------------------------------------------------------------------------------------
*************************************************************************************************************************
BrzinaIsticanjaUo[T1=const].txt     POCETAK
*************************************************************************************************************************
//-------------------------------------------------------------------------------------------------------------------------
*/
OXI oxi;
double P_oxi = oxi.OXIgetP_oxi();
double Toxi = oxi.OXIgetT_oxi();
double c_oxi = oxi.OXIgetc_oxi();

CCRM fluid;
double P_amb = fluid.CCRMgetP_amb();

XCEVI cev;
double odnosP = cev.XCEVIgetodnosP();//u pocetnom trenutku t=0

//predpostavka flov is choked Mt=1;  ili Mkrpp=1; 
//Moxi nasumicna predpostavka
//Toxi = const u imaginarnom poprecnom preseku IZMEDJU TANKA I CEVI (DEO SA PROMENLJIVIM PP)
//c je uzeto iz c_oxi

double M1 = 0.3;
#ifdef pro_UoOdMax


fstream myfile;
myfile.open ("BrzinaIsticanjaUo[T1=const].txt",ios::out);

for (double dM= 0;dM<0.5;dM+=0.01)
{


double Uocevi2 = dM*c_oxi;
myfile <<"dM=  "<<dM<<"    "<<"c_oxi ="<<c_oxi<<" m/s     "<<"  Uocevi2= "<<  Uocevi2<<"    m/s  "<<  endl;

}

myfile.close();
#endif
/*
//-------------------------------------------------------------------------------------------------------------------------
*************************************************************************************************************************
BrzinaIsticanjaUo[T1=const].txt      KRAJ
*************************************************************************************************************************
//-------------------------------------------------------------------------------------------------------------------------
*/


#ifdef pro_UodT
/*
//-------------------------------------------------------------------------------------------------------------------------
*************************************************************************************************************************
BrzinaIsticanjaUo[M=const].txt    POCETAK
*************************************************************************************************************************
//-------------------------------------------------------------------------------------------------------------------------
*/

///----------------------za M=censt T1/= const
fstream myfile2;
myfile2.open ("BrzinaIsticanjaUo[M=const].txt",ios::out);

for (double dT= Toxi-50;dT<473.1;dT+=0.1)
{


OXI oxi;
double kapa = oxi.OXIgetkapa_oxi();
double R_oxi = oxi.OXIgetR_oxi();
double P_oxi = oxi.OXIgetT_oxi();


double M1 = 0.3;





 double podkoren2 = kapa*R_oxi*dT;
double c_oxi2 = pow(podkoren2,0.5);
 
double Uocevi3 = M1*c_oxi2;
myfile2 <<"dT=  "<<dT-273.15<<"  C    "<<"c_oxi2 ="<<c_oxi2<<" m/s     "<<"  Uocevi3= "<<  Uocevi3<<"    m/s  "<<"M1 =  "<<M1<<  endl;

}

myfile2.close();
#endif


Uocevi = M1*c_oxi;//pri const T     Toxi  -> T1   -> Tkr
 
/*
//-------------------------------------------------------------------------------------------------------------------------
*************************************************************************************************************************
BrzinaIsticanjaUo[M=const].txt    KRAJ
*************************************************************************************************************************
//-------------------------------------------------------------------------------------------------------------------------
*/

return Uocevi;


}


double XCEVI::XCEVIgetUkriticno()
{



/*
//-------------------------------------------------------------------------------------------------------------------------
*************************************************************************************************************************
KriticnaBrzinaIsticanja[Toxinijeconst].txt    POCETAK
//NAPOMENA Ukriticno = ckriticno !!! iz Mkr = 1 tj p/po < 0.528
//KRITICNA BRZINA NA SPOJU CEVI I TANKA 
*************************************************************************************************************************
//-------------------------------------------------------------------------------------------------------------------------
*/

OXI oxi;
double kapa = oxi.OXIgetkapa_oxi();
double R_oxi = oxi.OXIgetR_oxi();

double To = oxi.OXIgetT_oxi(); 




#ifdef prodUkrodT
///----------------------za Mt=1 To/= const
fstream myfile5;
myfile5.open ("KriticnaBrzinaIsticanja[Toxinijeconst].txt",ios::out);

double Tkr =(2/(kapa+1))*To;
double podkoren2 = kapa*R_oxi*Tkr;



for (double dT= To-50;dT<473.1;dT+=0.1)
{

double Tkr2 =(2/(kapa+1))*dT;
double podkoren3 = kapa*R_oxi*Tkr2;

double Ukriticno2 = pow(podkoren3,0.5);
 

myfile5 <<"dT=  "<<dT-273.15<<"  C    "<<"Ukriticno ="<<Ukriticno2<<" m/s    "<<  endl;

}

myfile5.close();


 Ukriticno = pow(podkoren2,0.5);
 #endif
/*
//-------------------------------------------------------------------------------------------------------------------------
*************************************************************************************************************************
KriticnaBrzinaIsticanja[Toxinijeconst].txt    KRAJ
*************************************************************************************************************************
//-------------------------------------------------------------------------------------------------------------------------
*/

	return Ukriticno;
}


//KRITICNI PRITISAK U CEVI------------------------------------------------------------
double XCEVI::XCEVIgetPkriticno()
{
/*
//-------------------------------------------------------------------------------------------------------------------------
*************************************************************************************************************************
KriticanPritisakUcevi[dp].txt   POCETAK
*************************************************************************************************************************
//-------------------------------------------------------------------------------------------------------------------------
*/

   OXI oxi;
double kapa = oxi.OXIgetkapa_oxi();
double P_oxi = oxi.OXIgetP_oxi();


double podkoren = 2/(kapa+1);
double kv = kapa/(kapa-1);
double kvadrat = pow(podkoren,kv);



#ifdef pro_Pkroddp
fstream myfileP;
myfileP.open ("KriticanPritisakUcevi[dp].txt",ios::out);


for (double dp= P_oxi;dp>0;dp-=5000)
{
 

double Pkriticno2 = kvadrat*dp;
myfileP <<"dp=  "<<dp/100000<<"  BAR    "<<"Pkriticno ="<<Pkriticno2/100000<<" BAR    "<<kvadrat << endl;

}

myfileP.close();
#endif

Pkriticno = kvadrat*P_oxi;

/*
//-------------------------------------------------------------------------------------------------------------------------
*************************************************************************************************************************
KriticanPritisakUcevi[dp].txt   KRAJ
*************************************************************************************************************************
//-------------------------------------------------------------------------------------------------------------------------
*/

	return Pkriticno;
}


double XCEVI::XCEVIgetMaseniProtokKr()
{

/*
//-------------------------------------------------------------------------------------------------------------------------
*************************************************************************************************************************
MaseniProtokKr[dp].txt   POCETAK
//KRITICNI MASENI PROTOK KROZ CEV 
//VARIACIJA MASENOG PROTOKA U ZAVISNOSTI OD PRITISKA
*************************************************************************************************************************
//-------------------------------------------------------------------------------------------------------------------------
*/

   OXI oxi;
double kapa = oxi.OXIgetkapa_oxi();
double P_oxi = oxi.OXIgetP_oxi();
double T_oxi = oxi.OXIgetT_oxi(); 
double R_oxi = oxi.OXIgetR_oxi();

XCEVI cev;
double A_cevi = cev.XCEVIgetA_cevi()/1000000;//m2

double clan1_korenizkapa = pow(kapa,0.5);
double podkoren = 2/(kapa+1);
double kv = (kapa+1)/(2*(kapa-1));
double clan2_kvadrat = pow(podkoren,kv);


double RTo = R_oxi*T_oxi;
double korenRTo = pow(RTo,0.5);

double clan3_PoAkrsaRTo = (P_oxi*A_cevi)/korenRTo;

#ifdef pro_Mkroddp

fstream myfileMP;
myfileMP.open ("MaseniProtokKr[dp].txt",ios::out);


for (double dp= P_oxi;dp>0;dp-=1000)
{
 
double clan3_PoAkrsaRTo_2 = (dp*A_cevi)/korenRTo;
double MaseniProtokKr2 = clan1_korenizkapa*clan2_kvadrat*clan3_PoAkrsaRTo_2;

myfileMP <<"dp=  "<<dp/100000<<"  BAR    "<<"MaseniProtokKr ="<<MaseniProtokKr2<<" Kg/s    "<<"T_oxi=   " <<T_oxi-273.15<<"   C  "<<"kapa=  "<<kapa<<"      "<<"R_oxi=  "<<R_oxi<<"  J/KgK    "<<"Acevi= "<<A_cevi<<"   mm2    "<< endl;

}

myfileMP.close();
#endif

/*
//-------------------------------------------------------------------------------------------------------------------------
*************************************************************************************************************************
MaseniProtokKr[dp].txt   KRAJ
*************************************************************************************************************************
//-------------------------------------------------------------------------------------------------------------------------
*/


/*
//-------------------------------------------------------------------------------------------------------------------------
*************************************************************************************************************************
MaseniProtokKr[dD_cevi].txt   POCETAK

//VARIJACIJA MASENOG PROTOKA U ZAVISNOSTI OD Akr (TJ D CEVI)
*************************************************************************************************************************
//-------------------------------------------------------------------------------------------------------------------------
*/
#ifdef pro_MkroddD_cevi
fstream myfileMP2;
myfileMP2.open ("MaseniProtokKr[dD_cevi].txt",ios::out);


for (double dD_cevi= 0;dD_cevi<30;dD_cevi+=0.01)
{
 
double PI = 3.14159265359;

double A_cevi2 =  (((dD_cevi*dD_cevi)*PI)/4)/1000000;//m2

	
double clan3_PoAkrsaRTo_3 = (P_oxi*A_cevi2)/korenRTo;
double MaseniProtokKr3 = clan1_korenizkapa*clan2_kvadrat*clan3_PoAkrsaRTo_3;

myfileMP2 <<"dD_cevi=  "<<dD_cevi<<"  mm    "<<"A_cevi=  "<<A_cevi2*1000000<<"    mm2  "  <<"MaseniProtokKr ="<<MaseniProtokKr3<<" Kg/s    "<<"T_oxi=   " <<T_oxi-273.15<<"   C  "<<"kapa=  "<<kapa<<"      "<<"R_oxi=  "<<R_oxi<<"  J/KgK    "<<"P_oxi=   "<<P_oxi/100000<<"    BAR   " << endl;

}

myfileMP2.close();

MaseniProtokKr = clan1_korenizkapa*clan2_kvadrat*clan3_PoAkrsaRTo;
//mp = koren_iz_kapa *(2/kapa+1)na_(kapa+1/2(kapa-1)  * Poxi*Akr/ koren_iz_(Roxi*Toxi)
#endif
/*
//-------------------------------------------------------------------------------------------------------------------------
*************************************************************************************************************************
MaseniProtokKr[dp].txt   KRAJ
*************************************************************************************************************************
//-------------------------------------------------------------------------------------------------------------------------
*/

	return MaseniProtokKr;
}

double XCEVI::XCEVIgetZapreminskiKR()
{

XCEVI cev;
double A_cevi = cev.XCEVIgetA_cevi()/1000000;//m2
double Ukriticno =cev.XCEVIgetUkriticno();

ZapreminskiKR =A_cevi*Ukriticno;

	return ZapreminskiKR;
}


double XCEVI::XCEVIgetROkr()
{

	//mkr = RoKr*Ukr*Akr
XCEVI cev;
double MPkr= cev.XCEVIgetMaseniProtokKr();
double Akr= cev.XCEVIgetA_cevi()/1000000;
double Ukr= cev.XCEVIgetUkriticno();

ROkr = MPkr/(Akr*Ukr);
	
	
	return ROkr;
}



//----------------------za Mt=1 To/= const
double XCEVI::XCEVIgetTkr()
{

	
   OXI oxi;
double To = oxi.OXIgetT_oxi(); 
double kapa = oxi.OXIgetkapa_oxi();
 Tkr =(2/(kapa+1))*To;

return Tkr;
}

//ZA KRUZNE PP Dh = D
double XCEVI::XCEVIgetDhidraulicki()
{
//obim 2rPI ili R*PI
//Dhidraulicki = 4*A/obim
XCEVI cev;
double A_cevi = cev.XCEVIgetA_cevi();//m2
double D_cevi = cev.XCEVIgetD_cevi();

double PI = 3.14159265359;

Dhidraulicki = (4*A_cevi)/(D_cevi*PI);

return Dhidraulicki;
}


















