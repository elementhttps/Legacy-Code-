#include "stdafx.h"

#include "defineProracuni.h"


#include "xcevi_mp.h"
#include "xgo12.h"
#include "TankOxi.h"
#include "Math.h"
#include "Oxidator.h"
#include "CRMM.h"
#include "Performanse_HIB.h"
#include "GeoML.h"


#include <fstream.h>




double XGO::XGOgetP_generisano()
{

/*
PERF per;
double CZZ = per.PERFgetCZZ();
double Mp = per.PERFgetMaseniProtok();

GEOML geo;
double Akr = geo.GEOMLgetAkr();



//r = Mp_oxi/Mp_gor; odnos oxi i goriva koliko puta ima vise  oksidatora od goriva
//r = 1 -3
//Mp = Mp_gor*(r+1);


double r_oxigor = 2;
double odnosGorOxi = (r_oxigor+1);
double Mp_gor = Mp/odnosGorOxi;

double Mp_oxi = Mp-Mp_gor;

double N = 7;//broj otvora
double Aport = 200;//povrsina otvora

double Go = Mp_oxi/(N*(Aport/1000000));///!!!!!!!!!!!!!!!!!!!!!!!proveriti vrednosti

alfa  burning  or  regression  rate  coefficient  variable 
(units  of a  depend  on  value  of oxidizer flux  exponent) 
0.104*G^0.681 
0.155 n 0.5 za n20


V = VO +VN LN(MO/M)
M0 - pocetna masa 
M - trenutna masa


double n= 0.5;
double alfa = 0.155;

double kvG = pow(Go,n);

double r = alfa * kvG;//mm/s

fstream myfile;
myfile.open ("BrzinaSagorevanja.txt",ios::out);
 
myfile <<"Mp=  "<<Mp<<"  Kg/s    "<<endl;
myfile<<"r_oxigor ="<<r_oxigor<<"     "<<endl;
myfile <<"Mp_gor=   " <<Mp_gor<<"   Kg/s  "<<endl;
myfile <<"Mp_oxi=   " <<Mp_oxi<<"   Kg/s  "<<endl;
myfile <<"N=  "<<N<<"      "<<endl;
myfile <<"Aport= "<<Aport<<"   mm2    "<< endl;
myfile <<"Go= "<<Go<<"    "<< endl;
myfile <<"n= "<<n<<"    "<< endl;
myfile <<"alfa= "<<alfa<<"    "<< endl;
myfile <<"r= "<<r<<" mm/s   "<< endl;

myfile.close();
//P_generisano = C*mp/Ak
*/

	return P_generisano;
}

double XGO::XGOgetrt()
{
#ifdef pro_sagorevanje
/*
//-------------------------------------------------------------------------------------------------------------------------
*************************************************************************************************************************
BrzinaSagorevanja[Gox_n_alfa].txt      POCETAK
*************************************************************************************************************************
//-------------------------------------------------------------------------------------------------------------------------
*/

fstream myfile2;
myfile2.open ("BrzinaSagorevanja[Gox_n_alfa].txt",ios::out);

    for(double dn = 0.4; dn <=0.8; dn+=0.1)
	{
       for (double dalfa = 0.1; dalfa<=0.8; dalfa+=0.05)
	   {
 
		   for(double dGo = 8; dGo <=50;dGo+=0.1)
		   {
                  for(double dmt = 0; dmt <=1;dmt+=0.01)
				  {
//dalfa mm/s
//dGo  g/cm2/s
double kvG = pow(dGo,dn);
double PI = 3.14159265359;
//A_tank = ((D_tank*D_tank)*PI)/4);precnik
//4a/pi
double r = dalfa * kvG;//cm/s
double Aporta = dGo/dmt;
double podkv = (4*Aporta)/PI;
double Dporta =pow(podkv,0.5);




myfile2 <<"r ="<<r<<"  mm/s  "<<"alfa=  "<<dalfa<<"    " <<"n = "<< dn<< "      " <<"Go = " << dGo<< "   g/cm2*s  "<<"mppOXI = " << dmt<< "   kg/s  "<<"Aporta = " << Aporta<< "   cm2  "<<"Dport ="<<Dporta<<"    cm   "<<endl;
				  }
		   }

	   }

	}
myfile2.close();
#endif
/*
//-------------------------------------------------------------------------------------------------------------------------
*************************************************************************************************************************
BrzinaSagorevanja[Gox_n_alfa].txt      POCETAK
*************************************************************************************************************************
//-------------------------------------------------------------------------------------------------------------------------
*/

	return rt;
}

