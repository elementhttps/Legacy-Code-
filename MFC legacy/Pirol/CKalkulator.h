#if !defined(AFX_CKALKULATOR_H__2800A187_4B90_4621_B35C_AD2E589D2DB0__INCLUDED_)
#define AFX_CKALKULATOR_H__2800A187_4B90_4621_B35C_AD2E589D2DB0__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// CKalkulator.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CCKalkulator dialog

class CCKalkulator : public CDialog
{
// Construction
public:
	CCKalkulator(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CCKalkulator)
	enum { IDD = IDD_DIALOG1 };
	CListBox	lista;
	double	add1;
	double	add2;
	double	sum;
	CString	m_cstring;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCKalkulator)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CCKalkulator)
	afx_msg void OnButtonAdd();
	afx_msg void OnCmd();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CKALKULATOR_H__2800A187_4B90_4621_B35C_AD2E589D2DB0__INCLUDED_)
