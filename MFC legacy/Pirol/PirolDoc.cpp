// PirolDoc.cpp : implementation of the CPirolDoc class
//

#include "stdafx.h"
#include "Pirol.h"

#include "PirolDoc.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CPirolDoc

IMPLEMENT_DYNCREATE(CPirolDoc, CDocument)

BEGIN_MESSAGE_MAP(CPirolDoc, CDocument)
	//{{AFX_MSG_MAP(CPirolDoc)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CPirolDoc construction/destruction

CPirolDoc::CPirolDoc()
{
	// TODO: add one-time construction code here

}

CPirolDoc::~CPirolDoc()
{
}

BOOL CPirolDoc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;

	// TODO: add reinitialization code here
	// (SDI documents will reuse this document)

	return TRUE;
}



/////////////////////////////////////////////////////////////////////////////
// CPirolDoc serialization

void CPirolDoc::Serialize(CArchive& ar)
{
	if (ar.IsStoring())
	{
		// TODO: add storing code here
	}
	else
	{
		// TODO: add loading code here
	}
}

/////////////////////////////////////////////////////////////////////////////
// CPirolDoc diagnostics

#ifdef _DEBUG
void CPirolDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CPirolDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CPirolDoc commands
