// PirolView.cpp : implementation of the CPirolView class
//
//#include "d:\myFiles\mine.h"
#include "stdafx.h"
#include "Pirol.h"

#include "PirolDoc.h"
#include "PirolView.h"

#include <mmsystem.h>//header za zvuk u wav !! formatu
#pragma comment(lib,"winmm.lib")//za ispravljanje recompile bug-a

#include "CKalkulator.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CPirolView

IMPLEMENT_DYNCREATE(CPirolView, CScrollView)

BEGIN_MESSAGE_MAP(CPirolView, CScrollView)
	//{{AFX_MSG_MAP(CPirolView)
	ON_WM_VSCROLL()
	ON_WM_HSCROLL()
	ON_WM_MOUSEWHEEL()
	ON_COMMAND(IDM_KALKULATOR, OnKalkulator)
	ON_WM_KEYDOWN()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CPirolView construction/destruction

CPirolView::CPirolView()
{

	m_DGrafik = 0;
	m_Zumx= 20;
	m_Zumy= 20;

	m_IDev = 100000;
	m_GMreza = 5000;

	m_viewx = 1;
	m_viewy = -1;

	m_Orgfx =90;
	m_Orgfy =620;

	m_paint = 1;
	m_Mreza = 1;

    add1 = 5.0;
	add2 = 4.0;
	sum = 0.0;

	m_cstring = '-';
	

	

}

CPirolView::~CPirolView()
{
}

BOOL CPirolView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	return CScrollView::PreCreateWindow(cs);
}

/////////////////////////////////////////////////////////////////////////////
// CPirolView drawing

void CPirolView::OnDraw(CDC* pDC)
{
	CPirolDoc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);

//double -1.7E+308 to 1.7E+308 range of double
//int -32768 to 32767
//char -128 to 127
//unsigned int 0 to 65535 mogu biti samo pozitivni brojevi
//sizeof //koliko mesta zauzima broj,rec ... int i = 10   sizeof(i) = 2;
//(long double)double //tupe casting

/*
// (a > b) ? (c = 17) : (c = 23); 
vece je od b ? ubaci 17  defoult 23
je isto sto i:

if (a > b)
  { c = 17; }
else
  { c = 23; }

goto naci primere

*/
	goto matori;

pDC->SetMapMode(MM_ANISOTROPIC);
//pDC->SetViewportOrg(m_Orgfx,m_Orgfy);
pDC->SetWindowExt(m_Zumx,m_Zumy); //SetWindowExtent(100,100)  SetViewportExtent(200,200)  100 jedinica = 200 pixela

pDC->SetViewportExt(m_viewx,m_viewy);//1 -1

matori:int ini = 1;
//----------------------------------------------------------
// OSE
	CPen PenBlack40(PS_SOLID, 5, RGB(0, 0, 0));
	pDC->SelectObject(PenBlack40);

	pDC->MoveTo(-10000, -1000);
	pDC->LineTo( 20000000, -1000    );

	pDC->MoveTo(   0, -10000);
	pDC->LineTo(   0,  20000000);



	pDC->MoveTo(   0, -50000);
	pDC->LineTo(   100000,  20000);

//----------------------------------------------------------
//----------------------------------------------------------	
//POZICIJA OZNAKA MREZE LENJIRA
//----------------------------------------------------------	
 

    
switch (m_paint)
 {


		case 0 :

		break;


	case 1 :
			
		CPen PenBlack3(PS_SOLID, 3, RGB(0, 0, 0));
	pDC->SelectObject(PenBlack3);
    
	for(int koox = 0; koox < 100000; koox += 1000)
    {
        pDC->MoveTo(koox, 0);
        pDC->LineTo(koox, -1000);
    }

	for(int kooy = 0; kooy < 100000; kooy += 1000)
    {
        pDC->MoveTo(0, kooy);
        pDC->LineTo(-1000, kooy);
    }



	pDC->SelectObject(PenBlack3);
    for(int koox2 = 0; koox2 < 100000; koox2 += 5000)
    {
        pDC->MoveTo(koox2, 0);
        pDC->LineTo(koox2, -2000);
    }
	for(int kooy2 = 0; kooy2 < 100000; kooy2 += 5000)
    {
        pDC->MoveTo(0, kooy2);
        pDC->LineTo(-2000, kooy2);
    }

	for(int koox3 = 0; koox3 < 100000; koox3 += 10000)
    {
        pDC->MoveTo(koox3, 0);
        pDC->LineTo(koox3, -3000);
    }
	for(int kooy3 = 0; kooy3 < 100000; kooy3 += 10000)
    {
        pDC->MoveTo(0, kooy3);
        pDC->LineTo(-3000, kooy3);
    }

    pDC->SetTextColor(RGB(0, 0, 0));
	for (int kt1= 0; kt1<100000; kt1+=10000) 
	{
        
        CString string;
        string.Format (("%d "), (kt1 ) );
        pDC->TextOut (kt1, -3000, string);
    }

	pDC->SetTextColor(RGB(0, 0, 0));
	for (int kt2= 1000; kt2<100000; kt2+=10000) 
	{
        
        CString string;
        string.Format (("%d"), (((kt2-1000)) ) );
        pDC->TextOut (-6000, kt2, string);
    }
	//Invalidate();
         break;
	
		
}
	
		
//----------------------------------------------------------
//MREZA
	
	CPen PenBlack4(PS_SOLID, 1, RGB(0, 0, 0));
	pDC->SelectObject(PenBlack4);
	
	switch (m_Mreza =1)//PRIKAZ MREZE DA NE 1 0
	
	{
	case 0:
    
	

	break;

	case 1:
		for(int kooxx = 0; kooxx < 1000000; kooxx += m_GMreza)
    {
        pDC->MoveTo(kooxx, 0);
        pDC->LineTo(kooxx, 50000);
    }
	

	for(int kooyy = 0; kooyy < 1000000; kooyy += m_GMreza)
    {
        pDC->MoveTo(0, kooyy);
        pDC->LineTo(150000, kooyy);
    }
		break;
	}
//----------------------------------------------------------
//KONSTANTE ZA PRORACUNE U VIEW DOMENU
//////////////////////////////////////////////////////	
const double PI = 3.1415926;// const = konstanta :) //If you accidentally attempt to change a constant,Visual C++ will let you know
const double g = 9.81;//nije tacno
//////////////////////////////////////////////////////	
//----------------------------------------------------------


//----------------------------------------------------------
//KORAK I MAKSIMUM
//----------------------------------------------------------	

double kor = m_Korak;//1

double max = m_IDev; //oko milion
//----------------------------------------------------------



	// TODO: add draw code for native data here
}

void CPirolView::OnInitialUpdate()
{
	CScrollView::OnInitialUpdate();
   //CSize DocSize(20000,20000);
     CSize size(20000,20000);

      int nWidth = size.cx;
      int nHeight = size.cy;
   // Set mapping mode and document size.
   SetScrollSizes (MM_TEXT, CSize (nWidth, nHeight));

   //SetScrollSizes(MM_LOENGLISH, DocSize);
	CDC *pDC = this->GetDC();
    //pDC->SetViewportOrg(m_Orgfx,m_Orgfy);
	pDC->SetViewportOrg(60, 620);

	this->ReleaseDC(pDC); 
	Invalidate();



}

/////////////////////////////////////////////////////////////////////////////
// CPirolView diagnostics

#ifdef _DEBUG
void CPirolView::AssertValid() const
{
	CScrollView::AssertValid();
}

void CPirolView::Dump(CDumpContext& dc) const
{
	CScrollView::Dump(dc);
}

CPirolDoc* CPirolView::GetDocument() // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CPirolDoc)));
	return (CPirolDoc*)m_pDocument;
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CPirolView message handlers

void CPirolView::OnVScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar) 
{
	// TODO: Add your message handler code here and/or call default
	Invalidate();
	CScrollView::OnVScroll(nSBCode, nPos, pScrollBar);
}

void CPirolView::OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar) 
{
	// TODO: Add your message handler code here and/or call default
	Invalidate();
	CScrollView::OnHScroll(nSBCode, nPos, pScrollBar);
}

BOOL CPirolView::OnMouseWheel(UINT nFlags, short zDelta, CPoint pt) 
{
	// TODO: Add your message handler code here and/or call default
	Invalidate();
	return CScrollView::OnMouseWheel(nFlags, zDelta, pt);
}

void CPirolView::OnKalkulator() 
{
    PlaySound(_T("Type.wav"),NULL,SND_ASYNC);//SND_ASYNC SND_SYNC
	CCKalkulator dlg;

    

dlg.m_cstring = '-';
dlg.add1=add1;
dlg.add2=add2;
dlg.sum =sum;
	  

		if(dlg.DoModal()==IDOK)
		{

dlg.m_cstring;
dlg.add1=add1;
dlg.add2=add2;
dlg.sum =sum;
		Invalidate ();

		}
/**/
}

void CPirolView::OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags) 
{
	// TODO: Add your message handler code here and/or call default
	switch(nChar)
	{
	case VK_UP:	
		Beep(700,200);
		Invalidate();
	
        break;
	case VK_LEFT: 
		    Beep(500,200);
			Invalidate();
        break;

	case VK_RIGHT:  
		Beep(400,200);
			Invalidate();
        break;

 case VK_DOWN:
	 Beep(1000,200);
	 	Invalidate();
        break;

case 81:
	 Beep(1100,200);
	 	Invalidate();
        break;

case 87:
	 Beep(1200,200);
	 	Invalidate();
        break;

		case 69:
	 Beep(1300,200);
	 	Invalidate();
        break;

		case 82:
	 Beep(1400,200);
	 	Invalidate();
        break;

case 65:
	 Beep(1500,200);
	 	Invalidate();
        break;

		case 83:
	 Beep(1600,200);
	 	Invalidate();
        break;

		case 68:
	 Beep(1700,200);
	 	Invalidate();
        break;

		case 70:
	 Beep(1800,200);
	 	Invalidate();
        break;

		case 89:
	 Beep(1900,200);
	 	Invalidate();
        break;

		case 88:
	 Beep(2000,200);
	 	Invalidate();
        break;

		case 67:
	 Beep(2100,200);
	 	Invalidate();
        break;

		case 86:
	 Beep(2200,200);
	 	Invalidate();
        break;

			case 84:
	 Beep(200,200);
	 	Invalidate();
        break;

				case 71:
	 //Beep(100,200);
	 	Invalidate();
		//PlaySound(_T("charasel22.wav"),NULL,SND_ASYNC);//SND_ASYNC SND_SYNC
		//PlaySound(
        break;

				case 66:
	 Beep(50,200);
	 	Invalidate();
        break;


	}
	 
		
	CScrollView::OnKeyDown(nChar, nRepCnt, nFlags);
}
