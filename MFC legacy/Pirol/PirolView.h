// PirolView.h : interface of the CPirolView class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_PIROLVIEW_H__066291BD_1010_4071_8A15_9CC57A771F95__INCLUDED_)
#define AFX_PIROLVIEW_H__066291BD_1010_4071_8A15_9CC57A771F95__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


class CPirolView : public CScrollView
{
protected: // create from serialization only
	CPirolView();
	DECLARE_DYNCREATE(CPirolView)

// Attributes
public:
	CPirolDoc* GetDocument();

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CPirolView)
	public:
	virtual void OnDraw(CDC* pDC);  // overridden to draw this view
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	protected:
	virtual void OnInitialUpdate(); // called first time after construct
	//}}AFX_VIRTUAL

// Implementation
public:
	CString m_prikaz;
	CString m_cstring;

		//-----CCKalkulator
	
		double m_b1;
		double m_b2;
	
		double m_rez;
		double add1;
		double add2;
		double sum;
	     
	virtual ~CPirolView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	//{{AFX_MSG(CPirolView)
	afx_msg void OnVScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
	afx_msg void OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
	afx_msg BOOL OnMouseWheel(UINT nFlags, short zDelta, CPoint pt);
	afx_msg void OnKalkulator();
	afx_msg void OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
protected:
	int m_Zumx;
	int m_Zumy;
	int m_GMreza;

	int m_IDev;
	int m_Mreza;
	int m_DGrafik;


	int m_Orgfx;
	int m_Orgfy;

	int dx;
	int m_pp1;
	int m_paint;

	int m_viewx;
	int m_viewy; 

	int m_Korak;





};

#ifndef _DEBUG  // debug version in PirolView.cpp
inline CPirolDoc* CPirolView::GetDocument()
   { return (CPirolDoc*)m_pDocument; }
#endif

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_PIROLVIEW_H__066291BD_1010_4071_8A15_9CC57A771F95__INCLUDED_)
