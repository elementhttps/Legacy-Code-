#if !defined(AFX_CPARAMETRI_H__22A67071_C87C_4DDE_9F2E_F21939AE4F00__INCLUDED_)
#define AFX_CPARAMETRI_H__22A67071_C87C_4DDE_9F2E_F21939AE4F00__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// CParametri.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CCParametri dialog

class CCParametri : public CDialog
{
// Construction
public:
	CCParametri(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CCParametri)
	enum { IDD = IDD_DIALOG_PARAMETRI };
	double	m_Duz2;
	double	m_MasaR;
	double	m_MasaT;
	double	m_Valfa;
	double	m_Akx;
	double	m_Aky;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCParametri)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CCParametri)
		// NOTE: the ClassWizard will add member functions here
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CPARAMETRI_H__22A67071_C87C_4DDE_9F2E_F21939AE4F00__INCLUDED_)
