; CLW file contains information for the MFC ClassWizard

[General Info]
Version=1
LastClass=CCParametri
LastTemplate=CDialog
NewFileInclude1=#include "stdafx.h"
NewFileInclude2=#include "Polaris.h"
LastPage=0

ClassCount=7
Class1=CPolarisApp
Class2=CPolarisDoc
Class3=CPolarisView
Class4=CMainFrame

ResourceCount=4
Resource1=IDR_MAINFRAME
Resource2=IDR_POLARITYPE
Class5=CChildFrame
Class6=CAboutDlg
Resource3=IDD_ABOUTBOX
Class7=CCParametri
Resource4=IDD_DIALOG_PARAMETRI

[CLS:CPolarisApp]
Type=0
HeaderFile=Polaris.h
ImplementationFile=Polaris.cpp
Filter=N

[CLS:CPolarisDoc]
Type=0
HeaderFile=PolarisDoc.h
ImplementationFile=PolarisDoc.cpp
Filter=N

[CLS:CPolarisView]
Type=0
HeaderFile=PolarisView.h
ImplementationFile=PolarisView.cpp
Filter=C
BaseClass=CView
VirtualFilter=VWC
LastObject=CPolarisView


[CLS:CMainFrame]
Type=0
HeaderFile=MainFrm.h
ImplementationFile=MainFrm.cpp
Filter=T


[CLS:CChildFrame]
Type=0
HeaderFile=ChildFrm.h
ImplementationFile=ChildFrm.cpp
Filter=M


[CLS:CAboutDlg]
Type=0
HeaderFile=Polaris.cpp
ImplementationFile=Polaris.cpp
Filter=D

[DLG:IDD_ABOUTBOX]
Type=1
Class=CAboutDlg
ControlCount=4
Control1=IDC_STATIC,static,1342177283
Control2=IDC_STATIC,static,1342308480
Control3=IDC_STATIC,static,1342308352
Control4=IDOK,button,1342373889

[MNU:IDR_MAINFRAME]
Type=1
Class=CMainFrame
Command1=ID_FILE_NEW
Command2=ID_FILE_OPEN
Command3=ID_FILE_MRU_FILE1
Command4=ID_APP_EXIT
Command5=ID_APP_ABOUT
CommandCount=5

[MNU:IDR_POLARITYPE]
Type=1
Class=CPolarisView
Command1=ID_FILE_NEW
Command2=ID_FILE_OPEN
Command3=ID_FILE_CLOSE
Command4=ID_FILE_SAVE
Command5=ID_FILE_SAVE_AS
Command6=ID_FILE_MRU_FILE1
Command7=ID_APP_EXIT
Command8=ID_EDIT_UNDO
Command9=ID_EDIT_CUT
Command10=ID_EDIT_COPY
Command11=ID_EDIT_PASTE
Command12=ID_WINDOW_NEW
Command13=ID_WINDOW_CASCADE
Command14=ID_WINDOW_TILE_HORZ
Command15=ID_WINDOW_ARRANGE
Command16=ID_APP_ABOUT
Command17=IDM_PARAMETRI
CommandCount=17

[ACL:IDR_MAINFRAME]
Type=1
Class=CMainFrame
Command1=ID_FILE_NEW
Command2=ID_FILE_OPEN
Command3=ID_FILE_SAVE
Command4=ID_EDIT_UNDO
Command5=ID_EDIT_CUT
Command6=ID_EDIT_COPY
Command7=ID_EDIT_PASTE
Command8=ID_EDIT_UNDO
Command9=ID_EDIT_CUT
Command10=ID_EDIT_COPY
Command11=ID_EDIT_PASTE
Command12=ID_NEXT_PANE
Command13=ID_PREV_PANE
CommandCount=13

[DLG:IDD_DIALOG_PARAMETRI]
Type=1
Class=CCParametri
ControlCount=14
Control1=IDOK,button,1342242817
Control2=IDCANCEL,button,1342242816
Control3=IDC_EDIT_M1,edit,1350631552
Control4=IDC_EDIT_D1,edit,1350631552
Control5=IDC_EDIT_D2,edit,1350631552
Control6=IDC_EDIT_MT,edit,1350631552
Control7=IDC_EDIT_V_ALFA1,edit,1350631552
Control8=IDC_STATIC,static,1342308352
Control9=IDC_STATIC,static,1342308352
Control10=IDC_STATIC,static,1342308352
Control11=IDC_STATIC,static,1342308352
Control12=IDC_STATIC,static,1342308352
Control13=IDC_EDIT_AKY,edit,1350631552
Control14=IDC_STATIC,static,1342308352

[CLS:CCParametri]
Type=0
HeaderFile=CParametri.h
ImplementationFile=CParametri.cpp
BaseClass=CDialog
Filter=D
VirtualFilter=dWC
LastObject=CCParametri

