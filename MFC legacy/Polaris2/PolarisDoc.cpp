// PolarisDoc.cpp : implementation of the CPolarisDoc class
//

#include "stdafx.h"
#include "Polaris.h"

#include "PolarisDoc.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CPolarisDoc

IMPLEMENT_DYNCREATE(CPolarisDoc, CDocument)

BEGIN_MESSAGE_MAP(CPolarisDoc, CDocument)
	//{{AFX_MSG_MAP(CPolarisDoc)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CPolarisDoc construction/destruction

CPolarisDoc::CPolarisDoc()
{
	// TODO: add one-time construction code here

}

CPolarisDoc::~CPolarisDoc()
{
}

BOOL CPolarisDoc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;

	// TODO: add reinitialization code here
	// (SDI documents will reuse this document)

	return TRUE;
}



/////////////////////////////////////////////////////////////////////////////
// CPolarisDoc serialization

void CPolarisDoc::Serialize(CArchive& ar)
{
	if (ar.IsStoring())
	{
		// TODO: add storing code here
	}
	else
	{
		// TODO: add loading code here
	}
}

/////////////////////////////////////////////////////////////////////////////
// CPolarisDoc diagnostics

#ifdef _DEBUG
void CPolarisDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CPolarisDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CPolarisDoc commands
