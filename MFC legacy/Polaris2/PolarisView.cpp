// PolarisView.cpp : implementation of the CPolarisView class
//

#include "stdafx.h"
#include "Polaris.h"

#include "Math.h"
#include "CParametri.h"

#include "PolarisDoc.h"
#include "PolarisView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CPolarisView

IMPLEMENT_DYNCREATE(CPolarisView, CView)

BEGIN_MESSAGE_MAP(CPolarisView, CView)
	//{{AFX_MSG_MAP(CPolarisView)
	ON_WM_KEYDOWN()
	ON_COMMAND(IDM_PARAMETRI, OnParametri)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CPolarisView construction/destruction

CPolarisView::CPolarisView()
{
	m_Zumx = 8100;
	m_Zumy = 8100;
	alfa1 = 10;
	m_Aky= 1000;
	m_Duz2= 2000;
    m_MasaR=5;
    m_MasaT=50;
    m_Valfa=60;
	m_Akx = 2000;

}

CPolarisView::~CPolarisView()
{

}

BOOL CPolarisView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	return CView::PreCreateWindow(cs);
}

/////////////////////////////////////////////////////////////////////////////
// CPolarisView drawing

void CPolarisView::OnDraw(CDC* pDC)
{
		CClientDC;
	pDC->SetMapMode(MM_ANISOTROPIC);
	pDC->SetViewportOrg(90, 620); //90 -620
	pDC->SetWindowExt(m_Zumx,m_Zumy); //SetWindowExtent(100,100)  SetViewportExtent(200,200)  100 jedinica = 200 pixela
	                                                                                             //1 m = 2 pixela
	
	pDC->SetViewportExt(1000,-1000);





double PI = 3.14159265359;
double g = 9.8041;
//--------------------------------------------------------
CPen PenBlack1(PS_SOLID,1,RGB(0,0,0));
pDC->SelectObject(PenBlack1);

	pDC->MoveTo(-1000, 0);
	pDC->LineTo( 200000,0);

	pDC->MoveTo(   0, -1000);
	pDC->LineTo(   0,  200000);



//--------------------------------------------------------
//MREZA
//--------------------------------------------------------
pDC->SelectObject(PenBlack1);
for(int kooxa2 = 0; kooxa2 < 5100; kooxa2 += 500)
    {
        pDC->MoveTo(kooxa2, 0);
        pDC->LineTo(kooxa2, 5000);
	

    }

for(int kooya2 = 0; kooya2 < 5100; kooya2 += 500)
    {
        pDC->MoveTo(0, kooya2);
        pDC->LineTo(5000, kooya2);
	
    }
//--------------------------------------------------------
//GREDA 1 OSNOVA
//--------------------------------------------------------
CPen PenBlack3(PS_SOLID,20,RGB(0,0,0));
pDC->SelectObject(PenBlack3);

double xg1; 
double yg1;

double pox;
double poy; //duzina-visina osnove

double duzg1= m_Aky;

yg1= m_Aky;
pox = m_Akx; //osnova je na y osi
poy = 0;

 // u mm
xg1 = pox;



pDC->MoveTo( pox,poy );  
pDC->LineTo( xg1,yg1 );


//--------------------------------------------------------
//GREDA 2
//--------------------------------------------------------

pDC->SelectObject(PenBlack3);

double duzg2 = m_Duz2;
double dxg2;
double dyg2;

double dsina;
double dcosa;


//pDC->LineTo( duzg1+(duzg2*cos((PI*alfa1)/180)),((y1+y2)/2)+(duzg2*sin((PI*alfa1)/180)));

dsina =sin((PI*alfa1)/180);
dcosa =cos((PI*alfa1)/180);

dxg2 = xg1+(dcosa*duzg2);
dyg2 = duzg1+(dsina*duzg2);

pDC->MoveTo( xg1,yg1);  
pDC->LineTo( dxg2,dyg2);



//--------------------------------------------------------
//CENTRI MASA
//--------------------------------------------------------


//CENTAR MASE G1

CPen PenRed(PS_SOLID,40,RGB(255,0,0));
pDC->SelectObject(PenRed);

double xcg1;
double ycg1;

xcg1 = ((xg1+pox)/2);
ycg1 = ((yg1+poy)/2);
pDC->Ellipse(xcg1-20,ycg1-30,xcg1+30,ycg1+20);

//CENTAR MASE G2

double xcg2;
double ycg2;

xcg2 = ((dxg2+xg1)/2);
ycg2 = ((dyg2+yg1)/2);


pDC->Ellipse( xcg2-20,ycg2-30,xcg2+30,ycg2+20);

//--------------------------------------------------------
//KALKULACIJE POLARONA
//--------------------------------------------------------


double Ms1 = 1;
double Ms2 = m_MasaR;//masa ruke
double Mt = m_MasaT;//masa tereta
double dalfa1 = alfa1;
double v_alfa1 = m_Valfa; // stepeni/sek
double v_B;
double alfa1RAD;



alfa1RAD = ((PI*alfa1)/180);  //alfa u radianima



v_alfa1 =((PI*m_Valfa)/180)/1; //brzina u radianima/sek


v_B = v_alfa1*duzg2; //brzina tacke B u mm/s


double cos_duzg2;
double cos_alfa1;
double sin_alfa1;

sin_alfa1 =sin((PI*alfa1)/180);
cos_alfa1 =cos((PI*alfa1)/180);
cos_duzg2 =cos_alfa1*duzg2/1000;





double M1_duzg2;
double M2_duzg2;
/*
M1_duzg2 = (cos_duzg2)*(-g)*(Ms2);

M2_duzg2 = -((3*duzg2)/(2*1000))*cos_alfa1*Ms2*g;
*/

double sumMo;

M1_duzg2 = (duzg2/(2*1000))*cos_alfa1*(-Ms2*g);
M2_duzg2 = (duzg2/1000)*cos_alfa1*(-Mt*g);


sumMo = M1_duzg2 +M2_duzg2;

//Ubrzanja


double at;
double an;
double v_Bms;
v_Bms = v_B/1000;  // Vb u m/s
at = 0;//v_B/0;

an = (v_Bms*v_Bms)/(duzg2/1000);



double Rxa;
double Rya;
double v_alf2;


v_alf2 = v_alfa1*v_alfa1;


Rxa = Ms2*(duzg2/2000)*v_alf2*(-sin_alfa1)+(Mt*(duzg2/1000)*v_alf2*(-sin_alfa1));

Rya =Ms2*g+Mt*g+(Ms2*(duzg2/2000)*v_alf2*cos_alfa1)+(Mt*(duzg2/1000)*v_alf2*cos_alfa1);


double P;
double opm;
double varO;
double PkW;

varO = 360/m_Valfa;
opm = 60/varO;



//opm = 
//opm = 60;

P = sumMo*opm*((2*PI)/60);

PkW = P/1000;





//--------------------------------------------------------
//KRAJNJE TACKE GREDA
//--------------------------------------------------------

//KRAJNJA TACKA G1

CPen PenBlue(PS_SOLID,40,RGB(0,0,255));
pDC->SelectObject(PenBlue);
pDC->Ellipse(xg1-20,yg1-30,xg1+30,yg1+20);

//KRAJNJA TACKA G1

pDC->Ellipse(dxg2-20,dyg2-30,dxg2+30,dyg2+20);



//--------------------------------------------------------
//POMOCNE LINIJE
//--------------------------------------------------------


//--------
//ZA CM G2

CPen PenRed1(PS_SOLID,1,RGB(255,0,0));
pDC->SelectObject(PenRed1);

//Vertikala pomocna
pDC->MoveTo(xcg2,0);
pDC->LineTo(xcg2,ycg2);


//Horizontala
pDC->MoveTo(0,ycg2);
pDC->LineTo(xcg2,ycg2);


//-------
//ZA K G2

CPen PenBlue1(PS_SOLID,1,RGB(0,0,255));
pDC->SelectObject(PenBlue1);

//Vertikala pomocna
pDC->MoveTo(dxg2,0);
pDC->LineTo(dxg2,dyg2);


//Horizontala
pDC->MoveTo(0,dyg2);
pDC->LineTo(dxg2,dyg2);
//--------------------------------------------------------
//TEKST NA LENJIRIMA
//--------------------------------------------------------

pDC->SetTextColor(RGB(0, 0, 0));
pDC->SetBkMode(TRANSPARENT);

//ZA X OSU
	for (int kt1= 0; kt1<5500; kt1+=500) 
	{
		        
        CString string;
        string.Format (("%d"), (kt1 ) );
        pDC->TextOut (kt1, -250, string);
    }
//ZA Y OSU
	for (int kt2= 0; kt2<5500; kt2+=500) 
	{
        
        CString string;
        string.Format (("%d"), kt2 );
        pDC->TextOut (-300, kt2, string);
    }
//--------------------------------------------------------
//PODEOCI NA LENJIRU
//--------------------------------------------------------
	pDC->SelectObject(PenBlack1);
    for(int koox2 = 0; koox2 < 5500; koox2 += 500)
    {
        pDC->MoveTo(koox2, 0);
        pDC->LineTo(koox2, -250);
	

    }
	for(int kooy2 = 0; kooy2 < 5500; kooy2 += 500)
    {
        pDC->MoveTo(0, kooy2);
        pDC->LineTo(-250, kooy2);
	
    }

	for(int koox3 = 0; koox3 < 5500; koox3 += 250)
    {
        pDC->MoveTo(koox3, 0);
        pDC->LineTo(koox3, -100);
	
    }
	for(int kooy3 = 0; kooy3 < 5500; kooy3 += 250)
    {
        pDC->MoveTo(0, kooy3);
        pDC->LineTo(-100, kooy3);
	
    }
//--------------------------------------------------------
//TEKST TACAKA A I B
//--------------------------------------------------------

pDC->SetTextColor(RGB(255,0,0));

pDC->TextOut (xg1+50,yg1,"A");


pDC->TextOut (dxg2,dyg2 ,"B");




//--------------------------------------------------------
//GRAFICKI PRIKAZ INFORMACIJA U VIDU TEKSTA
//--------------------------------------------------------

pDC->SetTextColor(RGB(0,0,0));
char ma1[10];		 
sprintf(ma1,"Duzina osnove :    %2f mm", duzg1);
pDC->TextOut (5200,5000 ,ma1);

char ma2[10];		 
sprintf(ma2,"Duzina ruke      :    %2f mm", duzg2);
pDC->TextOut (5200,4800 ,ma2);



double TXTalfa1;
TXTalfa1 = alfa1;

char ma3[10];		 
sprintf(ma3,"Ugao alfa          :    %2f stepeni", dalfa1);
pDC->TextOut (5200,4600 ,ma3);

char mb3[10];		 
sprintf(mb3,"Ugao alfa(RAD) :    %2f radiana", alfa1RAD);
pDC->TextOut (5200,4400 ,mb3);

double TXTmzumx;
TXTmzumx= m_Zumx;

char ma4[10];		 
sprintf(ma4,"Zum X      :    %2f mm", TXTmzumx);
pDC->TextOut (7700,5000 ,ma4);

double TXTmzumy;
TXTmzumy= m_Zumy;

char ma5[10];		 
sprintf(ma5,"Zum Y      :    %2f mm", TXTmzumy);
pDC->TextOut (7700,4800 ,ma5);

pDC->SetTextColor(RGB(255,0,0));
char ma6[10];		 
sprintf(ma6,"CM g2 x            :    %2f mm", (dxg2+xg1)/2);
pDC->TextOut (5200,4200 ,ma6);

char ma7[10];		 
sprintf(ma7,"CM g2 y            :    %2f mm", (dyg2+yg1)/2);
pDC->TextOut (5200,4000 ,ma7);

pDC->SetTextColor(RGB(0,0,255));
char ma8[10];		 
sprintf(ma8,"dx g2                :    %2f mm", dxg2);
pDC->TextOut (5200,3800 ,ma8);

char ma9[10];		 
sprintf(ma9,"dy g2                :    %2f mm", dyg2);
pDC->TextOut (5200,3600 ,ma9);

pDC->SetTextColor(RGB(0,0,0));
char mc1[10];		 
sprintf(mc1,"Masa grede      :    %2f kg", Ms2);
pDC->TextOut (5200,3400 ,mc1);

char mc2[10];		 
sprintf(mc2,"Masa tereta      :    %2f kg", Mt);
pDC->TextOut (5200,3200 ,mc2);


char ma10[10];		 
sprintf(ma10,"Ugaona brzina g1:    %2f rad/s", v_alfa1);
pDC->TextOut (5200,3000 ,ma10);

char ma11[10];		 
sprintf(ma11,"Ugaona brzina g1:    %2f st/s",m_Valfa );
pDC->TextOut (5200,2800 ,ma11);

char ma12[10];		 
sprintf(ma12,"M usled grede:    %2f Nm",M1_duzg2 );
pDC->TextOut (5200,2600 ,ma12);


char ma13[10];		 
sprintf(ma13,"M usled tereta:    %2f Nm",M2_duzg2 );
pDC->TextOut (5200,2400 ,ma13);



char ma14[10];		 
sprintf(ma14,"Moment u A:    %2f Nm",sumMo );
pDC->TextOut (5200,2200 ,ma14);


char ma15[10];		 
sprintf(ma15,"Fx u A:    %2f N",Rxa );
pDC->TextOut (5200,2000 ,ma15);

char ma16[10];		 
sprintf(ma16,"Fy u A:    %2f N",Rya );
pDC->TextOut (5200,1800 ,ma16);

char ma17[10];		 
sprintf(ma17,"Brzina Vb:    %2f m/s", v_B/1000);
pDC->TextOut (5200,1600 ,ma17);

char ma18[10];		 
sprintf(ma18,"OPM:    %2f ",opm );
pDC->TextOut (5200,1400 ,ma18);

char ma19[10];		 
sprintf(ma19,"Snaga:    %2f kW ",PkW );
pDC->TextOut (5200,1200 ,ma19);

char ma20[10];		 
sprintf(ma20,"Normalno ubrzanje:    %2f m/s^2 ",an );
pDC->TextOut (5200,1000 ,ma20);


char ma21[10];		 
sprintf(ma21,"G:    %2f",an/g );
pDC->TextOut (5200,800 ,ma21);
/*

  ZA PROVERU
char ma21[10];		 
sprintf(ma21,"Snaga:    %2f ",sumMo*10*((2*PI)/60)/1000);
pDC->TextOut (5200,1000 ,ma21);
*/





ReleaseDC(pDC);
}

/////////////////////////////////////////////////////////////////////////////
// CPolarisView diagnostics

#ifdef _DEBUG
void CPolarisView::AssertValid() const
{
	CView::AssertValid();
}

void CPolarisView::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}

CPolarisDoc* CPolarisView::GetDocument() // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CPolarisDoc)));
	return (CPolarisDoc*)m_pDocument;
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CPolarisView message handlers

void CPolarisView::OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags) 
{
	switch (nChar)
 {
	case VK_F1:
		m_Zumx +=10;
		m_Zumy +=10;
	
		Invalidate();
		break;

	case VK_F2:
		m_Zumx -=10;
		m_Zumy -=10;
	
		if (m_Zumx < 10)
		{
			m_Zumx =10;
			m_Zumy =10;
		}
		Invalidate();
		break;

	case 0x51://q
	
		alfa1+=1;

	
		Invalidate();
		break;

	case 0x57://w
		
		alfa1-=1;

	
		Invalidate();
		break;
	}


	CView::OnKeyDown(nChar, nRepCnt, nFlags);
}

void CPolarisView::OnParametri() 
{
   CCParametri dlg;
   dlg.m_Aky = m_Aky;
   dlg.m_Duz2 = m_Duz2;
   dlg.m_MasaR = m_MasaR;
   dlg.m_MasaT = m_MasaT;
   dlg.m_Valfa = m_Valfa;
   dlg.m_Akx = m_Akx;

   if (dlg.DoModal () == IDOK)
	{
		m_Aky= dlg.m_Aky;
		m_Duz2= dlg.m_Duz2;
        m_MasaR=dlg.m_MasaR;
        m_MasaT=dlg.m_MasaT;
        m_Valfa=dlg.m_Valfa;
		m_Akx = dlg.m_Akx;

		Invalidate ();
	}
}
