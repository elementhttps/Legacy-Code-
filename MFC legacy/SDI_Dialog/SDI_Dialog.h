// SDI_Dialog.h : main header file for the SDI_DIALOG application
//

#if !defined(AFX_SDI_DIALOG_H__81AC8DE1_661F_4CCF_B943_49995DA6C6C9__INCLUDED_)
#define AFX_SDI_DIALOG_H__81AC8DE1_661F_4CCF_B943_49995DA6C6C9__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"       // main symbols

/////////////////////////////////////////////////////////////////////////////
// CSDI_DialogApp:
// See SDI_Dialog.cpp for the implementation of this class
//

class CSDI_DialogApp : public CWinApp
{
public:
	CSDI_DialogApp();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSDI_DialogApp)
	public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL

// Implementation
	//{{AFX_MSG(CSDI_DialogApp)
	afx_msg void OnAppAbout();
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SDI_DIALOG_H__81AC8DE1_661F_4CCF_B943_49995DA6C6C9__INCLUDED_)
