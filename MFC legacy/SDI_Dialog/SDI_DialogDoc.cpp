// SDI_DialogDoc.cpp : implementation of the CSDI_DialogDoc class
//

#include "stdafx.h"
#include "SDI_Dialog.h"

#include "SDI_DialogDoc.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CSDI_DialogDoc

IMPLEMENT_DYNCREATE(CSDI_DialogDoc, CDocument)

BEGIN_MESSAGE_MAP(CSDI_DialogDoc, CDocument)
	//{{AFX_MSG_MAP(CSDI_DialogDoc)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CSDI_DialogDoc construction/destruction

CSDI_DialogDoc::CSDI_DialogDoc()
{
	// TODO: add one-time construction code here

}

CSDI_DialogDoc::~CSDI_DialogDoc()
{
}

BOOL CSDI_DialogDoc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;

	// TODO: add reinitialization code here
	// (SDI documents will reuse this document)

	return TRUE;
}



/////////////////////////////////////////////////////////////////////////////
// CSDI_DialogDoc serialization

void CSDI_DialogDoc::Serialize(CArchive& ar)
{
	if (ar.IsStoring())
	{
		// TODO: add storing code here
	}
	else
	{
		// TODO: add loading code here
	}
}

/////////////////////////////////////////////////////////////////////////////
// CSDI_DialogDoc diagnostics

#ifdef _DEBUG
void CSDI_DialogDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CSDI_DialogDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CSDI_DialogDoc commands
