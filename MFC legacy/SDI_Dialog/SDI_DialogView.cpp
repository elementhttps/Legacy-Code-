// SDI_DialogView.cpp : implementation of the CSDI_DialogView class
//

#include "stdafx.h"
#include "SDI_Dialog.h"

#include "SDI_DialogDoc.h"
#include "SDI_DialogView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CSDI_DialogView

IMPLEMENT_DYNCREATE(CSDI_DialogView, CView)

BEGIN_MESSAGE_MAP(CSDI_DialogView, CView)
	//{{AFX_MSG_MAP(CSDI_DialogView)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG_MAP
	// Standard printing commands
	ON_COMMAND(ID_FILE_PRINT, CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_DIRECT, CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_PREVIEW, CView::OnFilePrintPreview)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CSDI_DialogView construction/destruction

CSDI_DialogView::CSDI_DialogView()
{
	// TODO: add construction code here

}

CSDI_DialogView::~CSDI_DialogView()
{
}

BOOL CSDI_DialogView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	return CView::PreCreateWindow(cs);
}

/////////////////////////////////////////////////////////////////////////////
// CSDI_DialogView drawing

void CSDI_DialogView::OnDraw(CDC* pDC)
{
	CSDI_DialogDoc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);
	// TODO: add draw code for native data here
}

/////////////////////////////////////////////////////////////////////////////
// CSDI_DialogView printing

BOOL CSDI_DialogView::OnPreparePrinting(CPrintInfo* pInfo)
{
	// default preparation
	return DoPreparePrinting(pInfo);
}

void CSDI_DialogView::OnBeginPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: add extra initialization before printing
}

void CSDI_DialogView::OnEndPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: add cleanup after printing
}

/////////////////////////////////////////////////////////////////////////////
// CSDI_DialogView diagnostics

#ifdef _DEBUG
void CSDI_DialogView::AssertValid() const
{
	CView::AssertValid();
}

void CSDI_DialogView::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}

CSDI_DialogDoc* CSDI_DialogView::GetDocument() // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CSDI_DialogDoc)));
	return (CSDI_DialogDoc*)m_pDocument;
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CSDI_DialogView message handlers
