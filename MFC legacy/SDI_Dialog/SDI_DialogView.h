// SDI_DialogView.h : interface of the CSDI_DialogView class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_SDI_DIALOGVIEW_H__5498E5A9_3AA0_4D28_BBE5_BFAF47FCD917__INCLUDED_)
#define AFX_SDI_DIALOGVIEW_H__5498E5A9_3AA0_4D28_BBE5_BFAF47FCD917__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


class CSDI_DialogView : public CView
{
protected: // create from serialization only
	CSDI_DialogView();
	DECLARE_DYNCREATE(CSDI_DialogView)

// Attributes
public:
	CSDI_DialogDoc* GetDocument();

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSDI_DialogView)
	public:
	virtual void OnDraw(CDC* pDC);  // overridden to draw this view
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	protected:
	virtual BOOL OnPreparePrinting(CPrintInfo* pInfo);
	virtual void OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo);
	virtual void OnEndPrinting(CDC* pDC, CPrintInfo* pInfo);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CSDI_DialogView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	//{{AFX_MSG(CSDI_DialogView)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

#ifndef _DEBUG  // debug version in SDI_DialogView.cpp
inline CSDI_DialogDoc* CSDI_DialogView::GetDocument()
   { return (CSDI_DialogDoc*)m_pDocument; }
#endif

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SDI_DIALOGVIEW_H__5498E5A9_3AA0_4D28_BBE5_BFAF47FCD917__INCLUDED_)
