// ShapesDoc.h : interface of the CShapesDoc class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_SHAPESDOC_H__7A5C150B_F154_4C61_9C00_10DC41B60D64__INCLUDED_)
#define AFX_SHAPESDOC_H__7A5C150B_F154_4C61_9C00_10DC41B60D64__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


class CShapesDoc : public CDocument
{
protected: // create from serialization only
	CShapesDoc();
	DECLARE_DYNCREATE(CShapesDoc)

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CShapesDoc)
	public:
	virtual BOOL OnNewDocument();
	virtual void Serialize(CArchive& ar);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CShapesDoc();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	//{{AFX_MSG(CShapesDoc)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SHAPESDOC_H__7A5C150B_F154_4C61_9C00_10DC41B60D64__INCLUDED_)
