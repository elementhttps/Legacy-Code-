// SkecherDoc.cpp : implementation of the CSkecherDoc class
//

#include "stdafx.h"
#include "Skecher.h"

#include "SkecherDoc.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CSkecherDoc

IMPLEMENT_DYNCREATE(CSkecherDoc, CDocument)

BEGIN_MESSAGE_MAP(CSkecherDoc, CDocument)
	//{{AFX_MSG_MAP(CSkecherDoc)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CSkecherDoc construction/destruction

CSkecherDoc::CSkecherDoc()
{
	// TODO: add one-time construction code here

}

CSkecherDoc::~CSkecherDoc()
{
}

BOOL CSkecherDoc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;

	// TODO: add reinitialization code here
	// (SDI documents will reuse this document)

	return TRUE;
}



/////////////////////////////////////////////////////////////////////////////
// CSkecherDoc serialization

void CSkecherDoc::Serialize(CArchive& ar)
{
	if (ar.IsStoring())
	{
		// TODO: add storing code here
	}
	else
	{
		// TODO: add loading code here
	}
}

/////////////////////////////////////////////////////////////////////////////
// CSkecherDoc diagnostics

#ifdef _DEBUG
void CSkecherDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CSkecherDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CSkecherDoc commands
