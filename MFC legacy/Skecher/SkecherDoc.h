// SkecherDoc.h : interface of the CSkecherDoc class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_SKECHERDOC_H__10A35B98_41DD_47E7_8476_9C95A047DF42__INCLUDED_)
#define AFX_SKECHERDOC_H__10A35B98_41DD_47E7_8476_9C95A047DF42__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


class CSkecherDoc : public CDocument
{
protected: // create from serialization only
	CSkecherDoc();
	DECLARE_DYNCREATE(CSkecherDoc)

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSkecherDoc)
	public:
	virtual BOOL OnNewDocument();
	virtual void Serialize(CArchive& ar);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CSkecherDoc();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	//{{AFX_MSG(CSkecherDoc)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SKECHERDOC_H__10A35B98_41DD_47E7_8476_9C95A047DF42__INCLUDED_)
