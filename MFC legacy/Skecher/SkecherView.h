// SkecherView.h : interface of the CSkecherView class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_SKECHERVIEW_H__93AA8781_7B02_4304_8F1D_5FCC4F549186__INCLUDED_)
#define AFX_SKECHERVIEW_H__93AA8781_7B02_4304_8F1D_5FCC4F549186__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


class CSkecherView : public CView
{
protected: // create from serialization only
	CSkecherView();
	DECLARE_DYNCREATE(CSkecherView)

// Attributes
public:
	CSkecherDoc* GetDocument();

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSkecherView)
	public:
	virtual void OnDraw(CDC* pDC);  // overridden to draw this view
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	protected:
	virtual BOOL OnPreparePrinting(CPrintInfo* pInfo);
	virtual void OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo);
	virtual void OnEndPrinting(CDC* pDC, CPrintInfo* pInfo);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CSkecherView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	//{{AFX_MSG(CSkecherView)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

#ifndef _DEBUG  // debug version in SkecherView.cpp
inline CSkecherDoc* CSkecherView::GetDocument()
   { return (CSkecherDoc*)m_pDocument; }
#endif

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SKECHERVIEW_H__93AA8781_7B02_4304_8F1D_5FCC4F549186__INCLUDED_)
