 // Implementations of the element classes
   
#include "stdafx.h"

#include "OurConstants.h"

#include "Elements.h"


   // Add definitions for member functions here

// CLine class constructor
   CLine::CLine(const CPoint& Start, const CPoint& End, const COLORREF& Color)

   {
      m_StartPoint = Start;      // Set line start point
      m_EndPoint = End;          // Set line end point
      m_Color = Color;           // Set line color
	  m_Pen = 1; 
	  // Define the enclosing rectangle
      m_EnclosingRect = CRect(Start, End);
      m_EnclosingRect.NormalizeRect();
   }
   void CLine::Draw(CDC* pDC) const
   {
      // Create a pen for this object and
      // initialize it to the object color and line width
      CPen aPen;
      if(!aPen.CreatePen(PS_SOLID, m_Pen, m_Color))
      {
         // Pen creation failed. Abort the program
         AfxMessageBox("Pen creation failed drawing a line", MB_OK);
         AfxAbort();
   }

      CPen* pOldPen = pDC->SelectObject(&aPen);  // Select the pen
      // Now draw the line
      pDC->MoveTo(m_StartPoint);
      pDC->LineTo(m_EndPoint);
      pDC->SelectObject(pOldPen);                // Restore the old pen

   }
   // CRectangle class constructor
   CRectangle:: CRectangle(const CPoint& Start, const CPoint& End,
const COLORREF&
   Color)
   {
      m_Color = Color;                   // Set rectangle color
      m_Pen = 1;                         // Set pen width
      // Define the enclosing rectangle
      m_EnclosingRect = CRect(Start, End);
      m_EnclosingRect.NormalizeRect();
   }

/*    BoundingRect  = m_EnclosingRect + CRect(m_Pen, m_Pen, m_Pen,m_Pen);

   BoundingRect = m_EnclosingRect;
   BoundingRect.top -= m_Pen;
   BoundingRect.left -= m_Pen;
   BoundingRect.bottom += m_Pen;
   BoundingRect.right += m_Pen;
*/