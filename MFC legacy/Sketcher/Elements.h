#if !defined(Elements_h)
   #define Elements_h
   // Generic element class
/////////////////////////////////////////////////////////////////////////////////
//objekat
/////////////////////////////////////////////////////////////////////////////////
   class CElement : public CObject
   {
      protected:
      COLORREF m_Color;                          // Color of an element
	  CRect m_EnclosingRect;                    // Rectangle enclosing an element

      int m_Pen;                                // Pen width

   public:
      virtual ~CElement(){}                      // Virtual destructor
      // Virtual draw operation
      virtual void Draw(CDC* pDC) const {}       // Virtual draw operation

      CRect GetBoundRect() const;                // Get the bounding rectangle for an element

                                                 
   protected:
      CElement(){}                               // Default constructor

   };


/////////////////////////////////////////////////////////////////////////////////
   // Class defining a line object
   
   
   
   class CLine : public CElement
   {
       public:
      virtual void Draw(CDC* pDC) const; // Function to display a line
      // Constructor for a line object
      CLine(const CPoint& Start, const CPoint& End, const COLORREF&Color);

   protected:
      CPoint m_StartPoint;               // Start point of line
      CPoint m_EndPoint;                 // End point of line
	 
      CLine(){}                          // Default constructor -should not be used

   };
// Get the bounding rectangle for an element
   CRect CElement::GetBoundRect() const
   {
      CRect BoundingRect;              // Object to store boundingrectangle

      BoundingRect = m_EnclosingRect;  // Store the enclosingrectangle

      // Increase the rectangle by the pen width
      BoundingRect.InflateRect(m_Pen, m_Pen);
      return BoundingRect;             // Return the boundingrectangle

   }




   // Class defining a rectangle object
   class CRectangle : public CElement
   {
      public:
      virtual void Draw(CDC* pDC) const; // Function to display arectangle

      // Constructor for a rectangle object
      CRectangle(const CPoint& Start, const CPoint& End, const COLORREF& Color);

   protected:
      CRectangle(){}                     // Default constructor -should not be used

   };
   // Class defining a circle object
   class CCircle : public CElement
   {
      // Add class definition here
   };
   // Class defining a curve object
   class CCurve : public CElement
   {
      // Add class definition here
   };
   #endif //!defined(Elements_h)