// SketcherDoc.cpp : implementation of the CSketcherDoc class
//

#include "stdafx.h"
#include "Sketcher.h"

#include "SketcherDoc.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CSketcherDoc

IMPLEMENT_DYNCREATE(CSketcherDoc, CDocument)

BEGIN_MESSAGE_MAP(CSketcherDoc, CDocument)
	//{{AFX_MSG_MAP(CSketcherDoc)
	ON_COMMAND(ID_COLOR_BLACK, OnColorBlack)
	ON_COMMAND(ID_COLOR_BLUE, OnColorBlue)
	ON_COMMAND(ID_COLOR_GREEN, OnColorGreen)
	ON_COMMAND(ID_COLOR_RED, OnColorRed)
	ON_COMMAND(ID_ELEMENT_CIRCLE, OnElementCircle)
	ON_COMMAND(ID_ELEMENT_CURVE, OnElementCurve)
	ON_COMMAND(ID_ELEMENT_LINE, OnElementLine)
	ON_COMMAND(ID_ELEMENT_RECTANGLE, OnElementRectangle)
	ON_UPDATE_COMMAND_UI(ID_COLOR_BLACK, OnUpdateColorBlack)
	ON_UPDATE_COMMAND_UI(ID_COLOR_BLUE, OnUpdateColorBlue)
	ON_UPDATE_COMMAND_UI(ID_COLOR_GREEN, OnUpdateColorGreen)
	ON_UPDATE_COMMAND_UI(ID_COLOR_RED, OnUpdateColorRed)
	ON_UPDATE_COMMAND_UI(ID_ELEMENT_CIRCLE, OnUpdateElementCircle)
	ON_UPDATE_COMMAND_UI(ID_ELEMENT_CURVE, OnUpdateElementCurve)
	ON_UPDATE_COMMAND_UI(ID_ELEMENT_LINE, OnUpdateElementLine)
	ON_UPDATE_COMMAND_UI(ID_ELEMENT_RECTANGLE, OnUpdateElementRectangle)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CSketcherDoc construction/destruction

CSketcherDoc::CSketcherDoc()
{
	// TODO: add one-time construction code here
	 m_Element = LINE;   // Set initial element type
     m_Color = BLACK;    // Set initial drawing color

}

CSketcherDoc::~CSketcherDoc()
{
}

BOOL CSketcherDoc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;

	// TODO: add reinitialization code here
	// (SDI documents will reuse this document)

	return TRUE;
}



/////////////////////////////////////////////////////////////////////////////
// CSketcherDoc serialization

void CSketcherDoc::Serialize(CArchive& ar)
{
	if (ar.IsStoring())
	{
		// TODO: add storing code here
	}
	else
	{
		// TODO: add loading code here
	}
}

/////////////////////////////////////////////////////////////////////////////
// CSketcherDoc diagnostics

#ifdef _DEBUG
void CSketcherDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CSketcherDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CSketcherDoc commands

void CSketcherDoc::OnColorBlack() 
{
	m_Color = BLACK;// Set the drawing color to black

	 
	
}

void CSketcherDoc::OnColorBlue() 
{
	
	 m_Color = BLUE;// Set the drawing color to black
	
}

void CSketcherDoc::OnColorGreen() 
{
	
	 m_Color = GREEN;// Set the drawing color to blacke
	
}

void CSketcherDoc::OnColorRed() 
{
	m_Color = RED;// Set the drawing color to black
	
}

void CSketcherDoc::OnElementCircle() 
{
	
	 m_Element = CIRCLE;        // Set element type as a line
	
}

void CSketcherDoc::OnElementCurve() 
{
	
	 m_Element = CURVE;        // Set element type as a line
	
}

void CSketcherDoc::OnElementLine() 
{
	 m_Element = LINE;        // Set element type as a line
	
}

void CSketcherDoc::OnElementRectangle() 
{
	 m_Element = RECTANGLE;        // Set element type as a line
	
}

void CSketcherDoc::OnUpdateColorBlack(CCmdUI* pCmdUI) 
{
	 pCmdUI->SetCheck(m_Color==BLACK);  //virtual void SetCheck(int nCheck = 1) FUNKCIJA 0 ILI 1 AKO JE m_Color == BLACK ONDA JE 1 AKO NIJE ONDA JE 0
	if(m_Color == BLACK)
         pCmdUI->SetText("BLACK");
      else
         pCmdUI->SetText("black");
}

void CSketcherDoc::OnUpdateColorBlue(CCmdUI* pCmdUI) 
{
	pCmdUI->SetCheck(m_Color==BLUE);
	
}

void CSketcherDoc::OnUpdateColorGreen(CCmdUI* pCmdUI) 
{
	pCmdUI->SetCheck(m_Color==GREEN);
	
}

void CSketcherDoc::OnUpdateColorRed(CCmdUI* pCmdUI) 
{
	pCmdUI->SetCheck(m_Color==RED);
	
}

void CSketcherDoc::OnUpdateElementCircle(CCmdUI* pCmdUI) 
{
	pCmdUI->SetCheck(m_Element==CIRCLE);
	
}

void CSketcherDoc::OnUpdateElementCurve(CCmdUI* pCmdUI) 
{
	pCmdUI->SetCheck(m_Element==CURVE);
	
}

void CSketcherDoc::OnUpdateElementLine(CCmdUI* pCmdUI) 
{
	pCmdUI->SetCheck(m_Element==LINE);
	
}

void CSketcherDoc::OnUpdateElementRectangle(CCmdUI* pCmdUI) 
{
	pCmdUI->SetCheck(m_Element==RECTANGLE);
	
}
