// SketcherView.cpp : implementation of the CSketcherView class
//

#include "stdafx.h"
#include "Sketcher.h"

#include "SketcherDoc.h"
#include "SketcherView.h"

#include "Elements.h"// NOVI ELEMENTI

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CSketcherView

IMPLEMENT_DYNCREATE(CSketcherView, CView)

BEGIN_MESSAGE_MAP(CSketcherView, CView)
	//{{AFX_MSG_MAP(CSketcherView)
	ON_WM_LBUTTONDOWN()
	ON_WM_LBUTTONUP()
	ON_WM_MOUSEMOVE()
	//}}AFX_MSG_MAP
	// Standard printing commands
	ON_COMMAND(ID_FILE_PRINT, CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_DIRECT, CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_PREVIEW, CView::OnFilePrintPreview)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CSketcherView construction/destruction

CSketcherView::CSketcherView()
{
	 m_FirstPoint = CPoint(0,0);               // Set  1st  recorded point to 0,0

     m_SecondPoint = CPoint(0,0);              // Set 2nd recorded point to 0,0
	 m_pTempElement = NULL;              // Set temporary element pointer to 0



}

CSketcherView::~CSketcherView()
{
}

BOOL CSketcherView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	return CView::PreCreateWindow(cs);
}

/////////////////////////////////////////////////////////////////////////////
// CSketcherView drawing

void CSketcherView::OnDraw(CDC* pDC)
{

	CSketcherDoc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);
	CPen aPen;
      aPen.CreatePen(PS_SOLID, 2, RGB(0, 0, 255));
      CPen* pOldPen = pDC->SelectObject(&aPen);
	// TODO: add draw code for native data here
	      pDC->MoveTo(50,50);        // Set the current position
      pDC->LineTo(50,200);       // Draw a vertical line down 150 units

      pDC->LineTo(150,200);      // Draw a horizontal line right 100 units

      pDC->LineTo(150,50);       //  Draw a vertical line up 150 units
      pDC->LineTo(50,50);   pDC->SelectObject(pOldPen);      // Draw a horizontal line left 100 units
	  pDC->Arc(50,50,150,150,100,50,150,100);  // Draw the 1st (large)circle

      // Define the bounding rectangle for the 2nd (smaller) circle
      CRect* pRect = new CRect(250,50,300,100);
      CPoint Start(275,100);                   // Arc start point
      CPoint End(250,75);                      // Arc end point
      pDC->Arc(pRect,Start, End);              // Draw the second circle

      delete pRect;
	 

}

/////////////////////////////////////////////////////////////////////////////
// CSketcherView printing

BOOL CSketcherView::OnPreparePrinting(CPrintInfo* pInfo)
{
	// default preparation
	return DoPreparePrinting(pInfo);
}

void CSketcherView::OnBeginPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: add extra initialization before printing
}

void CSketcherView::OnEndPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: add cleanup after printing
}

/////////////////////////////////////////////////////////////////////////////
// CSketcherView diagnostics

#ifdef _DEBUG
void CSketcherView::AssertValid() const
{
	CView::AssertValid();
}

void CSketcherView::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}

CSketcherDoc* CSketcherView::GetDocument() // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CSketcherDoc)));
	return (CSketcherDoc*)m_pDocument;
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CSketcherView message handlers

void CSketcherView::OnLButtonDown(UINT nFlags, CPoint point) // nFlags je broj a CPoint je objekat pozicije
{
	m_FirstPoint = point;                      // Record the cursor position

	
	CView::OnLButtonDown(nFlags, point);
}

void CSketcherView::OnLButtonUp(UINT nFlags, CPoint point) 
{
	if(nFlags & MK_LBUTTON)
      {
         m_SecondPoint = point;                  // Save the current cursor position

         // Test for a previous temporary element
         {
            // We get to here if there was a previous mouse move
            // so add code to delete the old element
         }
         // Add code to create new element
         // and cause it to be drawn
      }
	
	CView::OnLButtonUp(nFlags, point);
}

void CSketcherView::OnMouseMove(UINT nFlags, CPoint point) 
{
	// TODO: Add your message handler code here and/or call default
	
	CView::OnMouseMove(nFlags, point);
}
