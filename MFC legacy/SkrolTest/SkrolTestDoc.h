// SkrolTestDoc.h : interface of the CSkrolTestDoc class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_SKROLTESTDOC_H__2074E101_AE9F_4957_A8AD_0C4AF2D59CFB__INCLUDED_)
#define AFX_SKROLTESTDOC_H__2074E101_AE9F_4957_A8AD_0C4AF2D59CFB__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


class CSkrolTestDoc : public CDocument
{
protected: // create from serialization only
	CSkrolTestDoc();
	DECLARE_DYNCREATE(CSkrolTestDoc)

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSkrolTestDoc)
	public:
	virtual BOOL OnNewDocument();
	virtual void Serialize(CArchive& ar);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CSkrolTestDoc();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	//{{AFX_MSG(CSkrolTestDoc)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};
#ifndef _DEBUG  // debug version in ScrollDemoView.cpp
inline CScrollDemoDoc* CScrollDemoView::GetDocument()
   { return (CScrollDemoDoc*)m_pDocument; }
#endif

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SKROLTESTDOC_H__2074E101_AE9F_4957_A8AD_0C4AF2D59CFB__INCLUDED_)
