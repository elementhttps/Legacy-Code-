// SkrolTestView.cpp : implementation of the CSkrolTestView class
//

#include "stdafx.h"
#include "SkrolTest.h"
#include "SkrolTestDoc.h"

#include "SkrolTestView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CSkrolTestView

IMPLEMENT_DYNCREATE(CSkrolTestView, CView)

BEGIN_MESSAGE_MAP(CSkrolTestView, CView)
	//{{AFX_MSG_MAP(CSkrolTestView)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CSkrolTestView construction/destruction

CSkrolTestView::CSkrolTestView()
{
	// TODO: add construction code here

}

CSkrolTestView::~CSkrolTestView()
{
}

BOOL CSkrolTestView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	return CView::PreCreateWindow(cs);
}

/////////////////////////////////////////////////////////////////////////////
// CSkrolTestView drawing

void CSkrolTestView::OnDraw(CDC* pDC)
{
	CSkrolTestDoc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);
	//////////////////////////////////////////////////////
//CENTAR I GRAFICKI PRIKAZ
//////////////////////////////////////////////////////
   

	
	pDC->SetMapMode(MM_ANISOTROPIC);
	pDC->SetViewportOrg(60, 620);
	pDC->SetWindowExt(100000,100000); //SetWindowExtent(100,100)  SetViewportExtent(200,200)  100 jedinica = 200 pixela
	                                                                                             //1 m = 2 pixela
	pDC->SetViewportExt(1000,-1000);// Orijentacija osa

CSize size1;

    CPen pen (PS_SOLID, 0, RGB (0, 0, 0));
    CPen* pOldPen = pDC->SelectObject (&pen);
    for (int i=0; i<100; i++) {
        int y = (i * m_nCellHeight) + m_nCellHeight;
        pDC->MoveTo (0, y);
        pDC->LineTo (size1.cx, y);
    }

    for (int j=0; j<100; j++) {
        int x = (j * m_nCellWidth) + m_nRibbonWidth;
        pDC->MoveTo (x, 0);
        pDC->LineTo (x, size1.cy);
    }
	


//////////////////////////////////////////////////////
// OSE
//////////////////////////////////////////////////////	
	pDC->MoveTo(-1000,     0);
	pDC->LineTo( 20000000,     0);
	pDC->MoveTo(   0, -1000);
	pDC->LineTo(   0,  20000000);
//////////////////////////////////////////////////////	
//POZICIJA OZNAKA
//////////////////////////////////////////////////////		
	
	//Koordinate 
	//////////////////////////////////////////////////////	
	pDC->SetTextColor(RGB(100, 100, 0));

    pDC->TextOut(56000, 2000, 'X');
	pDC->TextOut(500, 35000, 'Y');
   //////////////////////////////////////////////////////	
    
	
	
	//Lenjiri po x i y osi
	//////////////////////////////////////////////////////	

    CPen PenBlack3(PS_SOLID, 2, RGB(0, 0, 0));
	pDC->SelectObject(PenBlack3);
    
	for(int koox = 0; koox < 1000000; koox += 1000)
    {
        pDC->MoveTo(koox, 0);
        pDC->LineTo(koox, -1000);
    }

	for(int kooy = 0; kooy < 1000000; kooy += 1000)
    {
        pDC->MoveTo(0, kooy);
        pDC->LineTo(-1000, kooy);
    }
	



	

    



}

/////////////////////////////////////////////////////////////////////////////
// CSkrolTestView diagnostics

#ifdef _DEBUG
void CSkrolTestView::AssertValid() const
{
	CView::AssertValid();
}

void CSkrolTestView::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}

CSkrolTestDoc* CSkrolTestView::GetDocument() // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CSkrolTestDoc)));
	return (CSkrolTestDoc*)m_pDocument;
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CSkrolTestView message handlers
