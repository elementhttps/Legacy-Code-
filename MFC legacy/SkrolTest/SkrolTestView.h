// SkrolTestView.h : interface of the CSkrolTestView class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_SKROLTESTVIEW_H__459E2ED2_9B84_4EE7_A489_D14E7636CD18__INCLUDED_)
#define AFX_SKROLTESTVIEW_H__459E2ED2_9B84_4EE7_A489_D14E7636CD18__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


class CSkrolTestView : public CView
{
protected: // create from serialization only
	CSkrolTestView();
	DECLARE_DYNCREATE(CSkrolTestView)

// Attributes
public:
	CSkrolTestDoc* GetDocument();

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSkrolTestView)
	public:
	virtual void OnDraw(CDC* pDC);  // overridden to draw this view
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	protected:
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CSkrolTestView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	int m_nRibbonWidth;
    int m_nCellHeight;
    int m_nCellWidth;

	//{{AFX_MSG(CSkrolTestView)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

#ifndef _DEBUG  // debug version in SkrolTestView.cpp
inline CSkrolTestDoc* CSkrolTestView::GetDocument()
   { return (CSkrolTestDoc*)m_pDocument; }
#endif

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SKROLTESTVIEW_H__459E2ED2_9B84_4EE7_A489_D14E7636CD18__INCLUDED_)
