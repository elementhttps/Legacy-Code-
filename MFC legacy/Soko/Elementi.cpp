#include "stdafx.h"

#include <math.h>

#include "const_OBJEKTI.h"
#include "Elementi.h"

CLine::CLine(const CPoint& Start, const CPoint& End, const COLORREF& Color)
{
   m_StartPoint = Start;      // Set line start point
   m_EndPoint = End;          // Set line end point
   m_Color = Color;           // Set line color
   m_Pen = 10;                 // Set pen width

   // Define the enclosing rectangle
   m_EnclosingRect = CRect(Start, End);
   m_EnclosingRect.NormalizeRect();
}

void CLine::Draw(CDC* pDC) const
{
   // Create a pen for this object and
   // initialize it to the object color and line width
   CPen aPen;
   if(!aPen.CreatePen(PS_SOLID, m_Pen, m_Color))
   {
      // Pen creation failed. Abort the program
      AfxMessageBox("Pen creation failed drawing a line", MB_OK);
      AfxAbort();
   }

   CPen* pOldPen = pDC->SelectObject(&aPen);  // Select the pen

   // Now draw the line
   pDC->MoveTo(m_StartPoint);
   pDC->LineTo(m_EndPoint);

   pDC->SelectObject(pOldPen);                // Restore the old pen
}

CRect CElement::GetBoundRect() const
{
   CRect BoundingRect;              // Object to store bounding rectangle
   BoundingRect = m_EnclosingRect;  // Store the enclosing rectangle

   // Increase the rectangle by the pen width
   BoundingRect.InflateRect(m_Pen, m_Pen);

   return BoundingRect;             // Return the bounding rectangle
}