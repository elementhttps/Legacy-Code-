  #if !defined(Elementi_h)
   #define Elementi_h
   // Generic element class
/*
SokoDoc.cpp
CSokoDoc::CSokoDoc()
{
   m_Element = DUZ;   // Set initial element type
   m_Color = CRNA;    // Set initial drawing color

}
*/
   class CElement :   public CObject
   {
      // Add virtual function declarations here
protected:

   COLORREF m_Color;                          // Color of an element

   CRect m_EnclosingRect;                  // Rectangle enclosing an element
   int m_Pen;                              // Pen width

public:
   virtual ~CElement(){}                      // Virtual destructor

   // Virtual draw operation
   virtual void Draw(CDC* pDC) const {}       // Virtual draw operation
   CRect GetBoundRect() const;                // Get the bounding rectangle for an element

protected:
   CElement(){} 

   };



   // Class defining a line object
   class CLine : public CElement
   {
      // Add class definition here
public:
   virtual void Draw(CDC* pDC) const;  //funkcija za prikaz linije

   // Constructor for a line object
   CLine(const CPoint& Start, const CPoint& End, const COLORREF& Color); //konstruktor i parametri

protected:
   CPoint m_StartPoint;          // Start point of line
   CPoint m_EndPoint;            // End point of line

   CLine(){}  
   };



  #endif //!defined(Elements_h