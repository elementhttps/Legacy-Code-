// Soko.h : main header file for the SOKO application
//

#if !defined(AFX_SOKO_H__E541A6BF_9D61_4A4F_82F8_16AAECC0B2DF__INCLUDED_)
#define AFX_SOKO_H__E541A6BF_9D61_4A4F_82F8_16AAECC0B2DF__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"       // main symbols

/////////////////////////////////////////////////////////////////////////////
// CSokoApp:
// See Soko.cpp for the implementation of this class
//


#include "const_OBJEKTI.h"



class CSokoApp : public CWinApp
{
public:
	CSokoApp();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSokoApp)
	public:
	virtual BOOL InitInstance();
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	//}}AFX_VIRTUAL

// Implementation
	//{{AFX_MSG(CSokoApp)
	afx_msg void OnAppAbout();
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SOKO_H__E541A6BF_9D61_4A4F_82F8_16AAECC0B2DF__INCLUDED_)
