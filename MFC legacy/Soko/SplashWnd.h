// SplashWnd.h : header file
//
// �1998-2001 Codejock Software, All Rights Reserved.
// Based on the Visual C++ splash screen component.
//
// support@codejock.com
// http://www.codejock.com
//
//////////////////////////////////////////////////////////////////////

#ifndef __SPLASHWND__
#define __SPLASHWND__

/////////////////////////////////////////////////////////////////////////////
//   Splash Screen class

class CSplashWnd : public CWnd
{
protected:

    // -> Remarks:  Constructs a CSplashWnd object.
	CSplashWnd();

    // -> Remarks:  Destroys a CSplashWnd object, handles cleanup and de-allocation.
	virtual ~CSplashWnd();

protected:

	static BOOL        m_bShowSplashWnd; // TRUE if the splash screen is enabled.
	static CSplashWnd* m_pSplashWnd;     // Points to the splash screen.

public:
	CBitmap            m_bitmap;         // Splash screen image.

	static BOOL ShowSplashScreen(UINT uTimeOut,CWnd* pParentWnd = NULL);

	static void EnableSplashScreen(BOOL bEnable = TRUE);

	static BOOL PreTranslateAppMessage(MSG* pMsg);

static void HideSplashScreen();
//protected:

	
	


protected:

	//{{AFX_MSG(CSplashWnd)
	afx_msg void OnPaint();
	afx_msg void OnTimer(UINT nIDEvent);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // __SPLASHWND__
