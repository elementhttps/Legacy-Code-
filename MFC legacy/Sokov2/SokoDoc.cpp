// SokoDoc.cpp : implementation of the CSokoDoc class
//

#include "stdafx.h"
#include "Soko.h"

#include "Elementi.h"

#include "SokoDoc.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CSokoDoc

IMPLEMENT_DYNCREATE(CSokoDoc, CDocument)

BEGIN_MESSAGE_MAP(CSokoDoc, CDocument)
	//{{AFX_MSG_MAP(CSokoDoc)
	ON_COMMAND(ID_BOJA_CRNA, OnBojaCrna)
	ON_COMMAND(ID_BOJA_CRVENA, OnBojaCrvena)
	ON_COMMAND(ID_BOJA_PLAVA, OnBojaPlava)
	ON_COMMAND(ID_BOJA_ZELENA, OnBojaZelena)
	ON_COMMAND(IDM_DUZ, OnDuz)
	ON_COMMAND(IDM_POVRS_K, OnPovrsK)
	ON_COMMAND(IDM_POVRS_P, OnPovrsP)
	ON_UPDATE_COMMAND_UI(ID_BOJA_CRNA, OnUpdateBojaCrna)
	ON_UPDATE_COMMAND_UI(ID_BOJA_CRVENA, OnUpdateBojaCrvena)
	ON_UPDATE_COMMAND_UI(ID_BOJA_PLAVA, OnUpdateBojaPlava)
	ON_UPDATE_COMMAND_UI(ID_BOJA_ZELENA, OnUpdateBojaZelena)
	ON_UPDATE_COMMAND_UI(IDM_DUZ, OnUpdateDuz)
	ON_UPDATE_COMMAND_UI(IDM_POVRS_K, OnUpdatePovrsK)
	ON_UPDATE_COMMAND_UI(IDM_POVRS_P, OnUpdatePovrsP)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CSokoDoc construction/destruction

CSokoDoc::CSokoDoc()
{
   m_Element = DUZ;   // Set initial element type
   m_Color = CRNA;    // Set initial drawing color

}

CSokoDoc::~CSokoDoc()
{

	   // Get the position at the head of the list
   POSITION aPosition = m_ElementList.GetHeadPosition();

   // Now delete the element pointed to by each list entry
   while(aPosition)
      delete m_ElementList.GetNext(aPosition);

   m_ElementList.RemoveAll();   // Finally delete all pointers
}

BOOL CSokoDoc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;

	// TODO: add reinitialization code here
	// (SDI documents will reuse this document)

	return TRUE;
}



/////////////////////////////////////////////////////////////////////////////
// CSokoDoc serialization

void CSokoDoc::Serialize(CArchive& ar)
{
	if (ar.IsStoring())
	{
		// TODO: add storing code here
	}
	else
	{
		// TODO: add loading code here
	}
}

/////////////////////////////////////////////////////////////////////////////
// CSokoDoc diagnostics

#ifdef _DEBUG
void CSokoDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CSokoDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CSokoDoc commands


//instalacija komandnog menija

void CSokoDoc::OnBojaCrna() 
{
 m_Color = CRNA;
	
}

void CSokoDoc::OnBojaCrvena() 
{
 m_Color = CRVENA;
	
}

void CSokoDoc::OnBojaPlava() 
{
	 m_Color = PLAVA;
	
}

void CSokoDoc::OnBojaZelena() 
{
	 m_Color = ZELENA;
	
}

void CSokoDoc::OnDuz() 
{
	m_Element = DUZ;
	
}

void CSokoDoc::OnPovrsK() 
{
m_Element = KRUG;
	
}

void CSokoDoc::OnPovrsP() 
{
	m_Element = PRAVOUGAONIK;
	
}

void CSokoDoc::OnUpdateBojaCrna(CCmdUI* pCmdUI) 
{
	pCmdUI->SetCheck( m_Color == CRNA);	
	
}

void CSokoDoc::OnUpdateBojaCrvena(CCmdUI* pCmdUI) 
{
	pCmdUI->SetCheck( m_Color == CRVENA);	
	
}

void CSokoDoc::OnUpdateBojaPlava(CCmdUI* pCmdUI) 
{
	pCmdUI->SetCheck( m_Color == PLAVA);	
	
}

void CSokoDoc::OnUpdateBojaZelena(CCmdUI* pCmdUI) 
{
	pCmdUI->SetCheck(m_Color ==ZELENA);	
	
}

void CSokoDoc::OnUpdateDuz(CCmdUI* pCmdUI) 
{
	pCmdUI->SetCheck(m_Element ==DUZ);	
}

void CSokoDoc::OnUpdatePovrsK(CCmdUI* pCmdUI) 
{
	
	pCmdUI->SetCheck(m_Element ==KRUG);	
}

void CSokoDoc::OnUpdatePovrsP(CCmdUI* pCmdUI) 
{
pCmdUI->SetCheck(m_Element ==PRAVOUGAONIK);	
}
