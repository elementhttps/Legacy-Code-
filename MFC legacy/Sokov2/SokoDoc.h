// SokoDoc.h : interface of the CSokoDoc class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_SOKODOC_H__30AA7A96_9602_4882_AAD6_94E36545571C__INCLUDED_)
#define AFX_SOKODOC_H__30AA7A96_9602_4882_AAD6_94E36545571C__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


class CSokoDoc : public CDocument
{
protected: // create from serialization only
	CSokoDoc();
	DECLARE_DYNCREATE(CSokoDoc)

// Attributes
public:
	COLORREF m_Color;        // Current drawing color
	WORD m_Element;          // Current element type
//    CTypedPtrList<CObList, CElement*> m_ElementList;  // Element list
	CTypedPtrList<CObList, CElement*> m_ElementList; 
// Operations FUNKCIJE u SokoDoc.h
public:
   WORD GetElementType() const               // Get the element type
   { 
	   return m_Element; //uzmi element iz elementi.h i vrati u zeljen fajl
   }

   COLORREF GetElementColor() const          // Get the element color
   { 
	   return m_Color; 
   } 
      void AddElement(CElement* pElement)       // Add an element to the list
   {
		  m_ElementList.AddTail(pElement);
	  }

  POSITION GetListHeadPosition() const   // Return list head POSITION value
   { return m_ElementList.GetHeadPosition(); }

   CElement* GetNext(POSITION& aPos) const // Return current element pointer
   { return m_ElementList.GetNext(aPos); }
   
   POSITION GetListTailPosition() const      // Return list tail POSITION value
   { return m_ElementList.GetTailPosition(); }

   CElement* GetPrev(POSITION& aPos) const   // Return current element pointer
   { return m_ElementList.GetPrev(aPos); }
   /*
POSITION GetListHeadPosition() const      // Return list head POSITION value
 { 
	return m_ElementList.GetHeadPosition();
 }*/
//      CElement* GetNext(POSITION& aPos) const   // Return current  element pointer
  //    { return m_ElementList.GetNext(aPos); }
// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSokoDoc)
	public:
	virtual BOOL OnNewDocument();
	virtual void Serialize(CArchive& ar);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CSokoDoc();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	//{{AFX_MSG(CSokoDoc)
	afx_msg void OnBojaCrna();
	afx_msg void OnBojaCrvena();
	afx_msg void OnBojaPlava();
	afx_msg void OnBojaZelena();
	afx_msg void OnDuz();
	afx_msg void OnPovrsK();
	afx_msg void OnPovrsP();
	afx_msg void OnUpdateBojaCrna(CCmdUI* pCmdUI);
	afx_msg void OnUpdateBojaCrvena(CCmdUI* pCmdUI);
	afx_msg void OnUpdateBojaPlava(CCmdUI* pCmdUI);
	afx_msg void OnUpdateBojaZelena(CCmdUI* pCmdUI);
	afx_msg void OnUpdateDuz(CCmdUI* pCmdUI);
	afx_msg void OnUpdatePovrsK(CCmdUI* pCmdUI);
	afx_msg void OnUpdatePovrsP(CCmdUI* pCmdUI);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SOKODOC_H__30AA7A96_9602_4882_AAD6_94E36545571C__INCLUDED_)
