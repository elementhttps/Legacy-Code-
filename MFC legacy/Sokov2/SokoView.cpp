// SokoView.cpp : implementation of the CSokoView class
//

#include "stdafx.h"
#include "Soko.h"


 #include "Elementi.h" //ukljucenje pre view-a

#include "SokoDoc.h"
#include "SokoView.h"



#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CSokoView

IMPLEMENT_DYNCREATE(CSokoView, CScrollView)

BEGIN_MESSAGE_MAP(CSokoView, CScrollView)
	//{{AFX_MSG_MAP(CSokoView)
	ON_WM_ERASEBKGND()
	ON_WM_KEYDOWN()
	ON_WM_MOUSEWHEEL()
	ON_WM_HSCROLL()
	ON_WM_VSCROLL()
	ON_WM_LBUTTONDBLCLK()
	ON_WM_MOUSEMOVE()
	ON_WM_LBUTTONUP()
	ON_WM_LBUTTONDOWN()
	ON_WM_RBUTTONDOWN()
	ON_WM_RBUTTONUP()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CSokoView construction/destruction

CSokoView::CSokoView()
{
	m_Zumx= 25;
	m_Zumy= 25;
	m_Orgfx =90;
	m_Orgfy =620;
	
	
	
	red=0;
	green=0;
	blue=255;

	 kor=500;

   m_FirstPoint = CPoint(0,0);         // Set 1st recorded point to 0,0
   m_SecondPoint = CPoint(0,0);        // Set 2nd recorded point to 0,0
   m_pTempElement = NULL; 
      m_pSelected = NULL;                 // No element selected initially
}

CSokoView::~CSokoView()
{
}

BOOL CSokoView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	return CScrollView::PreCreateWindow(cs);
}

/////////////////////////////////////////////////////////////////////////////
// CSokoView drawing

void CSokoView::OnDraw(CDC* pDC)
{
	CSokoDoc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);
  //CClientDC pDC(this);
     POSITION aPos = pDoc->GetListHeadPosition();//
   CElement* pElement = 0;              // Store for an element pointer
/**/

 



pDC->BitBlt(0,0,m_nBmpWidth+10000,m_nBmpHeight+10000,&m_MemDC,0,0,SRCCOPY);

pDC->SetROP2( R2_NOTXORPEN);
	CBrush brush (RGB (red, green, blue));
	pDC->SelectObject(brush);
pDC->Rectangle(0,0,m_nBmpWidth,m_nBmpHeight);

/* Binary raster ops 
#define R2_BLACK            1     0       
#define R2_NOTMERGEPEN      2    DPon     
#define R2_MASKNOTPEN       3    DPna     
#define R2_NOTCOPYPEN       4    PN       
#define R2_MASKPENNOT       5   /PDna     
#define R2_NOT              6    Dn       
#define R2_XORPEN           7    DPx      
#define R2_NOTMASKPEN       8    DPan     
#define R2_MASKPEN          9    DPa      
#define R2_NOTXORPEN        10   DPxn     
#define R2_NOP              11   D        
#define R2_MERGENOTPEN      12   DPno     
#define R2_COPYPEN          13   P        
#define R2_MERGEPENNOT      14   PDno     
#define R2_MERGEPEN         15   DPo      
#define R2_WHITE            16  
#define R2_LAST             16
*/
 pDC->SetROP2(R2_COPYPEN);
   while(aPos)                          // Loop while aPos is not null
   {
      pElement = pDoc->GetNext(aPos);   // Get the current element pointer
      // If the element is visible...
      if(pDC->RectVisible(pElement->GetBoundRect()))
         pElement->Draw(pDC);  // ...draw it
   }
  



CPen PenGreen1(PS_SOLID, 1,RGB(0, 255, 0));
pDC->SelectObject(PenGreen1);
for(int kooxa = 0; kooxa < m_nBmpWidth; kooxa += kor)
    {
        pDC->MoveTo(kooxa, 0);
        pDC->LineTo(kooxa, m_nBmpWidth+(kor*50));
	

    }
for(int kooya = 0; kooya < m_nBmpHeight; kooya += kor)
    {
        pDC->MoveTo(0, kooya);
        pDC->LineTo(m_nBmpHeight+(kor*5), kooya);
	
    }

 //pDC->MoveTo(m_FirstPoint);pDC->LineTo(m_SecondPoint);




pDC->SetMapMode(MM_ANISOTROPIC); //orijentacija osa PO ZELJI PROGRAMERA



pDC->SetViewportOrg(m_Orgfx, m_Orgfy);//90 620
pDC->SetWindowExt(m_Zumx,m_Zumy);
//	pDC->SetWindowExt(m_Zumx,m_Zumy); //SetWindowExtent(100,100)  SetViewportExtent(200,200)  100 jedinica = 200 pixela
	pDC->SetViewportExt(1,-1);//1 -1






CPen PenBlue2(PS_SOLID, 3,RGB(red, green, blue));
pDC->SelectObject(PenBlue2);




for(int kooxa2 = 0; kooxa2 < 15100; kooxa2 += 500)
    {
        pDC->MoveTo(kooxa2, 0);
        pDC->LineTo(kooxa2, 15000);
	

    }

for(int kooya2 = 0; kooya2 < 15100; kooya2 += 500)
    {
        pDC->MoveTo(0, kooya2);
        pDC->LineTo(15000, kooya2);
	
    }
pDC->AngleArc(0,0,500/2,0,360);
//pDC->Ellipse(kooxa,kooya,kooxa2,kooya2);
//pDC->Ellipse(0,0,500/2,500/2);





    SetScrollSizes(MM_TEXT, CSize(m_nBmpWidth,m_nBmpHeight));



}

void CSokoView::OnInitialUpdate()
{
	CDC *pDC = this->GetDC();

    m_MemDC.CreateCompatibleDC(pDC);
 
  

    m_bmpView.LoadBitmap(IDB_BITMAP1);    

    m_MemDC.SelectObject(&m_bmpView);

    BITMAP Bitmap;
    m_bmpView.GetBitmap(&Bitmap);

    m_nBmpHeight = Bitmap.bmHeight;
    m_nBmpWidth = Bitmap.bmWidth;

CScrollView::OnInitialUpdate();

   // Define document size as 30x30ins in MM_LOENGLISH
   CSize DocSize(m_nBmpWidth,m_nBmpHeight);

   // Set mapping mode and document size.
   SetScrollSizes(MM_LOENGLISH, DocSize);
   // CSize size(m_nBmpWidth,m_nBmpHeight);
   // SetScrollSizes(MM_TEXT,size);
	this->ReleaseDC(pDC); 
	//pDC->AngleArc(m_Orgfx,m_Orgfy,500/2,0,0);
	Invalidate();


}

/////////////////////////////////////////////////////////////////////////////
// CSokoView diagnostics

#ifdef _DEBUG
void CSokoView::AssertValid() const
{
	CScrollView::AssertValid();
}

void CSokoView::Dump(CDumpContext& dc) const
{
	CScrollView::Dump(dc);
}

CSokoDoc* CSokoView::GetDocument() // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CSokoDoc)));
	return (CSokoDoc*)m_pDocument;
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CSokoView message handlers

BOOL CSokoView::OnEraseBkgnd(CDC* pDC) 
{
	// TODO: Add your message handler code here and/or call default
	
	//return CScrollView::OnEraseBkgnd(pDC);
	return FALSE;
}
		





void CSokoView::OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags) 
{
	// TODO: Add your message handler code here and/or call default
	switch (nChar)
 {

	case VK_F2:
		m_Zumx +=10;
		m_Zumy +=10;
		
	
		Invalidate();
		break;

	case VK_F1:
		m_Zumx -=10;
		m_Zumy -=10;

	
		if (m_Zumx < 1)
		{
			m_Zumx =1;
			m_Zumy =1;
		}
			
		Invalidate();
		break;

	case VK_F3:
		m_Zumx -=1;
		m_Zumy -=1;
		
		if (m_Zumx < 1)
		{
			m_Zumx =1;
			m_Zumy =1;
		}

		Invalidate();
		break;

	case VK_F4://f4
		m_Zumx +=1;
		m_Zumy +=1;
	
	
		Invalidate();
		break;


	case 0x31://1
			red=255;
		green=0;
	 blue=0;
	
	
		Invalidate();
		break;


	case 0x32://2
			red=0;
		green=255;
	 blue=0;
	 
		
		
	
		Invalidate();
		break;

	case 0x33://3
			red=0;
		green=0;
	 blue=255;
	
	 
	
		Invalidate();
		break;

	case 0x34://4
			red=255;
		green=255;
	 blue=255;
	
	 
	
		Invalidate();
		break;

	case 0x35://5
			red=0;
		green=0;
	 blue=0;
	
	 
	
		Invalidate();
		break;


	case 0x51://q
			kor+=10;
	
		
		
	
		Invalidate();
		break;

	case 0x57://w
			kor-=10;
	if (kor < 10)
		{
			kor =10;
		
		}
		
		
	
		Invalidate();
		break;




}
	
	CScrollView::OnKeyDown(nChar, nRepCnt, nFlags);
}

BOOL CSokoView::OnMouseWheel(UINT nFlags, short zDelta, CPoint pt) 
{
Invalidate();	
	return CScrollView::OnMouseWheel(nFlags, zDelta, pt);
}

void CSokoView::OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar) 
{
	// TODO: Add your message handler code here and/or call default
	Invalidate();
	
	CScrollView::OnHScroll(nSBCode, nPos, pScrollBar);
}

void CSokoView::OnVScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar) 
{
	// TODO: Add your message handler code here and/or call default
	Invalidate();
	CScrollView::OnVScroll(nSBCode, nPos, pScrollBar);
}

void CSokoView::OnLButtonDblClk(UINT nFlags, CPoint point) 
{
 m_Orgfx= point.x;
	m_Orgfy= point.y;	
		Invalidate();

	CScrollView::OnLButtonDblClk(nFlags, point);
}











void CSokoView::OnLButtonDown(UINT nFlags, CPoint point) 
{
   CClientDC aDC(this);                // Create a device context
   OnPrepareDC(&aDC);                  // Get origin adjusted
   aDC.DPtoLP(&point);                 // convert point to Logical

 
   
   
      m_FirstPoint = point;               // Record the cursor position
      SetCapture();                       // Capture subsequent mouse messages
   
}



void CSokoView::OnMouseMove(UINT nFlags, CPoint point) 
{ 
	
	
   // Define a Device Context object for the view
   //CClientDC pDC(this);
   //CDC *pDC = this->GetDC();
	CClientDC aDC(this);
   aDC.SetROP2(R2_NOTXORPEN );    //R2_NOTXORPEN Set the drawing mode  //BITNO !! 
    OnPrepareDC(&aDC);            // Get origin adjusted
   if((nFlags & MK_LBUTTON) && (this == GetCapture()))
   {
	  aDC.DPtoLP(&point);        // convert point to Logical
      m_SecondPoint = point;     // Save the current cursor position
/**/
      if(m_pTempElement)
      {
         // Redraw the old element so it disappears from the view
         m_pTempElement->Draw(&aDC);
         delete m_pTempElement;        // Delete the old element
         m_pTempElement = 0;           // Reset the pointer to 0
      }

      // Create a temporary element of the type and color that
      // is recorded in the document object, and draw it
      m_pTempElement = CreateElement();  // Create a new element
      m_pTempElement->Draw(&aDC);        // Draw the element

   }
	::SetCursor(::LoadCursor(NULL, IDC_CROSS));
	//CScrollView::OnMouseMove(nFlags, point);
}

void CSokoView::OnLButtonUp(UINT nFlags, CPoint point) 
{
	if(this == GetCapture())
      ReleaseCapture();        // Stop capturing mouse messages
 
   // Make sure there is an element
   if(m_pTempElement)
   {
    
        GetDocument()->AddElement(m_pTempElement);
	    GetDocument()->UpdateAllViews(0,0,m_pTempElement);//update all views
        InvalidateRect(0);

      //delete m_pTempElement;   // This code is temporary
      m_pTempElement = 0;      // Reset the element pointer
   }
}

CElement* CSokoView::CreateElement()
{
   // Get a pointer to the document for this view
   CSokoDoc* pDoc = GetDocument();
   ASSERT_VALID(pDoc);                  // Verify the pointer is good

   // Now select the element using the type stored in the document
   switch(pDoc->GetElementType())
   {
  
      case DUZ: 
         return new CLine(m_FirstPoint, m_SecondPoint, pDoc->GetElementColor());

      default:
         // Something's gone wrong
         AfxMessageBox("Bad Element code", MB_OK);
         AfxAbort();
         return NULL;
   }
}

void CSokoView::OnUpdate(CView* pSender, LPARAM lHint, CObject* pHint) 
{
   // Invalidate the area corresponding to the element pointed to
   // if there is one, otherwise invalidate the whole client area
   if(pHint)
   {
      CClientDC aDC(this);            // Create a device context
      OnPrepareDC(&aDC);              // Get origin adjusted

      // Get the enclosing rectangle and convert to client coordinates
      CRect aRect = static_cast<CElement*>(pHint)->GetBoundRect();
      aDC.LPtoDP(aRect);
	  aRect.NormalizeRect();
      InvalidateRect(aRect);          // Get the area redrawn
   }
   else
      InvalidateRect(0);              // Invalidate the client area
}

void CSokoView::OnRButtonDown(UINT nFlags, CPoint point) 
{
	  CMenu aMenu;
      aMenu.LoadMenu(IDR_CURSOR_MENU);    // Load the cursor menu
      ClientToScreen(&point);             // Convert to screen coordinates
      // Display the pop-up at the cursor position
      aMenu.GetSubMenu(1)->TrackPopupMenu(TPM_LEFTALIGN|TPM_RIGHTBUTTON,point.x, point.y,this);//0,1 getsub menu
}

void CSokoView::OnRButtonUp(UINT nFlags, CPoint point) 
{
   CMenu aMenu;
   aMenu.LoadMenu(IDR_CURSOR_MENU);    // Load the cursor menu
   ClientToScreen(&point);             // Convert to screen coordinates

   // Display the pop-up at the cursor position
   if(m_pSelected)
      aMenu.GetSubMenu(0)->TrackPopupMenu(TPM_LEFTALIGN|TPM_RIGHTBUTTON,point.x, point.y, this);
   else
      aMenu.GetSubMenu(1)->TrackPopupMenu(TPM_LEFTALIGN|TPM_RIGHTBUTTON,point.x, point.y,this);
}

CElement* CSokoView::SelectElement(CPoint aPoint)
{
   // Convert parameter aPoint to logical coordinates
   CClientDC aDC(this);
   OnPrepareDC(&aDC);
   aDC.DPtoLP(&aPoint);

   CSokoDoc* pDoc=GetDocument();      // Get a pointer to the document
   CElement* pElement = 0;                // Store an element pointer
   CRect aRect(0,0,0,0);                  // Store a rectangle
   POSITION aPos = pDoc->GetListTailPosition();  // Get last element posn

   while(aPos)                            // Iterate through the list
   {
      pElement = pDoc->GetPrev(aPos);
      aRect = pElement->GetBoundRect();
      // Select the first element that appears under the cursor
      if(aRect.PtInRect(aPoint))
         return pElement;
   }
   return 0;                              // No element found
}