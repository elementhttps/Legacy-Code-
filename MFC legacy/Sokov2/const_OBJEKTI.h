//Definitions of constants
//definicije konstanti i naziva konstanti 
#ifndef const_OBJEKTI_h
#define const_OBJEKTI_h

   // Element type definitions
   // Each type value must be unique
   const WORD DUZ = 101U;
   const WORD PRAVOUGAONIK = 102U;
   const WORD KRUG = 103U;
   
   ///////////////////////////////////

   // Color values for drawing
   const COLORREF CRNA = RGB(0,0,0);//CRNA NIJE BOJA MAJMUNE
   const COLORREF CRVENA = RGB(255,0,0);
   const COLORREF ZELENA = RGB(0,255,0);
   const COLORREF PLAVA = RGB(0,0,255);
   ///////////////////////////////////

#endif //!defined(const_OBJEKTI.h)
