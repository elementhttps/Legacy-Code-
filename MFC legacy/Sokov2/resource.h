//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by Soko.rc
//
#define IDD_ABOUTBOX                    100
#define IDR_MAINFRAME                   128
#define IDR_SOKOTYPE                    129
#define IDB_BITMAP1                     130
#define BANNER2                         133
#define IDR_CURSOR_MENU                 136
#define ID_BUTTON32771                  32771
#define ID_BUTTON32772                  32772
#define IDM_DUZ                         32774
#define IDM_POVRS_P                     32775
#define IDM_POVRS_K                     32776
#define ID_BOJA_CRVENA                  32777
#define ID_BOJA_PLAVA                   32778
#define ID_BOJA_ZELENA                  32779
#define ID_BOJA_CRNA                    32780
#define ID_LINE                         32792
#define ID_NOELEMENT_BLACK              32793
#define ID_NOELEMENT_RED                32794
#define ID_NOELEMENT_GREEN              32795
#define ID_NOELEMENT_BLUE               32796
#define ID_ELEMENT_DELITE               32797
#define ID_ELEMENT_MOVE                 32798

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        137
#define _APS_NEXT_COMMAND_VALUE         32799
#define _APS_NEXT_CONTROL_VALUE         1000
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
