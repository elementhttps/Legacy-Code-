// ModelessDemoDlg.h : header file
//

#pragma once

#include "Modeless.h"

// CModelessDemoDlg dialog
class CModelessDemoDlg : public CDialog
{
// Construction
public:
	CModelessDemoDlg(CWnd* pParent = NULL);	// standard constructor

// Dialog Data
	enum { IDD = IDD_MODELESSDEMO_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support


// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()	
public:
	afx_msg void OnBnClickedButton1();
	CModeless* m_pmodeless;
	CString m_text;
};
