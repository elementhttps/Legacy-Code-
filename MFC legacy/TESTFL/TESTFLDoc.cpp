// TESTFLDoc.cpp : implementation of the CTESTFLDoc class
//

#include "stdafx.h"
#include "TESTFL.h"

#include "TESTFLDoc.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CTESTFLDoc

IMPLEMENT_DYNCREATE(CTESTFLDoc, CDocument)

BEGIN_MESSAGE_MAP(CTESTFLDoc, CDocument)
	//{{AFX_MSG_MAP(CTESTFLDoc)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CTESTFLDoc construction/destruction

CTESTFLDoc::CTESTFLDoc()
{
	// TODO: add one-time construction code here

}

CTESTFLDoc::~CTESTFLDoc()
{
}

BOOL CTESTFLDoc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;

	// TODO: add reinitialization code here
	// (SDI documents will reuse this document)

	return TRUE;
}



/////////////////////////////////////////////////////////////////////////////
// CTESTFLDoc serialization

void CTESTFLDoc::Serialize(CArchive& ar)
{
	if (ar.IsStoring())
	{
		// TODO: add storing code here
	}
	else
	{
		// TODO: add loading code here
	}
}

/////////////////////////////////////////////////////////////////////////////
// CTESTFLDoc diagnostics

#ifdef _DEBUG
void CTESTFLDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CTESTFLDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CTESTFLDoc commands
