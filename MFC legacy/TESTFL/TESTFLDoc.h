// TESTFLDoc.h : interface of the CTESTFLDoc class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_TESTFLDOC_H__E631CF48_3E5E_4B3E_8715_92DF0277DC91__INCLUDED_)
#define AFX_TESTFLDOC_H__E631CF48_3E5E_4B3E_8715_92DF0277DC91__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


class CTESTFLDoc : public CDocument
{
protected: // create from serialization only
	CTESTFLDoc();
	DECLARE_DYNCREATE(CTESTFLDoc)

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CTESTFLDoc)
	public:
	virtual BOOL OnNewDocument();
	virtual void Serialize(CArchive& ar);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CTESTFLDoc();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	//{{AFX_MSG(CTESTFLDoc)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_TESTFLDOC_H__E631CF48_3E5E_4B3E_8715_92DF0277DC91__INCLUDED_)
