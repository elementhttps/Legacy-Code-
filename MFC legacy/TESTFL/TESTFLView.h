// TESTFLView.h : interface of the CTESTFLView class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_TESTFLVIEW_H__054F391F_4EB3_4BED_A81A_F5009EE8A0F9__INCLUDED_)
#define AFX_TESTFLVIEW_H__054F391F_4EB3_4BED_A81A_F5009EE8A0F9__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


class CTESTFLView : public CView
{
protected: // create from serialization only
	CTESTFLView();
	DECLARE_DYNCREATE(CTESTFLView)

// Attributes
public:
	CTESTFLDoc* GetDocument();

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CTESTFLView)
	public:
	virtual void OnDraw(CDC* pDC);  // overridden to draw this view
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	virtual void OnPrepareDC(CDC* pDC, CPrintInfo* pInfo = NULL);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CTESTFLView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:
	CPoint point;




// Generated message map functions
protected:
	//{{AFX_MSG(CTESTFLView)
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

#ifndef _DEBUG  // debug version in TESTFLView.cpp
inline CTESTFLDoc* CTESTFLView::GetDocument()
   { return (CTESTFLDoc*)m_pDocument; }
#endif

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_TESTFLVIEW_H__054F391F_4EB3_4BED_A81A_F5009EE8A0F9__INCLUDED_)
