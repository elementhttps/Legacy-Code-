// Trajectory1Doc.cpp : implementation of the CTrajectory1Doc class
//

#include "stdafx.h"
#include "Trajectory1.h"

#include "Trajectory1Doc.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CTrajectory1Doc

IMPLEMENT_DYNCREATE(CTrajectory1Doc, CDocument)

BEGIN_MESSAGE_MAP(CTrajectory1Doc, CDocument)
	//{{AFX_MSG_MAP(CTrajectory1Doc)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CTrajectory1Doc construction/destruction

CTrajectory1Doc::CTrajectory1Doc()
{
	// TODO: add one-time construction code here

}

CTrajectory1Doc::~CTrajectory1Doc()
{
}

BOOL CTrajectory1Doc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;

	// TODO: add reinitialization code here
	// (SDI documents will reuse this document)

	return TRUE;
}



/////////////////////////////////////////////////////////////////////////////
// CTrajectory1Doc serialization

void CTrajectory1Doc::Serialize(CArchive& ar)
{
	if (ar.IsStoring())
	{
		// TODO: add storing code here
	}
	else
	{
		// TODO: add loading code here
	}
}

/////////////////////////////////////////////////////////////////////////////
// CTrajectory1Doc diagnostics

#ifdef _DEBUG
void CTrajectory1Doc::AssertValid() const
{
	CDocument::AssertValid();
}

void CTrajectory1Doc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CTrajectory1Doc commands
