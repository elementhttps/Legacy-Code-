// Trajectory1View.cpp : implementation of the CTrajectory1View class
//

#include "stdafx.h"
#include "Trajectory1.h"

#include "Trajectory1Doc.h"
#include "Trajectory1View.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CTrajectory1View

IMPLEMENT_DYNCREATE(CTrajectory1View, CView)

BEGIN_MESSAGE_MAP(CTrajectory1View, CView)
	//{{AFX_MSG_MAP(CTrajectory1View)
	ON_WM_TIMER()
	ON_WM_ERASEBKGND()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CTrajectory1View construction/destruction

CTrajectory1View::CTrajectory1View()
{
	x = 0;
	y = 200;
	MovingRight = false;

}

CTrajectory1View::~CTrajectory1View()
{
}

BOOL CTrajectory1View::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	return CView::PreCreateWindow(cs);
}

/////////////////////////////////////////////////////////////////////////////
// CTrajectory1View drawing

void CTrajectory1View::OnDraw(CDC* pDC)
{
	CTrajectory1Doc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);

	CPen   penYellow;
	CBrush brsBlack, brsFuchsia;
	CRect rctClient;

	brsBlack.CreateSolidBrush(RGB(0, 0, 0));
	brsFuchsia.CreateSolidBrush(RGB(255, 0, 255));
	penYellow.CreatePen(PS_SOLID, 1, RGB(255, 255, 0));

	GetWindowRect(&rctClient);

	CBrush *pOldBrush = pDC->SelectObject(&brsBlack);
	pDC->Rectangle(0, 0, rctClient.Width(), rctClient.Height());

	pOldBrush = pDC->SelectObject(&brsFuchsia);
	CPen *pOldPen = pDC->SelectObject(&penYellow);
	pDC->RoundRect(this->x, this->y, this->x + ShapeWidth,
		       this->y + ShapeHeight, 5, 5);

	pDC->SelectObject(pOldPen);
	pDC->SelectObject(pOldBrush);
	// TODO: add draw code for native data here
}

/////////////////////////////////////////////////////////////////////////////
// CTrajectory1View diagnostics

#ifdef _DEBUG
void CTrajectory1View::AssertValid() const
{
	CView::AssertValid();
}

void CTrajectory1View::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}

CTrajectory1Doc* CTrajectory1View::GetDocument() // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CTrajectory1Doc)));
	return (CTrajectory1Doc*)m_pDocument;
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CTrajectory1View message handlers

void CTrajectory1View::OnInitialUpdate() 
{
	CView::OnInitialUpdate();
	
     SetTimer(IDT_MOVE, 10, NULL);	
}

void CTrajectory1View::OnTimer(UINT nIDEvent) 
{
	CRect rctClient;

	GetWindowRect(&rctClient);

	if( this->x < 0 )
	{
		this->x = 0;
		this->MovingRight = true;
	}

	if( this->x > rctClient.Width() - ShapeWidth )
	{
		this->x = rctClient.Width() - ShapeWidth;
		this->MovingRight = FALSE;
	}

	if( x < 0 )
	{
		x = 0;
		MovingRight = TRUE;
	}

	if( x > rctClient.Width() - ShapeWidth )
	{
		x = rctClient.Width() - ShapeWidth;
		MovingRight = FALSE;
	}

	Invalidate();

	if( MovingRight == TRUE )
		x++;
	else
		x--;
	
	CView::OnTimer(nIDEvent);
}

BOOL CTrajectory1View::OnEraseBkgnd(CDC* pDC) 
{
    return TRUE; // CView::OnEraseBkgnd(pDC);	

}
