// Trajectory1View.h : interface of the CTrajectory1View class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_TRAJECTORY1VIEW_H__E6681AA2_9B03_4BD0_980C_BBAA33D44E85__INCLUDED_)
#define AFX_TRAJECTORY1VIEW_H__E6681AA2_9B03_4BD0_980C_BBAA33D44E85__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


class CTrajectory1View : public CView
{
protected: // create from serialization only
	CTrajectory1View();
	DECLARE_DYNCREATE(CTrajectory1View)

// Attributes
public:
	CTrajectory1Doc* GetDocument();

// Operations
public:
    int ShapeWidth;
    int ShapeHeight;
	int x;
	int y;
	bool MovingRight;

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CTrajectory1View)
	public:
	virtual void OnDraw(CDC* pDC);  // overridden to draw this view
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	virtual void OnInitialUpdate();
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CTrajectory1View();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	//{{AFX_MSG(CTrajectory1View)
	afx_msg void OnTimer(UINT nIDEvent);
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

#ifndef _DEBUG  // debug version in Trajectory1View.cpp
inline CTrajectory1Doc* CTrajectory1View::GetDocument()
   { return (CTrajectory1Doc*)m_pDocument; }
#endif

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_TRAJECTORY1VIEW_H__E6681AA2_9B03_4BD0_980C_BBAA33D44E85__INCLUDED_)
