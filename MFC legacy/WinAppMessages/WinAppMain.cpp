#include <windows.h>

//---------------------------------------------------------------------------
HWND hWnd;
LPCTSTR ClsName = "WndMsg";
LPCTSTR WindowCaption = "Windows and Controls Messages";
LRESULT CALLBACK WndProc(HWND hWnd, UINT Msg, WPARAM wParam, LPARAM lParam);
//---------------------------------------------------------------------------
INT WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance,
                   LPSTR lpCmdLine, int nCmdShow)
{
    MSG         Msg;
    WNDCLASSEX  WndClsEx;

    WndClsEx.cbSize        = sizeof(WNDCLASSEX);
    WndClsEx.style         = CS_HREDRAW | CS_VREDRAW;
    WndClsEx.lpfnWndProc   = WndProc;
    WndClsEx.cbClsExtra    = NULL;
    WndClsEx.cbWndExtra    = NULL;
    WndClsEx.hInstance     = hInstance;
    WndClsEx.hIcon         = LoadIcon(NULL, IDI_APPLICATION);
    WndClsEx.hCursor       = LoadCursor(NULL, IDC_ARROW);
    WndClsEx.hbrBackground = (HBRUSH)GetStockObject(WHITE_BRUSH);
    WndClsEx.lpszMenuName  = NULL;
    WndClsEx.lpszClassName = ClsName;
    WndClsEx.hIconSm       = LoadIcon(NULL, IDI_APPLICATION);

    RegisterClassEx(&WndClsEx);

    hWnd = CreateWindowEx(WS_EX_OVERLAPPEDWINDOW,
                          ClsName,
                          WindowCaption,
                          WS_OVERLAPPEDWINDOW,
                          100,
                          120,
                          640,
                          480,
                          NULL,
                          NULL,
                          hInstance,
                          NULL);

    ShowWindow(hWnd, nCmdShow);
    UpdateWindow(hWnd);

    while( GetMessage(&Msg, NULL, 0, 0) )
    {
        TranslateMessage(&Msg);
        DispatchMessage(&Msg);
    }

    return Msg.wParam;
}
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------
LRESULT CALLBACK WndProc(HWND hWnd, UINT Msg, WPARAM wParam, LPARAM lParam)
{
    HDC hDC;
    PAINTSTRUCT Ps;

    POINT Pt[12];
	int lpPts[] = { 3, 3, 3, 3 };

	// Top Triangle
	Pt[0].x = 125;
	Pt[0].y =  10;
	Pt[1].x = 95;
	Pt[1].y = 70;
	Pt[2].x = 155;
	Pt[2].y =  70;

	// Left Triangle
	Pt[3].x =  80;
	Pt[3].y =  80;
	Pt[4].x =  20;
	Pt[4].y = 110;
	Pt[5].x =  80;
	Pt[5].y = 140;

	// Bottom Triangle
	Pt[6].x =  95;
	Pt[6].y = 155;
	Pt[7].x = 125;
	Pt[7].y = 215;
	Pt[8].x = 155;
	Pt[8].y = 155;
	
	// Right Triangle
	Pt[9].x  = 170;
	Pt[9].y  =  80;
	Pt[10].x = 170;
	Pt[10].y = 140;
	Pt[11].x = 230;
	Pt[11].y = 110;
	COLORREF NewColor = RGB(255, 0, 255);

    switch(Msg)
    {
    case WM_PAINT:
        hDC = BeginPaint(hWnd, &Ps);
        PolyPolygon(hDC, Pt, lpPts, 4);
        EndPaint(hWnd, &Ps);
        break;
    case WM_DESTROY:
        PostQuitMessage(WM_QUIT);
        break;
    default:
        return DefWindowProc(hWnd, Msg, wParam, lParam);
    }
    return 0;
}
//---------------------------------------------------------------------------

/*BOJE
LRESULT CALLBACK WindProcedure(HWND hWnd, UINT Msg,
			       WPARAM wParam, LPARAM lParam)
{
	COLORREF NewColor = RGB(255, 0, 255);

	switch(Msg)
	{
	case WM_DESTROY:
		PostQuitMessage(WM_QUIT);
		break;
	default:
		return DefWindowProc(hWnd, Msg, wParam, lParam);
	}
	return 0;
}
//---------------------------------------------------------------------------POLYBRAZERTO KRIVA
LRESULT CALLBACK WndProc(HWND hWnd, UINT Msg, WPARAM wParam, LPARAM lParam)
{
    HDC hDC;
    PAINTSTRUCT Ps;

    POINT Pt[4] = { {  320, 120 }, {  88, 246 }, { 364, 122 } };
 
    switch(Msg)
    {
    case WM_PAINT:
        hDC = BeginPaint(hWnd, &Ps);
        PolyBezierTo(hDC, Pt, 3);
        EndPaint(hWnd, &Ps);
        break;
    case WM_DESTROY:
        PostQuitMessage(WM_QUIT);
        break;
    default:
        return DefWindowProc(hWnd, Msg, wParam, lParam);
    }
    return 0;
}
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------ODSECAK KRUGA
LRESULT CALLBACK WndProc(HWND hWnd, UINT Msg, WPARAM wParam, LPARAM lParam)
{
    HDC hDC;
    PAINTSTRUCT Ps;

    switch(Msg)
    {
    case WM_PAINT:
        hDC = BeginPaint(hWnd, &Ps);
	Pie(hDC, 40, 20, 226, 144, 155, 32, 202, 115);
        EndPaint(hWnd, &Ps);
        break;
    case WM_DESTROY:
        PostQuitMessage(WM_QUIT);
        break;
    default:
        return DefWindowProc(hWnd, Msg, wParam, lParam);
    }
    return 0;
}
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------PRAVOGAUNIK SA OBLINAMA
LRESULT CALLBACK WndProc(HWND hWnd, UINT Msg, WPARAM wParam, LPARAM lParam)
{
    HDC hDC;
    PAINTSTRUCT Ps;

    switch(Msg)
    {
    case WM_PAINT:
        hDC = BeginPaint(hWnd, &Ps);
	RoundRect(hDC, 20, 20, 275, 188, 42, 38);
        EndPaint(hWnd, &Ps);
        break;
    case WM_DESTROY:
        PostQuitMessage(WM_QUIT);
        break;
    default:
        return DefWindowProc(hWnd, Msg, wParam, lParam);
    }
    return 0;
}
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------ELIPSA
LRESULT CALLBACK WndProc(HWND hWnd, UINT Msg, WPARAM wParam, LPARAM lParam)
{
    HDC hDC;
    PAINTSTRUCT Ps;

    switch(Msg)
    {
    case WM_PAINT:
        hDC = BeginPaint(hWnd, &Ps);
		Ellipse(hDC, 20, 20, 226, 144);
        EndPaint(hWnd, &Ps);
        break;
    case WM_DESTROY:
        PostQuitMessage(WM_QUIT);
        break;
    default:
        return DefWindowProc(hWnd, Msg, wParam, lParam);
    }
    return 0;
}
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------PRAVOGONIK
LRESULT CALLBACK WndProc(HWND hWnd, UINT Msg, WPARAM wParam, LPARAM lParam)
{
    HDC hDC;
    PAINTSTRUCT Ps;

    switch(Msg)
    {
    case WM_PAINT:
        hDC = BeginPaint(hWnd, &Ps);
        Rectangle(hDC, 20, 20, 226, 144);
        EndPaint(hWnd, &Ps);
        break;
    case WM_DESTROY:
        PostQuitMessage(WM_QUIT);
        break;
    default:
        return DefWindowProc(hWnd, Msg, wParam, lParam);
    }
    return 0;
}
//---------------------------------------------------------------------------

LRESULT CALLBACK WndProc(HWND hWnd, UINT Msg, WPARAM wParam, LPARAM lParam)
{
    HDC hDC;
    PAINTSTRUCT Ps;

    switch(Msg)
    {
    case WM_PAINT:
	hDC = BeginPaint(hWnd, &Ps);
	MoveToEx(hDC, 60, 20, NULL);
	LineTo(hDC, 264, 122);
	EndPaint(hWnd, &Ps);
	break;
    case WM_DESTROY:
        PostQuitMessage(WM_QUIT);
        break;
    default:
        return DefWindowProc(hWnd, Msg, wParam, lParam);
    }
    return 0;
}
//---------------------------------------------------------------------------
LRESULT CALLBACK WndProc(HWND hWnd, UINT Msg, WPARAM wParam, LPARAM lParam)
{
    HDC hDC;
    PAINTSTRUCT Ps;

    switch(Msg)
    {
    case WM_PAINT:
        hDC = BeginPaint(hWnd, &Ps);
        MoveToEx(hDC, 60, 20, NULL);
        LineTo(hDC, 60, 122);
        LineTo(hDC, 264, 122);
        LineTo(hDC, 60, 20);
        EndPaint(hWnd, &Ps);
        break;
    case WM_DESTROY:
        PostQuitMessage(WM_QUIT);
        break;
    default:
        return DefWindowProc(hWnd, Msg, wParam, lParam);
    }
    return 0;
}
//--------------------------------------------------------------------------- KRIVA
LRESULT CALLBACK WndProc(HWND hWnd, UINT Msg, WPARAM wParam, LPARAM lParam)
{
    HDC hDC;
    PAINTSTRUCT Ps;

    POINT Pt[4] = { {  20,  12 }, {  88, 246 }, { 364, 192 }, { 250,  48 } };
 
    switch(Msg)
    {
    case WM_PAINT:
        hDC = BeginPaint(hWnd, &Ps);
        PolyBezier(hDC, Pt, 4);
        EndPaint(hWnd, &Ps);
        break;
    case WM_DESTROY:
        PostQuitMessage(WM_QUIT);
        break;
    default:
        return DefWindowProc(hWnd, Msg, wParam, lParam);
    }
    return 0;
}
//--------------------------------------------------------------------------- LINIJA
LRESULT CALLBACK WndProc(HWND hWnd, UINT Msg, WPARAM wParam, LPARAM lParam)
{
    HDC hDC;
    PAINTSTRUCT Ps;

    switch(Msg)
    {
    case WM_PAINT:
	hDC = BeginPaint(hWnd, &Ps);
	MoveToEx(hDC, 60, 20, NULL);
	LineTo(hDC, 264, 122);
	EndPaint(hWnd, &Ps);
	break;
    case WM_DESTROY:
        PostQuitMessage(WM_QUIT);
        break;
    default:
        return DefWindowProc(hWnd, Msg, wParam, lParam);
    }
    return 0;
}
//---------------------------------------------------------------------------TROUGAO
LRESULT CALLBACK WndProc(HWND hWnd, UINT Msg, WPARAM wParam, LPARAM lParam)
{
    HDC hDC;
    PAINTSTRUCT Ps;

    switch(Msg)
    {
    case WM_PAINT:
        hDC = BeginPaint(hWnd, &Ps);
        MoveToEx(hDC, 60, 20, NULL);
        LineTo(hDC, 60, 122);
        LineTo(hDC, 264, 122);
        LineTo(hDC, 60, 20);
        EndPaint(hWnd, &Ps);
        break;
    case WM_DESTROY:
        PostQuitMessage(WM_QUIT);
        break;
    default:
        return DefWindowProc(hWnd, Msg, wParam, lParam);
    }
    return 0;
}
//---------------------------------------------------------------------------ZA MENIJE
case WM_COMMAND:
		switch(LOWORD(wParam))
		{
		case IDM_FILE_NEW:
			break;

		case IDM_FILE_OPEN:
			break;

		case IDM_FILE_SAVE:
			break;

		case IDM_FILE_SAVEAS:
			break;

		case IDM_FILE_EXIT:
			PostQuitMessage(WM_QUIT);
			break;

//---------------------------------------------------------------------------POLYLINES 
LRESULT CALLBACK WndProc(HWND hWnd, UINT Msg, WPARAM wParam, LPARAM lParam)
{
    HDC hDC;
    PAINTSTRUCT Ps;
    POINT Pt[] = { 60, 20, 60, 122 };

    switch(Msg)
    {
    case WM_PAINT:
        hDC = BeginPaint(hWnd, &Ps);
        MoveToEx(hDC, Pt[0].x, Pt[0].y, NULL);
        LineTo(hDC, Pt[1].x, Pt[1].y);
        EndPaint(hWnd, &Ps);
        break;
    case WM_DESTROY:
        PostQuitMessage(WM_QUIT);
        break;
    default:
        return DefWindowProc(hWnd, Msg, wParam, lParam);
    }
    return 0;
}

//---------------------------------------------------------------------------PLLYLINES ARRAYS
LRESULT CALLBACK WndProc(HWND hWnd, UINT Msg, WPARAM wParam, LPARAM lParam)
{
    HDC hDC;
    PAINTSTRUCT Ps;
    POINT Pt[7];
    Pt[0].x = 20;  Pt[0].y = 50;
    Pt[1].x = 180; Pt[1].y = 50;
    Pt[2].x = 180; Pt[2].y = 20;
    Pt[3].x = 230; Pt[3].y = 70;
    Pt[4].x = 180; Pt[4].y = 120;
    Pt[5].x = 180; Pt[5].y = 90;
    Pt[6].x = 20;  Pt[6].y = 90;

    switch(Msg)
    {
    case WM_PAINT:
        hDC = BeginPaint(hWnd, &Ps);
        Polyline(hDC, Pt, 7);
        EndPaint(hWnd, &Ps);
        break;
    case WM_DESTROY:
        PostQuitMessage(WM_QUIT);
        break;
    default:
        return DefWindowProc(hWnd, Msg, wParam, lParam);
    }
    return 0;
}
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
LRESULT CALLBACK WndProc(HWND hWnd, UINT Msg, WPARAM wParam, LPARAM lParam)
{
    HDC hDC;
    PAINTSTRUCT Ps;

    POINT Pt[12];
	int lpPts[] = { 3, 3, 3, 3 };

	// Top Triangle
	Pt[0].x = 125;
	Pt[0].y =  10;
	Pt[1].x = 95;
	Pt[1].y = 70;
	Pt[2].x = 155;
	Pt[2].y =  70;

	// Left Triangle
	Pt[3].x =  80;
	Pt[3].y =  80;
	Pt[4].x =  20;
	Pt[4].y = 110;
	Pt[5].x =  80;
	Pt[5].y = 140;

	// Bottom Triangle
	Pt[6].x =  95;
	Pt[6].y = 155;
	Pt[7].x = 125;
	Pt[7].y = 215;
	Pt[8].x = 155;
	Pt[8].y = 155;
	
	// Right Triangle
	Pt[9].x  = 170;
	Pt[9].y  =  80;
	Pt[10].x = 170;
	Pt[10].y = 140;
	Pt[11].x = 230;
	Pt[11].y = 110;

    switch(Msg)
    {
    case WM_PAINT:
        hDC = BeginPaint(hWnd, &Ps);
        PolyPolygon(hDC, Pt, lpPts, 4);
        EndPaint(hWnd, &Ps);
        break;
    case WM_DESTROY:
        PostQuitMessage(WM_QUIT);
        break;
    default:
        return DefWindowProc(hWnd, Msg, wParam, lParam);
    }
    return 0;
}
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
LRESULT CALLBACK WndProc(HWND hWnd, UINT Msg, WPARAM wParam, LPARAM lParam)
{
    HDC hDC;
    PAINTSTRUCT Ps;

    POINT Pt[15];
	DWORD  lpPts[] = { 4, 4, 7 };

	// Left Triangle
	Pt[0].x = 50;
	Pt[0].y = 20;
	Pt[1].x = 20;
	Pt[1].y = 60;
	Pt[2].x = 80;
	Pt[2].y = 60;
	Pt[3].x = 50;
	Pt[3].y = 20;
	
	// Second Triangle
	Pt[4].x =  70;
	Pt[4].y =  20;
	Pt[5].x = 100;
	Pt[5].y =  60;
	Pt[6].x = 130;
	Pt[6].y =  20;
	Pt[7].x =  70;
	Pt[7].y =  20;

	// Hexagon
	Pt[8].x  = 145;
	Pt[8].y  =  20;
	Pt[9].x  = 130;
	Pt[9].y  =  40;
	Pt[10].x = 145;
	Pt[10].y =  60;
	Pt[11].x = 165;
	Pt[11].y =  60;
	Pt[12].x = 180;
	Pt[12].y =  40;
	Pt[13].x = 165;
	Pt[13].y =  20;
	Pt[14].x = 145;
	Pt[14].y =  20;

    switch(Msg)
    {
    case WM_PAINT:
        hDC = BeginPaint(hWnd, &Ps);
        PolyPolyline(hDC, Pt, lpPts, 3);
        EndPaint(hWnd, &Ps);
        break;
    case WM_DESTROY:
        PostQuitMessage(WM_QUIT);
        break;
    default:
        return DefWindowProc(hWnd, Msg, wParam, lParam);
    }
    return 0;
}
//---------------------------------------------------------------------------
*/