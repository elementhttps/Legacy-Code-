//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by MENI_resurs.rc
//
#define IDR_MENU1                       101
#define IDD_DIALOGBAR                   103
#define IDM_CLOSE                       40001
#define IDM_FILE_EXIT                   40001
#define IDM_LINIJA                      40002
#define IDM_OBJEKAT_LINIJA              40002
#define IDM_OBJEKAT_P                   40002
#define IDM_OBJEKAT_ELIPSA              40003
#define IDM_OBJEKAT_PP                  40003

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        102
#define _APS_NEXT_COMMAND_VALUE         40004
#define _APS_NEXT_CONTROL_VALUE         1001
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
