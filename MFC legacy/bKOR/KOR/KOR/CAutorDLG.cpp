// CAutorDLG.cpp : implementation file
//

#include "stdafx.h"
#include "KOR.h"
#include "CAutorDLG.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CCAutorDLG dialog


CCAutorDLG::CCAutorDLG(CWnd* pParent /*=NULL*/)
	: CDialog(CCAutorDLG::IDD, pParent)
{
	//{{AFX_DATA_INIT(CCAutorDLG)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void CCAutorDLG::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CCAutorDLG)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CCAutorDLG, CDialog)
	//{{AFX_MSG_MAP(CCAutorDLG)
		// NOTE: the ClassWizard will add message map macros here
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CCAutorDLG message handlers
