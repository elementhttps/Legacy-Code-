#include "stdafx.h"
#include "CRM.h"
#include "Math.h"



//------------------------------
//funkcije get iz konstruktora

double CCRM::CCRMgetM_mol()
{
       return M_mol ;
}


double CCRM::CCRMgetN_avo()
{
       return N_avo ;
}


double CCRM::CCRMgetKapa()
{
       return kapa;
}


double CCRM::CCRMgetK()
{
       return k;
}


double CCRM::CCRMgetP_amb()
{
       return P_amb;
}


double CCRM::CCRMgetP_kom()
{
       return P_kom;
}


double CCRM::CCRMgetT_kom()
{
       return T_kom;
}


double CCRM::CCRMgetA_iz()
{
       return A_iz;
}

double CCRM::CCRMgetMah_kr()
{
       return Mah_kr;
}



//----------------------------------
//funkcije VAN konstruktora
//----------------------------------
/*
double CCRM:
double CCRM:
double CCRM:
double CCRM:
double CCRM:
double CCRM:
double CCRM:
double CCRM:
*/
double CCRM::CCRMgetRu()
{
	   CCRM GasKon;
	   double Na = GasKon.CCRMgetN_avo();
	   double k = GasKon.CCRMgetK();
	   
	   double Ru = Na*k*1000;

       return Ru;
}


double CCRM::CCRMgetRg()
{
       CCRM GasO2;
	   double Ru = GasO2.CCRMgetRu();
	   double M_mol =GasO2.CCRMgetM_mol();

       double Rg = Ru/M_mol;

       return Rg;
}


double CCRM::CCRMgetCp()
{
	    CCRM  GasCp;
        double kapa = GasCp.CCRMgetKapa();
		double Ru = GasCp.CCRMgetRu();
		double M_mol = GasCp.CCRMgetM_mol();

		double kapaK1 = kapa/(kapa-1);
        double cp = kapaK1*(Ru/M_mol);

		return cp;
}


double CCRM::CCRMgetV_iz()
{
        CCRM GasV_iz;
		double kapa = GasV_iz.CCRMgetKapa();
		double Ru = GasV_iz.CCRMgetRu();
		double M_mol = GasV_iz.CCRMgetM_mol();
		double T_kom = GasV_iz.CCRMgetT_kom();
		double P_kom = GasV_iz.CCRMgetP_kom();
		double P_amb = GasV_iz.CCRMgetP_amb(); //napomena da li je pamb = pe
        
		double kapaK1 = (2*kapa)/(kapa-1);
		double kapaKV1 = (kapa-1)/kapa;
		double RTsaM = (Ru*T_kom)/M_mol;
		double PasPko = P_amb/P_kom;
        double par1 = kapaK1*RTsaM;
		double par2 = 1-(pow(PasPko,kapaKV1));
		double V_iz = sqrt(par1*par2);


		return V_iz;
}


double CCRM::CCRMgetCzKo()
{       
	    CCRM GasZvuk; 
        double kapa = GasZvuk.CCRMgetKapa();
		double Rg = GasZvuk.CCRMgetRg();
		double T_kom = GasZvuk.CCRMgetT_kom();

		double par1 = kapa*Rg*T_kom;
        double CzKo = sqrt(par1);

		
		return CzKo;
}


double CCRM::CCRMgetMahKo()  //moguca greska
{     
	    CCRM GasMah;
		double CzKo = GasMah.CCRMgetCzKo();
		double V_iz = GasMah.CCRMgetV_iz();
		
		double MahKo = V_iz/CzKo;


        return MahKo;
		
}	      


double CCRM::CCRMgetPambsPkom()//vrednost mora da bude manja od 0.528 da bi Mkr =1
{
	   CCRM GasPasPk;
	   double P_amb = GasPasPk.CCRMgetP_amb();
       double P_kom = GasPasPk.CCRMgetP_kom();

	   double PasPk = P_amb/P_kom;


	   return PasPk;
}













