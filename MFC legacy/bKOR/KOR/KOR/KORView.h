// KORView.h : interface of the CKORView class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_KORVIEW_H__EE350BF0_C273_4B1A_B360_678D12D52824__INCLUDED_)
#define AFX_KORVIEW_H__EE350BF0_C273_4B1A_B360_678D12D52824__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


class CKORView : public CView
{
protected: // create from serialization only
	CKORView();
	DECLARE_DYNCREATE(CKORView)

// Attributes
public:
	CKORDoc* GetDocument();

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CKORView)
	public:
	virtual void OnDraw(CDC* pDC);  // overridden to draw this view
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	protected:
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CKORView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:

	int m_Zumx;
	int m_Zumy;
	int I;
	double ualfa1;
	double ualfa2;
	double Dkm;
	double Lkm;
	double DM1;
	double DM2;
double CKORViewgetX();
double CKORViewgetI();



	//{{AFX_MSG(CKORView)
	afx_msg void OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags);
	afx_msg void OnAutorDlg();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

#ifndef _DEBUG  // debug version in KORView.cpp
inline CKORDoc* CKORView::GetDocument()
   { return (CKORDoc*)m_pDocument; }
#endif




/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_KORVIEW_H__EE350BF0_C273_4B1A_B360_678D12D52824__INCLUDED_)

