// cKORView.h : interface of the CCKORView class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_CKORVIEW_H__35ECF3F0_199A_46F7_B326_C783AC14303A__INCLUDED_)
#define AFX_CKORVIEW_H__35ECF3F0_199A_46F7_B326_C783AC14303A__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


class CCKORView : public CView
{
protected: // create from serialization only
	CCKORView();
	DECLARE_DYNCREATE(CCKORView)

// Attributes
public:
	CCKORDoc* GetDocument();

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCKORView)
	public:
	virtual void OnDraw(CDC* pDC);  // overridden to draw this view
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	protected:
	virtual BOOL OnPreparePrinting(CPrintInfo* pInfo);
	virtual void OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo);
	virtual void OnEndPrinting(CDC* pDC, CPrintInfo* pInfo);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CCKORView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	//{{AFX_MSG(CCKORView)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

#ifndef _DEBUG  // debug version in cKORView.cpp
inline CCKORDoc* CCKORView::GetDocument()
   { return (CCKORDoc*)m_pDocument; }
#endif

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CKORVIEW_H__35ECF3F0_199A_46F7_B326_C783AC14303A__INCLUDED_)
