#if !defined(AFX_C1NAPON_H__934715DE_1B7B_4594_AE6D_90ED1F9AAB14__INCLUDED_)
#define AFX_C1NAPON_H__934715DE_1B7B_4594_AE6D_90ED1F9AAB14__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// C1napon.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CC1napon dialog

class CC1napon : public CDialog
{
// Construction
public:
	CC1napon(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CC1napon)
	enum { IDD = IDD_1NAPON };
	double	m_rsp;
	double	m_run;
	double	m_debljina;
	double	m_odnosr;
	double	m_ntri;
	double	m_nrri;
	double	m_pPu;
	double	m_nlri;
	int		m_ena;
	double	m_taumax;
	double	m_srednjinn;
	double	m_Zatezna;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CC1napon)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CC1napon)
	afx_msg void OnButton2();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
		
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_C1NAPON_H__934715DE_1B7B_4594_AE6D_90ED1F9AAB14__INCLUDED_)
