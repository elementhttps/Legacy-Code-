// CAutorA.cpp : implementation file
//

#include "stdafx.h"
#include "cKOR.h"
#include "CAutorA.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CCAutorA dialog


CCAutorA::CCAutorA(CWnd* pParent /*=NULL*/)
	: CDialog(CCAutorA::IDD, pParent)
{
	//{{AFX_DATA_INIT(CCAutorA)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void CCAutorA::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CCAutorA)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CCAutorA, CDialog)
	//{{AFX_MSG_MAP(CCAutorA)
		// NOTE: the ClassWizard will add message map macros here
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CCAutorA message handlers
