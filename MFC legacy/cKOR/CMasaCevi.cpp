// CMasaCevi.cpp : implementation file
//

#include "stdafx.h"
#include "cKOR.h"
#include "CMasaCevi.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CCMasaCevi dialog


CCMasaCevi::CCMasaCevi(CWnd* pParent /*=NULL*/)
	: CDialog(CCMasaCevi::IDD, pParent)
{
	//{{AFX_DATA_INIT(CCMasaCevi)
	m_Rsp = 0.0;
	m_Run = 0.0;
	m_Lduz = 0.0;
	m_CenapoKg = 0.0;
	m_CenaUkupno = 0.0;
	m_Umasa = 0.0;
	m_ROg = 0.0;
	m_naponZ = 0.0;
	m_Pmax = 0.0;
	m_poason = 0.0;
	m_modulE = 0.0;
	//}}AFX_DATA_INIT
}


void CCMasaCevi::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CCMasaCevi)
	DDX_Text(pDX, IDC_EDIT1, m_Rsp);
	DDX_Text(pDX, IDC_EDIT2, m_Run);
	DDX_Text(pDX, IDC_EDIT3, m_Lduz);
	DDX_Text(pDX, IDC_EDIT6, m_CenapoKg);
	DDX_Text(pDX, IDC_EDIT7, m_CenaUkupno);
	DDX_Text(pDX, IDC_EDIT4, m_Umasa);
	DDX_Text(pDX, IDC_EDIT5, m_ROg);
	DDX_Text(pDX, IDC_EDIT8, m_naponZ);
	DDX_Text(pDX, IDC_EDIT9, m_Pmax);
	DDX_Text(pDX, IDC_EDIT11, m_poason);
	DDX_Text(pDX, IDC_EDIT15, m_modulE);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CCMasaCevi, CDialog)
	//{{AFX_MSG_MAP(CCMasaCevi)
	ON_BN_CLICKED(IDC_BUTTON1, OnButton1)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CCMasaCevi message handlers

void CCMasaCevi::OnButton1() 
{
	UpdateData (true);

double PI = 3.14159265359;
double Vcevi;
double Acevi;
double VceviM3;
double Konv = 1000000000; //pretvaranje mm3 u m3
double KvZ; //razlika pod zagradom R^2-r^2

KvZ = (m_Rsp*m_Rsp)-(m_Run*m_Run);
Acevi = KvZ*PI;
Vcevi = Acevi*m_Lduz;
VceviM3 = Vcevi/Konv;

m_Umasa = VceviM3*m_ROg;
m_CenaUkupno = m_Umasa*m_CenapoKg;
	
double dzida =(m_Rsp-m_Run)/1000;
double KonvBar = 100000;//pas u bare 10^5
double Konv2 = 1000000; //mm^2 u m^2
double NaponZ = m_naponZ*Konv2;
double Ppas;
Ppas = (2*dzida*NaponZ)/(m_Run/1000);
m_Pmax = Ppas/KonvBar;


	

	   
	UpdateData(false);
	//Invalidate ();
	
}
/*
	UpdateData(true);
	m_BrojTacaka =m_IDev/m_Korak;
	UpdateData(false);
	*/
