// CNaponCevi.cpp : implementation file
//

#include "stdafx.h"
#include "cKOR.h"
#include "CNaponCevi.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CCNaponCevi dialog


CCNaponCevi::CCNaponCevi(CWnd* pParent /*=NULL*/)
	: CDialog(CCNaponCevi::IDD, pParent)
{
	//{{AFX_DATA_INIT(CCNaponCevi)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void CCNaponCevi::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CCNaponCevi)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CCNaponCevi, CDialog)
	//{{AFX_MSG_MAP(CCNaponCevi)
		// NOTE: the ClassWizard will add message map macros here
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CCNaponCevi message handlers
