#if !defined(AFX_CRMOSNOVNO_H__CC904099_6A71_43AF_8B85_08DE578A5FD4__INCLUDED_)
#define AFX_CRMOSNOVNO_H__CC904099_6A71_43AF_8B85_08DE578A5FD4__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// CRMosnovno.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CCRMosnovno dialog

class CCRMosnovno : public CDialog
{
// Construction
public:
	CCRMosnovno(CWnd* pParent = NULL);   // standard constructor


double RMOgetm_Po();
double RMOgetm_Pi();
double RMOgetm_kapa();
double RMOgetm_Pamb();
double RMOgetm_To();
double RMOgetm_Rgg();




// Dialog Data
	//{{AFX_DATA(CCRMosnovno)
	enum { IDD = IDD_RMOSNOVNO };
	double	m_Po;
	double	m_Pi;
	double	m_kapa;
	double	m_Pamb;
	double	m_To;
	double	m_Rgg;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCRMosnovno)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CCRMosnovno)
	afx_msg void OnButton1();
	afx_msg void OnButton2();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CRMOSNOVNO_H__CC904099_6A71_43AF_8B85_08DE578A5FD4__INCLUDED_)
