// CTankozid.cpp : implementation file
//

#include "stdafx.h"
#include "cKOR.h"
#include "CTankozid.h"

#include "C1napon.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CCTankozid dialog


CCTankozid::CCTankozid(CWnd* pParent /*=NULL*/)
	: CDialog(CCTankozid::IDD, pParent)
{
	//{{AFX_DATA_INIT(CCTankozid)
	m_rsp2 = 0.0;
	m_run2 = 0.0;
	m_debljina2 = 0.0;
	m_pritisakPi = 0.0;
	m_hoop = 0.0;
	m_long = 0.0;
	m_radi = 0.0;
	m_zatezna2 = 0.0;
	m_ena2 = 0.0;
	m_odnosDsR = 0.0;
	m_gnn2 = 0.0;
	m_taumax2 = 0.0;
	//}}AFX_DATA_INIT
}


void CCTankozid::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CCTankozid)
	DDX_Text(pDX, IDC_EDIT1, m_rsp2);
	DDX_Text(pDX, IDC_EDIT2, m_run2);
	DDX_Text(pDX, IDC_EDIT3, m_debljina2);
	DDX_Text(pDX, IDC_EDIT4, m_pritisakPi);
	DDX_Text(pDX, IDC_EDIT5, m_hoop);
	DDX_Text(pDX, IDC_EDIT6, m_long);
	DDX_Text(pDX, IDC_EDIT7, m_radi);
	DDX_Text(pDX, IDC_EDIT8, m_zatezna2);
	DDX_Text(pDX, IDC_EDIT10, m_ena2);
	DDX_Text(pDX, IDC_EDIT9, m_odnosDsR);
	DDX_Text(pDX, IDC_EDIT11, m_gnn2);
	DDX_Text(pDX, IDC_EDIT12, m_taumax2);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CCTankozid, CDialog)
	//{{AFX_MSG_MAP(CCTankozid)
	ON_BN_CLICKED(IDC_BUTTON2, OnButton2)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CCTankozid message handlers

void CCTankozid::OnButton2() 
{

	UpdateData(true);
CC1napon napon;





	double ptobar = m_pritisakPi*100000;

	m_debljina2 = m_rsp2-m_run2;
    m_odnosDsR = m_debljina2/m_rsp2;


	m_hoop = (ptobar*m_run2)/((2*m_debljina2)*m_ena2);
	m_radi = 0;
	m_long = m_hoop/(2);
    


	m_gnn2 = (m_hoop+m_radi+m_long)/(3);
	m_taumax2 = (m_radi-m_hoop)/(2);
	UpdateData(false);
}
