#include "stdafx.h"
#include "Math.h"
#include "SaintRobert.h"
#include "Performanse_1.h"
#include "CRMM.h"
#include <fstream.h>// data stream
#include "MAR.h"
#include "GeoML.h"



//double STRO:

double STRO::STROgetn()
{
   return n;

}

double STRO::STROgetb()
{
   return b;

}

double STRO::STROgetRo()
{
     
	CCRM fluid;
	double pkom =fluid.CCRMgetP_kom();

	STRO para;
	double b = para.STROgetb();
	double n = para.STROgetn();
    
	double par1 = pow(pkom,n);
    double ro = par1*b;


	return ro;
}

double STRO::STROgetMODEL_Lenoir_Robillard()
{


	double alfa =2.15/100000;

    double beta =89;
	double d = 50; // u mm
GEOML geo;
double dkr = geo.GEOMLgetDkr(); //30;
	
   
    MAR masa;
	double ROpu = masa.MARgetROpu();//	1750;
	

	double CZZ = 1520;

    STRO para;
	double ro = para.STROgetRo();


	CCRM fluid;
	double pkom =fluid.CCRMgetP_kom();

    ///double G u zavisnosti sta je dato od pocetnih parametara G se izvodi od slucaja do slucaja
	
	
	double par1 = pkom/CZZ;
	double par2 = dkr/d;

    double	par3 = pow(par2,2);
    double	G =par1*par3;
	
//do ovde je uredu 


    
	double   pa1 = pow(G,0.8);

	double   pa2 = pow(d/1000,0.2);

	double   pa3 = pa1/pa2;

	double   pa4 = ((-beta)*ROpu*ro)/G;

	double   pa5 = exp(pa4);

	double   re = alfa*pa3*pa5;


     


	return re;


}


double STRO::STROgetR()
{

	STRO par;
	double ro = par.STROgetRo();
    double re =par.STROgetMODEL_Lenoir_Robillard();
    //double re = 0;

  
    double r = ro+re;
 

   
    
	return r;
}





/*
double STRO::MARgetMp()
{
PERF per;
double tr = per.PERFgetVremeRadaRM();
double mpp = per.PERFgetMaseniProtok();

double MPM = mpp*tr;
return MPM;


}
*/