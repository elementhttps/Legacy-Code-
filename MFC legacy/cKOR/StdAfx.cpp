// stdafx.cpp : source file that includes just the standard includes
//	cKOR.pch will be the pre-compiled header
//	stdafx.obj will contain the pre-compiled type information

#include "stdafx.h"

/*
#include "stdafx.h"
#include "Math.h"
//---------------------------------------------------
//OSNOVNA KLASA CCRM 
//---------------------------------------------------

class CCRM
{
public:	
	double M_mol;
   // double Ru;
	double k; //Bolcmanova konstanta
	double Rgg;
	double N_avo;
	double kapa;
    //double cp;
    double T_kom;
    double P_kom;
    double P_amb;
    double Mah_kr;
    double A_iz;
	double P_iz;
//----------------------------------
//BITNE NAPOMENE

str 639  ili u pdf 653 underexpanded!!! i adaptiran mlaznik


public:
	CCRM()//KONSTRUKTOR KLASE
	{
		M_mol = 32;// Kg/mol
		//Ru = 8315; // Univerzalna gasna konstanta za O2
		kapa = 1.4; //za O2

		k = 1.38;//pow(10,-23);// Bolcmanova konstanta
		N_avo = 6.022;//*pow(10,23);//Avogadrov broj

		T_kom = 2800;
		P_kom = 50*100000; //Paskala 1*10^5
		P_amb = 1*100000;  //ATMOSFERSKI PRITISAK
		P_iz = 1*100000;
		Mah_kr = 1;//str 624 dA = 0 => M=1   Napomena KPP mora da bude nesto veci od izracunatog

		A_iz = 0.001; // m^2
        Rgg = 355.4;


	}
//---------------------------------
//get funkcije u klasi //omogucava pristup vrednostima proracunatim u CRM.ccp fajlu


//-------------------
//PARAMETRI IZ KONSTRUKTORA
double CCRMgetM_mol();
double CCRMgetK();
double CCRMgetKapa();
double CCRMgetN_avo();
double CCRMgetT_kom();
double CCRMgetP_kom();
double CCRMgetP_amb();
double CCRMgetMah_kr();
double CCRMgetA_iz();
double CCRMgetRgg();
double CCRMgetP_izl();
//---------------------
//FUNKCIJE
double CCRMgetRu(); // Ru = Navo*K; 
double CCRMgetRg(); // Rg = Ru/M_mol
double CCRMgetCzKo();
double CCRMgetCp();
double CCRMgetV_iz();
double CCRMgetMahKo();
double CCRMgetPambsPkom(); //0.528

double CCRMget
double CCRMget
double CCRMget
double CCRMget
double CCRMget

};




  #include "stdafx.h"
#include "CRMM.h"
#include "Math.h"




//------------------------------
//funkcije get iz konstruktora

double CCRM::CCRMgetM_mol()
{
       return M_mol ;
}


double CCRM::CCRMgetN_avo()
{
       return N_avo ;
}


double CCRM::CCRMgetKapa()
{
       return kapa;
}


double CCRM::CCRMgetK()
{
       return k;
}


double CCRM::CCRMgetP_amb()
{
       return P_amb;
}


double CCRM::CCRMgetP_kom()
{
       return P_kom;
}


double CCRM::CCRMgetT_kom()
{
       return T_kom;
}


double CCRM::CCRMgetA_iz()
{
       return A_iz;
}

double CCRM::CCRMgetMah_kr()
{
       return Mah_kr;
}



//----------------------------------
//funkcije VAN konstruktora
//----------------------------------

double CCRM:
double CCRM:
double CCRM:
double CCRM:
double CCRM:
double CCRM:
double CCRM:
double CCRM:

double CCRM::CCRMgetRu()
{
	   CCRM GasKon;
	   double Na = GasKon.CCRMgetN_avo();
	   double k = GasKon.CCRMgetK();
	   
	   double Ru = Na*k*1000;

       return Ru;
}


double CCRM::CCRMgetRg()
{
       CCRM GasO2;
	   double Ru = GasO2.CCRMgetRu();
	   double M_mol =GasO2.CCRMgetM_mol();

       double Rg = Ru/M_mol;

       return Rg;
}


double CCRM::CCRMgetCp()
{
	    CCRM  GasCp;
        double kapa = GasCp.CCRMgetKapa();
		double Ru = GasCp.CCRMgetRu();
		double M_mol = GasCp.CCRMgetM_mol();

		double kapaK1 = kapa/(kapa-1);
        double cp = kapaK1*(Ru/M_mol);

		return cp;
}


double CCRM::CCRMgetV_iz()
{
        CCRM GasV_iz;
		double kapa = GasV_iz.CCRMgetKapa();
		double Ru = GasV_iz.CCRMgetRu();
		double M_mol = GasV_iz.CCRMgetM_mol();
		double T_kom = GasV_iz.CCRMgetT_kom();
		double P_kom = GasV_iz.CCRMgetP_kom();
		double P_amb = GasV_iz.CCRMgetP_amb(); //napomena da li je pamb = pe
        
		double kapaK1 = (2*kapa)/(kapa-1);
		double kapaKV1 = (kapa-1)/kapa;
		double RTsaM = (Ru*T_kom)/M_mol;
		double PasPko = P_amb/P_kom;
        double par1 = kapaK1*RTsaM;
		double par2 = 1-(pow(PasPko,kapaKV1));
		double V_iz = sqrt(par1*par2);


		return V_iz;
}


double CCRM::CCRMgetCzKo()
{       
	    CCRM GasZvuk; 
        double kapa = GasZvuk.CCRMgetKapa();
		double Rg = GasZvuk.CCRMgetRg();
		double T_kom = GasZvuk.CCRMgetT_kom();

		double par1 = kapa*Rg*T_kom;
        double CzKo = sqrt(par1);

		
		return CzKo;
}


double CCRM::CCRMgetMahKo()  //moguca greska
{     
	    CCRM GasMah;
		double CzKo = GasMah.CCRMgetCzKo();
		double V_iz = GasMah.CCRMgetV_iz();
		
		double MahKo = V_iz/CzKo;


        return MahKo;
		
}	      


double CCRM::CCRMgetPambsPkom()//vrednost mora da bude manja od 0.528 da bi Mkr =1
{
	   CCRM GasPasPk;
	   double P_amb = GasPasPk.CCRMgetP_amb();
       double P_kom = GasPasPk.CCRMgetP_kom();

	   double PasPk = P_amb/P_kom;


	   return PasPk;
}










*/


