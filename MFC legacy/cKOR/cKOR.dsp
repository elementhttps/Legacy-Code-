# Microsoft Developer Studio Project File - Name="cKOR" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Application" 0x0101

CFG=cKOR - Win32 Debug
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "cKOR.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "cKOR.mak" CFG="cKOR - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "cKOR - Win32 Release" (based on "Win32 (x86) Application")
!MESSAGE "cKOR - Win32 Debug" (based on "Win32 (x86) Application")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName ""
# PROP Scc_LocalPath ""
CPP=cl.exe
MTL=midl.exe
RSC=rc.exe

!IF  "$(CFG)" == "cKOR - Win32 Release"

# PROP BASE Use_MFC 6
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 6
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "Release"
# PROP Intermediate_Dir "Release"
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MD /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_AFXDLL" /Yu"stdafx.h" /FD /c
# ADD CPP /nologo /MD /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_AFXDLL" /D "_MBCS" /Yu"stdafx.h" /FD /c
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x409 /d "NDEBUG" /d "_AFXDLL"
# ADD RSC /l 0x409 /d "NDEBUG" /d "_AFXDLL"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 /nologo /subsystem:windows /machine:I386
# ADD LINK32 /nologo /subsystem:windows /machine:I386

!ELSEIF  "$(CFG)" == "cKOR - Win32 Debug"

# PROP BASE Use_MFC 6
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 6
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "Debug"
# PROP Intermediate_Dir "Debug"
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MDd /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_AFXDLL" /Yu"stdafx.h" /FD /GZ /c
# ADD CPP /nologo /MDd /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_AFXDLL" /D "_MBCS" /Yu"stdafx.h" /FD /GZ /c
# ADD BASE MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x409 /d "_DEBUG" /d "_AFXDLL"
# ADD RSC /l 0x409 /d "_DEBUG" /d "_AFXDLL"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 /nologo /subsystem:windows /debug /machine:I386 /pdbtype:sept
# ADD LINK32 /nologo /subsystem:windows /debug /machine:I386 /pdbtype:sept

!ENDIF 

# Begin Target

# Name "cKOR - Win32 Release"
# Name "cKOR - Win32 Debug"
# Begin Group "Source Files"

# PROP Default_Filter "cpp;c;cxx;rc;def;r;odl;idl;hpj;bat"
# Begin Group "Dijalozi"

# PROP Default_Filter ""
# Begin Source File

SOURCE=.\C1napon.cpp
# End Source File
# Begin Source File

SOURCE=.\C3napon.cpp
# End Source File
# Begin Source File

SOURCE=.\CAutorA.cpp
# End Source File
# Begin Source File

SOURCE=.\CDeformacije.cpp
# End Source File
# Begin Source File

SOURCE=.\CMasaCevi.cpp
# End Source File
# Begin Source File

SOURCE=.\CMtablica.cpp
# End Source File
# Begin Source File

SOURCE=.\CRMosnovno.cpp
# End Source File
# Begin Source File

SOURCE=.\CTankozid.cpp
# End Source File
# Begin Source File

SOURCE=.\Parametri.cpp
# End Source File
# End Group
# Begin Source File

SOURCE=.\ChildFrm.cpp
# End Source File
# Begin Source File

SOURCE=.\cKOR.cpp
# End Source File
# Begin Source File

SOURCE=.\cKOR.rc
# End Source File
# Begin Source File

SOURCE=.\cKORDoc.cpp
# End Source File
# Begin Source File

SOURCE=.\cKORView.cpp
# End Source File
# Begin Source File

SOURCE=.\MainFrm.cpp
# End Source File
# Begin Source File

SOURCE=.\StdAfx.cpp
# ADD CPP /Yc"stdafx.h"
# End Source File
# End Group
# Begin Group "Header Files"

# PROP Default_Filter "h;hpp;hxx;hm;inl"
# Begin Group "Dijalozi No. 1"

# PROP Default_Filter ""
# Begin Source File

SOURCE=.\C1napon.h
# End Source File
# Begin Source File

SOURCE=.\C3napon.h
# End Source File
# Begin Source File

SOURCE=.\CAutorA.h
# End Source File
# Begin Source File

SOURCE=.\CDeformacije.h
# End Source File
# Begin Source File

SOURCE=.\CMasaCevi.h
# End Source File
# Begin Source File

SOURCE=.\CMtablica.h
# End Source File
# Begin Source File

SOURCE=.\CRMosnovno.h
# End Source File
# Begin Source File

SOURCE=.\CTankozid.h
# End Source File
# Begin Source File

SOURCE=.\Parametri.h
# End Source File
# End Group
# Begin Source File

SOURCE=.\ChildFrm.h
# End Source File
# Begin Source File

SOURCE=.\cKOR.h
# End Source File
# Begin Source File

SOURCE=.\cKORDoc.h
# End Source File
# Begin Source File

SOURCE=.\cKORView.h
# End Source File
# Begin Source File

SOURCE=.\MainFrm.h
# End Source File
# Begin Source File

SOURCE=.\Resource.h
# End Source File
# Begin Source File

SOURCE=.\StdAfx.h
# End Source File
# End Group
# Begin Group "Resource Files"

# PROP Default_Filter "ico;cur;bmp;dlg;rc2;rct;bin;rgs;gif;jpg;jpeg;jpe"
# Begin Source File

SOURCE=.\res\1napon.bmp
# End Source File
# Begin Source File

SOURCE=.\res\CevA.bmp
# End Source File
# Begin Source File

SOURCE=.\res\cKOR.ico
# End Source File
# Begin Source File

SOURCE=.\res\cKOR.rc2
# End Source File
# Begin Source File

SOURCE=.\res\cKORDoc.ico
# End Source File
# Begin Source File

SOURCE=.\res\MLpro.bmp
# End Source File
# Begin Source File

SOURCE=.\res\MTabllica.bmp
# End Source File
# Begin Source File

SOURCE=.\res\p2.bmp
# End Source File
# Begin Source File

SOURCE=.\res\ProKORc.bmp
# End Source File
# Begin Source File

SOURCE=.\res\Toolbar.bmp
# End Source File
# End Group
# Begin Group "RMproracuni"

# PROP Default_Filter ""
# End Group
# Begin Group "MFluida"

# PROP Default_Filter ""
# Begin Source File

SOURCE=.\CRMM.CPP
# End Source File
# Begin Source File

SOURCE=.\CRMM.H
# End Source File
# End Group
# Begin Group "GeoOdnos"

# PROP Default_Filter ""
# Begin Source File

SOURCE=.\GeoML.cpp
# End Source File
# Begin Source File

SOURCE=.\GeoML.h
# End Source File
# End Group
# Begin Group "PerformanseRM"

# PROP Default_Filter ""
# Begin Source File

SOURCE=.\Performanse_1.cpp
# End Source File
# Begin Source File

SOURCE=.\Performanse_1.h
# End Source File
# End Group
# Begin Group "Masa Rakete"

# PROP Default_Filter ""
# Begin Source File

SOURCE=.\MAR.CPP
# End Source File
# Begin Source File

SOURCE=.\MAR.H
# End Source File
# End Group
# Begin Group "uzmi vrednost"

# PROP Default_Filter ""
# Begin Source File

SOURCE=.\pro_vre.cpp
# End Source File
# Begin Source File

SOURCE=.\pro_vre.h
# End Source File
# End Group
# Begin Group "SaintRobert"

# PROP Default_Filter ""
# Begin Source File

SOURCE=.\SaintRobert.cpp
# End Source File
# Begin Source File

SOURCE=.\SaintRobert.h
# End Source File
# End Group
# Begin Group "test"

# PROP Default_Filter ""
# Begin Source File

SOURCE=.\PODT.CPP
# End Source File
# Begin Source File

SOURCE=.\PODT.H
# End Source File
# End Group
# Begin Source File

SOURCE=.\cKOR.reg
# End Source File
# Begin Source File

SOURCE=.\ReadMe.txt
# End Source File
# Begin Source File

SOURCE=.\test.cpp
# End Source File
# End Target
# End Project
