// cKOR.h : main header file for the CKOR application
//

#if !defined(AFX_CKOR_H__4F034696_7CB1_4D40_8E31_579DB94D468F__INCLUDED_)
#define AFX_CKOR_H__4F034696_7CB1_4D40_8E31_579DB94D468F__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"       // main symbols

/////////////////////////////////////////////////////////////////////////////
// CCKORApp:
// See cKOR.cpp for the implementation of this class
//

class CCKORApp : public CWinApp
{
public:
	CCKORApp();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCKORApp)
	public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL

// Implementation
	//{{AFX_MSG(CCKORApp)
	afx_msg void OnAppAbout();
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CKOR_H__4F034696_7CB1_4D40_8E31_579DB94D468F__INCLUDED_)
