//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by cKOR.rc
//
#define IDD_ABOUTBOX                    100
#define IDR_MAINFRAME                   128
#define IDR_CKORTYPE                    129
#define IDD_PARAMETRI                   130
#define IDB_BITMAP1                     132
#define IDD_MASACEVI                    133
#define IDD_AUTOR                       134
#define IDB_BITMAP2                     137
#define IDB_BITMAP3                     138
#define IDD_MTABLICA                    139
#define IDB_BITMAP4                     141
#define IDD_1NAPON                      142
#define IDB_BITMAP5                     143
#define IDD_3NAPON                      145
#define IDD_TANKOZID                    146
#define IDD_DEFORMACIJE                 147
#define IDD_RMOSNOVNO                   148
#define IDB_BITMAP6                     149
#define IDC_ALFA                        1000
#define IDC_BETA                        1001
#define IDC_DUZ_DKO                     1002
#define IDC_DUZ_DDI                     1003
#define IDC_RKR                         1004
#define IDC_DKR                         1005
#define IDC_EDIT1                       1006
#define IDC_EDIT2                       1007
#define IDC_EDIT3                       1008
#define IDC_EDIT4                       1009
#define IDC_EDIT5                       1010
#define IDC_EDIT6                       1011
#define IDC_EDIT7                       1012
#define IDC_EDIT8                       1013
#define IDC_BUTTON1                     1014
#define IDC_EDIT10                      1014
#define IDC_EDIT9                       1015
#define IDC_EDIT11                      1016
#define IDC_BUTTON2                     1017
#define IDC_EDIT15                      1017
#define IDC_EDIT12                      1018
#define IDC_LIST1                       1020
#define IDC_EDIT13                      1026
#define IDC_EDIT14                      1027
#define IDC_EDIT17                      1030
#define IDC_EDIT18                      1031
#define IDC_EDIT19                      1032
#define IDC_EDIT22                      1035
#define IDM_PAR                         32771
#define IDM_MREZA                       32772
#define IDM_POZ                         32773
#define IDM_AUTOR                       32774
#define IDM_MASACENA                    32775
#define IDM_MTABLICA                    32776
#define IDM_1NAPON                      32777
#define IDM_3NAPON                      32778
#define IDM_TANKOZID                    32779
#define IDM_DEBELO                      32780
#define IDM_TANKOZIDNE                  32781
#define IDM_RMO                         32782
#define IDM_TEST_SAVE                   32783
#define IDM_TEST_OPEN                   32784
#define IDM_EXE1                        32785
#define IDM_KT                          32786
#define IDM_POLARIS                     32787

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_3D_CONTROLS                     1
#define _APS_NEXT_RESOURCE_VALUE        150
#define _APS_NEXT_COMMAND_VALUE         32788
#define _APS_NEXT_CONTROL_VALUE         1021
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
