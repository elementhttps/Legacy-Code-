// ChildFrm.cpp : implementation of the CChildFrame class
//

#include "stdafx.h"
#include "GRAFIK3.h"
#include "SplashWnd.h"
#include "ChildFrm.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CChildFrame

IMPLEMENT_DYNCREATE(CChildFrame, CMDIChildWnd)

BEGIN_MESSAGE_MAP(CChildFrame, CMDIChildWnd)
	//{{AFX_MSG_MAP(CChildFrame)
	ON_WM_ERASEBKGND()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CChildFrame construction/destruction

CChildFrame::CChildFrame()
{
	// TODO: add member initialization code here

	// salje 1 poruku svaki put kada otvori novi child-pomocni prozor
//::MessageBox(NULL, "poruka iz pomocnog prozora", "", MB_OK | MB_ICONQUESTION);


/*
::ShellExecute(this->m_hWnd,"open","C:\\Documents and Settings\\PROJEKTOVANJE\\Desktop\\splashscreen\\splash_screen\\Dialogspl.exe",
		"","",SW_SHOW );

	::ShellExecute(this->m_hWnd,"open","C:\\Program Files\\VideoLAN\\VLC\\vlc.exe",
		"E:\\muzika\\x\\04-RyouteippainoJonii.mp3","",SW_SHOW );
*/	
}

CChildFrame::~CChildFrame()
{
}

BOOL CChildFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying

	//  the CREATESTRUCT cs
//	CSplashWnd::ShowSplashScreen(this);



//salje 3 poruke ? //::MessageBox(NULL, "poruka iz pomocnog prozora PreCreateWindow", "", MB_OK | MB_ICONQUESTION);


	if( !CMDIChildWnd::PreCreateWindow(cs) )
		return FALSE;

	cs.style = WS_CHILD | WS_VISIBLE | WS_OVERLAPPED | WS_CAPTION | WS_SYSMENU
		| FWS_ADDTOTITLE | WS_THICKFRAME | WS_MINIMIZEBOX | WS_MAXIMIZEBOX | WS_MAXIMIZE;

	return TRUE;
}

void CChildFrame::ActivateFrame(int nCmdShow)
{
	// TODO: Modify this function to change how the frame is activated.

	nCmdShow = SW_MAXIMIZE;
	CMDIChildWnd::ActivateFrame(nCmdShow);
}


/////////////////////////////////////////////////////////////////////////////
// CChildFrame diagnostics

#ifdef _DEBUG
void CChildFrame::AssertValid() const
{
	CMDIChildWnd::AssertValid();
}

void CChildFrame::Dump(CDumpContext& dc) const
{
	CMDIChildWnd::Dump(dc);
}

#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CChildFrame message handlers

/*
::ShellExecute(this->m_hWnd,"open","C:\\Program Files\\VideoLAN\\VLC\\vlc.exe",
		"E:\\muzika\\x\\ace combat 3 Mind flow.mp3","",SW_SHOW );
*/

BOOL CChildFrame::OnEraseBkgnd(CDC* pDC) 
{
	// TODO: Add your message handler code here and/or call default
	
	return FALSE;
}
