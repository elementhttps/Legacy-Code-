// GRAFIK3.cpp : Defines the class behaviors for the application.
//

#include "stdafx.h"
#include "GRAFIK3.h"

#include "MainFrm.h"
#include "ChildFrm.h"
#include "GRAFIK3Doc.h"
#include "GRAFIK3View.h"

#include "SplashWnd.h"

#include <mmsystem.h>
#pragma comment(lib,"winmm.lib")

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CGRAFIK3App

BEGIN_MESSAGE_MAP(CGRAFIK3App, CWinApp)
	//{{AFX_MSG_MAP(CGRAFIK3App)
	ON_COMMAND(ID_APP_ABOUT, OnAppAbout)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG_MAP
	// Standard file based document commands
	ON_COMMAND(ID_FILE_NEW, CWinApp::OnFileNew)
	ON_COMMAND(ID_FILE_OPEN, CWinApp::OnFileOpen)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CGRAFIK3App construction

CGRAFIK3App::CGRAFIK3App()
{
	// TODO: add construction code here,
	// Place all significant initialization in InitInstance


	//salje samo jednu poruku !!!
	//OVDE STAVITI SPLASH SCREEN 
	


	//::MessageBox(NULL, "poruka iz GRAFIK3.cpp fajla ", "", MB_OK | MB_ICONQUESTION);
}

/////////////////////////////////////////////////////////////////////////////
// The one and only CGRAFIK3App object

CGRAFIK3App theApp;

/////////////////////////////////////////////////////////////////////////////
// CGRAFIK3App initialization

BOOL CGRAFIK3App::InitInstance()
{
	CCommandLineInfo cmdInfo;
	ParseCommandLine(cmdInfo);
		//::MessageBox(NULL, "poruka iz pomocnog prozora", "", MB_OK | MB_ICONQUESTION);

AfxEnableControlContainer();
	CSplashWnd::EnableSplashScreen(cmdInfo.m_bShowSplash);
CSplashWnd::ShowSplashScreen(5000);
/*
Beep(1000,500);//frekfencija , duzina trajanja tona

for(int i = 1;i<5;i++)
{
Beep(1000,500);
Beep(800,1000);
Beep(900,500);
}

Beep(2000,2500);

Beep(1000,500);
Beep(800,1000);
Beep(900,500);
Beep(1000,500);
Beep(800,1000);
Beep(900,500);
*/
	PlaySound(_T("EnergyShield.wav"),NULL,SND_ASYNC);
Sleep(3000);
CSplashWnd::HideSplashScreen();
	





	
	// Standard initialization
	// If you are not using these features and wish to reduce the size
	//  of your final executable, you should remove from the following
	//  the specific initialization routines you do not need.

	// Change the registry key under which our settings are stored.
	// TODO: You should modify this string to be something appropriate
	// such as the name of your company or organization.
	SetRegistryKey(_T("Local AppWizard-Generated Applications"));

	LoadStdProfileSettings();  // Load standard INI file options (including MRU)

	// Register the application's document templates.  Document templates
	//  serve as the connection between documents, frame windows and views.

	CMultiDocTemplate* pDocTemplate;
	pDocTemplate = new CMultiDocTemplate(
		IDR_GRAFIKTYPE,
		RUNTIME_CLASS(CGRAFIK3Doc),
		RUNTIME_CLASS(CChildFrame), // custom MDI child frame
		RUNTIME_CLASS(CGRAFIK3View));
	AddDocTemplate(pDocTemplate);


	// create main MDI Frame window
	CMainFrame* pMainFrame = new CMainFrame;
	if (!pMainFrame->LoadFrame(IDR_MAINFRAME))
		return FALSE;
	m_pMainWnd = pMainFrame;

	// Parse command line for standard shell commands, DDE, file open


	// Dispatch commands specified on the command line
	if (!ProcessShellCommand(cmdInfo))
		return FALSE;

	// The main window has been initialized, so show and update it.
	pMainFrame->ShowWindow(SW_SHOWMAXIMIZED); //prikaz
	pMainFrame->UpdateWindow();


	return TRUE;
}


/////////////////////////////////////////////////////////////////////////////
// CAboutDlg dialog used for App About

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// Dialog Data
	//{{AFX_DATA(CAboutDlg)
	enum { IDD = IDD_ABOUTBOX };
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CAboutDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	//{{AFX_MSG(CAboutDlg)
		// No message handlers
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
	//{{AFX_DATA_INIT(CAboutDlg)
	//}}AFX_DATA_INIT
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CAboutDlg)
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
	//{{AFX_MSG_MAP(CAboutDlg)
		// No message handlers
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

// App command to run the dialog
void CGRAFIK3App::OnAppAbout()
{
	CAboutDlg aboutDlg;
	aboutDlg.DoModal();
}

/////////////////////////////////////////////////////////////////////////////
// CGRAFIK3App message handlers


BOOL CGRAFIK3App::PreTranslateMessage(MSG* pMsg) 
{
	if (CSplashWnd::PreTranslateAppMessage(pMsg)) {
		return TRUE;
	}	
	return CWinApp::PreTranslateMessage(pMsg);
}
