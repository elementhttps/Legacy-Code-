// GRAFIK3Doc.cpp : implementation of the CGRAFIK3Doc class
//

#include "stdafx.h"
#include "GRAFIK3.h"
//#include "SplashWnd.h"
#include "GRAFIK3Doc.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CGRAFIK3Doc

IMPLEMENT_DYNCREATE(CGRAFIK3Doc, CDocument)

BEGIN_MESSAGE_MAP(CGRAFIK3Doc, CDocument)
	//{{AFX_MSG_MAP(CGRAFIK3Doc)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CGRAFIK3Doc construction/destruction

CGRAFIK3Doc::CGRAFIK3Doc()
{
	// TODO: add one-time construction code here

}

CGRAFIK3Doc::~CGRAFIK3Doc()
{
}

BOOL CGRAFIK3Doc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;

	// TODO: add reinitialization code here
	// (SDI documents will reuse this document)

	return TRUE;
}



/////////////////////////////////////////////////////////////////////////////
// CGRAFIK3Doc serialization

void CGRAFIK3Doc::Serialize(CArchive& ar)
{
	if (ar.IsStoring())
	{
		// TODO: add storing code here
	}
	else
	{
		// TODO: add loading code here
	}
}

/////////////////////////////////////////////////////////////////////////////
// CGRAFIK3Doc diagnostics

#ifdef _DEBUG
void CGRAFIK3Doc::AssertValid() const
{
	CDocument::AssertValid();
}

void CGRAFIK3Doc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CGRAFIK3Doc commands
