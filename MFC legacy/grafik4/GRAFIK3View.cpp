// GRAFIK3View.cpp : implementation of the CGRAFIK3View class
//

#include "stdafx.h"
#include "GRAFIK3.h"
#include <fstream.h>
#include "Math.h"
#include "Performanse_1.h"
#include "GeoML.h"
#include "CRMM.h"



#include "memdc.h" 

#include <mmsystem.h>//header za zvuk u wav !! formatu
#pragma comment(lib,"winmm.lib")//za ispravljanje recompile bug-a

#include "GRAFIK3Doc.h"
#include "GRAFIK3View.h"

#include "CAutor.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CGRAFIK3View

IMPLEMENT_DYNCREATE(CGRAFIK3View, CView)

BEGIN_MESSAGE_MAP(CGRAFIK3View, CView)
	//{{AFX_MSG_MAP(CGRAFIK3View)
	ON_WM_KEYDOWN()
	ON_WM_KEYUP()
	ON_WM_MOUSEMOVE()
	ON_WM_LBUTTONDOWN()
	ON_WM_LBUTTONDBLCLK()
	ON_WM_RBUTTONDOWN()
	ON_WM_TIMER()
	ON_COMMAND(IDM_AUTOR, OnAutor)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CGRAFIK3View construction/destruction

CGRAFIK3View::CGRAFIK3View()
{
	m_Mreza = 1;
	m_DGrafik = 0;
	m_Zumx= 20;
	m_Zumy= 20;
	m_Korak = 10;
	m_IDev = 100000;
	m_GMreza = 5000;

	m_viewx = 1;
	m_viewy = -1;

	m_Orgfx =90;
	m_Orgfy =620;

	dx = 1;
	m_paint = 1;


	m_pp1 = 1;

   nWidth =0;
   nHeight =0;
   Px = 0; //nema promene u pocetnom trenutku P=0
   Py = 0;
   P = 0;
   dxzum =0; 
   dyzum =0; 

   dxs =0;
   dys =0;
   x =0;
   y=0;
   t=1;
   swichT=1;

  
 

}

CGRAFIK3View::~CGRAFIK3View()
{
}

BOOL CGRAFIK3View::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	return CView::PreCreateWindow(cs);
}

/////////////////////////////////////////////////////////////////////////////
// CGRAFIK3View drawing

void CGRAFIK3View::OnDraw(CDC* dc)                //(CDC* pDC)
{
//CMemDC pDC(dc);
	
    //CMemDC pDC(pDC, &rcBounds);
	CGRAFIK3Doc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);
	if (!pDoc)
		return;
	CRect rect;
	CBrush brsBlack, *brsOld;

GetClientRect(&rect);
	brsBlack.CreateSolidBrush(RGB(0, 0, 0));


	CBitmap bitID;
	CDC dcID;

	// Load the bitmap from the resource
	bitID.LoadBitmap(IDB_BITMAP1);
	// Create a memory device compatible with the above CPaintDC variable
	dcID.CreateCompatibleDC(dc);
	// Select the new bitmap
	CBitmap *BmpPrevious = dcID.SelectObject(&bitID);

	// Copy the bits from the memory DC into the current dc
	dc->BitBlt(20, 10, 400, 400, &dcID, 0, 0, SRCCOPY);

	// Restore the old bitmap
	dc->SelectObject(BmpPrevious);
	// Do not call CView::OnPaint() for painting messages
	


/*/----------------------------------------------------------
// POKRETNE LINIJE NA POZICIJU MISA
//----------------------------------------------------------
 dc->MoveTo(point.x-4, 0 );//LINIJA ZA PRACENJE X OSE  
 dc->LineTo(point.x-4, point.y+10000);

 dc->MoveTo(0,point.y-40);//LINIJA ZA PRACENJE Y OSE
 dc->LineTo(point.x+10000, point.y-40);
//----------------------------------------------------------*/


	CRect Recto; //PRAVOUGAONIK EKRANA
    GetClientRect (&Recto);//PRAV. KLIJENTA TJ KORISNIKA I NJEGOVE REZOLUCIJE

	nWidth  =Recto.Width();//int MERA SIRINE
    nHeight = Recto.Height();// int MERA DUZINE

GetCursorPos(&point);// ZADNJA POZICIJA MISA KORISNIKA



//----------------------------------------------------------	     
//POZICIJA TEKSTA NA KORDINATU EKRANA!!! KOS1 FIKSNA POZICIJA
//----------------------------------------------------------
char t1[10];
sprintf(t1, "Pozicija  (%d,%d)",point.x,  point.y);
dc->TextOut (nWidth/1.2,nHeight/5 ,t1);

char t2[10];
sprintf(t2, "ORG (%dx,%dy)",m_Orgfx,  m_Orgfy);
dc->TextOut (nWidth/1.2,nHeight/6 ,t2);

char t3[10];
sprintf(t3, "Zum (%dx,%dy)",m_Zumx,  m_Zumy);
dc->TextOut (nWidth/1.2,nHeight/8,t3);

char t4[10];
sprintf(t4, "(X: %d Y: %d)",xLogical+dxzum,yLogical+dyzum);
dc->TextOut (nWidth/1.2,nHeight/10 ,t4);

char t6[10];
sprintf(t6, "(t: %d)",t);
dc->TextOut (nWidth/1.2,nHeight/4.5 ,t6);


int xLogical1= (point.x-m_Orgfx)/(m_viewx/m_Zumx);
int yLogical1 =(point.y-m_Orgfy)/(m_viewy/m_Zumy);

	 int sxLogical1=xLogical1+dxzum;

    int  syLogical1=yLogical1+dyzum;
//Invalidate();

dxg =sxLogical1;
dyg =syLogical1;


char t5[10];
sprintf(t5, "(dX: %d dY: %d)",dxg,dyg);
dc->TextOut (nWidth/1.2,nHeight/13 ,t5);
//----------------------------------------------------------


//----------------------------------------------------------
//LINIJE NA CENTRU NEMOBILAN PO PRESEKU REZOLUCIJE EKRANA	
//----------------------------------------------------------
	CPen PenBlue2(PS_SOLID, 1, RGB(0, 0, 255));
	dc->SelectObject(PenBlue2);

    dc->MoveTo(nWidth / 2, 0);  //LINIJA ZA PRIKAZ POLOVINE X EKRANA
	dc->LineTo(nWidth / 2, nHeight);
	dc->MoveTo(0, nHeight/ 2);//LINIJA ZA PRIKAZ POLOVINE Y EKRANA
	dc->LineTo(nWidth, nHeight / 2);
//----------------------------------------------------------


//----------------------------------------------------------
//POKRETNI TEKST NA POZICIJU MISA X I Y	
//----------------------------------------------------------	
	dc->SetTextColor(RGB(100, 100, 0)); 

    dc->TextOut(point.x-100, point.y-50, 'X');
	dc->TextOut(point.x-4, point.y+50, 'Y');
   //////////////////////////////////////////////////////
/*
   MSG msg;
  
        if (!AfxGetApp ()->PumpMessage ()) {
            ::PostQuitMessage (0);
         
        }
    LONG lIdle = 0;
    while (AfxGetApp ()->OnIdle (lIdle++));
    return TRUE;

*/


   dc->SetMapMode(MM_ANISOTROPIC); //orijentacija osa PO ZELJI PROGRAMERA



dc->SetViewportOrg(m_Orgfx, m_Orgfy);

/*
To control your own unit system, the orientation of the axes or how the 
application converts the units used on your application, use either the 
MM_ISOTROPIC or the MM_ANISOTROPIC map modes.
*/


   //  //90 -620  definise pocetnu referentnu tacku na ekranu  


/*
Since you are drawing on a device context, all you need to do is simply call
the CDC::SetViewportOrg() method. It is overloaded with two versions, which 
allow you to use either the X and the Y coordinates or a defined point. 
 
   The syntaxes of this method are:

SetViewportOrg(int X, int Y);
SetViewportOrg(CPoint Pt);


*/

	dc->SetWindowExt(m_Zumx,m_Zumy); //SetWindowExtent(100,100)  SetViewportExtent(200,200)  100 jedinica = 200 pixela
/*
CSize SetWindowExt(int cx, int cy);
CSize SetWindowExt(SIZE size);

Therefore, after calling SetMapMode() and specifying the MM_ISOTROPIC (or MM_ANISOTROPIC),
you must call the CDC:SetWindowExt() method. This method specifies how much each new unit
will be multiplied by the old or default unit system.


*/ //1 m = 2 pixela
	


	dc->SetViewportExt(m_viewx,m_viewy);//1 -1
/*
CSize SetViewportExt(int cx, int cy);
CSize SetViewportExt(SIZE size);



To use the first version of this function, you must provide the units of device conversion 
as cx for the horizontal axis and as cy for the vertical axis.



----------------------------------
Logical coordinates  //jedinice npr metri...
----------------------------------
(also referred to as page coordinates ) are determined
by the mapping mode. For example, the MM_LOENGLISH mapping mode has
logical coordinates in units of 0.01 inches, with the origin in the top left corner
of the client area, and the positive y axis direction running from bottom to top.

----------------------------------
Device coordinates   //pikseli
----------------------------------
(also referred to as client coordinates  in a window) are
measured in pixels in the case of a window, with the origin at the top left corner
of the client area, and with the positive y axis direction from top to bottom.
These are used outside of a device context, for example for defining the
position of the cursor in mouse message handlers.


----------------------------------
Screen coordinates 
----------------------------------
are measured in pixels and have the origin at the top left
corner of the screen, with the positive y axis direction from top to bottom.
These are used when getting or setting the cursor position.


The formulae that are used by Windows to convert from logical coordinates to device
coordinates are:


xDevice = (xLogical-xWindowOrg)*(xViewPortExt/xWindowExt)+xViewportOrg

yDevice = (yLogical-yWindowOrg)*(yViewPortExt/yWindowExt)+yViewportOrg



dc->SetViewportOrg(m_Orgfx, m_Orgfy); ViewOrg pozicija x,y
dc->SetWindowExt(m_Zumx,m_Zumy);   winExt je zum
dc->SetViewportExt(m_viewx,m_viewy); orijentacija osa
dc->SetWindowOrg




*/
	
	





//PRETVARANJE POZICIJA MISA U LOGICKE KOORDINATE ZA PRIKAZ NA SEKUNDARNOM KOS
   xLogical= (point2.x-m_Orgfx)/(m_viewx/m_Zumx);
   yLogical =(point2.y-m_Orgfy)/(m_viewy/m_Zumy);

	 int sxLogical=xLogical+dxzum;

    int  syLogical=yLogical+dyzum;




CPen PenGreen2(PS_SOLID, 1, RGB(0,255, 0));
dc->SelectObject(PenGreen2);



     dc->MoveTo(sxLogical, 0 );
     dc->LineTo(sxLogical, syLogical);

     dc->MoveTo(0, syLogical );
     dc->LineTo(sxLogical, syLogical);

//=---------------------------------------------------------

/*zum je 10 uvecannje je za 500 po x i y osi
	     11 550
         12 600
         ...
  za zum 20 uvecanje je 1000 x y

  za svaki 1 zum uvecanje je za 50 m_orgx po x ili y pravcu

dx = 1000
zum 20

dzum = 1000/20 = 50

xLogic = F(x)
za svaku promenu zuma ili pozicije x y treba dodati ili oduzeti funkciju od zuma

-u pocetnom trenutku vrednost treba da bude 0
- nakon pomene oduzeti ili dodati vrednost F(zum)

  1.pre promene

  xLogic = F(x)+0

  2.nakon promene

  xLogic = F(x)+-F(zum)

  F(zum) = dx


  dx = 50*20; jeste 1000 tacno

  dx = 50*m_zumx

  ideja uvesti varijablu promene

  dx = 50*m_zumx*P
  
	P=1,0

  P = 0 u pocetnom
  P = -1,+1 u zavisnosti od smera kretanja 
RESENO POMERENJE POMOCU TASTATURE

*/


/*
	 PRILIKOM ZUMA 
	 zum je ekvivalentan m_Zum*(trenutna_pozicija)*P

  trenutna_pozicija misa 

  m_Zum   pozicija
   1           251  
   2           2*251 = 502
   3           753
    ...        ...
   10          2510

  xLog =m_Zumx*(point.x)*P
  yLog =m_Zumy*(point.y)*P

  DukupnoX = dxzum+dxs
  DukupnoX = 50*m_zumx*P+m_Zumx*(point.x)*P


	 
	 
	 
 */






   xLogical2= (point3.x-m_Orgfx)/(m_viewx/m_Zumx);
   yLogical2 =(point3.y-m_Orgfy)/(m_viewy/m_Zumy);



   	 int sx2Logical=xLogical2+dxzum;

    int  sy2Logical=yLogical2+dyzum;

dxs =sx2Logical;
dys =sy2Logical;

CPen PenOrange(PS_SOLID, 1, RGB(100,100, 0));
dc->SelectObject(PenOrange);



     dc->MoveTo(dxs, 0 );
     dc->LineTo(dxs, dys);

     dc->MoveTo(0, dys );
     dc->LineTo(dxs, dys);
/*
*/
//----------------------------------------------------------
// OSE
	CPen PenBlack40(PS_SOLID, 5, RGB(0, 0, 0));
	dc->SelectObject(PenBlack40);
	dc->MoveTo(-1000,     0);
	dc->LineTo( 20000000,     0);
	dc->MoveTo(   0, -1000);
	dc->LineTo(   0,  20000000);


//----------------------------------------------------------



//----------------------------------------------------------	
//POZICIJA OZNAKA MREZE LENJIRA
//----------------------------------------------------------	
	

    
switch (m_paint)
 {


		case 0 :

		break;


	case 1 :
			
		CPen PenBlack3(PS_SOLID, 3, RGB(0, 0, 0));
	dc->SelectObject(PenBlack3);
    
	for(int koox = 0; koox < 100000; koox += 1000)
    {
        dc->MoveTo(koox, 0);
        dc->LineTo(koox, -1000);
    }

	for(int kooy = 0; kooy < 100000; kooy += 1000)
    {
        dc->MoveTo(0, kooy);
        dc->LineTo(-1000, kooy);
    }



	dc->SelectObject(PenBlack3);
    for(int koox2 = 0; koox2 < 100000; koox2 += 5000)
    {
        dc->MoveTo(koox2, 0);
        dc->LineTo(koox2, -2000);
    }
	for(int kooy2 = 0; kooy2 < 100000; kooy2 += 5000)
    {
        dc->MoveTo(0, kooy2);
        dc->LineTo(-2000, kooy2);
    }

	for(int koox3 = 0; koox3 < 100000; koox3 += 10000)
    {
        dc->MoveTo(koox3, 0);
        dc->LineTo(koox3, -3000);
    }
	for(int kooy3 = 0; kooy3 < 100000; kooy3 += 10000)
    {
        dc->MoveTo(0, kooy3);
        dc->LineTo(-3000, kooy3);
    }

    dc->SetTextColor(RGB(0, 0, 0));
	for (int kt1= 0; kt1<100000; kt1+=10000) 
	{
        
        CString string;
        string.Format (("%d "), (kt1 ) );
        dc->TextOut (kt1, -3000, string);
    }

	dc->SetTextColor(RGB(0, 0, 0));
	for (int kt2= 1000; kt2<100000; kt2+=10000) 
	{
        
        CString string;
        string.Format (("%d"), (((kt2-1000)) ) );
        dc->TextOut (-6000, kt2, string);
    }
	//Invalidate();
         break;
	
	

			
	
	


		
}
	
	//Lenjiri po x i y osi
	//////////////////////////////////////////////////////	

    



//----------------------------------------------------------
//MREZA
	
	CPen PenBlack4(PS_SOLID, 1, RGB(0, 0, 0));
	dc->SelectObject(PenBlack4);
	
	switch (m_Mreza =0)//PRIKAZ MREZE DA NE 1 0
	
	{
	case 0:
    
	

	break;

	case 1:
		for(int kooxx = 0; kooxx < 1000000; kooxx += m_GMreza)
    {
        dc->MoveTo(kooxx, 0);
        dc->LineTo(kooxx, 50000);
    }
	

	for(int kooyy = 0; kooyy < 1000000; kooyy += m_GMreza)
    {
        dc->MoveTo(0, kooyy);
        dc->LineTo(150000, kooyy);
    }
		break;
	}
//----------------------------------------------------------

	

//KONSTANTE ZA PRORACUNE U VIEW DOMENU
//////////////////////////////////////////////////////	
double PI = 3.1415926;
double g = 9.81;
//////////////////////////////////////////////////////	
//----------------------------------------------------------




//----------------------------------------------------------
//KORAK I MAKSIMUM
//----------------------------------------------------------	

double kor = m_Korak;//1

double max = m_IDev; //oko milion
//----------------------------------------------------------	



//----------------------------------------------------------	
//FUNKCIJE
//----------------------------------------------------------
/*
#include <stdlib.h> //Needed for "exit" function
#include <fstream.h>

fstream myfile;
myfile.open ("data.txt",ios::out);
for (double i2=0.0; i2<360.0; i2+=0.1)
{

myfile <<""<<i2<<"      "<<i2*sin(i2) <<endl;

cout<<""<<i2<<"      "<<i2*sin(i2)<<endl;

   
	if(i2 ==360)
{	
    break;
	
}

}
myfile.close();	
*/



for (double i7= 1;i7<t;i7+=3000)
{

 double x7 = i7;//+dx;
 double y7;
	 y7 = (2000)*t*sin(((PI*x7)/180)+(t*0.2));
 
//sin(w*t+omega)
dc->SetPixel(i7, y7, RGB(255, 0, 255));	
	 
}




for (double i= 1;i<2000;i+=0.5)
{

 double x = i;//+dx;
 double y = (2000)*sin(((PI*x)/180)+(t*0.5));
 
//sin(w*t+omega)
dc->SetPixel(i++, y, RGB(255, 0, 0));	
	 
}


for (double i3= 1;i3<2000;i3+=0.5)
{

 double x3 = i3;//+dx;
 double y3 = (2000)*(t)/(t-1)*sin(((PI*x3)/180)+(t*0.7));
 
//sin(w*t+omega)
	
dc->SetPixel(i3, y3, RGB(0, 0, 255));	 
}


fstream myfile;
myfile.open ("data.txt",ios::out);
for (double i4= 1;i4<2000;i4+=0.5)
{


 double x4 = i4;//+dx;
 double y4 = (2000)*sin(((PI*x4)/180)+(2*t))+(2000)*cos(((PI*x4)/180)+(t));
 myfile <<""<<t<<"    "<<i4<<"    "<<y4<<endl;

  //double y4 = (2000)*sin(((PI*x4)/180)+(t*2))+1000;
//sin(w*t+omega)
dc->SetPixel(i4, y4, RGB(0, 0, 0));	
	 
}
myfile.close();











        

}


/////////////////////////////////////////////////////////////////////////////
// CGRAFIK3View diagnostics

#ifdef _DEBUG
void CGRAFIK3View::AssertValid() const
{
	CView::AssertValid();
}

void CGRAFIK3View::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}

CGRAFIK3Doc* CGRAFIK3View::GetDocument() // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CGRAFIK3Doc)));
	return (CGRAFIK3Doc*)m_pDocument;
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CGRAFIK3View message handlers



   


void CGRAFIK3View::OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags) 
{
	switch (nChar)
 {
	case VK_RIGHT :	PlaySound(_T("Type.wav"),NULL,SND_ASYNC);
			m_Orgfx-=50;
			m_pp1 = 0;
			xLogical=+50;
			
			Px=1;
		    dxzum -=50*m_Zumx*Px;
			Px=0;
	
            //int dyzum =50*m_Zumy*Py; 
   
			
	
			Invalidate();
         break;

	case VK_LEFT :	PlaySound(_T("Type.wav"),NULL,SND_ASYNC);
		m_Orgfx+=50;
			m_pp1 = 0;
			xLogical=-50;
			Px=1;
			dxzum +=50*m_Zumx*Px;
			Px=0;
		
		
	
			Invalidate();
		break;

	case VK_UP :	PlaySound(_T("Type.wav"),NULL,SND_ASYNC);
		m_Orgfy+=50;
			m_pp1 = 0;
			Py=1;
			dyzum -=50*m_Zumy*Py;
			Py=0;
			
		




	
	
			Invalidate();
		break;

    case VK_DOWN:	PlaySound(_T("Type.wav"),NULL,SND_ASYNC);
		m_Orgfy-=50;
			m_pp1 = 0;
			Py=1;
			dyzum +=50*m_Zumy*Py;
			Py=0;

	



		
		Invalidate();
		break;
    
	case VK_HOME:	PlaySound(_T("Type.wav"),NULL,SND_ASYNC);
		m_Orgfx = 90;
		m_Orgfy = 620;
		m_Zumx= 60;
        m_Zumy = 60;
			m_pp1 = 0;
		Invalidate();
		break;

	case VK_F2:	PlaySound(_T("Type.wav"),NULL,SND_ASYNC);
		m_Zumx +=10;
		m_Zumy +=10;
			m_pp1 = 0;
			dxzum =0;
			dyzum =0;
	
		Invalidate();
		break;

	case VK_F1:	PlaySound(_T("Type.wav"),NULL,SND_ASYNC);
		m_Zumx -=10;
		m_Zumy -=10;
		m_pp1 = 0;
		dxzum =0;
		dyzum =0;
	
		if (m_Zumx < 1)
		{
			m_Zumx =1;
			m_Zumy =1;
		}
			
		Invalidate();
		break;

	case VK_F3:	PlaySound(_T("Type.wav"),NULL,SND_ASYNC);
		m_Zumx -=1;
		m_Zumy -=1;
		
		if (m_Zumx < 1)
		{
			m_Zumx =1;
			m_Zumy =1;
		}
			m_pp1 = 0;
		Invalidate();
		break;

	case VK_F4:	PlaySound(_T("Type.wav"),NULL,SND_ASYNC);//f4
		m_Zumx +=1;
		m_Zumy +=1;
		m_pp1 = 0;
	
		Invalidate();
		break;



	case 0x51://q
		PlaySound(_T("Type.wav"),NULL,SND_ASYNC);
		dx +=100;
		m_pp1 = 0;
	
		Invalidate();
		break;

	case 0x57://w
			PlaySound(_T("Type.wav"),NULL,SND_ASYNC);
		dx -=100;
		m_pp1 = 0;
	
		Invalidate();
		break;
	
	case 0x31://1
	PlaySound(_T("Beep06.wav"),NULL,SND_ASYNC);
		swichT=0;
		//swichT=0; 0 off --- 1 on
        KillTimer (1);//rokni tajmer
	
		Invalidate();
		break;
	case 0x32://2
	PlaySound(_T("Type.wav"),NULL,SND_ASYNC);
		swichT=1;
		SetTimer(1, 1000, NULL);
		

	
		Invalidate();
		break;
	case 0x33://3
	PlaySound(_T("Type.wav"),NULL,SND_ASYNC);
		swichT=2;//vreme nazad
		SetTimer(1, 1000, NULL);	
		

	
		Invalidate();
		break;

}
       
	
	CView::OnKeyDown(nChar, nRepCnt, nFlags);
}



void CGRAFIK3View::OnKeyUp(UINT nChar, UINT nRepCnt, UINT nFlags) 
{
		switch (nChar)
 {
	case VK_RIGHT :
			
			m_pp1 = 1;
			Px=0;
		    //dxzum -=50*m_Zumx*Px; 
            //int dyzum =50*m_Zumy*Py; 
			
//EnergyShield
			
	
			Invalidate();
         break;

	case VK_LEFT :
	m_pp1 = 1;
	Px=0;

	
		
	
			Invalidate();
		break;

	case VK_UP :
	m_pp1 = 1;
	Py=0;
	


	
	
	
			Invalidate();
		break;

    case VK_DOWN:
		m_pp1 = 1;
		Py=0;
	
		
		Invalidate();
		break;
    
	case VK_HOME:
	m_pp1 = 1;
		Invalidate();
		break;

	case VK_F2:
	m_pp1 = 1;
	
		Invalidate();
		break;

	case VK_F1:
	m_pp1 = 1;
	P=1;
	dxs -=m_Zumx*(point3.x)*P;
    dys -=m_Zumy*(point3.y)*P;
		
	
		break;

	case VK_F3:
     m_pp1 = 1;
		Invalidate();
		break;

	case VK_F4://f4
	m_pp1 = 1;
	
		Invalidate();
		break;



	case 0x51://q
	
	m_pp1 = 1;
	
		Invalidate();
		break;

	case 0x57://w
		
	m_pp1 = 1;
	
		Invalidate();
		break;
	

	


	case 0x41://a
	
			m_viewx -=1;
		

	
		Invalidate();
		break;

	case 0x53://s
		
		m_viewx +=1;
		

	
		Invalidate();
		break;

	case 0x5a://z
	
		m_viewy -=1;

	
		Invalidate();
		break;

	case 0x58://x
	
         m_viewy +=1;
	
		Invalidate();
		break;


	case 0x1b://escape
	

switch(::MessageBox(NULL, "Izaz iz programa ?", "", MB_YESNO | MB_ICONQUESTION))
   { 
  
    case IDNO:
      
        break;
    case IDYES:
           exit(0); 
        break;

    default:
        ASSERT(FALSE);
        break;
   } 

     
	
		Invalidate();
		break;

/*		*/
	


}
       
	CView::OnKeyUp(nChar, nRepCnt, nFlags);
}

void CGRAFIK3View::OnMouseMove(UINT nFlags, CPoint point) 
{
	 
      //previous = point;
 
	::SetCursor(::LoadCursor(NULL, IDC_CROSS));
m_paint =0;
	//Invalidate();
		m_paint =1;
	

CView::OnMouseMove(nFlags, point);
}

void CGRAFIK3View::OnLButtonDown(UINT nFlags, CPoint point) 
{
      


	point2.x= point.x;
	point2.y= point.y;
	dxzum =0;
    dyzum =0;
    


	CView::OnLButtonDown(nFlags, point);

	Invalidate();
}

void CGRAFIK3View::OnLButtonDblClk(UINT nFlags, CPoint point) 
{

    m_Orgfx= point.x;
	m_Orgfy= point.y;

	
	CView::OnLButtonDblClk(nFlags, point);
}

void CGRAFIK3View::OnRButtonDown(UINT nFlags, CPoint point) 
{
	
point3.x =point.x;
point3.y =point.y;

dxzum =0;
dyzum =0;


	CView::OnRButtonDown(nFlags, point);
	Invalidate();
}






double CGRAFIK3ViewgetX()
{
	int x = 1;
	return x;
}
/*
BOOL CGRAFIK3View::OnEraseBkgnd(CDC* dc) 
{
	// TODO: Add your message handler code here and/or call default
	//Invalidate();

	return true;
}
*/





void CGRAFIK3View::OnTimer(UINT nIDEvent) 
{
	switch (swichT)
{
    case 0://vreme stani
	

	Invalidate();
		break;

	case 1://vreme napred
        
       
		t++;
	Invalidate();
		break;
		

	case 2://vreme unazad
        
       
		t--;
	Invalidate();
		break;
}






	CView::OnTimer(nIDEvent);
}



void CGRAFIK3View::OnInitialUpdate() 
{
		SetTimer(1, 1000, NULL);	
	CView::OnInitialUpdate();

	



  }


/*
              USB port (COMn)
                 |
                 |
                 |
                 V

                ||
               [  ]
             /------\
	         |      | 
	         |      |
	         |      |
	         |      |
		     \------/
		       |--|
			   |  |
               |  |-----------> to computer
               |--|
               |--|
			   |  |
               |  |-----------> from computer
               |--|
			   |--|
			   |  |
                ||------------> negative power      
                ()




*/


/*
	::CreateFile(
		::ReadFile(
		::WriteFile(
::fopen(
::fread(
::fwrite(

::GetCommState(
::SetCommState(

// Open the serial port.
hPort = CreateFile(
           lpszPortName,  // Pointer to the name of the port
           GENERIC_READ | GENERIC_WRITE,  // Access (read/write) mode
           0,              // Share mode
           NULL,          // Pointer to the security attribute
           OPEN_EXISTING, // How to open the serial port
           0,            // Port attributes
           NULL);         // Handle to port with attribute to copy

hCom = ::CreateFile(
                    _T("\\\\.\\COMn"),             // filename
                    GENERIC_READ | GENERIC_WRITE,  // desired access
                    0,                             // exclusive
                    NULL,                          // security irrelevant
                    OPEN_EXISTING,                 // it has to be there
                    FILE_FLAG_OVERLAPPED,          // open asynchronous
                    NULL); 






PortDCB.DCBlength = sizeof(DCB);

GetCommState(hPort, &PortDCB);            // Get the default port setting information.
// Change the DCB structure settings.
PortDCB.BaudRate = 9600;                  // Current baud rate
PortDCB.fBinary = TRUE;                   // Binary mode; no EOF check
PortDCB.fParity = TRUE;                   // Enable parity checking.
PortDCB.fOutxCtsFlow = FALSE;             // No CTS output flow control
PortDCB.fOutxDsrFlow = FALSE;             // No DSR output flow control
PortDCB.fDtrControl = DTR_CONTROL_ENABLE; // DTR flow control type
PortDCB.fDsrSensitivity = FALSE;          // DSR sensitivity
PortDCB.fTXContinueOnXoff = TRUE;         // XOFF continues Tx
PortDCB.fOutX = FALSE;                    // No XON/XOFF out flow control
PortDCB.fInX = FALSE;                     // No XON/XOFF in flow control
PortDCB.fErrorChar = FALSE;               // Disable error replacement.
PortDCB.fNull = FALSE;                    // Disable null stripping.
PortDCB.fRtsControl = RTS_CONTROL_ENABLE; // RTS flow control
PortDCB.fAbortOnError = FALSE;            // Do not abort reads/writes on error
PortDCB.ByteSize = 8;                     // Number of bits/bytes, 4-8
PortDCB.Parity = NOPARITY;                // 0-4=no,odd,even,mark,space
PortDCB.StopBits = ONESTOPBIT;            // 0,1,2 = 1, 1.5, 2
// Configure the port according to the specifications of the DCB structure.

typedef struct _DCB {
    DWORD DCBlength;      /* sizeof(DCB)                     
    DWORD BaudRate;       /* Baudrate at which running       
    DWORD fBinary: 1;     /* Binary Mode (skip EOF check)    
    DWORD fParity: 1;     /* Enable parity checking          
    DWORD fOutxCtsFlow:1; /* CTS handshaking on output       
    DWORD fOutxDsrFlow:1; /* DSR handshaking on output       
    DWORD fDtrControl:2;  /* DTR Flow control                
    DWORD fDsrSensitivity:1; /* DSR Sensitivity              
    DWORD fTXContinueOnXoff: 1; /* Continue TX when Xoff sent 
    DWORD fOutX: 1;       /* Enable output X-ON/X-OFF        
    DWORD fInX: 1;        /* Enable input X-ON/X-OFF         
    DWORD fErrorChar: 1;  /* Enable Err Replacement          
    DWORD fNull: 1;       /* Enable Null stripping           
    DWORD fRtsControl:2;  /* Rts Flow control                
    DWORD fAbortOnError:1; /* Abort all reads and writes on Error 
    DWORD fDummy2:17;     /* Reserved                        
    WORD wReserved;       /* Not currently used              
    WORD XonLim;          /* Transmit X-ON threshold         
    WORD XoffLim;         /* Transmit X-OFF threshold        
    BYTE ByteSize;        /* Number of bits/byte, 4-8        
    BYTE Parity;          /* 0-4=None,Odd,Even,Mark,Space    
    BYTE StopBits;        /* 0,1,2 = 1, 1.5, 2               
    char XonChar;         /* Tx and Rx X-ON character        
    char XoffChar;        /* Tx and Rx X-OFF character       
    char ErrorChar;       /* Error replacement char          
    char EofChar;         /* End of Input character          
    char EvtChar;         /* Received Event character        
    WORD wReserved1;      /* Fill for now.                   
} DCB, *LPDCB;


typedef struct _COMMTIMEOUTS {
    DWORD ReadIntervalTimeout;          /* Maximum time between read chars. 
    DWORD ReadTotalTimeoutMultiplier;   /* Multiplier of characters.        
    DWORD ReadTotalTimeoutConstant;     /* Constant in milliseconds.        
    DWORD WriteTotalTimeoutMultiplier;  /* Multiplier of characters.        
    DWORD WriteTotalTimeoutConstant;    /* Constant in milliseconds.        
} COMMTIMEOUTS,*LPCOMMTIMEOUTS;

FILE_FLAG_OVERLAPPED
COMMTIMEOUTS
*/
















void CGRAFIK3View::OnAutor() 
{
	// TODO: Add your command handler code here
	CCAutor dlg;
	if (dlg.DoModal () == IDOK)
	{

	

	   Invalidate ();
    } 
}
