// GRAFIK3View.h : interface of the CGRAFIK3View class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_GRAFIK3VIEW_H__44D2B11E_C747_4C17_8D68_BF233897DFB4__INCLUDED_)
#define AFX_GRAFIK3VIEW_H__44D2B11E_C747_4C17_8D68_BF233897DFB4__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


class CGRAFIK3View : public CView
{
protected: // create from serialization only
	CGRAFIK3View();
	DECLARE_DYNCREATE(CGRAFIK3View)

// Attributes
public:
	CGRAFIK3Doc* GetDocument();

// Operations
public:


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CGRAFIK3View)
	public:
	virtual void OnDraw(CDC* pDC);  // overridden to draw this view
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	virtual void OnInitialUpdate();
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CGRAFIK3View();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions

	//{{AFX_MSG(CGRAFIK3View)
	afx_msg void OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags);
	afx_msg void OnKeyUp(UINT nChar, UINT nRepCnt, UINT nFlags);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnLButtonDblClk(UINT nFlags, CPoint point);
	afx_msg void OnRButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnTimer(UINT nIDEvent);
	afx_msg void OnAutor();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
		protected:
	
	double m_Korak;
	int m_Zumx;
	int m_Zumy;
	int m_GMreza;

	int m_IDev;
	int m_Mreza;
	int m_DGrafik;


	int m_Orgfx;
	int m_Orgfy;

	int dx;
	int m_pp1;

	double m_viewx;
	double m_viewy; 
    
	CPoint point;
	CPoint point2;
	CPoint point3;
    CPoint previous;

	CRect Recto;
int m_paint;
	int nWidth;
    int nHeight;

	int xLogical;
    int yLogical;
	int xLogical2;
    int yLogical2;
	int  sxLogical;
	int  syLogical;

	int Px; //faktor promene smera
	int Py; //faktor promene smera
	int P;
	int dxzum; 
    int dyzum; 

	int dxs;
	int dys;

	int dxg;
	int dyg;
	double x,y,t;

int swichT; 

public:
 DCB dcb;  // DCB CONFIG NEOPHODAN JE ZA DOBIJANJE STATUSA PORTA DA LI JE OTVOREN ILI NE ... 
 HANDLE hSerial; // HANDLE ZA DEVICE U OVOM SLUCAJU ZA USB TO RS232 KONEKTOR
 COMMTIMEOUTS timeouts;
double CGRAFIK3ViewgetX();

};

#ifndef _DEBUG  // debug version in GRAFIK3View.cpp
inline CGRAFIK3Doc* CGRAFIK3View::GetDocument()
   { return (CGRAFIK3Doc*)m_pDocument; }
#endif

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_GRAFIK3VIEW_H__44D2B11E_C747_4C17_8D68_BF233897DFB4__INCLUDED_)
