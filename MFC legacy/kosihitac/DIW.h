#if !defined(AFX_DIW_H__87912107_719F_4FE1_A170_DBB45D43BA60__INCLUDED_)
#define AFX_DIW_H__87912107_719F_4FE1_A170_DBB45D43BA60__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DIW.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// DIW dialog

class DIW : public CDialog
{
// Construction
public:
	DIW(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(DIW)
	enum { IDD = IDD_DIW_DIALOG };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(DIW)
	public:
	virtual void OnFinalRelease();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(DIW)
		// NOTE: the ClassWizard will add member functions here
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
	// Generated OLE dispatch map functions
	//{{AFX_DISPATCH(DIW)
		// NOTE - the ClassWizard will add and remove member functions here.
	//}}AFX_DISPATCH
	DECLARE_DISPATCH_MAP()
	DECLARE_INTERFACE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DIW_H__87912107_719F_4FE1_A170_DBB45D43BA60__INCLUDED_)
