// kosihitac.h : main header file for the KOSIHITAC application
//

#if !defined(AFX_KOSIHITAC_H__55BA6EED_1D83_438B_8156_0DC5DCF810DF__INCLUDED_)
#define AFX_KOSIHITAC_H__55BA6EED_1D83_438B_8156_0DC5DCF810DF__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"		// main symbols

/////////////////////////////////////////////////////////////////////////////
// CKosihitacApp:
// See kosihitac.cpp for the implementation of this class
//

class CKosihitacApp : public CWinApp
{
public:
	CKosihitacApp();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CKosihitacApp)
	public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL

// Implementation

	//{{AFX_MSG(CKosihitacApp)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_KOSIHITAC_H__55BA6EED_1D83_438B_8156_0DC5DCF810DF__INCLUDED_)
