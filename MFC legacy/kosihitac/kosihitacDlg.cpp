// kosihitacDlg.cpp : implementation file
//

#include "stdafx.h"
#include "kosihitac.h"
#include "kosihitacDlg.h"
#include <math.h>
#define PI 3.14159265

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CKosihitacDlg dialog

CKosihitacDlg::CKosihitacDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CKosihitacDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CKosihitacDlg)
	VP = 0.0;
	ALFA = 0.0;
	DOMET = 0.0;
	KILO = 0.0;
	PRX = 0.0;
	PRY = 0.0;
	sin2a = 0.0;
	otpor = 0.0;
	DOMETO = 0.0;
	PRO = 0.0;
	prD = 0.0;
	DOMET2 = 0.0;
	KVISINA = 0.0;
	VISINA = 0.0;
	PR = 0.0;
	KDOMET = 0.0;
	VREME = 0.0;
	//}}AFX_DATA_INIT
	// Note that LoadIcon does not require a subsequent DestroyIcon in Win32
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CKosihitacDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CKosihitacDlg)
	DDX_Text(pDX, IDC_EDIT1, VP);
	DDX_Text(pDX, IDC_EDIT2, ALFA);
	DDX_Text(pDX, IDC_EDIT3, DOMET);
	DDX_Text(pDX, IDC_EDIT4, KILO);
	DDX_Text(pDX, IDC_EDIT5, PRX);
	DDX_Text(pDX, IDC_EDIT6, PRY);
	DDX_Text(pDX, IDC_EDIT7, sin2a);
	DDX_Text(pDX, IDC_EDIT8, prD);
	DDX_Text(pDX, IDC_EDIT9, DOMET2);
	DDX_Text(pDX, IDC_EDIT11, KVISINA);
	DDX_Text(pDX, IDC_EDIT10, VISINA);
	DDX_Text(pDX, IDC_EDIT12, PR);
	DDX_Text(pDX, IDC_EDIT13, KDOMET);
	DDX_Text(pDX, IDC_EDIT14, VREME);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CKosihitacDlg, CDialog)
	//{{AFX_MSG_MAP(CKosihitacDlg)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_BUTTON1, OnButton1)
	ON_BN_CLICKED(IDC_BUTTON2, OnButton2)
	ON_BN_CLICKED(IDC_BUTTON3, OnButton3)
	ON_BN_CLICKED(IDC_BUTTON4, OnButton4)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CKosihitacDlg message handlers

BOOL CKosihitacDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon
	
	// TODO: Add extra initialization here
    
	
	return TRUE;  // return TRUE  unless you set the focus to a control
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CKosihitacDlg::OnPaint() 
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, (WPARAM) dc.GetSafeHdc(), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// The system calls this to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CKosihitacDlg::OnQueryDragIcon()
{
	return (HCURSOR) m_hIcon;
}

void CKosihitacDlg::OnButton1() 
{
	// TODO: Add your control notification handler code here
UpdateData (true);
double res;
res = sin (2*ALFA*PI/180);  // operator... sin (2alfa)
DOMET = (VP*VP*res)/9.81;   //p-rezultat... domet d=(v^2*sin(2alfa))/g
KILO = DOMET/1000;          //p-rezultat... domet u km d/1000
double resX, resY;
resX = sin (ALFA*PI/180);   
resY = cos (ALFA*PI/180);
PRX = resY*VP;              //p-rezultat... projekcija brzine v na x osu Vx  
PRY = resX*VP;              //p-rezultat... projekcija brzine v na y osu Vy 
sin2a = res;


UpdateData (false);	
}

void CKosihitacDlg::OnButton2() 
{
UpdateData (true);
double prD1;
prD1 = DOMET/100;
DOMET2 = DOMET - prD1*prD;
KDOMET = DOMET2/1000;
UpdateData (false);	
}

void CKosihitacDlg::OnButton3() 
{
UpdateData (true);                                         // klasa koja se prikazuje nesme biti pod double
double resH;
resH = sin (ALFA*PI/180);
VISINA = ((VP*VP*resH*resH)/(2*9.81));
KVISINA = VISINA/1000;
PR = resH;
UpdateData (false);	
}

void CKosihitacDlg::OnButton4() 
{
UpdateData (true); 
double resH;
resH = sin (ALFA*PI/180);
VREME = (2*VP*resH)/(9.81);
UpdateData (false);	
}
