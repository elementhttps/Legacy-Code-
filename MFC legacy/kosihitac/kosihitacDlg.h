// kosihitacDlg.h : header file
//

#if !defined(AFX_KOSIHITACDLG_H__C1716A58_6579_42F8_A638_B191BA4B5ED2__INCLUDED_)
#define AFX_KOSIHITACDLG_H__C1716A58_6579_42F8_A638_B191BA4B5ED2__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

/////////////////////////////////////////////////////////////////////////////
// CKosihitacDlg dialog

class CKosihitacDlg : public CDialog
{
// Construction
public:
	CKosihitacDlg(CWnd* pParent = NULL);	// standard constructor

// Dialog Data
	//{{AFX_DATA(CKosihitacDlg)
	enum { IDD = IDD_KOSIHITAC_DIALOG };
	CSliderCtrl	REGotpora;
	double	VP;
	double	ALFA;
	double	DOMET;
	double	KILO;
	double	PRX;
	double	PRY;
	double	sin2a;
	int		PRotp;
	double	otpor;
	double	DOMETO;
	double	PRO;
	double	prD;
	double	DOMET2;
	double	KVISINA;
	double	VISINA;
	double	PR;
	double	KDOMET;
	double	VREME;
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CKosihitacDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	//{{AFX_MSG(CKosihitacDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	afx_msg void OnButton1();
	afx_msg void OnOutofmemorySlider1(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
	afx_msg void OnButton2();
	afx_msg void OnChangeEdit8();
	afx_msg void OnChangeEdit7();
	afx_msg void OnButton3();
	afx_msg void OnButton4();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_KOSIHITACDLG_H__C1716A58_6579_42F8_A638_B191BA4B5ED2__INCLUDED_)
