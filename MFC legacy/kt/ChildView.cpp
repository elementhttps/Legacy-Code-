// ChildView.cpp : implementation of the CChildView class
//

#include "stdafx.h"

//Osnovne funkcije
#include "KreatorTrajektorija.h"
#include "ChildView.h"
#include "Math.h"

//Dijalog
#include "ParametriDlg.h"
#include "Autor.h"
#include "Uputstvo.h"
#include "Mreza.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CChildView

CChildView::CChildView()
{
	m_Brzina_f1 = 0;
	m_Brzina_f2 = 0;
	m_Brzina_f3= 0;
	m_Ugao_f1= 0;
    m_Ugao_f2= 0;
	m_Ugao_f3= 0;
	m_Mreza = 0;



}

CChildView::~CChildView()
{
}


BEGIN_MESSAGE_MAP(CChildView,CWnd )
	//{{AFX_MSG_MAP(CChildView)
	ON_WM_PAINT()
	ON_COMMAND(IDM_DLG_OPCIJE, OnDlgOpcije)
	ON_COMMAND(IDM_AUTOR, OnAutor)
	ON_COMMAND(ID_INFO_UPUTSTVA, OnInfoUputstva)
	ON_COMMAND(IDM_MREZA, OnMreza)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


/////////////////////////////////////////////////////////////////////////////
// CChildView message handlers

BOOL CChildView::PreCreateWindow(CREATESTRUCT& cs) 
{
	if (!CWnd::PreCreateWindow(cs))
		return FALSE;

	cs.dwExStyle |= WS_EX_CLIENTEDGE;
	cs.style &= ~WS_BORDER;
	cs.lpszClass = AfxRegisterWndClass(CS_HREDRAW|CS_VREDRAW|CS_DBLCLKS, 
		::LoadCursor(NULL, IDC_ARROW), HBRUSH(COLOR_WINDOW+1), NULL);

	return TRUE;
}

void CChildView::OnPaint() 
{
	CPaintDC dc(this); // device context for painting
	
    // Initialize the device context.
    //
    dc.SetMapMode(MM_ISOTROPIC);
	dc.SetViewportOrg(60, 420);
	dc.SetWindowExt(10000,10000);
	dc.SetViewportExt(100,-100);




	
	

	
//Mreza
	
	CPen PenBlack4(PS_SOLID, 1, RGB(0, 0, 0));
	dc.SelectObject(PenBlack4);
	
	switch (m_Mreza)
	
	{
	case 0:
    
	

	break;

	case 1:
		for(int kooxx = 0; kooxx < 1000000; kooxx += 5000)
    {
        dc.MoveTo(kooxx, 0);
        dc.LineTo(kooxx, 50000);
    }
	

	for(int kooyy = 0; kooyy < 1000000; kooyy += 5000)
    {
        dc.MoveTo(0, kooyy);
        dc.LineTo(150000, kooyy);
    }
		break;
	}






	


// OSE
	dc.MoveTo(-1000,     0);
	dc.LineTo( 20000000,     0);
	dc.MoveTo(   0, -1000);
	dc.LineTo(   0,  20000000);


//KONSTANTE

double PI = 3.14159;
double g = 9.81;

//PROMENE BRZINE I UGLA
int alfa1 = m_Ugao_f1;
int alfa2 = m_Ugao_f2;
int alfa3 = m_Ugao_f3;

int v1 = m_Brzina_f1;
int v2 = m_Brzina_f2;
int v3 = m_Brzina_f3;
	

CString string;

dc.SetTextColor(RGB(255, 0, 0));
string.Format (("-Funkcija f1 Ugao %d, Brzina %d m/s "), alfa1, v1);

dc.TextOut (60000,35000 , string);

dc.SetTextColor(RGB(0, 0, 255));
string.Format (("-Funkcija f1 Ugao %d, Brzina %d m/s "), alfa2, v2);
dc.TextOut (60000,33000 , string);

dc.SetTextColor(RGB(0, 255, 0));
string.Format (("-Funkcija f1 Ugao %d, Brzina %d m/s "), alfa3, v3);
dc.TextOut (60000,31000 , string);




//FUNKCIJE



//KORAK I MAKSIMUM


double kor = 1;

double max = 150000; //oko milion


//Funkcija f1 CRVENA

	for(double i = 1; i < max; i +=kor)
	{ 
        
		double c1 = 1  ;


	    double f = (i*tan((PI*alfa1)/180))-((g*i*i)/(2*v1*v1*cos((PI*alfa1)/180)*cos((PI*alfa1)/180)));
	    dc.SetPixel(i, f, RGB(255, 0, 0));
	}

//Funkcija f2 PLAVA

	for(double i2 = 1; i2 < max; i2 +=kor)
	{
		int c2 = 0;
	    double f2 = (i2*tan((PI*alfa2)/180))-((g*i2*i2)/(2*v2*v2*cos((PI*alfa2)/180)*cos((PI*alfa2)/180)));
			//tan(i2)*i2;
	    dc.SetPixel(i2, f2, RGB(0, 0, 255));

	}

//Funkcija f3 ZELENA

    for(double i3 = 1; i3 < max; i3 +=kor)
	{
		int c3 = 0;
	    double f3 = (i3*tan((PI*alfa3)/180))-((g*i3*i3)/(2*v3*v3*cos((PI*alfa3)/180)*cos((PI*alfa3)/180)));;
	    dc.SetPixel(i3, f3, RGB(0, 255, 0));
	}

    CBrush brush (RGB (255, 255, 255));
    dc.SelectObject(brush);
	dc.Rectangle(-10000,-1,200000,-10000000);

//POZICIJA OZNAKA
	
	
	//Koordinate 
	dc.SetTextColor(RGB(100, 100, 0));

    dc.TextOut(56000, 2000, 'X');
	dc.TextOut(500, 35000, 'Y');
   
    //Lenjiri po x i y osi
    CPen PenBlack3(PS_SOLID, 200, RGB(0, 0, 0));
	dc.SelectObject(PenBlack3);
    
	for(int koox = 0; koox < 1000000; koox += 1000)
    {
        dc.MoveTo(koox, 0);
        dc.LineTo(koox, -1000);
    }

	for(int kooy = 0; kooy < 1000000; kooy += 1000)
    {
        dc.MoveTo(0, kooy);
        dc.LineTo(-1000, kooy);
    }



	dc.SelectObject(PenBlack3);
    for(int koox2 = 0; koox2 < 1000000; koox2 += 5000)
    {
        dc.MoveTo(koox2, 0);
        dc.LineTo(koox2, -2000);
    }
	for(int kooy2 = 0; kooy2 < 1000000; kooy2 += 5000)
    {
        dc.MoveTo(0, kooy2);
        dc.LineTo(-2000, kooy2);
    }

	for(int koox3 = 0; koox3 < 1000000; koox3 += 10000)
    {
        dc.MoveTo(koox3, 0);
        dc.LineTo(koox3, -3000);
    }
	for(int kooy3 = 0; kooy3 < 1000000; kooy3 += 10000)
    {
        dc.MoveTo(0, kooy3);
        dc.LineTo(-3000, kooy3);
    }

    dc.SetTextColor(RGB(0, 0, 0));
	for (int kt1= 0; kt1<1000000; kt1+=10000) 
	{
        
        CString string;
        string.Format (("%d km"), (kt1/1000 ) );
        dc.TextOut (kt1, -3000, string);
    }

	dc.SetTextColor(RGB(0, 0, 0));
	for (int kt2= 1000; kt2<1000000; kt2+=10000) 
	{
        
        CString string;
        string.Format (("%dkm"), (((kt2-1000)/1000) ) );
        dc.TextOut (-6000, kt2, string);
    }



	
/*
	
	// Do not call CWnd::OnPaint() for painting messages

	// TODO: add draw code for native data here
*/
}


void CChildView::OnDlgOpcije() 
{
	// TODO: Add your command handler code here
	CParametriDlg dlg;

    dlg.m_Brzina_f1 =m_Brzina_f1;
	dlg.m_Brzina_f2 =m_Brzina_f2;
	dlg.m_Brzina_f3 =m_Brzina_f3;

	dlg.m_Ugao_f1 =m_Ugao_f1;
	dlg.m_Ugao_f2 =m_Ugao_f2;
	dlg.m_Ugao_f3 =m_Ugao_f3;




    if (dlg.DoModal () == IDOK)
	{
        m_Brzina_f1 = dlg.m_Brzina_f1;
		m_Brzina_f2 = dlg.m_Brzina_f2;
		m_Brzina_f3 = dlg.m_Brzina_f3;

		m_Ugao_f1 = dlg.m_Ugao_f1;
		m_Ugao_f2 = dlg.m_Ugao_f2;
		m_Ugao_f3 = dlg.m_Ugao_f3;
             
        Invalidate ();
    }    

	
}

void CChildView::OnAutor() 
{
	CAutor dlg;
	dlg.DoModal();
	
}

void CChildView::OnInfoUputstva() 
{
	CUputstvo dlg;
	dlg.DoModal();
	
}

void CChildView::OnMreza() 
{

	CMreza dlg;
	dlg.m_Mreza = m_Mreza;
	
	if (dlg.DoModal () == IDOK)
	{
		m_Mreza= dlg.m_Mreza;
		Invalidate ();
	}
	
}
