// ChildView.h : interface of the CChildView class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_CHILDVIEW_H__95820FA6_F0E7_45FF_9E8B_8241BCE4C079__INCLUDED_)
#define AFX_CHILDVIEW_H__95820FA6_F0E7_45FF_9E8B_8241BCE4C079__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

/////////////////////////////////////////////////////////////////////////////
// CChildView window

class CChildView : public CWnd
{
// Construction
public:
	CChildView();

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CChildView)
	protected:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CChildView();

	// Generated message map functions
protected:
	int m_Mreza;
	int m_Brzina_f3;
	int m_Ugao_f3;

	int m_Brzina_f2;
	int m_Ugao_f2;

	int m_Brzina_f1;
	int m_Ugao_f1;

	//{{AFX_MSG(CChildView)
	afx_msg void OnPaint();
	afx_msg void OnDlgOpcije();
	afx_msg void OnAutor();
	afx_msg void OnInfoUputstva();
	afx_msg void OnMreza();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CHILDVIEW_H__95820FA6_F0E7_45FF_9E8B_8241BCE4C079__INCLUDED_)
