; CLW file contains information for the MFC ClassWizard

[General Info]
Version=1
LastClass=CMreza
LastTemplate=CDialog
NewFileInclude1=#include "stdafx.h"
NewFileInclude2=#include "KreatorTrajektorija.h"
LastPage=0

ClassCount=8
Class1=CKreatorTrajektorijaApp
Class3=CMainFrame
Class4=CAboutDlg

ResourceCount=6
Resource1=IDD_AUTOR
Resource2=IDD_UPUTSTVA
Class2=CChildView
Class5=CParametriDlg
Resource3=IDR_MAINFRAME
Class6=CAutor
Resource4=IDD_ABOUTBOX
Class7=CUputstvo
Resource5=IDD_PARAMETRI
Class8=CMreza
Resource6=IDD_MREZA

[CLS:CKreatorTrajektorijaApp]
Type=0
HeaderFile=KreatorTrajektorija.h
ImplementationFile=KreatorTrajektorija.cpp
Filter=N
LastObject=CKreatorTrajektorijaApp

[CLS:CChildView]
Type=0
HeaderFile=ChildView.h
ImplementationFile=ChildView.cpp
Filter=N
BaseClass=CWnd 
VirtualFilter=WC
LastObject=IDM_MREZA

[CLS:CMainFrame]
Type=0
HeaderFile=MainFrm.h
ImplementationFile=MainFrm.cpp
Filter=T
LastObject=ID_INFO_UPUTSTVA




[CLS:CAboutDlg]
Type=0
HeaderFile=KreatorTrajektorija.cpp
ImplementationFile=KreatorTrajektorija.cpp
Filter=D
LastObject=CAboutDlg

[DLG:IDD_ABOUTBOX]
Type=1
Class=CAboutDlg
ControlCount=4
Control1=IDC_STATIC,static,1342177283
Control2=IDC_STATIC,static,1342308480
Control3=IDC_STATIC,static,1342308352
Control4=IDOK,button,1342373889

[MNU:IDR_MAINFRAME]
Type=1
Class=CMainFrame
Command1=ID_APP_EXIT
Command2=IDM_DLG_OPCIJE
Command3=IDM_MREZA
Command4=ID_APP_ABOUT
Command5=IDM_AUTOR
Command6=ID_INFO_UPUTSTVA
CommandCount=6

[ACL:IDR_MAINFRAME]
Type=1
Class=CMainFrame
Command1=ID_EDIT_COPY
Command2=ID_EDIT_PASTE
Command3=ID_EDIT_UNDO
Command4=ID_EDIT_CUT
Command5=ID_NEXT_PANE
Command6=ID_PREV_PANE
Command7=ID_EDIT_COPY
Command8=ID_EDIT_PASTE
Command9=ID_EDIT_CUT
Command10=ID_EDIT_UNDO
CommandCount=10

[DLG:IDD_PARAMETRI]
Type=1
Class=CParametriDlg
ControlCount=18
Control1=IDOK,button,1342242817
Control2=IDCANCEL,button,1342242816
Control3=IDC_UGAO_F1,edit,1350631552
Control4=IDC_BRZINA_F1,edit,1350631552
Control5=IDC_UGAO_F2,edit,1350631552
Control6=IDC_BRZINA_F2,edit,1350631552
Control7=IDC_STATIC,static,1342308352
Control8=IDC_STATIC,static,1342308352
Control9=IDC_UGAO_F3,edit,1350631552
Control10=IDC_BRZINA_F3,edit,1350631552
Control11=IDC_STATIC,static,1342308352
Control12=IDC_STATIC,static,1342308352
Control13=IDC_STATIC,static,1342308352
Control14=IDC_STATIC,static,1342308352
Control15=IDC_STATIC,button,1342177287
Control16=IDC_STATIC,button,1342177287
Control17=IDC_STATIC,button,1342177287
Control18=IDC_STATIC,button,1342178055

[CLS:CParametriDlg]
Type=0
HeaderFile=ParametriDlg.h
ImplementationFile=ParametriDlg.cpp
BaseClass=CDialog
Filter=D
LastObject=CParametriDlg
VirtualFilter=dWC

[DLG:IDD_AUTOR]
Type=1
Class=CAutor
ControlCount=6
Control1=IDOK,button,1342242817
Control2=IDC_STATIC,static,1342308864
Control3=IDC_STATIC,static,1342308864
Control4=IDC_STATIC,static,1342308352
Control5=IDC_STATIC,static,1342308352
Control6=IDC_STATIC,static,1342177294

[CLS:CAutor]
Type=0
HeaderFile=Autor.h
ImplementationFile=Autor.cpp
BaseClass=CDialog
Filter=D
LastObject=ID_NEXT_PANE

[DLG:IDD_UPUTSTVA]
Type=1
Class=CUputstvo
ControlCount=2
Control1=IDOK,button,1342242817
Control2=IDC_STATIC,static,1342308352

[CLS:CUputstvo]
Type=0
HeaderFile=Uputstvo.h
ImplementationFile=Uputstvo.cpp
BaseClass=CDialog
Filter=D
LastObject=CUputstvo

[DLG:IDD_MREZA]
Type=1
Class=CMreza
ControlCount=5
Control1=IDOK,button,1342242817
Control2=IDCANCEL,button,1342242816
Control3=IDC_MREZA_DA,button,1342308361
Control4=IDC_MREZA_NE,button,1342177289
Control5=IDC_STATIC,button,1342177287

[CLS:CMreza]
Type=0
HeaderFile=Mreza.h
ImplementationFile=Mreza.cpp
BaseClass=CDialog
Filter=D
LastObject=CMreza
VirtualFilter=dWC

