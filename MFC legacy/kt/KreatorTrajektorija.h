// KreatorTrajektorija.h : main header file for the KREATORTRAJEKTORIJA application
//

#if !defined(AFX_KREATORTRAJEKTORIJA_H__FFD18B9B_9E39_47AF_AA10_14CE9355A905__INCLUDED_)
#define AFX_KREATORTRAJEKTORIJA_H__FFD18B9B_9E39_47AF_AA10_14CE9355A905__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"       // main symbols

/////////////////////////////////////////////////////////////////////////////
// CKreatorTrajektorijaApp:
// See KreatorTrajektorija.cpp for the implementation of this class
//

class CKreatorTrajektorijaApp : public CWinApp
{
public:
	CKreatorTrajektorijaApp();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CKreatorTrajektorijaApp)
	public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL

// Implementation

public:
	//{{AFX_MSG(CKreatorTrajektorijaApp)
	afx_msg void OnAppAbout();
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_KREATORTRAJEKTORIJA_H__FFD18B9B_9E39_47AF_AA10_14CE9355A905__INCLUDED_)
