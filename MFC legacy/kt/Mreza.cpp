// Mreza.cpp : implementation file
//

#include "stdafx.h"
#include "KreatorTrajektorija.h"
#include "Mreza.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CMreza dialog


CMreza::CMreza(CWnd* pParent /*=NULL*/)
	: CDialog(CMreza::IDD, pParent)
{
	//{{AFX_DATA_INIT(CMreza)
	m_Mreza = -1;
	//}}AFX_DATA_INIT
}


void CMreza::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CMreza)
	DDX_Radio(pDX, IDC_MREZA_DA, m_Mreza);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CMreza, CDialog)
	//{{AFX_MSG_MAP(CMreza)
		// NOTE: the ClassWizard will add message map macros here
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CMreza message handlers
