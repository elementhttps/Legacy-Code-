//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by KreatorTrajektorija.rc
//
#define IDD_ABOUTBOX                    100
#define IDR_MAINFRAME                   128
#define IDR_KREATOTYPE                  129
#define IDD_PARAMETRI                   130
#define IDD_AUTOR                       131
#define IDB_POBEDA                      132
#define IDD_UPUTSTVA                    133
#define IDD_MREZA                       134
#define IDC_UGAO_F1                     1000
#define IDC_BRZINA_F1                   1001
#define IDC_UGAO_F2                     1002
#define IDC_MREZA_DA                    1002
#define IDC_BRZINA_F2                   1003
#define IDC_MREZA_NE                    1003
#define IDC_UGAO_F3                     1004
#define IDC_BRZINA_F3                   1005
#define IDM_DLG_OPCIJE                  32771
#define IDM_AUTOR                       32772
#define ID_INFO_UPUTSTVA                32773
#define IDM_MREZA                       32774

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        135
#define _APS_NEXT_COMMAND_VALUE         32775
#define _APS_NEXT_CONTROL_VALUE         1003
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
