// OptionsDialog.cpp : implementation file
//

#include "stdafx.h"
#include "mfc_test10.h"
#include "OptionsDialog.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// COptionsDialog dialog


COptionsDialog::COptionsDialog(CWnd* pParent /*=NULL*/)
	: CDialog(COptionsDialog::IDD, pParent)
{
	//{{AFX_DATA_INIT(COptionsDialog)
	m_nWidth = 0;
	m_Height = 0;
	//}}AFX_DATA_INIT
}


void COptionsDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(COptionsDialog)
	DDX_Text(pDX, IDC_EDIT1, m_nWidth);
	DDX_Text(pDX, IDC_EDIT2, m_Height);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(COptionsDialog, CDialog)
	//{{AFX_MSG_MAP(COptionsDialog)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// COptionsDialog message handlers

void COptionsDialog::OnOK() 
{
	
	UpdateData(TRUE);//DODATO *********************************************
	

}

void COptionsDialog::OnCancel() 
{
	
	DestroyWindow ();//DODATO *********************************************

}

BOOL COptionsDialog::Create()//DODATO *********************************************
{
	return CDialog::Create(COptionsDialog::IDD);//DODATO *********************************************
}
