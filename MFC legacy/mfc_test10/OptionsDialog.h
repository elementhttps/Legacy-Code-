#if !defined(AFX_OPTIONSDIALOG_H__1CE3066D_02F6_4DB8_97E3_A1B5C0555220__INCLUDED_)
#define AFX_OPTIONSDIALOG_H__1CE3066D_02F6_4DB8_97E3_A1B5C0555220__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// OptionsDialog.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// COptionsDialog dialog

class COptionsDialog : public CDialog
{
// Construction
private:         //  dodato view  DODATO *********************************************
	CView* m_pView;
public:
	COptionsDialog(CWnd* pParent = NULL);   // standard constructor
	COptionsDialog(CView* m_pDlg);//iz view DODATO *********************************************    
    BOOL Create();//kreiraj DODATO *********************************************

// Dialog Data
	//{{AFX_DATA(COptionsDialog)
	enum { IDD = IDD_OPTIONS };
	int		m_nWidth;
	int		m_Height;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(COptionsDialog)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(COptionsDialog)
	virtual void OnOK();
	virtual void OnCancel();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_OPTIONSDIALOG_H__1CE3066D_02F6_4DB8_97E3_A1B5C0555220__INCLUDED_)
