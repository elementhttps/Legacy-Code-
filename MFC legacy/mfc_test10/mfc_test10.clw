; CLW file contains information for the MFC ClassWizard

[General Info]
Version=1
LastClass=COptionsDialog
LastTemplate=CDialog
NewFileInclude1=#include "stdafx.h"
NewFileInclude2=#include "mfc_test10.h"
LastPage=0

ClassCount=6
Class1=CMfc_test10App
Class2=CMfc_test10Doc
Class3=CMfc_test10View
Class4=CMainFrame

ResourceCount=3
Resource1=IDR_MAINFRAME
Resource2=IDD_ABOUTBOX
Class5=CAboutDlg
Class6=COptionsDialog
Resource3=IDD_OPTIONS

[CLS:CMfc_test10App]
Type=0
HeaderFile=mfc_test10.h
ImplementationFile=mfc_test10.cpp
Filter=N

[CLS:CMfc_test10Doc]
Type=0
HeaderFile=mfc_test10Doc.h
ImplementationFile=mfc_test10Doc.cpp
Filter=N
LastObject=CMfc_test10Doc

[CLS:CMfc_test10View]
Type=0
HeaderFile=mfc_test10View.h
ImplementationFile=mfc_test10View.cpp
Filter=C
BaseClass=CView
VirtualFilter=VWC
LastObject=ID_FILE_OPTIONS


[CLS:CMainFrame]
Type=0
HeaderFile=MainFrm.h
ImplementationFile=MainFrm.cpp
Filter=T
LastObject=CMainFrame




[CLS:CAboutDlg]
Type=0
HeaderFile=mfc_test10.cpp
ImplementationFile=mfc_test10.cpp
Filter=D
LastObject=CAboutDlg

[DLG:IDD_ABOUTBOX]
Type=1
Class=CAboutDlg
ControlCount=4
Control1=IDC_STATIC,static,1342177283
Control2=IDC_STATIC,static,1342308480
Control3=IDC_STATIC,static,1342308352
Control4=IDOK,button,1342373889

[MNU:IDR_MAINFRAME]
Type=1
Class=CMainFrame
Command1=ID_FILE_NEW
Command2=ID_FILE_OPEN
Command3=ID_FILE_SAVE
Command4=ID_FILE_SAVE_AS
Command5=ID_FILE_MRU_FILE1
Command6=ID_APP_EXIT
Command7=ID_FILE_OPTIONS
Command8=ID_EDIT_UNDO
Command9=ID_EDIT_CUT
Command10=ID_EDIT_COPY
Command11=ID_EDIT_PASTE
Command12=ID_APP_ABOUT
CommandCount=12

[ACL:IDR_MAINFRAME]
Type=1
Class=CMainFrame
Command1=ID_FILE_NEW
Command2=ID_FILE_OPEN
Command3=ID_FILE_SAVE
Command4=ID_EDIT_UNDO
Command5=ID_EDIT_CUT
Command6=ID_EDIT_COPY
Command7=ID_EDIT_PASTE
Command8=ID_EDIT_UNDO
Command9=ID_EDIT_CUT
Command10=ID_EDIT_COPY
Command11=ID_EDIT_PASTE
Command12=ID_NEXT_PANE
Command13=ID_PREV_PANE
CommandCount=13

[DLG:IDD_OPTIONS]
Type=1
Class=COptionsDialog
ControlCount=5
Control1=IDOK,button,1342242817
Control2=IDCANCEL,button,1342242816
Control3=IDC_EDIT1,edit,1350631552
Control4=IDC_RESET,button,1342242816
Control5=IDC_EDIT2,edit,1350631552

[CLS:COptionsDialog]
Type=0
HeaderFile=OptionsDialog.h
ImplementationFile=OptionsDialog.cpp
BaseClass=CDialog
Filter=D
LastObject=IDCANCEL
VirtualFilter=dWC

