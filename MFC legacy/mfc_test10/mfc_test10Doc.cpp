// mfc_test10Doc.cpp : implementation of the CMfc_test10Doc class
//

#include "stdafx.h"
#include "mfc_test10.h"

#include "mfc_test10Doc.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CMfc_test10Doc

IMPLEMENT_DYNCREATE(CMfc_test10Doc, CDocument)

BEGIN_MESSAGE_MAP(CMfc_test10Doc, CDocument)
	//{{AFX_MSG_MAP(CMfc_test10Doc)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CMfc_test10Doc construction/destruction

CMfc_test10Doc::CMfc_test10Doc()
{
	// TODO: add one-time construction code here

}

CMfc_test10Doc::~CMfc_test10Doc()
{
}

BOOL CMfc_test10Doc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;

	// TODO: add reinitialization code here
	// (SDI documents will reuse this document)

	return TRUE;
}



/////////////////////////////////////////////////////////////////////////////
// CMfc_test10Doc serialization

void CMfc_test10Doc::Serialize(CArchive& ar)
{
	if (ar.IsStoring())
	{
		// TODO: add storing code here
	}
	else
	{
		// TODO: add loading code here
	}
}

/////////////////////////////////////////////////////////////////////////////
// CMfc_test10Doc diagnostics

#ifdef _DEBUG
void CMfc_test10Doc::AssertValid() const
{
	CDocument::AssertValid();
}

void CMfc_test10Doc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CMfc_test10Doc commands
