// mfc_test10Doc.h : interface of the CMfc_test10Doc class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_MFC_TEST10DOC_H__E2ADDE90_909A_4B50_AA6E_195C12E1C86B__INCLUDED_)
#define AFX_MFC_TEST10DOC_H__E2ADDE90_909A_4B50_AA6E_195C12E1C86B__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


class CMfc_test10Doc : public CDocument
{
protected: // create from serialization only
	CMfc_test10Doc();
	DECLARE_DYNCREATE(CMfc_test10Doc)

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMfc_test10Doc)
	public:
	virtual BOOL OnNewDocument();
	virtual void Serialize(CArchive& ar);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CMfc_test10Doc();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	//{{AFX_MSG(CMfc_test10Doc)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_MFC_TEST10DOC_H__E2ADDE90_909A_4B50_AA6E_195C12E1C86B__INCLUDED_)
