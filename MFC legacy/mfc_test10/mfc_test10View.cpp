// mfc_test10View.cpp : implementation of the CMfc_test10View class
//

#include "stdafx.h"
#include "mfc_test10.h"

#include "mfc_test10Doc.h"
#include "mfc_test10View.h"

//dijalozi
#include "OptionsDialog.h"//DODATO *********************************************


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CMfc_test10View

IMPLEMENT_DYNCREATE(CMfc_test10View, CView)

BEGIN_MESSAGE_MAP(CMfc_test10View, CView)
	//{{AFX_MSG_MAP(CMfc_test10View)
	ON_COMMAND(ID_FILE_OPTIONS, OnFileOptions)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CMfc_test10View construction/destruction

CMfc_test10View::CMfc_test10View()
{
	m_nWidth = 4;
    m_Height = 2;
    m_pDlg = NULL;//naziv dijaloga DODATO *********************************************


}

CMfc_test10View::~CMfc_test10View()
{
}

BOOL CMfc_test10View::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	return CView::PreCreateWindow(cs);
}

/////////////////////////////////////////////////////////////////////////////
// CMfc_test10View drawing

void CMfc_test10View::OnDraw(CDC* pDC)
{
	CMfc_test10Doc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);
	// TODO: add draw code for native data here
}

/////////////////////////////////////////////////////////////////////////////
// CMfc_test10View diagnostics

#ifdef _DEBUG
void CMfc_test10View::AssertValid() const
{
	CView::AssertValid();
}

void CMfc_test10View::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}

CMfc_test10Doc* CMfc_test10View::GetDocument() // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CMfc_test10Doc)));
	return (CMfc_test10Doc*)m_pDocument;
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CMfc_test10View message handlers

void CMfc_test10View::OnFileOptions() 
{

       m_pDlg = new COptionsDialog;// novi dijalog /neophodna komanda za unistenje DODATO *********************************************

	   m_pDlg->m_nWidth = m_nWidth;
       m_pDlg->m_Height = m_Height;
	   m_pDlg->Create();
       m_pDlg->ShowWindow (SW_SHOW);
    
}

	

