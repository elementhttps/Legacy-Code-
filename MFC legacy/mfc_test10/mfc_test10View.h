// mfc_test10View.h : interface of the CMfc_test10View class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_MFC_TEST10VIEW_H__78F23207_1035_46EB_99B9_697E1FCDF2B5__INCLUDED_)
#define AFX_MFC_TEST10VIEW_H__78F23207_1035_46EB_99B9_697E1FCDF2B5__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


class COptionsDialog;//DODATO *********************************************

class CMfc_test10View : public CView
{
protected: // create from serialization only
	CMfc_test10View();
	DECLARE_DYNCREATE(CMfc_test10View)

// Attributes
public:
	CMfc_test10Doc* GetDocument();
	COptionsDialog* m_pDlg;//DODATO *********************************************

// Operations
public:
	

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMfc_test10View)
	public:
	virtual void OnDraw(CDC* pDC);  // overridden to draw this view
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	protected:
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CMfc_test10View();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:
	

// Generated message map functions
protected:
    
    int m_Height;
    int m_nWidth;

	//{{AFX_MSG(CMfc_test10View)
	afx_msg void OnFileOptions();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

#ifndef _DEBUG  // debug version in mfc_test10View.cpp
inline CMfc_test10Doc* CMfc_test10View::GetDocument()
   { return (CMfc_test10Doc*)m_pDocument; }
#endif

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_MFC_TEST10VIEW_H__78F23207_1035_46EB_99B9_697E1FCDF2B5__INCLUDED_)
