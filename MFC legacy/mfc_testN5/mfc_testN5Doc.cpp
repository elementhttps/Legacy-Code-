// mfc_testN5Doc.cpp : implementation of the CMfc_testN5Doc class
//

#include "stdafx.h"
#include "mfc_testN5.h"

#include "mfc_testN5Doc.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CMfc_testN5Doc

IMPLEMENT_DYNCREATE(CMfc_testN5Doc, CDocument)

BEGIN_MESSAGE_MAP(CMfc_testN5Doc, CDocument)
	//{{AFX_MSG_MAP(CMfc_testN5Doc)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG_MAP
	ON_COMMAND(ID_FILE_SEND_MAIL, OnFileSendMail)
	ON_UPDATE_COMMAND_UI(ID_FILE_SEND_MAIL, OnUpdateFileSendMail)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CMfc_testN5Doc construction/destruction

CMfc_testN5Doc::CMfc_testN5Doc()
{
	// TODO: add one-time construction code here

}

CMfc_testN5Doc::~CMfc_testN5Doc()
{
}

BOOL CMfc_testN5Doc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;

	// TODO: add reinitialization code here
	// (SDI documents will reuse this document)

	return TRUE;
}



/////////////////////////////////////////////////////////////////////////////
// CMfc_testN5Doc serialization

void CMfc_testN5Doc::Serialize(CArchive& ar)
{
	if (ar.IsStoring())
	{
		// TODO: add storing code here
	}
	else
	{
		// TODO: add loading code here
	}
}

/////////////////////////////////////////////////////////////////////////////
// CMfc_testN5Doc diagnostics

#ifdef _DEBUG
void CMfc_testN5Doc::AssertValid() const
{
	CDocument::AssertValid();
}

void CMfc_testN5Doc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CMfc_testN5Doc commands
