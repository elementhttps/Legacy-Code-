#if !defined(AFX_AUTOR_H__0B5ED30B_EE3A_4E47_8789_5EA276FF9CB7__INCLUDED_)
#define AFX_AUTOR_H__0B5ED30B_EE3A_4E47_8789_5EA276FF9CB7__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// Autor.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CAutor dialog

class CAutor : public CDialog
{
// Construction
public:
	CAutor(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CAutor)
	enum { IDD = IDD_AUTOR };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA I.D


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CAutor)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CAutor)
		// NOTE: the ClassWizard will add member functions here
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_AUTOR_H__0B5ED30B_EE3A_4E47_8789_5EA276FF9CB7__INCLUDED_)
