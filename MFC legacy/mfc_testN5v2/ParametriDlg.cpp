// ParametriDlg.cpp : implementation file
//

#include "stdafx.h"
#include "mfc_testN5.h"
#include "ParametriDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CParametriDlg dialog


CParametriDlg::CParametriDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CParametriDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CParametriDlg)
	m_Brzina = 0;
	m_Ugao = 0;
	//}}AFX_DATA_INIT
}


void CParametriDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CParametriDlg)
	DDX_Text(pDX, IDC_EDIT1, m_Brzina);
	DDV_MinMaxInt(pDX, m_Brzina, 0, 100000);
	DDX_Text(pDX, IDC_EDIT2, m_Ugao);
	DDV_MinMaxInt(pDX, m_Ugao, 0, 100000000);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CParametriDlg, CDialog)
	//{{AFX_MSG_MAP(CParametriDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CParametriDlg message handlers


