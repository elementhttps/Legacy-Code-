// mfc_testN5.h : main header file for the MFC_TESTN5 application
//

#if !defined(AFX_MFC_TESTN5_H__F7F7037D_1589_4F56_9BE7_B3CAA4F1D606__INCLUDED_)
#define AFX_MFC_TESTN5_H__F7F7037D_1589_4F56_9BE7_B3CAA4F1D606__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"       // main symbols

/////////////////////////////////////////////////////////////////////////////
// CMfc_testN5App:
// See mfc_testN5.cpp for the implementation of this class
//

class CMfc_testN5App : public CWinApp
{
public:
	CMfc_testN5App();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMfc_testN5App)
	public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL

// Implementation
	//{{AFX_MSG(CMfc_testN5App)

		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_MFC_TESTN5_H__F7F7037D_1589_4F56_9BE7_B3CAA4F1D606__INCLUDED_)
