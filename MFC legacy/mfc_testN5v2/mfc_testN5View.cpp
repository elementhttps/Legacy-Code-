// mfc_testN5View.cpp : implementation of the CMfc_testN5View class
//

#include "stdafx.h"
#include "mfc_testN5.h"
#include "ParametriDlg.h"
#include "mfc_testN5Doc.h"
#include "mfc_testN5View.h"
#include "math.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CMfc_testN5View

IMPLEMENT_DYNCREATE(CMfc_testN5View, CView)

BEGIN_MESSAGE_MAP(CMfc_testN5View, CView)
	//{{AFX_MSG_MAP(CMfc_testN5View)
	ON_WM_LBUTTONDOWN()
	ON_COMMAND(IDM_POMOCNIDLG, OnPomocnidlg)
	//}}AFX_MSG_MAP
	// Standard printing commands
	ON_COMMAND(ID_FILE_PRINT, CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_DIRECT, CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_PREVIEW, CView::OnFilePrintPreview)
	

END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CMfc_testN5View construction/destruction

CMfc_testN5View::CMfc_testN5View()
{
	// TODO: add construction code here

}

CMfc_testN5View::~CMfc_testN5View()
{
}

BOOL CMfc_testN5View::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	return CView::PreCreateWindow(cs);
}

void CMfc_testN5View::OnPomocnidlg() 
{
	// TODO: Add your command handler code here
	CParametriDlg dlg;

    dlg.m_Brzina = m_Brzina;
    dlg.m_Ugao = m_Ugao;
    

    if (dlg.DoModal () == IDOK) 
	{
        m_Brzina = dlg.m_Brzina;
        m_Ugao = dlg.m_Ugao;
       
        Invalidate ();
    }    

}
/////////////////////////////////////////////////////////////////////////////
// CMfc_testN5View drawing

void CMfc_testN5View::OnDraw(CDC* pDC)
{
	CMfc_testN5Doc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);
	if (!pDoc)
		return;





//SISTEM PRIKAZA

	pDC->SetMapMode(MM_ISOTROPIC);
	pDC->SetViewportOrg(30, 420);
	pDC->SetWindowExt(10000,10000);
	pDC->SetViewportExt(100,-100);
	

	CPen PenBlack(PS_SOLID, 200, RGB(0, 0, 0));
	pDC->SelectObject(PenBlack);

	// OSE
	pDC->MoveTo(-1000,     0);
	pDC->LineTo( 20000000,     0);
	pDC->MoveTo(   0, -1000);
	pDC->LineTo(   0,  20000000);

	
;   
	
	
	
//KONSTANTE

double PI = 3.14159;
double g = 9.81;

//PROMENE BRZINE I UGLA
int alfa1 = m_Ugao;
double alfa2 = 44;
double alfa3 = 45;

int v1 = m_Brzina;
double v2 = 890;
double v3 = 900;
	
//KORAK I MAKSIMUM


double kor = 1;

double max = 90000; //oko milion


// Multiplikator koordinata pozicije teksta
	double kox = 1500;
	double koy = 1000;

    pDC->SetTextColor(RGB(255, 0, 0)); 
	pDC->TextOut(50*kox, 40*koy, "Funkcija  f1" , 12);
	pDC->SetTextColor(RGB(0, 0, 255));
	pDC->TextOut(50*kox, 35*koy, "Funkcija  f2" , 12);
	pDC->SetTextColor(RGB(0, 255, 0));
	pDC->TextOut(50*kox, 30*koy, "Funkcija  f3" , 12);


//Funkcija f1 CRVENA
UpdateData(true);

	for(double i = 1; i < max; i +=kor)
	{ 
        
		double c1 = 1  ;

		



	    double f = (i*tan((PI*m_Ugao)/180))-((g*i*i)/(2*m_Brzina*m_Brzina*cos((PI*m_Ugao)/180)*cos((PI*m_Ugao)/180)));
	    pDC->SetPixel(i, f, RGB(255, 0, 0));
	}

//Funkcija f2 PLAVA

	for(double i2 = 1; i2 < max; i2 +=kor)
	{
		int c2 = 0;
	    double f2 =(i2*tan((PI*alfa2)/180))-((g*i2*i2)/(2*v2*v2*cos((PI*alfa2)/180)*cos((PI*alfa2)/180))); ;
			//tan(i2)*i2
	    pDC->SetPixel(i2, f2, RGB(0, 0, 255));
	}

//Funkcija f3 ZELENA

    for(double i3 = 1; i3 < max; i3 +=kor)
	{
		int c3 = 0;
	    double f3 = (i3*tan((PI*alfa3)/180))-((g*i3*i3)/(2*v3*v3*cos((PI*alfa3)/180)*cos((PI*alfa3)/180)));;
	    pDC->SetPixel(i3, f3, RGB(0, 255, 0));
	}



//POZICIJA OZNAKA


	
	
	
	//Koordinate 
	pDC->SetTextColor(RGB(100, 100, 0));

    pDC->TextOut(56000, 2000, 'X');
	pDC->TextOut(500, 35000, 'Y');
   
    //Lenjiri po x i y osi
    CPen PenBlack3(PS_SOLID, 100, RGB(0, 0, 0));
	pDC->SelectObject(PenBlack3);
    
	for(int koox = 0; koox < 1000000; koox += 1000)
    {
        pDC->MoveTo(koox, 0);
        pDC->LineTo(koox, -1000);
    }

	for(int kooy = 0; kooy < 1000000; kooy += 1000)
    {
        pDC->MoveTo(0, kooy);
        pDC->LineTo(-1000, kooy);
    }



	pDC->SelectObject(PenBlack3);
    for(int koox2 = 0; koox2 < 1000000; koox2 += 5000)
    {
        pDC->MoveTo(koox2, 0);
        pDC->LineTo(koox2, -2000);
    }
	for(int kooy2 = 0; kooy2 < 1000000; kooy2 += 5000)
    {
        pDC->MoveTo(0, kooy2);
        pDC->LineTo(-2000, kooy2);
    }

	for(int koox3 = 0; koox3 < 1000000; koox3 += 10000)
    {
        pDC->MoveTo(koox3, 0);
        pDC->LineTo(koox3, -3000);
    }
	for(int kooy3 = 0; kooy3 < 1000000; kooy3 += 10000)
    {
        pDC->MoveTo(0, kooy3);
        pDC->LineTo(-3000, kooy3);
    }


	
	
	


	// TODO: add draw code for native data here
}

/////////////////////////////////////////////////////////////////////////////
// CMfc_testN5View printing

BOOL CMfc_testN5View::OnPreparePrinting(CPrintInfo* pInfo)
{
	// default preparation
	return DoPreparePrinting(pInfo);
}

void CMfc_testN5View::OnBeginPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: add extra initialization before printing
}

void CMfc_testN5View::OnEndPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: add cleanup after printing
}

/////////////////////////////////////////////////////////////////////////////
// CMfc_testN5View diagnostics

#ifdef _DEBUG
void CMfc_testN5View::AssertValid() const
{
	CView::AssertValid();
}

void CMfc_testN5View::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}

CMfc_testN5Doc* CMfc_testN5View::GetDocument() // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CMfc_testN5Doc)));
	return (CMfc_testN5Doc*)m_pDocument;
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CMfc_testN5View message handlers





