// p2Doc.cpp : implementation of the CP2Doc class
//

#include "stdafx.h"
#include "p2.h"

#include "p2Doc.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CP2Doc

IMPLEMENT_DYNCREATE(CP2Doc, CDocument)

BEGIN_MESSAGE_MAP(CP2Doc, CDocument)
	//{{AFX_MSG_MAP(CP2Doc)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CP2Doc construction/destruction

CP2Doc::CP2Doc()
{
	// TODO: add one-time construction code here

}

CP2Doc::~CP2Doc()
{
}

BOOL CP2Doc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;

	// TODO: add reinitialization code here
	// (SDI documents will reuse this document)

	return TRUE;
}



/////////////////////////////////////////////////////////////////////////////
// CP2Doc serialization

void CP2Doc::Serialize(CArchive& ar)
{
	if (ar.IsStoring())
	{
		// TODO: add storing code here
	}
	else
	{
		// TODO: add loading code here
	}
}

/////////////////////////////////////////////////////////////////////////////
// CP2Doc diagnostics

#ifdef _DEBUG
void CP2Doc::AssertValid() const
{
	CDocument::AssertValid();
}

void CP2Doc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CP2Doc commands
