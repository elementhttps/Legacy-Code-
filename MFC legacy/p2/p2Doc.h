// p2Doc.h : interface of the CP2Doc class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_P2DOC_H__A3E80CD7_EE5E_46B9_88B4_792FCD13FA7B__INCLUDED_)
#define AFX_P2DOC_H__A3E80CD7_EE5E_46B9_88B4_792FCD13FA7B__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


class CP2Doc : public CDocument
{
protected: // create from serialization only
	CP2Doc();
	DECLARE_DYNCREATE(CP2Doc)

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CP2Doc)
	public:
	virtual BOOL OnNewDocument();
	virtual void Serialize(CArchive& ar);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CP2Doc();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	//{{AFX_MSG(CP2Doc)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_P2DOC_H__A3E80CD7_EE5E_46B9_88B4_792FCD13FA7B__INCLUDED_)
