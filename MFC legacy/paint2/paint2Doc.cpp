// paint2Doc.cpp : implementation of the CPaint2Doc class
//

#include "stdafx.h"
#include "paint2.h"

#include "paint2Doc.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CPaint2Doc

IMPLEMENT_DYNCREATE(CPaint2Doc, CDocument)

BEGIN_MESSAGE_MAP(CPaint2Doc, CDocument)
	//{{AFX_MSG_MAP(CPaint2Doc)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CPaint2Doc construction/destruction

CPaint2Doc::CPaint2Doc()
{
	// TODO: add one-time construction code here

}

CPaint2Doc::~CPaint2Doc()
{
}

BOOL CPaint2Doc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;

	// TODO: add reinitialization code here
	// (SDI documents will reuse this document)

	return TRUE;
}



/////////////////////////////////////////////////////////////////////////////
// CPaint2Doc serialization

void CPaint2Doc::Serialize(CArchive& ar)
{
	if (ar.IsStoring())
	{
		// TODO: add storing code here
	}
	else
	{
		// TODO: add loading code here
	}
}

/////////////////////////////////////////////////////////////////////////////
// CPaint2Doc diagnostics

#ifdef _DEBUG
void CPaint2Doc::AssertValid() const
{
	CDocument::AssertValid();
}

void CPaint2Doc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CPaint2Doc commands
