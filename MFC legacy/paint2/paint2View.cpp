// paint2View.cpp : implementation of the CPaint2View class
//

#include "stdafx.h"
#include "paint2.h"

#include "paint2Doc.h"
#include "paint2View.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CPaint2View

IMPLEMENT_DYNCREATE(CPaint2View, CView)

BEGIN_MESSAGE_MAP(CPaint2View, CView)
	//{{AFX_MSG_MAP(CPaint2View)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG_MAP
	// Standard printing commands
	ON_COMMAND(ID_FILE_PRINT, CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_DIRECT, CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_PREVIEW, CView::OnFilePrintPreview)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CPaint2View construction/destruction

CPaint2View::CPaint2View()
{
	// TODO: add construction code here

}

CPaint2View::~CPaint2View()
{
}

BOOL CPaint2View::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	return CView::PreCreateWindow(cs);
}

/////////////////////////////////////////////////////////////////////////////
// CPaint2View drawing

void CPaint2View::OnDraw(CDC* pDC)
{
	CPaint2Doc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);
	if (!pDoc)
    return;
//+++++++++++++++++++++++++++++++++++++++


COLORREF qCircleColor = RGB(255,0,0);
CPen qCirclePen(PS_SOLID, 7, qCircleColor);
CPen* pqOrigPen = pDC->SelectObject(&qCirclePen);

int x;
int y;

x = 100;
y = 100;




for (int a = 0 ;  a < x ; a++)

for (int b = 0 ; b < y ; b++)

/*pDC->Ellipse(b+100, 100, a+99, 99);
//+++++++++++++++++++++++++++++++++++++++
*/


//+++++++++++++++++++++++++++++++++++++++

COLORREF qLineColor = RGB(0,0,0);
CPen qLinePen(PS_SOLID, 7, qLineColor);
pDC->SelectObject(&qLinePen);


pDC->MoveTo(300, 300);
pDC->LineTo(10, 300);
pDC->SelectObject(pqOrigPen);

//+++++++++++++++++++++++++++++++++++++++
COLORREF qLineColor1 = RGB(0,0,0);
CPen qLinePen1(PS_SOLID, 7, qLineColor);
pDC->SelectObject(&qLinePen);


pDC->MoveTo(10, 10);         //x^,    y-
pDC->LineTo(10, 300);
pDC->SelectObject(pqOrigPen);

//+++++++++++++++++++++++++++++++++++++++





	// TODO: add draw code for native data here
}

/////////////////////////////////////////////////////////////////////////////
// CPaint2View printing

BOOL CPaint2View::OnPreparePrinting(CPrintInfo* pInfo)
{
	// default preparation
	return DoPreparePrinting(pInfo);
}

void CPaint2View::OnBeginPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: add extra initialization before printing
}

void CPaint2View::OnEndPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: add cleanup after printing
}

/////////////////////////////////////////////////////////////////////////////
// CPaint2View diagnostics

#ifdef _DEBUG
void CPaint2View::AssertValid() const
{
	CView::AssertValid();
}

void CPaint2View::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}

CPaint2Doc* CPaint2View::GetDocument() // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CPaint2Doc)));
	return (CPaint2Doc*)m_pDocument;
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CPaint2View message handlers
