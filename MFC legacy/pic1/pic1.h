// pic1.h : main header file for the PIC1 application
//

#if !defined(AFX_PIC1_H__A1B5E4F8_CD58_4AC1_B4F3_DBD0BB8ABA08__INCLUDED_)
#define AFX_PIC1_H__A1B5E4F8_CD58_4AC1_B4F3_DBD0BB8ABA08__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"		// main symbols

/////////////////////////////////////////////////////////////////////////////
// CPic1App:
// See pic1.cpp for the implementation of this class
//

class CPic1App : public CWinApp
{
public:
	CPic1App();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CPic1App)
	public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL

// Implementation

	//{{AFX_MSG(CPic1App)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_PIC1_H__A1B5E4F8_CD58_4AC1_B4F3_DBD0BB8ABA08__INCLUDED_)
