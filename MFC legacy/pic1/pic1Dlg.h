// pic1Dlg.h : header file
//

#if !defined(AFX_PIC1DLG_H__03C2235A_C45E_45B5_857F_B3C8A44E069E__INCLUDED_)
#define AFX_PIC1DLG_H__03C2235A_C45E_45B5_857F_B3C8A44E069E__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

/////////////////////////////////////////////////////////////////////////////
// CPic1Dlg dialog

class CPic1Dlg : public CDialog
{
// Construction
public:
	CPic1Dlg(CWnd* pParent = NULL);	// standard constructor

// Dialog Data
	//{{AFX_DATA(CPic1Dlg)
	enum { IDD = IDD_PIC1_DIALOG };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CPic1Dlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	//{{AFX_MSG(CPic1Dlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_PIC1DLG_H__03C2235A_C45E_45B5_857F_B3C8A44E069E__INCLUDED_)
