// sliderDlg.h : header file
//

#if !defined(AFX_SLIDERDLG_H__762CD1B2_015E_4BEC_BC23_831F1FB2C545__INCLUDED_)
#define AFX_SLIDERDLG_H__762CD1B2_015E_4BEC_BC23_831F1FB2C545__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

/////////////////////////////////////////////////////////////////////////////
// CSliderDlg dialog

class CSliderDlg : public CDialog
{
// Construction
public:
	CSliderDlg(CWnd* pParent = NULL);	// standard constructor

// Dialog Data
	//{{AFX_DATA(CSliderDlg)
	enum { IDD = IDD_SLIDER_DIALOG };
	CSliderCtrl	m_Slider;
	CString	m_SliderValue;
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSliderDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	//{{AFX_MSG(CSliderDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	afx_msg void OnOutofmemorySlider1(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void IDC_SLIDER_VALUE();
	afx_msg void OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SLIDERDLG_H__762CD1B2_015E_4BEC_BC23_831F1FB2C545__INCLUDED_)
