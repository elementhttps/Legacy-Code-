// test6.cpp : Defines the entry point for the console application.
//array u funkciji

#include "stdafx.h"

#include <iostream>
using namespace std;

void DisplayTheArray(double member[])
{
	for(int i = 0; i < 13; ++i)
		cout << "\nDistance " << i + 1 << ": " << member[i];
	cout << endl;
}

int main()
{
	const int numberOfItems = 13;
	double distance[numberOfItems] = {44.14, 720.52, 96.08, 468.78, 6.28,1 ,2 ,3 ,4 ,5 ,6 , 7, 8};

	cout << "Members of the array";
	DisplayTheArray(distance);

	return 0;
}