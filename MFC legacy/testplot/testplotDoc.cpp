// testplotDoc.cpp : implementation of the CTestplotDoc class
//

#include "stdafx.h"
#include "testplot.h"

#include "testplotDoc.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CTestplotDoc

IMPLEMENT_DYNCREATE(CTestplotDoc, CDocument)

BEGIN_MESSAGE_MAP(CTestplotDoc, CDocument)
	//{{AFX_MSG_MAP(CTestplotDoc)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CTestplotDoc construction/destruction

CTestplotDoc::CTestplotDoc()
{
	// TODO: add one-time construction code here

}

CTestplotDoc::~CTestplotDoc()
{
}

BOOL CTestplotDoc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;

	// TODO: add reinitialization code here
	// (SDI documents will reuse this document)

	return TRUE;
}



/////////////////////////////////////////////////////////////////////////////
// CTestplotDoc serialization

void CTestplotDoc::Serialize(CArchive& ar)
{
	if (ar.IsStoring())
	{
		// TODO: add storing code here
	}
	else
	{
		// TODO: add loading code here
	}
}

/////////////////////////////////////////////////////////////////////////////
// CTestplotDoc diagnostics

#ifdef _DEBUG
void CTestplotDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CTestplotDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CTestplotDoc commands
