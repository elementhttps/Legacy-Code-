
#include <iostream>
#include <stdlib.h> //Needed for "exit" function

#include <glut.h>

int zpp =5;

void handleKeypress(unsigned char key,int x, int y) 
{    
	switch (key)
	{
		case 27: //Escape key !!
			zpp+=1;
			
			
	}
}
void initRendering() 
{

	glEnable(GL_DEPTH_TEST);	//Makes 3D drawing work when something is in front of something else
}

void handleResize(int w, int h) 
{
	//Tell OpenGL how to convert from coordinates to pixel values
	glViewport(0, 0, w, h);
	
	glMatrixMode(GL_PROJECTION); //Switch to setting the camera perspective
	
	//Set the camera perspective
	glLoadIdentity(); //Reset the camera
	gluPerspective(45.0,                  //The camera angle ili ugao oka je uvek konstantan
				   (double)w / (double)h, //The width-to-height ratio
				   1.0,                   //The near z clipping coordinate - z koordinata koja predstavlja granicu u pozitivnom smeru sve sto je manje od ove vrednosti nrmoj da crtas
				   200.0);                //The far z clipping coordinate
}


void drawScene()
 {
	//Clear information from last draw
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);//ocisti sve predhodno
	
	glMatrixMode(GL_MODELVIEW); //Switch to the drawing perspective
	glLoadIdentity(); //Reset the drawing perspective
	
	glBegin(GL_QUADS); //Begin quadrilateral coordinates - OVDE POCINJU OBJEKTI
	
	//Trapezoid /4 koordinate f je za float mozda i nemora f
	glVertex3f(-0.7, -1.5, -zpp);
	glVertex3f(0.7, -1.5, -5.0);
	glVertex3f(0.4, -0.5, -5.0);
	glVertex3f(-0.4, -0.5, -5.0);
	
	glEnd(); //End quadrilateral coordinates zavrsi sa trapezom
	
	
	glutSwapBuffers(); //Send the 3D scene to the screen
}






int main(int argc, char** argv)
 {
	//Initialize GLUT
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
	glutInitWindowSize(400, 400); //Set the window size
	
	//Create the window
	glutCreateWindow("TEST");//naziv prozora
	initRendering(); //Initialize rendering
	
    glutKeyboardFunc(handleKeypress);
	glutDisplayFunc(drawScene);//napravi ili nacrtaj
	glutReshapeFunc(handleResize);//funkcija koja omogucava da se obezbedi resize za prozor
	
	glutMainLoop(); //Start the main loop.  glutMainLoop doesn't return.
	return 0; //This line is never reached
}