
#ifdef _WIN32
#include <windows.h>//requires that windows.h be included before either gl.h or glu.h, because some macros
#endif
#include <glut.h>
#include <stdlib.h> //Needed for "exit" function


float k1 = 0.25;
float k2 = 0.75;


//(k1,k2)  (k1,k1) (k2,k1)  (k2,k2)

void display(void)
{
/*  clear all pixels  */
    glClear(GL_COLOR_BUFFER_BIT);
	/*  draw white polygon (rectangle) with corners at
 *  (0.25, 0.25, 0.0) and (0.75, 0.75, 0.0)  
 */
   

for (float k = 0;k<=2.0;k+=0.5)
{
	 glColor3f(1.0, 1.0, 0.0+k-0.5);
    glBegin(GL_POLYGON);
        glVertex3f(k1+k, k1+k, 0.0);
        glVertex3f(k2+k, k1+k, 0.0);
        glVertex3f(k2+k, k2+k, 0.0);
        glVertex3f(k1+k, k2+k, 0.0);
    glEnd();
}
/*
    glBegin(GL_POLYGON);
        glVertex3f(0.25, 0.25, 0.0);
        glVertex3f(0.75, 0.25, 0.0);
        glVertex3f(0.75, 0.75, 0.0);
        glVertex3f(0.25, 0.75, 0.0);
    glEnd();
*/

	
glBegin(GL_LINES);
for (float i = 0;i<0.50;i+=0.05)
{

 glColor3f(1.0, 0.0, 0.0);
glVertex3f(0.25f+i, 0.75f, 0.0f);

 glColor3f(1.0, 0.0, 0.0);
glVertex3f(0.25f+i, 0.25f, 0.0f);


}
 glEnd();


glBegin(GL_LINES);
for (float j = 0;j<0.50;j+=0.05)
{

 glColor3f(1.0, 0.0, 0.0);
glVertex3f(0.25f, 0.25f+j, 0.0f);

 glColor3f(1.0, 0.0, 0.0);
glVertex3f(0.75f, 0.25f+j, 0.0f);


}
 glEnd();
/*  don�t wait!  
 *  start processing buffered OpenGL routines 
 */
    glFlush();
}
void init(void) 
{
/*  select clearing (background) color       */
    glClearColor(0.0, 0.0, 0.0, 0.0);
/*  initialize viewing values  */
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    //glOrtho(0.0, 1.0, 0.0, 1.0, -1.0, 1.0);
	glOrtho(0.0, 3.0, 0.0, 3.0, -1.0, 1.0);
}
/*
 *  Declare initial window size, position, and display mode
 *  (single buffer and RGBA).  Open window with �hello�
 *  in its title bar.  Call initialization routines.
 *  Register callback function to display graphics.
 *  Enter main loop and process events.
 */
int main(int argc, char** argv)
{
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);
    glutInitWindowSize(600, 600); 
    glutInitWindowPosition(100, 100);
    glutCreateWindow("hello");
    init();
    glutDisplayFunc(display); 
    glutMainLoop();
    return 0;   /* ISO C requires main to return int. */
}