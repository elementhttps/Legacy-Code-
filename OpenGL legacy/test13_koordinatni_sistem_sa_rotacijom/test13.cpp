
#ifdef _WIN32
#include <windows.h>//requires that windows.h be included before either gl.h or glu.h, because some macros
#endif
#include <glut.h>
#include <stdlib.h> //Needed for "exit" function




/* !!!!!!!!!!!!!!!!!!!!!

GLdouble dvect[3] = {5.0, 9.0, 1992.0};
glVertex3dv(dvect);

*/
// all variables initialized to 1.0, meaning
// the triangle will initially be white
float red=1.0f, blue=1.0f, green=1.0f;
	//glRotatef(angle, 1.0f, 0.0f, 0.0f);
float osax= 1.0f,osay= 1.0f, osaz= 1.0f;
// angle for rotating triangle
float angle = 0.0f,angle1 = 1.0f;
float camx = 0.0f, camy = 0.0f,camz = 0.0f;


float k1 = 0.25;
float k2 = 0.75;
 float h = 5.0f;
 float n = 0;
//glEnableClientState
void changeSize(int w, int h) 
{

	// Prevent a divide by zero, when window is too short
	// (you cant make a window of zero width).
	if (h == 0)
		h = 1;
	float ratio =  w * 1.0 / h;

        // Use the Projection Matrix
	glMatrixMode(GL_PROJECTION);

        // Reset Matrix
	glLoadIdentity();

	// Set the viewport to be the entire window
	glViewport(0, 0, w, h);

	// Set the correct perspective.
	gluPerspective(45.0f, ratio, 0.1f, 100.0f);

	// Get Back to the Modelview
	glMatrixMode(GL_MODELVIEW);
}

void renderScene(void) 
{

	// Clear Color and Depth Buffers
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// Reset transformations
	glLoadIdentity();

	// Set the camera
	gluLookAt(	camx, camy, camz+10.0f,
			camx, camy,  camz,
			0.0f, 1.0f,  0.0f);



	glRotatef(angle1, osax, osay, osaz);


  glClearColor(red-0.5, green-0.5, blue-0.5, 0.0);
   glClear(GL_COLOR_BUFFER_BIT);
   glColor3f(1.0, 1.0, 1.0);


	glColor3f(red,green,blue);

	glBegin(GL_LINES); // OVDE POCINJU OBJEKTI

		glColor3f(0.0f,green,0.0f);
	glVertex3f(0.0,0.0, 0);
	glVertex3f(50, 0, -5);
glEnd(); //Marks the end of a vertex-data list


	glBegin(GL_LINES); // OVDE POCINJU OBJEKTI
	
		glColor3f(red,0.0f,0.0f);
	glVertex3f(0.0,0.0, 0);
	glVertex3f(0, 50, -5);
glEnd(); //Marks the end of a vertex-data list


	glBegin(GL_LINES); // OVDE POCINJU OBJEKTI

		glColor3f(0.0f,0.0f,blue);
	glVertex3f(0.0,0.0, 0);
	glVertex3f(0, 0, -50);
glEnd(); //Marks the end of a vertex-data list

	glFlush();//Forces previously issued OpenGL commands to begin execution, thus 
              //guaranteeing that they complete in finite time.
//------------------------------------------------------------------
for ( float k = 0;k<h;k+=1)
{
	glColor3f(0.0+((k-0.8)/k), 1.0, 0.0);

for (  n = 0;n<h;n+=1)
{
	 
    glBegin(GL_POLYGON);
//glEnable(GL_POLYGON_MODE);
//glPolygonMode(GL_FRONT, GL_FILL);
//glPolygonMode(GL_BACK, GL_LINE);
 glColor3f(0.0, 1.0, 0.0+((n-0.8)/n));



float z=0;
for (float i = 0 ;i<h;i+=1)//efekat stepenica
{
if (n==i)
{
	z =2+i;
}
}

float a = 0.5;
float dx1 = n*a;
float dx2 = a*(1+n);
float s1 =0.5;
float s2 =k;

        glVertex3f(dx1, s2, 0.0+z);
        glVertex3f(dx1, s1, 0.0+z);
        glVertex3f(dx2, s1, 0.0+z);
        glVertex3f(dx2, s2, 0.0+z);
  
    glEnd();//Marks the end of a vertex-data list
	
}
}
//-----------------------------------------

/*
for (float k = 0;k<=2.0;k+=0.5)
{
	 glColor3f(1.0, 1.0, 0.0+k-0.5);
    glBegin(GL_POLYGON);
        glVertex3f(k1+k, k1+k, 0.0-k);
        glVertex3f(k2+k, k1+k, 0.0-k);
        glVertex3f(k2+k, k2+k, 0.0-k);
        glVertex3f(k1+k, k2+k, 0.0-k);
    glEnd();
}
*/




	glFlush();//Forces previously issued OpenGL commands to begin execution, thus 
              //guaranteeing that they complete in finite time.

	angle1+=angle;///////******  Motion = Redraw + Swap

	glutSwapBuffers();
}

void processNormalKeys(unsigned char key, int x, int y) 
{

	if (key == 27)
		exit(0);

	switch(key) 
	{
			case 119://malo w
			camy -= 0.5f; break;
					
			case 115://malo s
			camy += 0.5f; break;
			
			case 100://malo d
			camx -= 0.5f; break;
				
			case 97://malo a
			camx += 0.5f; break;
			
			case 101://malo e
			camz -= 0.5f; break;

			case 113://malo q
			camz += 0.5f; break;

			case 120://malo x
			h -= 1.0f; break;

			case 122://malo y  //121 je y ali tastatura je drugacije organizovana pa je y=122 umesto 121 po ascii dec
			h += 1.0f; break;



	}
}

void processSpecialKeys(int key, int x, int y)
{
	switch(key) 
	{
		case GLUT_KEY_F1 :
				red = 1.0f;
				green = 0.0f;
				blue = 0.0f; break;
		case GLUT_KEY_F2 :
				red = 0.0f;
				green = 1.0f;
				blue = 0.0f; break;
		case GLUT_KEY_F3 :
				red = 0.0f;
				green = 0.0f;
				blue = 1.0f; break;
		case GLUT_KEY_F4 :
				osax += 0.5f;
			//	osay = 0.0f;
			  //osaz = 0.0f; 
				break;

       	case GLUT_KEY_LEFT :
				osax -= 0.5f;
			//	osay = 0.0f;
			  //osaz = 0.0f; 
				break;


		case GLUT_KEY_F5 :
			//	osax = 0.0f;
				osay += 0.5f;
				//o saz = 0.0f;
				break;
		
		case GLUT_KEY_RIGHT :
			//	osax = 0.0f;
				osay -= 0.5f;
				//o saz = 0.0f;
				break;

        case GLUT_KEY_F6 :
				//osax = 0.0f;
				//osay = 0.0f;
				osaz += 0.5f; break;

        case GLUT_KEY_UP :
				//osax = 0.0f;
				//osay = 0.0f;
				osaz -= 0.5f; 
			  break;


         case GLUT_KEY_DOWN :
				osax = 0.0f;
				osay = 0.0f;
				osaz = 0.0f; 
 red=0.8f, blue=1.0f, green=0.8f;break;

//#define GLUT_KEY_DOWN



		case GLUT_KEY_F7 :
				
				angle -= 0.5f; break;
		case GLUT_KEY_F8 :
			
				angle += 0.5f; break;

	   	case GLUT_KEY_F9 :
			
				angle += 0.0f; break;
//	    	case 101://a
			
//				angle += 0.5f; break;
	   
	



	}
}


void mouse(int button, int state, int x, int y)
{
	switch (button) 
	{//////////////////////////////////
   
case GLUT_LEFT_BUTTON:

if (state == GLUT_DOWN)
{

//glutIdleFunc(spinDisplay);
	angle -= 0.1f; break;
}
break;

case GLUT_RIGHT_BUTTON:

if (state == GLUT_DOWN)
{

//glutIdleFunc(spinDisplay2);
	angle += 0.1f; break;
}
break;


case GLUT_MIDDLE_BUTTON:

if (state == GLUT_DOWN)
{
	angle = 0.0f; break;
//glutIdleFunc(NULL);
}
break;

default:break;

	} ////////////////////////////////
}

void init(void) 
{
/*  select clearing (background) color       */
    glClearColor(0.0, 0.0, 0.0, 0.0);
/*  initialize viewing values  */
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glOrtho(0.0, 1.0, 0.0, 1.0, -1.0, 1.0);//gluOrtho2D(0.0, (GLdouble) w, 0.0, (GLdouble) h);
}


void main(int argc, char **argv) {

	// init GLUT and create window
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DEPTH | GLUT_DOUBLE | GLUT_RGBA);
	glutInitWindowPosition(100,100);
	glutInitWindowSize(800,600);
	glutCreateWindow("Lighthouse3D- GLUT Tutorial");





	// register callbacks
	glutDisplayFunc(renderScene);
	glutReshapeFunc(changeSize);	

	glutIdleFunc(renderScene);

	// here are the new entries
	glutKeyboardFunc(processNormalKeys);
	glutSpecialFunc(processSpecialKeys);
    glutMouseFunc(mouse);//komande misa
	// enter GLUT event processing cycle
	glutMainLoop();
}