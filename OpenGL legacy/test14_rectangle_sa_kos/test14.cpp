
#ifdef _WIN32
#include <windows.h>//requires that windows.h be included before either gl.h or glu.h, because some macros
#endif
#include <glut.h>
#include <stdlib.h> //Needed for "exit" function
#include <math.h>

float red=1.0f, blue=1.0f, green=1.0f;

static GLfloat spin = 0.0;
void init(void) 
{
   glClearColor(0.0, 0.0, 0.0, 0.0);
   glShadeModel(GL_FLAT);
}
void display(void)
{
   glClear(GL_COLOR_BUFFER_BIT);
   glPushMatrix();
   glRotatef(spin, 0.0, 0.0, 1.0);///ROTIRAJ SVE OBJEKTE OD  glPushMatrix(); DO glPopMatrix();
   glColor3f(1.0, 1.0, 1.0);
   glRectf(-25.0, -25.0, 25.0, 25.0);
   

   	glBegin(GL_LINES); // OVDE POCINJU OBJEKTI

		glColor3f(0.0f,green,0.0f);
	glVertex3f(0.0,0.0, 0);
	glVertex3f(50, 0, -5);
glEnd(); 
double PI = 3.14;

double G = 6.67/100000000000;

double teta;
double x1,y1,x2,y2;

	glBegin(GL_LINES);//GL_POINTS 
		for ( double teta2= 0; teta2 < 360; teta2+=1)
		{
			teta2=teta;

double L = 10000000;
double M = 10000000;
double Msol = 1000000000000000;
double e=0;
//double teta;
double D = M*Msol*G;


double r;

double L2 = pow(L,2);
double DM = D*M;
double l2dm = L2/DM;
double pod1 = cos((PI*teta)/180);
double pod2 = 1+(e*pod1);
r = l2dm/pod2;
				x1 = r*cos((PI*teta)/180);
				y1 = r*sin((PI*teta)/180);

				glVertex3f(x1,y1,0.0f);
					
			
		}
	glEnd();


	glBegin(GL_LINES); // OVDE POCINJU OBJEKTI
	
		glColor3f(red,0.0f,0.0f);
	glVertex3f(0.0,0.0, 0);
	glVertex3f(0, 50, -5);
glEnd(); 


	glBegin(GL_LINES); // OVDE POCINJU OBJEKTI

		glColor3f(0.0f,0.0f,blue);
	glVertex3f(0.0,0.0, 0);
	glVertex3f(0, 0, -50);
glEnd(); 
glPopMatrix();
   glutSwapBuffers();
}

void spinDisplay(void)
{
   spin = spin + 2.0;
   if (spin > 360.0)
      spin = spin - 360.0;
   glutPostRedisplay();
}
void reshape(int w, int h)
{
   glViewport(0, 0, (GLsizei) w, (GLsizei) h);
   glMatrixMode(GL_PROJECTION);
   glLoadIdentity();
   glOrtho(-50.0, 50.0, -50.0, 50.0, -1.0, 1.0);
   glMatrixMode(GL_MODELVIEW);
   glLoadIdentity();
}

void mouse(int button, int state, int x, int y) 
{
   switch (button) {
      case GLUT_LEFT_BUTTON:
         if (state == GLUT_DOWN)
            glutIdleFunc(spinDisplay);
         break;
      case GLUT_MIDDLE_BUTTON:
         if (state == GLUT_DOWN)
            glutIdleFunc(NULL);
         break;
      default:
         break;
   }
}
/*
 *  Request double buffer display mode.
 *  Register mouse input callback functions
 */


int main(int argc, char** argv)
{
   glutInit(&argc, argv);
   glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB);
   glutInitWindowSize(250, 250); 
   glutInitWindowPosition(100, 100);
   glutCreateWindow(argv[0]);
   init();
   glutDisplayFunc(display); 
   glutReshapeFunc(reshape); 
   glutMouseFunc(mouse);
   glutMainLoop();
   return 0;
}
