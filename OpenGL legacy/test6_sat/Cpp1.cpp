
#include <glut.h>
#include <stdlib.h> //Needed for "exit" function
void tastatura(unsigned char key,int x, int y) 
{    
	switch (key)
	{
		case 27: //Escape key !!
			exit(0);
			
			
	}
}
int _ugao;
int _ugao2;

void handleResize(int w, int h) 
{
	//Tell OpenGL how to convert from coordinates to pixel values
	glViewport(0, 0, w, h);
	
	glMatrixMode(GL_PROJECTION); //Switch to setting the camera perspective
	
	//Set the camera perspective
	glLoadIdentity(); //Reset the camera
	gluPerspective(45.0,                  //The camera angle ili ugao oka je uvek konstantan
				   (double)w / (double)h, //The width-to-height ratio
				   1.0,                   //The near z clipping coordinate - z koordinata koja predstavlja granicu u pozitivnom smeru sve sto je manje od ove vrednosti nrmoj da crtas
				   200.0);                //The far z clipping coordinate
}


void drawScene()
 {
	//Clear information from last draw
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);//ocisti sve predhodno
	glClearColor ( 0.0, 0.0, 1.0, 0.0 );//set window buffer as yellow
    glColor3f(0.0f, 0.0f,0.0f); //draw  in blue

	glMatrixMode(GL_MODELVIEW); //Switch to the drawing perspective
	glLoadIdentity(); //Reset the drawing perspective
	
	glPushMatrix(); //Save the transformations performed thus far
	glRotatef(_ugao, 0.0f, 0.0f, 5.0f); //Rotate about the the vector (1, 2, 3)
	glBegin(GL_LINES); // OVDE POCINJU OBJEKTI
	
	
	glVertex3f(-0.0,- 0.0, -5);
	glVertex3f(-1.5, -1.5, -5);
	
	glEnd(); 

	glRotatef(_ugao2, 0.0f, 0.0f, 5.0f); //Rotate about the the vector (1, 2, 3)
	glBegin(GL_LINES); // OVDE POCINJU OBJEKTI
	
	
	glVertex3f(-0.0,- 0.0, -5);
	glVertex3f(-1, -1, -5);
	
	glEnd(); 

	
	glutSwapBuffers(); //Send the 3D scene to the screen
}

void initRendering() 
{

	glEnable(GL_DEPTH_TEST);	//Makes 3D drawing work when something is in front of something else
}


void update(int value)
 {
	_ugao -= 0.1;
	_ugao2 -= 6;
	

	if (_ugao > 360) 
	{
			_ugao -= 360;
	}
		if (_ugao2 > 360) 
	{
			_ugao2 -= 360;
	}

	
	glutPostRedisplay(); //Tell GLUT that the display has changed
	
	//Tell GLUT to call update again in 25 milliseconds
	glutTimerFunc(1000, update, 0);
}


int main(int argc, char** argv)
 {

	//Initialize GLUT
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
	glutInitWindowSize(400, 400); //Set the window size
	
	//Create the window
	glutCreateWindow("TEST");//naziv prozora
	initRendering(); //Initialize rendering
	
    glutKeyboardFunc(tastatura);
	glutDisplayFunc(drawScene);//napravi ili nacrtaj
	glutReshapeFunc(handleResize);//funkcija koja omogucava da se obezbedi resize za prozor
	
	glutTimerFunc(25, update, 0); //Add a timer


	glutMainLoop(); //Start the main loop.  glutMainLoop doesn't return.
	return 0; //This line is never reached
}