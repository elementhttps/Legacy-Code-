#include <glut.h>
#include <stdlib.h> //Needed for "exit" function

// all variables initialized to 1.0, meaning
// the triangle will initially be white
float red=1.0f, blue=1.0f, green=1.0f;
	//glRotatef(angle, 1.0f, 0.0f, 0.0f);
float osax= 1.0f,osay= 1.0f, osaz= 1.0f;
// angle for rotating triangle
float angle = 0.0f,angle1 = 1.0f;

void changeSize(int w, int h) {

	// Prevent a divide by zero, when window is too short
	// (you cant make a window of zero width).
	if (h == 0)
		h = 1;
	float ratio =  w * 1.0 / h;

        // Use the Projection Matrix
	glMatrixMode(GL_PROJECTION);

        // Reset Matrix
	glLoadIdentity();

	// Set the viewport to be the entire window
	glViewport(0, 0, w, h);

	// Set the correct perspective.
	gluPerspective(45.0f, ratio, 0.1f, 100.0f);

	// Get Back to the Modelview
	glMatrixMode(GL_MODELVIEW);
}

void renderScene(void) {

	// Clear Color and Depth Buffers
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// Reset transformations
	glLoadIdentity();

	// Set the camera
	gluLookAt(	0.0f, 0.0f, 10.0f,
			0.0f, 0.0f,  0.0f,
			0.0f, 1.0f,  0.0f);

	glRotatef(angle1, osax, osay, osaz);

	glColor3f(red,green,blue);
	glBegin(GL_TRIANGLES);
		glVertex3f(-2.0f,-2.0f, 0.0f);
		glVertex3f( 2.0f, 0.0f, 0.0);
		glVertex3f( 0.0f, 2.0f, 0.0);
	glEnd();

	angle1+=angle;

	glutSwapBuffers();
}

void processNormalKeys(unsigned char key, int x, int y) {

	if (key == 27)
		exit(0);
}

void processSpecialKeys(int key, int x, int y)
{
	switch(key) 
	{
		case GLUT_KEY_F1 :
				red = 1.0f;
				green = 0.0f;
				blue = 0.0f; break;
		case GLUT_KEY_F2 :
				red = 0.0f;
				green = 1.0f;
				blue = 0.0f; break;
		case GLUT_KEY_F3 :
				red = 0.0f;
				green = 0.0f;
				blue = 1.0f; break;
		case GLUT_KEY_F4 :
				osax += 0.5f;
			//	osay = 0.0f;
			  //osaz = 0.0f; 
				break;

       	case GLUT_KEY_LEFT :
				osax -= 0.5f;
			//	osay = 0.0f;
			  //osaz = 0.0f; 
				break;


		case GLUT_KEY_F5 :
			//	osax = 0.0f;
				osay += 0.5f;
				//o saz = 0.0f;
				break;
		
		case GLUT_KEY_RIGHT :
			//	osax = 0.0f;
				osay -= 0.5f;
				//o saz = 0.0f;
				break;

        case GLUT_KEY_F6 :
				//osax = 0.0f;
				//osay = 0.0f;
				osaz += 0.5f; break;

        case GLUT_KEY_UP :
				//osax = 0.0f;
				//osay = 0.0f;
				osaz -= 0.5f; break;


         case GLUT_KEY_DOWN :
				osax = 0.0f;
				osay = 0.0f;
				osaz = 0.0f; break;


//#define GLUT_KEY_DOWN



		case GLUT_KEY_F7 :
				
				angle -= 0.5f; break;
		case GLUT_KEY_F8 :
			
				angle += 0.5f; break;

	   	case GLUT_KEY_F9 :
			
				angle += 0.0f; break;
	


	}
}

void main(int argc, char **argv) {

	// init GLUT and create window
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DEPTH | GLUT_DOUBLE | GLUT_RGBA);
	glutInitWindowPosition(100,100);
	glutInitWindowSize(800,600);
	glutCreateWindow("Lighthouse3D- GLUT Tutorial");


    glClearColor(0.0, 0.0, 0.0, 0.0);
	glClear(GL_COLOR_BUFFER_BIT);
	// register callbacks
	glutDisplayFunc(renderScene);
	glutReshapeFunc(changeSize);
	glutIdleFunc(renderScene);

	// here are the new entries
	glutKeyboardFunc(processNormalKeys);
	glutSpecialFunc(processSpecialKeys);

	// enter GLUT event processing cycle
	glutMainLoop();
}