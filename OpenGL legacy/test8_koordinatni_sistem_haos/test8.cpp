
#ifdef _WIN32
#include <windows.h>//requires that windows.h be included before either gl.h or glu.h, because some macros
#endif
#include <glut.h>
#include <stdlib.h> //Needed for "exit" function
#include <math.h>
// all variables initialized to 1.0, meaning
// the triangle will initially be white
float red=1.0f, blue=1.0f, green=1.0f;
	//glRotatef(angle, 1.0f, 0.0f, 0.0f);
float osax= 1.0f,osay= 1.0f, osaz= 1.0f;
// angle for rotating triangle
float angle = 0.0f,angle1 = 1.0f;
float camx = 0.0f, camy = 0.0f,camz = 0.0f;


float k1 = 0.25;
float k2 = 0.75;


void changeSize(int w, int h) 
{

	// Prevent a divide by zero, when window is too short
	// (you cant make a window of zero width).
	if (h == 0)
		h = 1;
	float ratio =  w * 1.0 / h;

        // Use the Projection Matrix
	glMatrixMode(GL_PROJECTION);

        // Reset Matrix
	glLoadIdentity();

	// Set the viewport to be the entire window
	glViewport(0, 0, w, h);

	// Set the correct perspective.
	gluPerspective(45.0f, ratio, 0.1f, 100.0f);

	// Get Back to the Modelview
	glMatrixMode(GL_MODELVIEW);
}

void renderScene(void) 
{

	// Clear Color and Depth Buffers
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// Reset transformations
	glLoadIdentity();

	// Set the camera
	gluLookAt(	camx, camy, camz+10.0f,
			camx, camy,  camz,
			0.0f, 1.0f,  0.0f);


	/*
	gluLookAt(	camx, camy, camz+10.0f,
			0.0f, 0.0f,  0.0f,
			0.0f, 1.0f,  0.0f);
  */
	glRotatef(angle1, osax, osay, osaz);


  glClearColor(red-0.5, green-0.5, blue-0.5, 0.0);
   glClear(GL_COLOR_BUFFER_BIT);
   glColor3f(1.0, 1.0, 1.0);


	glColor3f(red,green,blue);

	glBegin(GL_LINES); // OVDE POCINJU OBJEKTI

		glColor3f(0.0f,green,0.0f);
	glVertex3f(0.0,0.0, 0);
	glVertex3f(50, 0, -5);
glEnd(); 


	glBegin(GL_LINES); // OVDE POCINJU OBJEKTI
	
		glColor3f(red,0.0f,0.0f);
	glVertex3f(0.0,0.0, 0);
	glVertex3f(0, 50, -5);
glEnd(); 


	glBegin(GL_LINES); // OVDE POCINJU OBJEKTI

		glColor3f(0.0f,0.0f,blue);
	glVertex3f(0.0,0.0, 0);
	glVertex3f(0, 0, -50);
glEnd(); 





	glBegin(GL_TRIANGLES);
		glVertex3f(-2.0f,-2.0f, 0.0f);
		glVertex3f( 2.0f, 0.0f, 0.0);
		glVertex3f( 0.0f, 2.0f, 0.0);
	glEnd();


for (float k = 0;k<=2.0;k+=0.5)
{
	 glColor3f(1.0, 1.0, 0.0+k-0.5);
    glBegin(GL_POLYGON);
        glVertex3f(k1+k, k1+k, 0.0-k);
        glVertex3f(k2+k, k1+k, 0.0-k);
        glVertex3f(k2+k, k2+k, 0.0-k);
        glVertex3f(k1+k, k2+k, 0.0-k);
    glEnd();
}



	glBegin(GL_TRIANGLES);
//Send the vertices and colors for the triangle
glColor4f(1.0f, green, 0.0f, 1.0f);
glVertex3f(2.0f, 2.5f, -1.0f);

glVertex3f(-3.5f, -2.5f, -1.0f);
glColor4f(0.0f, 0.0f, blue, 1.0f);
glVertex3f(2.0f, -4.0f, -1.0f);
glEnd();
/*
   glBegin(GL_TRIANGLE_FAN);
//Send the vertices and colors for the pentagon
glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
glVertex3f(-1.0f, 2.0f, 5.0f);
glColor4f(1.0f, 1.0f, 5.0f, 1.0f);
glVertex3f(-5.0f, -0.5f, 0.0f);
glColor4f(0.0f, 1.0f, 1.0f, 1.0f);
glVertex3f(-1.5f, -3.0f, 0.0f);
glColor4f(0.0f, 0.0f, 0.0f, 1.0f);
glVertex3f(1.0f, -2.0f, 0.0f);
glColor4f(1.0f, 0.0f, -1.0f, 1.0f);
glVertex3f(-1.0f, 1.0f, 0.0f);
glEnd();

*/
double PI = 3.14;

double G = 6.67/10000;

double teta=0;
double x1,y1;

		for ( double teta2= 0; teta2 < 760; teta2+=1)
		{
	glBegin(GL_POINTS);//GL_POINTS GL_LINES
/*			teta2=teta;

double L = 10000;
double M = 10000;
double Msol = 10000000;
double e=0;
//double teta;
double D = M*Msol*G;


double r;

double L2 = pow(L,2);
double DM = D*M;
double l2dm = L2/DM;
double pod1 = cos((PI*teta2)/180);
double pod2 = 1+(e*pod1);
r = l2dm/pod2;*/
double r = 2500;
				x1 = r*r*sin((PI*teta2)/180);
				y1 = r*r*cos((PI*teta2)/180);
           glColor4f(red, green, blue, 1.0f);
				glVertex3f(x1,y1, 1.0f);
					
			glEnd();	
		}

glBegin(GL_LINES);
for (float i = 0;i<10;i+=0.5)
{
	for (float j = 0;i<5;i+=0.5)
{
		glColor4f(red-i, green-j, blue-0.1, 1.0f);
        glVertex3f(1.0f+i, 2.0f+j, 2.0f);

}

glColor4f(red-0.6, green-0.1, blue-0.1, 1.0f);
glVertex3f(1.0f+i, 2.0f, 2.0f);

glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
glVertex3f(-5.0f, 2.0f+i, 2.0f);

glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
glVertex3f(1.0f, -5.0f, -2.0f+i);
}
glColor4f(1.0f, 1.0f, 5.0f, 1.0f);
glVertex3f(-5.0f, -0.5f, 0.0f);
glColor4f(0.0f, 1.0f, 1.0f, 1.0f);
glVertex3f(-1.5f, -3.0f, 0.0f);
glColor4f(0.0f, 0.0f, 0.0f, 1.0f);
glVertex3f(1.0f, -2.0f, 0.0f);
glColor4f(1.0f, 0.0f, -1.0f, 1.0f);
glVertex3f(-1.0f, 1.0f, 0.0f);
glEnd();

	glFlush();

	angle1+=angle;

	glutSwapBuffers();
}

void processNormalKeys(unsigned char key, int x, int y) 
{

	if (key == 27)
		exit(0);

	switch(key) 
	{
			case 119://malo w
			camy -= 0.5f; break;
					
			case 115://malo s
			camy += 0.5f; break;
			
			case 100://malo d
			camx -= 0.5f; break;
				
			case 97://malo a
			camx += 0.5f; break;
			
			case 101://malo e
			camz -= 0.5f; break;

			case 113://malo q
			camz += 0.5f; break;



	}
}

void processSpecialKeys(int key, int x, int y)
{
	switch(key) 
	{
		case GLUT_KEY_F1 :
				red = 1.0f;
				green = 0.0f;
				blue = 0.0f; break;
		case GLUT_KEY_F2 :
				red = 0.0f;
				green = 1.0f;
				blue = 0.0f; break;
		case GLUT_KEY_F3 :
				red = 0.0f;
				green = 0.0f;
				blue = 1.0f; break;
		case GLUT_KEY_F4 :
				osax += 0.5f;
			//	osay = 0.0f;
			  //osaz = 0.0f; 
				break;

       	case GLUT_KEY_LEFT :
				osax -= 0.5f;
			//	osay = 0.0f;
			  //osaz = 0.0f; 
				break;


		case GLUT_KEY_F5 :
			//	osax = 0.0f;
				osay += 0.5f;
				//o saz = 0.0f;
				break;
		
		case GLUT_KEY_RIGHT :
			//	osax = 0.0f;
				osay -= 0.5f;
				//o saz = 0.0f;
				break;

        case GLUT_KEY_F6 :
				//osax = 0.0f;
				//osay = 0.0f;
				osaz += 0.5f; break;

        case GLUT_KEY_UP :
				//osax = 0.0f;
				//osay = 0.0f;
				osaz -= 0.5f; break;


         case GLUT_KEY_DOWN :
				osax = 0.0f;
				osay = 0.0f;
				osaz = 0.0f; 
 red=0.8f, blue=1.0f, green=0.8f;break;

//#define GLUT_KEY_DOWN



		case GLUT_KEY_F7 :
				
				angle -= 0.5f; break;
		case GLUT_KEY_F8 :
			
				angle += 0.5f; break;

	   	case GLUT_KEY_F9 :
			
				angle += 0.0f; break;
//	    	case 101://a
			
//				angle += 0.5f; break;
	   
	



	}
}


void mouse(int button, int state, int x, int y)
{
	switch (button) 
	{//////////////////////////////////
   
case GLUT_LEFT_BUTTON:

if (state == GLUT_DOWN)
{

//glutIdleFunc(spinDisplay);
	angle -= 0.1f; break;
}
break;

case GLUT_RIGHT_BUTTON:

if (state == GLUT_DOWN)
{

//glutIdleFunc(spinDisplay2);
	angle += 0.1f; break;
}
break;


case GLUT_MIDDLE_BUTTON:

if (state == GLUT_DOWN)
{
	angle = 0.0f; break;
//glutIdleFunc(NULL);
}
break;

default:break;

	} ////////////////////////////////
}

void init(void) 
{
/*  select clearing (background) color       */
    glClearColor(0.0, 0.0, 0.0, 0.0);
/*  initialize viewing values  */
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glOrtho(0.0, 1.0, 0.0, 1.0, -1.0, 1.0);
}


void main(int argc, char **argv) {

	// init GLUT and create window
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DEPTH | GLUT_DOUBLE | GLUT_RGBA);
	glutInitWindowPosition(100,100);
	glutInitWindowSize(800,600);
	glutCreateWindow("Lighthouse3D- GLUT Tutorial");





	// register callbacks
	glutDisplayFunc(renderScene);
	glutReshapeFunc(changeSize);	

	glutIdleFunc(renderScene);

	// here are the new entries
	glutKeyboardFunc(processNormalKeys);
	glutSpecialFunc(processSpecialKeys);
    glutMouseFunc(mouse);//komande misa
	// enter GLUT event processing cycle
	glutMainLoop();
}