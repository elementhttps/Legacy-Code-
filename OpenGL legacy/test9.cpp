#include <windows.h>

#include <glut.h>

static GLfloat spin = 0.0;
//static GLfloat spin2 = 0.0;

void init(void)
{
glClearColor(0.0, 0.0, 0.0, 0.0);
glShadeModel(GL_FLAT);
}

void display(void)
{
glClear(GL_COLOR_BUFFER_BIT);
glPushMatrix();
glRotatef(spin, 0.0, 0.0, 1.0);//interesantno glRotatef(spin, 1.0, 1.0, 1.0);
glColor3f(0.0, 0.0, 1.0);
glRectf(250.0, 250.0, 500.0, 500.0);
glPopMatrix();
glutSwapBuffers();
}


void spinDisplay(void)
{
spin = spin + 5.0;
if (spin > 360.0)

spin = spin - 360.0;

glutPostRedisplay();


}

void spinDisplay2(void)
{
spin = spin - 5.0;
if (spin > 360.0)

spin = spin - 360.0;

glutPostRedisplay();


}



void reshape(int w, int h)
{
glViewport(0, 0, (GLsizei) w, (GLsizei) h);
glMatrixMode(GL_PROJECTION);
/**/glLoadIdentity();
glOrtho(-1.0, 1.0, -1.0, 1.0, -1.0, 1.0);
glMatrixMode(GL_MODELVIEW);
glLoadIdentity();
gluOrtho2D(0.0, (GLdouble) w, 0.0, (GLdouble) h);
}


void mouse(int button, int state, int x, int y)
{
switch (button) 
{

case GLUT_LEFT_BUTTON:

if (state == GLUT_DOWN)
{

glutIdleFunc(spinDisplay);
}
break;

case GLUT_RIGHT_BUTTON:

if (state == GLUT_DOWN)
{

glutIdleFunc(spinDisplay2);
}
break;


case GLUT_MIDDLE_BUTTON:

if (state == GLUT_DOWN)
{
glutIdleFunc(NULL);
}
break;

default:break;

}
}










/*
* Declare initial window size, position, and display mode
* (single buffer and RGBA). Open window with "hello"
* in its title bar. Call initialization routines.
* Register callback function to display graphics.
* Enter main loop and process events.
*/

int main(int argc, char** argv)
{
glutInit(&argc, argv);
glutInitDisplayMode(GLUT_DOUBLE| GLUT_RGB);
glutInitWindowSize(250, 250);
glutInitWindowPosition(100, 100);
glutCreateWindow(argv[0]);
init();
glutDisplayFunc(display);
glutReshapeFunc(reshape);
glutMouseFunc(mouse);
glutMainLoop();
return 0;
}