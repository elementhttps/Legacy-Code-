 //klick button change paragraf background-color and paragraf text color
 $(":button").click(function(){

 $('p').css("background-color",'yellow').css("color",'green');
}); 

 //klick submit change paragraf background-color and paragraf text color
 $(":submit").click(function(){

 $('p').css("background-color",'#ffffff').css("color",'#555555');
}); 

//klick <a> element to change paragraf background-color and paragraf text color
 $("a").click(function(){

 $('p').css("background-color",'#ffffff').css("color",'#555555');
}); 

//klick <a>  or span element to change paragraf background-color and paragraf text color
 $("a,span").click(function(){

 $('p').css("background-color",'#ffffff').css("color",'#555555');
}); 

