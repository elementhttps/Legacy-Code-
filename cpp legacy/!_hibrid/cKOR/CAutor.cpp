// CAutor.cpp : implementation file
//

#include "stdafx.h"
#include "cKOR.h"
#include "CAutor.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CCAutor dialog


CCAutor::CCAutor(CWnd* pParent /*=NULL*/)
	: CDialog(CCAutor::IDD, pParent)
{
	//{{AFX_DATA_INIT(CCAutor)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void CCAutor::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CCAutor)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CCAutor, CDialog)
	//{{AFX_MSG_MAP(CCAutor)
	ON_COMMAND(IDM_AUTOR, OnAutor)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CCAutor message handlers


