#if !defined(AFX_CAUTORA_H__1AD4DADC_6579_45E1_B1DD_8E02C4F1EE2A__INCLUDED_)
#define AFX_CAUTORA_H__1AD4DADC_6579_45E1_B1DD_8E02C4F1EE2A__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// CAutorA.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CCAutorA dialog

class CCAutorA : public CDialog
{
// Construction
public:
	CCAutorA(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CCAutorA)
	enum { IDD = IDD_AUTOR };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCAutorA)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CCAutorA)
		// NOTE: the ClassWizard will add member functions here
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CAUTORA_H__1AD4DADC_6579_45E1_B1DD_8E02C4F1EE2A__INCLUDED_)
