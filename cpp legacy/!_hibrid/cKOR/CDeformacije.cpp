// CDeformacije.cpp : implementation file
//

#include "stdafx.h"
#include "cKOR.h"
#include "CDeformacije.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CCDeformacije dialog


CCDeformacije::CCDeformacije(CWnd* pParent /*=NULL*/)
	: CDialog(CCDeformacije::IDD, pParent)
{
	//{{AFX_DATA_INIT(CCDeformacije)
	m_dex = 0.0;
	m_dey = 0.0;
	m_dez = 0.0;
	m_zapreminaVo = 0.0;
	m_dVo = 0.0;
	m_radnaT = 0.0;
	m_defT = 0.0;
	//}}AFX_DATA_INIT
}


void CCDeformacije::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CCDeformacije)
	DDX_Text(pDX, IDC_EDIT1, m_dex);
	DDX_Text(pDX, IDC_EDIT2, m_dey);
	DDX_Text(pDX, IDC_EDIT3, m_dez);
	DDX_Text(pDX, IDC_EDIT4, m_zapreminaVo);
	DDX_Text(pDX, IDC_EDIT5, m_dVo);
	DDX_Text(pDX, IDC_EDIT6, m_radnaT);
	DDX_Text(pDX, IDC_EDIT7, m_defT);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CCDeformacije, CDialog)
	//{{AFX_MSG_MAP(CCDeformacije)
	ON_BN_CLICKED(IDC_BUTTON1, OnButton1)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CCDeformacije message handlers

void CCDeformacije::OnButton1() 
{
	UpdateData(true);

	m_dex =(1/t_modulE)*(t_radi-(t_poason*(t_long+t_hoop)));
    m_dey =(1/t_modulE)*(t_long-(t_poason*(t_radi+t_hoop)));
    m_dez=(1/t_modulE)*(t_hoop-(t_poason*(t_long+t_radi)));
		UpdateData(false);
	
}
