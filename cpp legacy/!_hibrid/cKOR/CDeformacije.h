#if !defined(AFX_CDEFORMACIJE_H__5DA73A7B_488D_4637_B40B_E46446F93969__INCLUDED_)
#define AFX_CDEFORMACIJE_H__5DA73A7B_488D_4637_B40B_E46446F93969__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// CDeformacije.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CCDeformacije dialog

class CCDeformacije : public CDialog
{
// Construction
public:
	double t_radi;
	double t_long;
	double t_hoop;
	double t_poason;
	double t_modulE;
	
	CCDeformacije(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CCDeformacije)
	enum { IDD = IDD_DEFORMACIJE };
	double	m_dex;
	double	m_dey;
	double	m_dez;
	double	m_zapreminaVo;
	double	m_dVo;
	double	m_radnaT;
	double	m_defT;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCDeformacije)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CCDeformacije)
	afx_msg void OnTankozid();
	afx_msg void OnButton1();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CDEFORMACIJE_H__5DA73A7B_488D_4637_B40B_E46446F93969__INCLUDED_)
