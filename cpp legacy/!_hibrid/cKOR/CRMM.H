#include "stdafx.h"
#include "Math.h"

//---------------------------------------------------
//OSNOVNA KLASA CCRM 
//---------------------------------------------------

class CCRM
{
public:	
	double M_mol;
    // double Ru;
	double k; //Bolcmanova konstanta
	double Rgg;
	double N_avo;
	double kapa;
    //double cp;
    double T_kom;
    double P_kom;
    double P_amb;
    double Mah_kr;
    double A_iz;
	double P_iz;
//----------------------------------
//BITNE NAPOMENE

//str 639  ili u pdf 653 underexpanded!!! i adaptiran mlaznik


public:
	CCRM()//KONSTRUKTOR KLASE
	{
		
     	M_mol = 32;// Kg/mol
		//Ru = 8315; // Univerzalna gasna konstanta za O2
		kapa = 1.3; //za O2

		k = 1.38;//pow(10,-23);// Bolcmanova konstanta
		N_avo = 6.022;//*pow(10,23);//Avogadrov broj

	
		T_kom = 3000;  // u K naravno :)
		P_kom = (70)*100000; //Paskala 1*10^5
		P_amb = 1*100000;  //ATMOSFERSKI PRITISAK
		P_iz = 1*100000;
		Mah_kr = 1;//str 624 dA = 0 => M=1   Napomena KPP mora da bude nesto veci od izracunatog

		//A_iz = 0.001; // m^2
        Rgg = 355.4;//355.4;
		


	}
//---------------------------------
//get funkcije u klasi //omogucava pristup vrednostima proracunatim u CRM.ccp fajlu


//-------------------
//PARAMETRI IZ KONSTRUKTORA
double CCRMgetM_mol();
double CCRMgetK();
double CCRMgetKapa();
double CCRMgetN_avo();
double CCRMgetT_kom();
double CCRMgetP_kom();
double CCRMgetP_amb();
double CCRMgetMah_kr();
double CCRMgetA_iz();
double CCRMgetRgg();
double CCRMgetP_izl();
//---------------------
//FUNKCIJE
double CCRMgetRu(); // Ru = Navo*K; 
double CCRMgetRg(); // Rg = Ru/M_mol
double CCRMgetCzKo();
double CCRMgetCp();
double CCRMgetV_iz();
double CCRMgetMahKo();
double CCRMgetPambsPkom(); //0.528
double CCRMgetROo();
double CCRMgetROkr();
double CCRMgetROiz();

};

