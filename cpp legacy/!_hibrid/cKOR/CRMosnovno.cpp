// CRMosnovno.cpp : implementation file
//

#include "stdafx.h"
#include "cKOR.h"
#include "CRMosnovno.h"
#include "CRMM.h"



#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CCRMosnovno dialog


CCRMosnovno::CCRMosnovno(CWnd* pParent /*=NULL*/)
	: CDialog(CCRMosnovno::IDD, pParent)
{
	//{{AFX_DATA_INIT(CCRMosnovno)
	

	m_Po = 0;
	m_Pi = 0;
	m_kapa = 0.0;
	m_Pamb = 0.0;
	m_To = 0.0;
	m_Rgg = 0.0;
	//}}AFX_DATA_INIT
}


void CCRMosnovno::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CCRMosnovno)

	DDX_Text(pDX, IDC_EDIT1, m_Po);
	DDX_Text(pDX, IDC_EDIT2, m_Pi);
	DDX_Text(pDX, IDC_EDIT3, m_kapa);
	DDX_Text(pDX, IDC_EDIT4, m_Pamb);
	DDX_Text(pDX, IDC_EDIT5, m_To);
	DDX_Text(pDX, IDC_EDIT6, m_Rgg);
	//}}AFX_DATA_MAP
}


double CCRMosnovno::RMOgetm_Po()
{
	return m_Po;
}


double CCRMosnovno::RMOgetm_Pi()
{
	return m_Pi;
}

double CCRMosnovno::RMOgetm_kapa()
{
	return m_kapa;
}

double CCRMosnovno::RMOgetm_Pamb()
{
	return m_Pamb;
}

double CCRMosnovno::RMOgetm_To()
{
	return m_To;
}

double CCRMosnovno::RMOgetm_Rgg()
{
	return m_Rgg;
}


BEGIN_MESSAGE_MAP(CCRMosnovno, CDialog)
	//{{AFX_MSG_MAP(CCRMosnovno)
	ON_BN_CLICKED(IDC_BUTTON1, OnButton1)
	ON_BN_CLICKED(IDC_BUTTON2, OnButton2)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CCRMosnovno message handlers

void CCRMosnovno::OnButton1() 
{
    this->UpdateData();

	CFile flEmployees;
	char strFilter[] = { "Employees Records (*.mpl)|*.mpl|All Files (*.*)|*.*||" };

	CFileDialog FileDlg(FALSE, ".mpl", NULL, 0, strFilter);

	if( FileDlg.DoModal() == IDOK )
	{
		if( flEmployees.Open(FileDlg.GetFileName(), CFile::modeCreate | CFile::modeWrite) == FALSE )
			return;
		CArchive ar(&flEmployees, CArchive::store);
/*
	CString   str;
	str.Format("%d",m_kapa);
	ar<<str;
	char a1[32];	wsprintf(a1,"%d",m_kapa)*/      

		
		ar<<m_kapa;
		ar.Close();
	}
	else
		return;

	flEmployees.Close();
		
	
}

void CCRMosnovno::OnButton2() 
{
    CFile flEmployees;
	char strFilter[] = { "Employees Records (*.mpl)|*.mpl|All Files (*.*)|*.*||" };

	CFileDialog FileDlg(TRUE, ".mpl", NULL, 0, strFilter);
	
	if( FileDlg.DoModal() == IDOK )
	{
		if( flEmployees.Open(FileDlg.GetFileName(), CFile::modeRead) == FALSE )
			return;
		CArchive ar(&flEmployees, CArchive::load);

		ar >> m_kapa;
		ar.Close();
	}
	else
		return;

	flEmployees.Close();

	UpdateData(FALSE);	
}
