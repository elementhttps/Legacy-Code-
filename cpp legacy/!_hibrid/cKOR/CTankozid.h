#if !defined(AFX_CTANKOZID_H__816BB2E6_5BFC_46D9_B3CE_A8FD1A920854__INCLUDED_)
#define AFX_CTANKOZID_H__816BB2E6_5BFC_46D9_B3CE_A8FD1A920854__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// CTankozid.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CCTankozid dialog

class CCTankozid : public CDialog
{
// Construction
public:
	CCTankozid(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CCTankozid)
	enum { IDD = IDD_TANKOZID };
	double	m_rsp2;
	double	m_run2;
	double	m_debljina2;
	double	m_pritisakPi;
	double	m_hoop;
	double	m_long;
	double	m_radi;
	double	m_zatezna2;
	double	m_ena2;
	double	m_odnosDsR;
	double	m_gnn2;
	double	m_taumax2;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCTankozid)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CCTankozid)
	afx_msg void OnButton2();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CTANKOZID_H__816BB2E6_5BFC_46D9_B3CE_A8FD1A920854__INCLUDED_)
