#include "stdafx.h"

#include "Math.h"




class GEOML
{
public:	
	double alfa; //ugao div
	double beta; //konvg ugao
	
	double aiz; // izlazni pp
	double akr; // kriticni pp
	double aul; //povrsina na ulazu u ML definisana sirinom cevi

	double dkr;
	double diz;
	double EE; //stepen sirenja mlaznika Ai sa Akr

	double Ldi;// duzina divergentnog dpp
	double Lko;// duzina konvergentnog dpp
	double Lk;// duzina kriticnog dpp
	

public:
	GEOML()//KONSTRUKTOR KLASE
	{
		double PI = 3.14159265359;

		alfa = 12 ;//STEPENI !!!
	    beta = 30;
		EE = 4;
		aiz= 2;
		dkr = 30; // POLUPRECNIK kriticnog pp mlaznika u !!  milimetrima  !! mm
		akr= pow(dkr/1000,2)*PI; // povrsina kpp r^2 * PI
		aul= 2;
		Ldi= 2;
		Lko= 2;
		Lk= 2;


	}

//---------------------------------
//get funkcije u klasi //omogucava pristup vrednostima proracunatim u xxx.ccp YEAH ! fajlu

double GEOMLgetAlfa();
double GEOMLgetBeta();
double GEOMLgetAiz();
double GEOMLgetAkr();
double GEOMLgetAul();
double GEOMLgetEE();
double GEOMLgetLdi();
double GEOMLgetLko();
double GEOMLgetLk();
double GEOMLgetDkr(); //POLUPRECNIK !!!
double GEOMLgetDiz();
double GEOMLgetL();

};
