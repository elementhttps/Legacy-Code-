#include <stdio.h>
#include <stdlib.h>
#define PI 3.14 //define constant PI in capital letters to look different in program
int main()
{
    const int RADIUS = 10;//2nd way of declaring constants
    int area;
    area = PI*RADIUS*RADIUS;
    printf("Area of the circle of radius %d is:%d",RADIUS,area);
    return 0;
}
