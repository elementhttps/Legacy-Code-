// BeepFilter.Cpp
/* Compile options needed: none
   Execute as: BeepFilter.exe <path>Beeper.exe
*/
#include <windows.h>
#include <process.h>
#include <memory.h>
#include <string.h>
#include <stdio.h>
#include <fcntl.h>
#include <io.h>

#define   OUT_BUFF_SIZE 512
#define   READ_HANDLE 0
#define   WRITE_HANDLE 1
#define   BEEP_CHAR 7

char szBuffer[OUT_BUFF_SIZE];

int Filter(char* szBuff, ULONG nSize, int nChar)
{
   char* szPos = szBuff + nSize -1;
   char* szEnd = szPos;
   int nRet = nSize;

   while (szPos > szBuff)
   {
      if (*szPos == nChar)
         {
            memmove(szPos, szPos+1, szEnd - szPos);
            --nRet;
         }
      --szPos;
   }
   return nRet;
}

int main(int argc, char** argv)
{
   int nExitCode = STILL_ACTIVE;
   if (argc >= 2)
   {
      HANDLE hProcess;
      int hStdOut;
      int hStdOutPipe[2];

      // Create the pipe
      if(_pipe(hStdOutPipe, 512, O_BINARY | O_NOINHERIT) == -1)
         return   1;

      // Duplicate stdout handle (next line will close original)
      hStdOut = _dup(_fileno(stdout));

      // Duplicate write end of pipe to stdout handle
      if(_dup2(hStdOutPipe[WRITE_HANDLE], _fileno(stdout)) != 0)
         return   2;

      // Close original write end of pipe
      close(hStdOutPipe[WRITE_HANDLE]);

      // Spawn process
      hProcess = (HANDLE)spawnvp(P_NOWAIT, argv[1],
       (const char* const*)&argv[1]);

      // Duplicate copy of original stdout back into stdout
      if(_dup2(hStdOut, _fileno(stdout)) != 0)
         return   3;

      // Close duplicate copy of original stdout
      close(hStdOut);

      if(hProcess)
      {
         int nOutRead;
         while   (nExitCode == STILL_ACTIVE)
         {
            nOutRead = read(hStdOutPipe[READ_HANDLE],
             szBuffer, OUT_BUFF_SIZE);
            if(nOutRead)
            {
               nOutRead = Filter(szBuffer, nOutRead, BEEP_CHAR);
               fwrite(szBuffer, 1, nOutRead, stdout);
            }

            if(!GetExitCodeProcess(hProcess,(unsigned long*)&nExitCode))
               return 4;
         }
      }
   }

   printf("\nPress \'ENTER\' key to continue... ");
   getchar();
   return nExitCode;
}