// COM_test_alfaDoc.cpp : implementation of the CCOM_test_alfaDoc class
//

#include "stdafx.h"
#include "COM_test_alfa.h"

#include "COM_test_alfaDoc.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CCOM_test_alfaDoc

IMPLEMENT_DYNCREATE(CCOM_test_alfaDoc, CDocument)

BEGIN_MESSAGE_MAP(CCOM_test_alfaDoc, CDocument)
	//{{AFX_MSG_MAP(CCOM_test_alfaDoc)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CCOM_test_alfaDoc construction/destruction

CCOM_test_alfaDoc::CCOM_test_alfaDoc()
{
	// TODO: add one-time construction code here

}

CCOM_test_alfaDoc::~CCOM_test_alfaDoc()
{
}

BOOL CCOM_test_alfaDoc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;

	// TODO: add reinitialization code here
	// (SDI documents will reuse this document)

	return TRUE;
}



/////////////////////////////////////////////////////////////////////////////
// CCOM_test_alfaDoc serialization

void CCOM_test_alfaDoc::Serialize(CArchive& ar)
{
	if (ar.IsStoring())
	{
		// TODO: add storing code here
	}
	else
	{
		// TODO: add loading code here
	}
}

/////////////////////////////////////////////////////////////////////////////
// CCOM_test_alfaDoc diagnostics

#ifdef _DEBUG
void CCOM_test_alfaDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CCOM_test_alfaDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CCOM_test_alfaDoc commands
