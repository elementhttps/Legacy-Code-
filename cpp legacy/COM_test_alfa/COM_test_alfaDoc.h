// COM_test_alfaDoc.h : interface of the CCOM_test_alfaDoc class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_COM_TEST_ALFADOC_H__167B4AD3_E9A8_497C_83C8_53B04FFDCA2A__INCLUDED_)
#define AFX_COM_TEST_ALFADOC_H__167B4AD3_E9A8_497C_83C8_53B04FFDCA2A__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


class CCOM_test_alfaDoc : public CDocument
{
protected: // create from serialization only
	CCOM_test_alfaDoc();
	DECLARE_DYNCREATE(CCOM_test_alfaDoc)

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCOM_test_alfaDoc)
	public:
	virtual BOOL OnNewDocument();
	virtual void Serialize(CArchive& ar);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CCOM_test_alfaDoc();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	//{{AFX_MSG(CCOM_test_alfaDoc)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_COM_TEST_ALFADOC_H__167B4AD3_E9A8_497C_83C8_53B04FFDCA2A__INCLUDED_)
