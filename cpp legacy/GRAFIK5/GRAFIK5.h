// GRAFIK5.h : main header file for the GRAFIK5 application
//

#if !defined(AFX_GRAFIK5_H__B48EFE3A_0BF5_4968_82B3_63866C103984__INCLUDED_)
#define AFX_GRAFIK5_H__B48EFE3A_0BF5_4968_82B3_63866C103984__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"       // main symbols

/////////////////////////////////////////////////////////////////////////////
// CGRAFIK5App:
// See GRAFIK5.cpp for the implementation of this class
//

class CGRAFIK5App : public CWinApp
{
public:
	CGRAFIK5App();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CGRAFIK5App)
	public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL

// Implementation
	//{{AFX_MSG(CGRAFIK5App)
	afx_msg void OnAppAbout();
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_GRAFIK5_H__B48EFE3A_0BF5_4968_82B3_63866C103984__INCLUDED_)
