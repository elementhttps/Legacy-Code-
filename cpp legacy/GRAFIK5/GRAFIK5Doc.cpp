// GRAFIK5Doc.cpp : implementation of the CGRAFIK5Doc class
//

#include "stdafx.h"
#include "GRAFIK5.h"

#include "GRAFIK5Doc.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CGRAFIK5Doc

IMPLEMENT_DYNCREATE(CGRAFIK5Doc, CDocument)

BEGIN_MESSAGE_MAP(CGRAFIK5Doc, CDocument)
	//{{AFX_MSG_MAP(CGRAFIK5Doc)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CGRAFIK5Doc construction/destruction

CGRAFIK5Doc::CGRAFIK5Doc()
{
	// TODO: add one-time construction code here

}

CGRAFIK5Doc::~CGRAFIK5Doc()
{
}

BOOL CGRAFIK5Doc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;

	// TODO: add reinitialization code here
	// (SDI documents will reuse this document)

	return TRUE;
}



/////////////////////////////////////////////////////////////////////////////
// CGRAFIK5Doc serialization

void CGRAFIK5Doc::Serialize(CArchive& ar)
{
	if (ar.IsStoring())
	{
		// TODO: add storing code here
	}
	else
	{
		// TODO: add loading code here
	}
}

/////////////////////////////////////////////////////////////////////////////
// CGRAFIK5Doc diagnostics

#ifdef _DEBUG
void CGRAFIK5Doc::AssertValid() const
{
	CDocument::AssertValid();
}

void CGRAFIK5Doc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CGRAFIK5Doc commands
