// GRAFIK5Doc.h : interface of the CGRAFIK5Doc class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_GRAFIK5DOC_H__A1FEF7F1_561F_4169_854B_19753F4619DF__INCLUDED_)
#define AFX_GRAFIK5DOC_H__A1FEF7F1_561F_4169_854B_19753F4619DF__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


class CGRAFIK5Doc : public CDocument
{
protected: // create from serialization only
	CGRAFIK5Doc();
	DECLARE_DYNCREATE(CGRAFIK5Doc)

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CGRAFIK5Doc)
	public:
	virtual BOOL OnNewDocument();
	virtual void Serialize(CArchive& ar);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CGRAFIK5Doc();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	//{{AFX_MSG(CGRAFIK5Doc)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_GRAFIK5DOC_H__A1FEF7F1_561F_4169_854B_19753F4619DF__INCLUDED_)
