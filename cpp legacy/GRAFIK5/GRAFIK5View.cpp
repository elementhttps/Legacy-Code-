// GRAFIK5View.cpp : implementation of the CGRAFIK5View class
//


#include "stdafx.h"
#include "GRAFIK5.h"

#include <stdlib.h> //Needed for "exit" function
#include <fstream.h>


#include "GRAFIK5Doc.h"
#include "GRAFIK5View.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CGRAFIK5View

IMPLEMENT_DYNCREATE(CGRAFIK5View, CView)

BEGIN_MESSAGE_MAP(CGRAFIK5View, CView)
	//{{AFX_MSG_MAP(CGRAFIK5View)
	ON_WM_KEYDOWN()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CGRAFIK5View construction/destruction

CGRAFIK5View::CGRAFIK5View()
{
		m_Mreza = 1;
	m_DGrafik = 0;
	m_Zumx= 20;
	m_Zumy= 20;
	m_Korak = 10;
	m_IDev = 100000;
	m_GMreza = 5000;

	m_viewx = 1;
	m_viewy = -1;

	m_Orgfx =90;
	m_Orgfy =620;

	dx = 1;
	m_paint = 1;


	m_pp1 = 1;

   nWidth =0;
   nHeight =0;
   Px = 0; //nema promene u pocetnom trenutku P=0
   Py = 0;
   P = 0;
   dxzum =0; 
   dyzum =0; 

   dxs =0;
   dys =0;
   x =0;
   y=0;
   t=1;
   swichT=1;


}

CGRAFIK5View::~CGRAFIK5View()
{
}

BOOL CGRAFIK5View::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	return CView::PreCreateWindow(cs);
}

/////////////////////////////////////////////////////////////////////////////
// CGRAFIK5View drawing

void CGRAFIK5View::OnDraw(CDC* dc)
{
	CGRAFIK5Doc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);
	if (!pDoc)
		return;
	CRect rect;
	CBrush brsBlack, *brsOld;

GetClientRect(&rect);
	brsBlack.CreateSolidBrush(RGB(0, 0, 0));


	CBitmap bitID;
	CDC dcID;

	// Load the bitmap from the resource
	bitID.LoadBitmap(IDB_BITMAP1);
	// Create a memory device compatible with the above CPaintDC variable
	dcID.CreateCompatibleDC(dc);
	// Select the new bitmap
	CBitmap *BmpPrevious = dcID.SelectObject(&bitID);

	// Copy the bits from the memory DC into the current dc
	dc->BitBlt(20, 10, 400, 400, &dcID, 0, 0, SRCCOPY);

	// Restore the old bitmap
	dc->SelectObject(BmpPrevious);
	// Do not call CView::OnPaint() for painting messages
	


/*/----------------------------------------------------------
// POKRETNE LINIJE NA POZICIJU MISA
//----------------------------------------------------------
 dc->MoveTo(point.x-4, 0 );//LINIJA ZA PRACENJE X OSE  
 dc->LineTo(point.x-4, point.y+10000);

 dc->MoveTo(0,point.y-40);//LINIJA ZA PRACENJE Y OSE
 dc->LineTo(point.x+10000, point.y-40);
//----------------------------------------------------------*/


	CRect Recto; //PRAVOUGAONIK EKRANA
    GetClientRect (&Recto);//PRAV. KLIJENTA TJ KORISNIKA I NJEGOVE REZOLUCIJE

	nWidth  =Recto.Width();//int MERA SIRINE
    nHeight = Recto.Height();// int MERA DUZINE

GetCursorPos(&point);// ZADNJA POZICIJA MISA KORISNIKA



//----------------------------------------------------------	     
//POZICIJA TEKSTA NA KORDINATU EKRANA!!! KOS1 FIKSNA POZICIJA
//----------------------------------------------------------
char t1[10];
sprintf(t1, "Pozicija  (%d,%d)",point.x,  point.y);
dc->TextOut (nWidth/1.2,nHeight/5 ,t1);

char t2[10];
sprintf(t2, "ORG (%dx,%dy)",m_Orgfx,  m_Orgfy);
dc->TextOut (nWidth/1.2,nHeight/6 ,t2);

char t3[10];
sprintf(t3, "Zum (%dx,%dy)",m_Zumx,  m_Zumy);
dc->TextOut (nWidth/1.2,nHeight/8,t3);

char t4[10];
sprintf(t4, "(X: %d Y: %d)",xLogical+dxzum,yLogical+dyzum);
dc->TextOut (nWidth/1.2,nHeight/10 ,t4);

char t6[10];
sprintf(t6, "(t: %d)",t);
dc->TextOut (nWidth/1.2,nHeight/4.5 ,t6);


int xLogical1= (point.x-m_Orgfx)/(m_viewx/m_Zumx);
int yLogical1 =(point.y-m_Orgfy)/(m_viewy/m_Zumy);

	 int sxLogical1=xLogical1+dxzum;

    int  syLogical1=yLogical1+dyzum;
//Invalidate();

dxg =sxLogical1;
dyg =syLogical1;


char t5[10];
sprintf(t5, "(dX: %d dY: %d)",dxg,dyg);
dc->TextOut (nWidth/1.2,nHeight/13 ,t5);
//----------------------------------------------------------


//----------------------------------------------------------
//LINIJE NA CENTRU NEMOBILAN PO PRESEKU REZOLUCIJE EKRANA	
//----------------------------------------------------------
	CPen PenBlue2(PS_SOLID, 1, RGB(0, 0, 255));
	dc->SelectObject(PenBlue2);

    dc->MoveTo(nWidth / 2, 0);  //LINIJA ZA PRIKAZ POLOVINE X EKRANA
	dc->LineTo(nWidth / 2, nHeight);
	dc->MoveTo(0, nHeight/ 2);//LINIJA ZA PRIKAZ POLOVINE Y EKRANA
	dc->LineTo(nWidth, nHeight / 2);
//----------------------------------------------------------


//----------------------------------------------------------
//POKRETNI TEKST NA POZICIJU MISA X I Y	
//----------------------------------------------------------	
	dc->SetTextColor(RGB(100, 100, 0)); 

    dc->TextOut(point.x-100, point.y-50, 'X');
	dc->TextOut(point.x-4, point.y+50, 'Y');
   //////////////////////////////////////////////////////
/*
   MSG msg;
  
        if (!AfxGetApp ()->PumpMessage ()) {
            ::PostQuitMessage (0);
         
        }
    LONG lIdle = 0;
    while (AfxGetApp ()->OnIdle (lIdle++));
    return TRUE;

*/


   dc->SetMapMode(MM_ANISOTROPIC); //orijentacija osa PO ZELJI PROGRAMERA



dc->SetViewportOrg(m_Orgfx, m_Orgfy);

/*
To control your own unit system, the orientation of the axes or how the 
application converts the units used on your application, use either the 
MM_ISOTROPIC or the MM_ANISOTROPIC map modes.
*/


   //  //90 -620  definise pocetnu referentnu tacku na ekranu  


/*
Since you are drawing on a device context, all you need to do is simply call
the CDC::SetViewportOrg() method. It is overloaded with two versions, which 
allow you to use either the X and the Y coordinates or a defined point. 
 
   The syntaxes of this method are:

SetViewportOrg(int X, int Y);
SetViewportOrg(CPoint Pt);


*/

	dc->SetWindowExt(m_Zumx,m_Zumy); //SetWindowExtent(100,100)  SetViewportExtent(200,200)  100 jedinica = 200 pixela
/*
CSize SetWindowExt(int cx, int cy);
CSize SetWindowExt(SIZE size);

Therefore, after calling SetMapMode() and specifying the MM_ISOTROPIC (or MM_ANISOTROPIC),
you must call the CDC:SetWindowExt() method. This method specifies how much each new unit
will be multiplied by the old or default unit system.


*/ //1 m = 2 pixela
	


	dc->SetViewportExt(m_viewx,m_viewy);//1 -1
/*
CSize SetViewportExt(int cx, int cy);
CSize SetViewportExt(SIZE size);



To use the first version of this function, you must provide the units of device conversion 
as cx for the horizontal axis and as cy for the vertical axis.



----------------------------------
Logical coordinates  //jedinice npr metri...
----------------------------------
(also referred to as page coordinates ) are determined
by the mapping mode. For example, the MM_LOENGLISH mapping mode has
logical coordinates in units of 0.01 inches, with the origin in the top left corner
of the client area, and the positive y axis direction running from bottom to top.

----------------------------------
Device coordinates   //pikseli
----------------------------------
(also referred to as client coordinates  in a window) are
measured in pixels in the case of a window, with the origin at the top left corner
of the client area, and with the positive y axis direction from top to bottom.
These are used outside of a device context, for example for defining the
position of the cursor in mouse message handlers.


----------------------------------
Screen coordinates 
----------------------------------
are measured in pixels and have the origin at the top left
corner of the screen, with the positive y axis direction from top to bottom.
These are used when getting or setting the cursor position.


The formulae that are used by Windows to convert from logical coordinates to device
coordinates are:


xDevice = (xLogical-xWindowOrg)*(xViewPortExt/xWindowExt)+xViewportOrg

yDevice = (yLogical-yWindowOrg)*(yViewPortExt/yWindowExt)+yViewportOrg



dc->SetViewportOrg(m_Orgfx, m_Orgfy); ViewOrg pozicija x,y
dc->SetWindowExt(m_Zumx,m_Zumy);   winExt je zum
dc->SetViewportExt(m_viewx,m_viewy); orijentacija osa
dc->SetWindowOrg




*/
	
	





//PRETVARANJE POZICIJA MISA U LOGICKE KOORDINATE ZA PRIKAZ NA SEKUNDARNOM KOS
   xLogical= (point2.x-m_Orgfx)/(m_viewx/m_Zumx);
   yLogical =(point2.y-m_Orgfy)/(m_viewy/m_Zumy);

	 int sxLogical=xLogical+dxzum;

    int  syLogical=yLogical+dyzum;




CPen PenGreen2(PS_SOLID, 1, RGB(0,255, 0));
dc->SelectObject(PenGreen2);



     dc->MoveTo(sxLogical, 0 );
     dc->LineTo(sxLogical, syLogical);

     dc->MoveTo(0, syLogical );
     dc->LineTo(sxLogical, syLogical);

//=---------------------------------------------------------

/*zum je 10 uvecannje je za 500 po x i y osi
	     11 550
         12 600
         ...
  za zum 20 uvecanje je 1000 x y

  za svaki 1 zum uvecanje je za 50 m_orgx po x ili y pravcu

dx = 1000
zum 20

dzum = 1000/20 = 50

xLogic = F(x)
za svaku promenu zuma ili pozicije x y treba dodati ili oduzeti funkciju od zuma

-u pocetnom trenutku vrednost treba da bude 0
- nakon pomene oduzeti ili dodati vrednost F(zum)

  1.pre promene

  xLogic = F(x)+0

  2.nakon promene

  xLogic = F(x)+-F(zum)

  F(zum) = dx


  dx = 50*20; jeste 1000 tacno

  dx = 50*m_zumx

  ideja uvesti varijablu promene

  dx = 50*m_zumx*P
  
	P=1,0

  P = 0 u pocetnom
  P = -1,+1 u zavisnosti od smera kretanja 
RESENO POMERENJE POMOCU TASTATURE

*/


/*
	 PRILIKOM ZUMA 
	 zum je ekvivalentan m_Zum*(trenutna_pozicija)*P

  trenutna_pozicija misa 

  m_Zum   pozicija
   1           251  
   2           2*251 = 502
   3           753
    ...        ...
   10          2510

  xLog =m_Zumx*(point.x)*P
  yLog =m_Zumy*(point.y)*P

  DukupnoX = dxzum+dxs
  DukupnoX = 50*m_zumx*P+m_Zumx*(point.x)*P


	 
	 
	 
 */






   xLogical2= (point3.x-m_Orgfx)/(m_viewx/m_Zumx);
   yLogical2 =(point3.y-m_Orgfy)/(m_viewy/m_Zumy);



   	 int sx2Logical=xLogical2+dxzum;

    int  sy2Logical=yLogical2+dyzum;

dxs =sx2Logical;
dys =sy2Logical;

CPen PenOrange(PS_SOLID, 1, RGB(100,100, 0));
dc->SelectObject(PenOrange);



     dc->MoveTo(dxs, 0 );
     dc->LineTo(dxs, dys);

     dc->MoveTo(0, dys );
     dc->LineTo(dxs, dys);
/*
*/

//----------------------------------------------------------
// OSE
	CPen PenBlack40(PS_SOLID, 5, RGB(0, 0, 0));
	int osax = 20000000;
	int osay = 20000000;
	int startx = 0;
	int starty = 0;
	
	dc->SelectObject(PenBlack40);
	dc->MoveTo(-1000,     starty);
	dc->LineTo( osax,     starty);
	dc->MoveTo(   startx, -1000);
	dc->LineTo(   startx,  osay);


//----------------------------------------------------------



//----------------------------------------------------------	
//POZICIJA OZNAKA MREZE LENJIRA
//----------------------------------------------------------	
	

    
switch (m_paint)
 {


		case 0 :

		break;


	case 1 :
//linije na svakih skala po x i y osi
double skala_xy_1 = 100;
double skala_xy_2 = 500;
double skala_xy_3 = 1000;


		
	CPen PenBlack3(PS_SOLID, 3, RGB(0, 0, 0));
	dc->SelectObject(PenBlack3);
    
	for(double koox = 0; koox < 100000; koox += skala_xy_1)
    {
        dc->MoveTo(koox, 0);
        dc->LineTo(koox, 1000/m_viewy);//bilo -1000
    }

	for(double kooy = 0; kooy < 100000; kooy += skala_xy_1)
    {
        dc->MoveTo(0, kooy);
        dc->LineTo(-1000/m_viewx, kooy);
    }


//linije na svakih skalaA po x i y osi



	dc->SelectObject(PenBlack3);
    for(double koox2 = 0; koox2 < 100000; koox2 += skala_xy_2)
    {
        dc->MoveTo(koox2, 0);
        dc->LineTo(koox2, 2000/m_viewy);
    }
	for(double kooy2 = 0; kooy2 < 100000; kooy2 += skala_xy_2)
    {
        dc->MoveTo(0, kooy2);
        dc->LineTo(-2000/m_viewx, kooy2);
    }
//linije na svakih n po x i y osi 



	for(double koox3 = 0; koox3 < 100000; koox3 += skala_xy_3)
    {
        dc->MoveTo(koox3, 0);
        dc->LineTo(koox3, 10000/m_viewy);
    }

	
	for(double kooy3 = 0; kooy3 < 100000; kooy3 += skala_xy_3)
    {
        dc->MoveTo(0, kooy3);
        dc->LineTo(-10000/m_viewx, kooy3);
    }
//----------------------------------------------------------------------
    dc->SetTextColor(RGB(0, 0, 0));
	for (int kt1= 0; kt1<20000; kt1+=1000) 
	{
        
        CString string;
        string.Format (("%d "), (kt1/100 ) );
        dc->TextOut (kt1, 3000/m_viewy, string);
    }
/**/
	dc->SetTextColor(RGB(255, 0, 0));

	for (int kt2= 0; kt2<100000; kt2+=10000) 
	{
        
        CString string;
        string.Format (("%d"), (((kt2)) ) );
        dc->TextOut (-8000/m_viewx, kt2, string);
    }

         break;
			
}
	
fstream inStream;
//inStream.open("DL2_data1.txt", ios ::in);
inStream.open("dDDpodLmlaznika[data].txt", ios ::in);//PotisakPritisak_data
if(!inStream) 
{
	cout << "File would not open\n";
	return;
}
//CPen PenBlue2(PS_SOLID, 2, RGB(0, 0, 255));

	CPen PenBlue0(PS_SOLID, 1, RGB(94, 198, 7));//PS_SOLID
dc->SelectObject(PenBlue0);

double xj,yj;

while(!inStream.eof()) 
	 
{
dc->MoveTo(xj*100, yj*1);
inStream >> xj >> yj; //read from disc file
dc->SetPixel(xj*100,  yj*1, RGB(94, 19, 87));
dc->LineTo(xj*100,  yj*1);

	
 }
inStream.close();


CPen PenBlue1(PS_SOLID, 1, RGB(0,0, 255));

dc->SelectObject(PenBlue1);
double xj_skala =xj*100;
double yj_skala = yj*1;
//MAX vrednost upravno na osu X
dc->MoveTo(xj_skala,0);
dc->LineTo(xj_skala,yj_skala);

//MAX vrednost upravno na osu Y
dc->MoveTo(0,yj_skala);
dc->LineTo(xj_skala,yj_skala);
/**/


	

/*
fstream inStream2;
inStream2.open("DL2_data.txt", ios ::in);
if(!inStream2) 
{
	cout << "File would not open\n";
	return;
}

double xj2,yj2;
while(!inStream2.eof()) 
	  {
inStream2 >> xj2 >> yj2; //read from disc file

dc->SetPixel(xj2*100, yj2*100, RGB(0, 0, 255));	
 }

inStream2.close();
*/

















    


	// TODO: add draw code for native data here
}

/////////////////////////////////////////////////////////////////////////////
// CGRAFIK5View diagnostics

#ifdef _DEBUG
void CGRAFIK5View::AssertValid() const
{
	CView::AssertValid();
}

void CGRAFIK5View::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}

CGRAFIK5Doc* CGRAFIK5View::GetDocument() // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CGRAFIK5Doc)));
	return (CGRAFIK5Doc*)m_pDocument;
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CGRAFIK5View message handlers

void CGRAFIK5View::OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags) 
{
	switch (nChar)
 {
	case VK_RIGHT :	//PlaySound(_T("Type.wav"),NULL,SND_ASYNC);
			m_Orgfx-=50;
			m_pp1 = 0;
			xLogical=+50;
			
			Px=1;
		    dxzum -=50*m_Zumx*Px;
			Px=0;
	
            //int dyzum =50*m_Zumy*Py; 
   
			
	
			Invalidate();
         break;

	case VK_LEFT :
		m_Orgfx+=50;
			m_pp1 = 0;
			xLogical=-50;
			Px=1;
			dxzum +=50*m_Zumx*Px;
			Px=0;
		
		
	
			Invalidate();
		break;

	case VK_UP :
		m_Orgfy+=50;
			m_pp1 = 0;
			Py=1;
			dyzum -=50*m_Zumy*Py;
			Py=0;
			
		




	
	
			Invalidate();
		break;

    case VK_DOWN:
		m_Orgfy-=50;
			m_pp1 = 0;
			Py=1;
			dyzum +=50*m_Zumy*Py;
			Py=0;

	



		
		Invalidate();
		break;
    
	case VK_HOME:
		m_Orgfx = 90;
		m_Orgfy = 620;
		m_Zumx= 60;
        m_Zumy = 60;
	    m_viewx = 1;
	    m_viewy = -1;

			m_pp1 = 0;
		Invalidate();
		break;

	case VK_F2:	
		m_Zumx +=10;
		m_Zumy +=10;
			m_pp1 = 0;
			dxzum =0;
			dyzum =0;
	
		Invalidate();
		break;

	case VK_F1:	
		m_Zumx -=10;
		m_Zumy -=10;
		m_pp1 = 0;
		dxzum =0;
		dyzum =0;
	
		if (m_Zumx < 1)
		{
			m_Zumx =1;
			m_Zumy =1;
		}
			
		Invalidate();
		break;

	case VK_F3:	
		m_Zumx -=1;
		m_Zumy -=1;
		
		if (m_Zumx < 1)
		{
			m_Zumx =1;
			m_Zumy =1;
		}
			m_pp1 = 0;
		Invalidate();
		break;

	case VK_F4:	//f4
		m_Zumx +=1;
		m_Zumy +=1;
		m_pp1 = 0;
	
		Invalidate();
		break;

	case 0x41://a
	
			m_viewx -=1;
		

	
		Invalidate();
		break;

	case 0x53://s
		
		m_viewx +=1;
		

	
		Invalidate();
		break;

	case 0x5a://z
	
		m_viewy -=1;

	
		Invalidate();
		break;

	case 0x58://x
	
         m_viewy +=1;
	
		Invalidate();
		break;



}	
	CView::OnKeyDown(nChar, nRepCnt, nFlags);
}
