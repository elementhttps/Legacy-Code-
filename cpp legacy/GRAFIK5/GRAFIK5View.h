// GRAFIK5View.h : interface of the CGRAFIK5View class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_GRAFIK5VIEW_H__03C64E15_D5D7_440D_AE8D_C3C521E59DDB__INCLUDED_)
#define AFX_GRAFIK5VIEW_H__03C64E15_D5D7_440D_AE8D_C3C521E59DDB__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


class CGRAFIK5View : public CView
{
protected: // create from serialization only
	CGRAFIK5View();
	DECLARE_DYNCREATE(CGRAFIK5View)

// Attributes
public:
	CGRAFIK5Doc* GetDocument();

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CGRAFIK5View)
	public:
	virtual void OnDraw(CDC* pDC);  // overridden to draw this view
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	protected:
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CGRAFIK5View();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	//{{AFX_MSG(CGRAFIK5View)
	afx_msg void OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
	protected:
			double m_Korak;
	int m_Zumx;
	int m_Zumy;
	int m_GMreza;

	int m_IDev;
	int m_Mreza;
	int m_DGrafik;


	int m_Orgfx;
	int m_Orgfy;

	int dx;
	int m_pp1;

	double m_viewx;
	double m_viewy; 
    
	CPoint point;
	CPoint point2;
	CPoint point3;
    CPoint previous;

	CRect Recto;
int m_paint;
	int nWidth;
    int nHeight;

	int xLogical;
    int yLogical;
	int xLogical2;
    int yLogical2;
	int  sxLogical;
	int  syLogical;

	int Px; //faktor promene smera
	int Py; //faktor promene smera
	int P;
	int dxzum; 
    int dyzum; 

	int dxs;
	int dys;

	int dxg;
	int dyg;
	double x,y,t;

int swichT; 
};

#ifndef _DEBUG  // debug version in GRAFIK5View.cpp
inline CGRAFIK5Doc* CGRAFIK5View::GetDocument()
   { return (CGRAFIK5Doc*)m_pDocument; }
#endif

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_GRAFIK5VIEW_H__03C64E15_D5D7_440D_AE8D_C3C521E59DDB__INCLUDED_)
