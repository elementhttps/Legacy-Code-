
#ifdef _WIN32
#include <windows.h>//requires that windows.h be included before either gl.h or glu.h, because some macros
#endif
#include <glut.h>
#include <stdlib.h> //Needed for "exit" function


static int year = 0, day = 0;
double alfa = 0;
float camA =4;

void init(void) 
{
   glClearColor(0.0, 0.0, 0.0, 0.0);
   glShadeModel(GL_FLAT);
}
void display(void)
{
   glTranslatef(0.0, 0.0, 0.0); 
   glClear(GL_COLOR_BUFFER_BIT);
   glColor3f(1.0, 1.0, 1.0);//rgb
   glPushMatrix();/////////////////////////////////////////////////////////
   glutWireSphere(1, 15, 2);   /* draw sun */


   
   glTranslatef(alfa, 0.0, 0.0); 
   glColor3f(0.0, 1.0, 1.0);
     glutWireSphere(1, 15, 2);    /* draw smaller planet */
     
  
 glPopMatrix();

    

   glutSwapBuffers();
}
void reshape(int w, int h)
{
	glClearColor(0.0, 0.0, 0.0, 0.0);
/*  initialize viewing values  */
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    //glOrtho(0.0, 1.0, 0.0, 1.0, -1.0, 1.0);
	glOrtho(0.0, 1.0, 0.0, 1.0, -1.0, 1.0);
	/*
   glViewport(0, 0, (GLsizei) w, (GLsizei) h); 
   glMatrixMode(GL_PROJECTION);
   glLoadIdentity();
   gluPerspective(90.0, (GLfloat) w/(GLfloat) h, 1.0, 10.0);
   glMatrixMode(GL_MODELVIEW);
   glLoadIdentity();
   
   gluLookAt(0.0, 0.0, 1.0+camA, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0);*/
}
void keyboard(unsigned char key, int x, int y)
{
   switch (key) {


          case 'e':
         alfa+=0.1;
         glutPostRedisplay();
         break;

		     case 'r':
         alfa-=0.1;
         glutPostRedisplay();
         break;

  
	   case 'q':
         camA += 1;
       
         glutPostRedisplay();
         break;

      default:
		           break;
   }
}
int main(int argc, char** argv)
{
   glutInit(&argc, argv);
   glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB);
   glutInitWindowSize(1000, 1000); 
   glutInitWindowPosition(100, 100);
   glutCreateWindow(argv[0]);
   init();
   glutDisplayFunc(display); 
   glutReshapeFunc(reshape);
   glutKeyboardFunc(keyboard);
   glutMainLoop();
   return 0;
}