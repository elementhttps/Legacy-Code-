/*******************************************************************************
 * NAME: SerialADEmbed.h
 * DATE:  7/5/2003
 * PRGR: Y. Bai - Copyright 2003 - 2007
 * DESC:  Header file for serial A/D converter testing program
 ******************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <conio.h>
#define    MAXNUM 100 // Max data length
#define     BYTE         unsigned char // Data type
#define     USHORT unsigned short
#define    COM1 3f8h // COM1 base address
#define    IER 3f9h // COM1 Interrupt Enable Register
#define    LCR 3fbh // COM1 Line Control Register
#define    MCR 3fch // COM1 Modem Control Register
#define     LSR 3fdh // COM1 Line Status Register
#define     MSR 3feh // COM1 Modem Status Register
void      Port_Init();
void      c_inial();
BYTE  Port_Read();
BYTE  c_getad();