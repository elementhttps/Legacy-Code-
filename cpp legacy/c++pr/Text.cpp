#include<stdio.h>
#include <math.h>
#define PI 3.14159265


int main() {
   double x1[50],y1[50],Step,Min,Max,x,y;
   int points, i;
   Min = 0.000000;
   Max = 6.000000;
   points = 50;
   Step = (Max - Min)/(points-1);
   for(i=0;i<points;i++) {
      x = Min + (i*Step);
      y = sin(sin(2*x));
      x1[i] = x;
      y1[i] = y;
   }
   plot (x1, y1, "Plot of y = sin(sin(2*x))", "X", "Y");
}