#include <iostream>
#include <math.h>

using namespace std;

int main()
{


    //Cil1 Aspo =6283  D*PI*L   D^2*PI/4 *L =V =A*L   314*L=31400
    /*
    nagib zljeba 9.27 na horizontalu ili 80.73 na vertikalu
    D20 L=100
    pitch zljeba = 10mm
    broj okreta = 7
    duzina sipke = 100mm
    visina zljeba na sipci= pitch * broj okreta = 70mm
    Cil2 Aspo =4056  Aun=1684   6283-5740=543
    dubina_zljeba= 2.5
    visna_zljeba= 5
    povrsina_zljeba=2.5*5=12.5
    povrsina_zljeba_putanje =940
    duzina spirale =940/2.5=376  ..325    430 5375
    srednja_vrednost_putanje= (430+325 ) /2 =377
    12.5*376 =4700mm3
    zapremina_po_krugu 4700mm3/7=671mm3


    */
    double Vm3 = pow(1000,3);
    double Vmm3 =671/Vm3;



    double PI=3.14;
    double protok_volumen,brzina,protok_masa,povrsina,otvor_precnik,otvor_precnik_m,gustina;
    double povrsina_A,Ds_m,Ds_mm,Dvratila_m,Dvratila_mm,brzina_vm,brzina_vcm,maseni_protok;
    double brojokreta_min,brojokreta_s;
    brojokreta_min =100;
    brojokreta_s = brojokreta_min/60;
    brzina_vcm = 1;
    brzina_vm = brzina_vcm/100;
    Ds_mm = 20;//spoljni precnik srafa
    Ds_m = Ds_mm/1000;
    Dvratila_mm =10;//precnik vratila
    Dvratila_m = Dvratila_mm/1000;
    povrsina_A = (PI/4)*(pow(Ds_m,2)-pow(Dvratila_m,2));//m^2
    gustina =980;//kg/m^3
    maseni_protok = povrsina_A*gustina*brzina_vm;
    double grama_po_okretu = Vmm3*gustina*1000;
    cout<<Vmm3<<" m^3"<<endl;
    cout<<grama_po_okretu<<" g po okretu "<<endl;
    cout<<brojokreta_s*grama_po_okretu<<" g/s ili "<< brojokreta_s*grama_po_okretu/1000<<" Kg/s"<<endl;
    cout<<"Pri "<<brojokreta_min<<" okreta po minuti ili "<<brojokreta_s<<" BO po sekundi"<<endl;
    cout<<maseni_protok<<"Kg/s pri brzini od "<<brzina_vm<<" m/s"<<endl;
    cout<<maseni_protok*1000<<"g/s pri brzini od "<<brzina_vcm<<" cm/s"<<endl;


    otvor_precnik = 5;//mm
    otvor_precnik_m =otvor_precnik/1000;
    brzina= 0.1;//10cm/s
    povrsina = (PI*pow(otvor_precnik_m,2))/4;
    protok_volumen = brzina*povrsina;
    protok_masa = protok_volumen*gustina*1000;
    cout<<protok_volumen<<" m^3/s"<<endl;
    cout<<protok_masa<<" g/s"<<endl;
    cout << "Hello world!" << endl;
    return 0;
}
