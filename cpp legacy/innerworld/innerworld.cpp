// innerworld.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>

using namespace std;


void funkcija2()
{
	int x = 20;
	cout << "x u funkciji2 je " << x << endl;
	{ 
		cout << "u malom lokalu x ima " << x << endl;
		int x = 30;
		cout << "u jos manjem lokalu x je " << x << endl;
	}

}

int main()
{
	int x = 10;
	cout << "x u main() je "<< x << endl;

	funkcija2();

	cout << "x na kraju je " << x << endl;

	return 0;

}
//  funkcija mora uvek da ima zagrade npr int main(), double f(), void f()...
//  u okviru funkcije int main () x ima vrednost 10, compiler nastavlja i ulazi u funkciju2() 
// u funkciji2() uvodimo vrednost iste oznake ali razlicitog broja x u funkciji2() ima vrednost x = 20
// u okviru funkcije2() nalazi se blok 
//                                       {
//                                             int x = 30
//                                        }
// 
// vrednost u bloku je novo x koje samo na tom nivou ima vrednost x = 30
// 
// 
// 
// ZAKLJUCAK >>>> vrednosti iste oznake ali razlicitog inteziteta postoje samo u okviru svojih nivoa ogranicenim zagradama {} 
// date vrednosti se gube tj nestaju pri prelaska iz jednog u drugi nivo
// 
