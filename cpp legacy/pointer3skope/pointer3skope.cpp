// pointer3skope.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
using namespace std;



void fos(int x);
void fosP(int *ptr);        // koriscenjem pointera tj. adrese koja pokazuje na vrednost izbegava se skope



int main()
{
	
	int var = 10;

	cout << "vrednost var je " << var << endl;

	int * p1;
	p1 = &var;
	
	cout << "Pointer1 ima dodeljenu vrednost " << *p1 << endl; // ili umesto *pointer1 moze se napisati vrednost var
	

	
	fos(var);

	cout << "da li je vrednost var povecana nakon funkcije fos  " << *p1 << endl;



  fosP(p1);
  // ili fosP(&var) c++ ce da prepozna kao pointer type

	cout << "da li je vrednost var povecana nakon funkcije fosP  " << *p1 << endl;

	
	
	return 0;
}
void fos(int x)
{
	x++;
}

void fosP(int *ptr)
{
   (*ptr)++;
}