
#ifdef _WIN32
#include <windows.h>//requires that windows.h be included before either gl.h or glu.h, because some macros
#endif
#include <glut.h>
#include <stdlib.h> //Needed for "exit" function
#include "math.h"
#include <glframe.h> 

/*
void glOrtho(GLdouble left, GLdouble right, GLdouble bottom,
GLdouble top, GLdouble near, GLdouble far );

Left and right values specify the minimum and maximum coordinate value displayed along the x-axis;
Bottom and top are for the y-axis. 
Near and far parameters are for the z-axis,


*/

const int nD = 5; //medjusobno rastojanje sfera
int brojSfera = 50;//broj sfera
float gustinaSfere = 50;//50


float dr = 0.00000001; 



float PI = 3.14;




GLFrame    frameCamera;

float camx = 0.0f, camy = 0.0f,camz = 0.0f;
float red=1.0f, blue=1.0f, green=1.0f;
float deltaX = 80;
float n = 10;

float dX=0.0;
float dY=0.0;
float dZ=0.0;


float dX1=0.0;
float dY1=0.0;
float dZ1=0.0;

///////////////////////////////////////////////////////////
// Called to draw scene
void RenderScene(void)
	{
	// Clear the window with current clearing color
	glClear(GL_COLOR_BUFFER_BIT);

   	// Set current drawing color to red
	//		   R	 G	   B
	glColor3f(1.0f, 0.0f, 0.0f);

	// Draw a filled rectangle with current color
//	glRectf(-25.0f, 25.0f, 25.0f, -25.0f);//draw  rect


	//HMMMMMMMMMMMMMMM????????
/*
	// Set the camera
	gluLookAt(	camx, camy, 10, //-10
			camx, camy,  camz,
			0.0f, 1.0f,  0.0f);




   glClear(GL_COLOR_BUFFER_BIT);

		gluLookAt(	0.1, camy, -1,
			camx, camy,  camz,
			0.0f, 1.0f,  0.0f);

*/

float drs,dru,r,podkoren,konstV,rs3;
long double Vc;

Vc =900;//neki broj;
r =10;



drs = r+dr;
rs3 =drs; //pow(drs,3);
konstV = (3*PI)/(4*Vc);


podkoren = rs3-konstV;

dru = pow(podkoren,0.01);


float r1,r2,deltaru;
r1=20;//900-400=500
r2=r1+n;
float r3,r4;
float r5,r6;

r3=r2+n;
deltaru=(r2*r2)-(r1*r1);//A je konstantno
r4=sqrt((deltaru+(r3*r3)));
r5=r4+n; 
r6=sqrt((deltaru+(r5*r5)));
float r7,r8;

r7=r6+n;
r8=sqrt((deltaru+(r7*r7)));




//----------------------------------------------------------------
    glPushMatrix();/////////////////////////////////////////////////////////
frameCamera.ApplyCameraTransform();


//center screen kos

glBegin(GL_LINES); // OVDE POCINJU OBJEKTI
	glColor3f(0.0f,green,0.0f);
	glVertex3f(0.0,0.0, 0);
	glVertex3f(50, 0, -5);
glEnd(); 


glBegin(GL_LINES); // OVDE POCINJU OBJEKTI
	glColor3f(red,0.0f,0.0f);
	glVertex3f(0.0,0.0, 0);
	glVertex3f(0, 50, -5);
glEnd(); 


glBegin(GL_LINES); // OVDE POCINJU OBJEKTI
	glColor3f(0.0f,0.0f,blue);
	glVertex3f(0.0,0.0, 0);
	glVertex3f(0, 0, -50);
glEnd(); 


	glColor3f(1.0, 0.0, 0.0);//center sphere
     glutWireSphere(1, gustinaSfere, 2);    /*  */
////////////////////////////////////////////////////////

	
	

		  


		glTranslatef(dX-100, dY-100, dZ); 


/*
BITNO 

x+1

x+i
nije isto kad se primeni u loopu 
x+1 - povecava za 1 10 11 12 ... 
x+i - povecava svaki za PREDHODNO i 10 +i  11+i 12+i



*/

//loop za broj sfera i crtanje koordinata



   for (int o=0; o<brojSfera;o+=1)//obroj crtanja
   {
	  // POMERI svaku sferu translatorno za +10 Y 
          glTranslatef(dX1, dY1+nD, dZ1);///+ n bitnoooooo!!!!!!!!!


//verzija 2
    glColor3f(1.0, 0.0, 0.0);
   glutWireSphere(10+dru, gustinaSfere, 2);   /*  */ //fiksni omotac 
   glutWireSphere(drs, gustinaSfere, 2);    /* */




//nacrtaj za svaku sferu horizontalne koordinatne y ose 
 glBegin(GL_LINES); // OVDE POCINJU OBJEKTI
	glColor3f(0.0f,green,0.0f);
	glVertex3f(0.0,0.0, 0);//startne koordinate POSLE TRANSLATE!!! tj od translirane pozicije
	glVertex3f(2500, 0, -5);//y osa ide do 2500
 glEnd();



//loop u loopu u okviru sfera nacrtaj podeoke na x osi
for(int i=0;i<30;i++)//podeoci na x osama  
{
//nacrtaj podeoke na  y osama
glBegin(GL_LINES); // OVDE POCINJU OBJEKTI
	glColor3f(red,0.0f,0.0f);
	glVertex3f(0.0+i*10,0.0, 0);
	glVertex3f(0.0+i*10, 50, -5);
glEnd();
}







//unutrasnja sfera crvena
     glColor3f(1.0, 0.0, 0.0);
     glutWireSphere(1, gustinaSfere, 2);    /*  */


//prvi spoljni omotac 
  glColor3f(1.0, 1.0, 0.0);
   glutWireSphere(r2, 20, 10);   /*  */ //fiksni omotac 
   glutWireSphere(r1, gustinaSfere, 2);    /* */
 
	 
//omotac 2.
 glColor3f(0.1, 1.0, 1.0);
     glutWireSphere(r3, gustinaSfere, 2);    /* */
     glutWireSphere(r4, gustinaSfere, 2);    /*  */


    // omotac 3
 glColor3f(0.5, 0.1, 1.0);
     glutWireSphere(r5, gustinaSfere, 2);    /* */
     glutWireSphere(r6, gustinaSfere, 2);    /*  */
        
	     // omotac 4  //spoljni omotac najveci !!!
 glColor3f(1.0, 1.0, 1.0);
	    glutWireSphere(r7, gustinaSfere, 2);  
        glutWireSphere(r8, gustinaSfere, 2);    

  }
 glPopMatrix();


    

   glutSwapBuffers();
	// Flush drawing commands
    glFlush();
	}


///////////////////////////////////////////////////////////
// Setup the rendering state
void SetupRC(void)
    {
    // Set clear color to blue
    glClearColor(0.0f, 0.0f, 0.0f, 0.0f);//set (clear) background 
    }


///////////////////////////////////////////////////////////
// Called by GLUT library when the window has chanaged size
void ChangeSize(int w, int h)
	{
	GLfloat aspectRatio;

	// Prevent a divide by zero
	if(h == 0)
		h = 1;
		
	// Set Viewport to window dimensions
    glViewport(0, 0, w, h);

	// Reset coordinate system
	glMatrixMode(GL_PROJECTION);//The projection matrix is the place where you actually define your viewing volume
	glLoadIdentity();

	// Establish clipping volume (left, right, bottom, top, near, far)
	aspectRatio = (GLfloat)w / (GLfloat)h;
    if (w <= h) 
		glOrtho (-100.0, 100.0, -100 / aspectRatio, 100.0 / aspectRatio, 1.0, -1.0);
    else 
		glOrtho (-100.0 * aspectRatio, 100.0 * aspectRatio, -100.0, 100.0, 1.0, -1.0);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	}


void keyboard(unsigned char key, int x, int y)
{
   switch (key) {


	   case 'y':
         dr-=1;
	         glutPostRedisplay();
         break;

		     case 'u':
        dr+=1;
         glutPostRedisplay();
         break;




          case 'e':
         deltaX-=1;
		  glFlush();
         glutPostRedisplay();
         break;

		     case 'r':
        deltaX+=1;
         glutPostRedisplay();
         break;

  
	   case 'f':
         n -= 1;
       
         glutPostRedisplay();
         break;

	  case 'g':
         n += 1;
       
         glutPostRedisplay();
         break;

     

   case 'w':
         dY += 1;
       
         glutPostRedisplay();
         break;

   case 's':
         dY -= 1;
       
         glutPostRedisplay();
         break;
   
   case 'a':
         dX -= 1;
       
         glutPostRedisplay();
         break;

   case 'd':
         dX += 1;
       
         glutPostRedisplay();
         break;




      default:
		           break;
   }
}

// Respond to arrow keys by moving the camera frame of reference
void SpecialKeys(int key, int x, int y)
    {
    if(key == GLUT_KEY_UP)
       frameCamera.MoveForward(0.1f); //u dubinu ili Z osa
    if(key == GLUT_KEY_DOWN)
        frameCamera.MoveForward(-0.1f);
   
                        
    // Refresh the Window
    glutPostRedisplay();
    }
///////////////////////////////////////////////////////////
// Main program entry point
int main(int argc, char* argv[])
    {
    glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);
       glutInitWindowSize(1200, 700);
	glutCreateWindow("GLRect");
	glutDisplayFunc(RenderScene);
    glutReshapeFunc(ChangeSize);
	  glutKeyboardFunc(keyboard);
	     glutSpecialFunc(SpecialKeys);
	SetupRC();
	glutMainLoop();
        
    return 0;
    }

/*


  interesantno !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


  int broj_sfera =5;

glColor3f(1.0, 0.0, 0.0);
 	for (int a = 1; a <= broj_sfera; a++)
			{
				  int ra1,ra2;
				  ra1=10*a;
				             
				   glutWireSphere(ra1, gustinaSfere, 2); 
				  ra2=sqrt((deltaru+(ra1*ra1)));
				  glutWireSphere(ra2, gustinaSfere, 2); 
		//		glVertex3f(hMapX, hHeighFtield[a], hMapZ);
					
			}

  */