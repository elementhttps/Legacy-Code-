
#ifdef _WIN32
#include <windows.h>//requires that windows.h be included before either gl.h or glu.h, because some macros
#endif
#include <glut.h>
#include <stdlib.h> //Needed for "exit" function
#include "math.h"


/*
void glOrtho(GLdouble left, GLdouble right, GLdouble bottom,
GLdouble top, GLdouble near, GLdouble far );

Left and right values specify the minimum and maximum coordinate value displayed along the x-axis;
Bottom and top are for the y-axis. 
Near and far parameters are for the z-axis,





*/


float deltaX = 80;
float n = 10;
///////////////////////////////////////////////////////////
// Called to draw scene
void RenderScene(void)
	{
	// Clear the window with current clearing color
	glClear(GL_COLOR_BUFFER_BIT);

   	// Set current drawing color to red
	//		   R	 G	   B
	glColor3f(1.0f, 0.0f, 0.0f);

	// Draw a filled rectangle with current color
	glRectf(-25.0f, 25.0f, 25.0f, -25.0f);//draw red rect


	glTranslatef(0.0, 0.0, 0.0); 
   glClear(GL_COLOR_BUFFER_BIT);


float r1,r2,deltaru,gustinaSfere;
r1=20;//900-400=500
r2=r1+n;
float r3,r4;
float r5,r6;

r3=r2+n;
deltaru=(r2*r2)-(r1*r1);//A je konstantno
r4=sqrt((deltaru+(r3*r3)));
r5=r4+n; 
/* //r_nepar=r_n-1+n
//rpar=sqrt(deltaru+(r_nepar*r_enepar))
     
	   glColor3f(1.0, 0.0, 0.0);
      glutWireSphere(r1, gustinaSfere, 2);  
    glutWireSphere(r2, gustinaSfere, 2);
    

	glBegin(GL_POINTS);//GL_POINTS 
		for (int hMapX = 0; hMapX < hmWidth; hMapX++)
		{
			for (int hMapZ = 0; hMapZ < hmHeight; hMapZ++)
			{
				
				glVertex3f(hMapX, hHeighFtield[hMapX][hMapZ], hMapZ);
					
			}
		}




*/
r6=sqrt((deltaru+(r5*r5)));
float r7,r8;

r7=r6+n;
r8=sqrt((deltaru+(r7*r7)));


gustinaSfere=20;

//----------------------------------------------------------------

   
   glPushMatrix();/////////////////////////////////////////////////////////
     glColor3f(1.0, 0.0, 0.0);

     glutWireSphere(1, gustinaSfere, 2);    /*  */


  glColor3f(1.0, 1.0, 1.0);//rgb
   glutWireSphere(r2, 20, 10);   /* draw sun */
 glColor3f(1.0, 1.0, 1.0);
     glutWireSphere(r1, gustinaSfere, 2);    /* draw smaller planet */

	 
//spoljni omotac
 glColor3f(1.0, 1.0, 1.0);
     glutWireSphere(r3, gustinaSfere, 2);    /* */

    glColor3f(1.0, 1.0, 1.0);
     glutWireSphere(r4, gustinaSfere, 2);    /*  */


    //spoljni omotac 2
	  glColor3f(1.0, 1.0, 1.0);
     glutWireSphere(r5, gustinaSfere, 2);    /* */

   glColor3f(1.0, 1.0, 1.0);
     glutWireSphere(r6, gustinaSfere, 2);    /*  */

	    glutWireSphere(r7, gustinaSfere, 2);  
        glutWireSphere(r8, gustinaSfere, 2);    

	 
	//---------------- POKRETNA SFERA//------------------------------------
	  
   
   glTranslatef(deltaX+1, 0.0, 0.0); 
   int broj_sfera =4;
     glColor3f(1.0, 0.0, 0.0);

     glutWireSphere(1, gustinaSfere, 2);    /*  */


  glColor3f(1.0, 1.0, 1.0);//rgb
   glutWireSphere(20, 20, 10);   /* draw sun */

 	for (int a = 0; a <= broj_sfera; a++)
			{
				  int ra1,ra2;
				  ra1=(r1+(a*10))+n;
				  int ra=20;
				        glColor3f(1.0, 0.0, 0.0);     
				   glutWireSphere(ra1, gustinaSfere, 2); 


				   int dr =ra1+a; 
				   int deltaru2=(ra1*ra1)-(ra*ra);//A je konstantno
			       	  ra2=sqrt(deltaru2+(dr*dr));
				  glColor3f(1.0, 0.0, 1.0);
				 glutWireSphere(ra2, gustinaSfere, 2); 
	
					
			}
/*
      glColor3f(0.0, 1.0, 1.0);
     glutWireSphere(1, gustinaSfere, 2);  


   glColor3f(0.0, 1.0, 1.0);
     glutWireSphere(r2, gustinaSfere, 2);   
    glColor3f(0.0, 1.0, 1.0);
     glutWireSphere(r1, gustinaSfere, 2);   
  //-------------------------------------------

//spoljni omotac
//r4=sqrt((deltaru+(r3*r3)));
	    glColor3f(0.0, 1.0, 1.0);
     glutWireSphere(r3, gustinaSfere, 2);    

   glColor3f(0.0, 1.0, 1.0);
     glutWireSphere(r4, gustinaSfere, 2);    
	 
    //spoljni omotac 2
	   glColor3f(0.0, 1.0, 1.0);
     glutWireSphere(r5, gustinaSfere, 2);   

    glColor3f(0.0, 1.0, 1.0);
     glutWireSphere(r6, gustinaSfere, 2);   
    
    glutWireSphere(r7, gustinaSfere, 2);  
    glutWireSphere(r8, gustinaSfere, 2);  
 */
 glPopMatrix();

    

   glutSwapBuffers();
	// Flush drawing commands
    glFlush();
	}


///////////////////////////////////////////////////////////
// Setup the rendering state
void SetupRC(void)
    {
    // Set clear color to blue
    glClearColor(0.0f, 0.0f, 0.0f, 0.0f);//set (clear) background 
    }


///////////////////////////////////////////////////////////
// Called by GLUT library when the window has chanaged size
void ChangeSize(int w, int h)
	{
	GLfloat aspectRatio;

	// Prevent a divide by zero
	if(h == 0)
		h = 1;
		
	// Set Viewport to window dimensions
    glViewport(0, 0, w, h);

	// Reset coordinate system
	glMatrixMode(GL_PROJECTION);//The projection matrix is the place where you actually define your viewing volume
	glLoadIdentity();

	// Establish clipping volume (left, right, bottom, top, near, far)
	aspectRatio = (GLfloat)w / (GLfloat)h;
    if (w <= h) 
		glOrtho (-100.0, 100.0, -100 / aspectRatio, 100.0 / aspectRatio, 1.0, -1.0);
    else 
		glOrtho (-100.0 * aspectRatio, 100.0 * aspectRatio, -100.0, 100.0, 1.0, -1.0);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	}


void keyboard(unsigned char key, int x, int y)
{
   switch (key) {


          case 'e':
         deltaX-=1;
		  glFlush();
         glutPostRedisplay();
         break;

		     case 'r':
        deltaX+=1;
         glutPostRedisplay();
         break;

  
	   case 'q':
         n -= 1;
       
         glutPostRedisplay();
         break;

	  case 'w':
         n += 1;
       
         glutPostRedisplay();
         break;

      default:
		           break;
   }
}

///////////////////////////////////////////////////////////
// Main program entry point
int main(int argc, char* argv[])
    {
    glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);
       glutInitWindowSize(800, 600);
	glutCreateWindow("GLRect");
	glutDisplayFunc(RenderScene);
    glutReshapeFunc(ChangeSize);
	  glutKeyboardFunc(keyboard);
	SetupRC();
	glutMainLoop();
        
    return 0;
    }

/*


  interesantno !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


  int broj_sfera =5;

glColor3f(1.0, 0.0, 0.0);
 	for (int a = 1; a <= broj_sfera; a++)
			{
				  int ra1,ra2;
				  ra1=10*a;
				             
				   glutWireSphere(ra1, gustinaSfere, 2); 
				  ra2=sqrt((deltaru+(ra1*ra1)));
				  glutWireSphere(ra2, gustinaSfere, 2); 
		//		glVertex3f(hMapX, hHeighFtield[a], hMapZ);
					
			}

  */