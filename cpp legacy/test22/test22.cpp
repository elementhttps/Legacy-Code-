
#ifdef _WIN32
#include <windows.h>//requires that windows.h be included before either gl.h or glu.h, because some macros
#endif
#include <glut.h>
#include <stdlib.h> //Needed for "exit" function
#include <math.h>


// Define a constant for the value of PI
#define GL_PI 3.1415f

// Rotation amounts
static GLfloat xRot = 0.0f;
static GLfloat yRot = 0.0f;
static GLfloat zRot = 0.0f;

// Called to draw scene
void RenderScene(void)
	{
	GLfloat x,y,z,angle; // Storeage for coordinates and angles

GLfloat sizes[2];    // Store supported line width range
GLfloat step;        // Store supported line width increments
// Get supported line width range and step size
glGetFloatv(GL_LINE_WIDTH_RANGE,sizes);
glGetFloatv(GL_LINE_WIDTH_GRANULARITY,&step);


//GLfloat sizes[2];       // Store supported point size range
//GLfloat step;           // Store supported point size increments
GLfloat curSize;        // Store current point size
	// Clear the window with current clearing color
	glClear(GL_COLOR_BUFFER_BIT);

	// Save matrix state and do the rotation
	glPushMatrix();
	glRotatef(xRot, 1.0f, 0.0f, 0.0f);
	glRotatef(yRot, 0.0f, 1.0f, 0.0f);
    glRotatef(zRot, 0.0f, 0.0f, 1.0f);

	// Get supported point size range and step size
//glGetFloatv(GL_POINT_SIZE_RANGE,sizes);
//glGetFloatv(GL_POINT_SIZE_GRANULARITY,&step);//

// Set the initial point size
curSize = sizes[0];
// Call only once for all remaining points
//g// Enable Stippling


	z = -80.0f;
	for(angle = 0.0f; angle <= (2.0f*GL_PI)*3.0f; angle += 0.05f)
		{
		x = 80.0f*sin(angle);
		y = 80.0f*cos(angle);
	
glLineWidth(curSize);
		// Specify the point size before the primitive is specified
//        glPointSize(curSize);//glPointSize must be called outside the glBegin/glEnd statements
	// Draw the point
glBegin(GL_LINES);
glVertex3f(x, y, z);
glVertex3f(x, y, z-angle);
glEnd();/**/
curSize += step;
		z += 0.5f;
		}
glEnd();


	// Restore transformations
	glPopMatrix();

	// Flush drawing commands
	glutSwapBuffers();
	}

// This function does any needed initialization on the rendering
// context. 
void SetupRC()
	{
	// Black background
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f );

	// Set drawing color to green
	glColor3f(0.0f, 1.0f, 0.0f);
	}

void SpecialKeys(int key, int x, int y)
	{
	if(key == GLUT_KEY_UP)
		xRot-= 5.0f;

	if(key == GLUT_KEY_DOWN)
		xRot += 5.0f;

	if(key == GLUT_KEY_LEFT)
		yRot -= 5.0f;

	if(key == GLUT_KEY_RIGHT)
		yRot += 5.0f;

	if(key == GLUT_KEY_F1)
		zRot += 5.0f;

	if(key == GLUT_KEY_F2)
		zRot -= 5.0f;

	if(key > 356.0f)
		zRot = 0.0f;

	if(key < -1.0f)
		zRot = 355.0f;

	if(key > 356.0f)
		xRot = 0.0f;

	if(key < -1.0f)
		xRot = 355.0f;

	if(key > 356.0f)
		yRot = 0.0f;

	if(key < -1.0f)
		yRot = 355.0f;

	// Refresh the Window
	glutPostRedisplay();
	}


void ChangeSize(int w, int h)
	{
	GLfloat nRange = 100.0f;

	// Prevent a divide by zero
	if(h == 0)
		h = 1;

	// Set Viewport to window dimensions
    glViewport(0, 0, w, h);

	// Reset projection matrix stack
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	// Establish clipping volume (left, right, bottom, top, near, far)
    if (w <= h) 
		glOrtho (-nRange, nRange, -nRange*h/w, nRange*h/w, -nRange, nRange);
    else 
		glOrtho (-nRange*w/h, nRange*w/h, -nRange, nRange, -nRange, nRange);

	// Reset Model view matrix stack
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	}

int main(int argc, char* argv[])
	{
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);//double-buffered window, where the drawing commands are actually
	                                             //executed on an offscreen buffer and then quickly
                                                 //swapped into view on the window
	glutCreateWindow("Points Example");
	glutReshapeFunc(ChangeSize);
	glutSpecialFunc(SpecialKeys);
	glutDisplayFunc(RenderScene);
	SetupRC();
	glutMainLoop();

	return 0;
	}
/*
There are two advantages to using a strip of triangles instead of specifying each triangle
separately. First, after specifying the first three vertices for the initial triangle, you need to
specify only a single point for each additional triangle. This saves a lot of program or data
storage space when you have many triangles to draw. The second advantage is mathemati-
cal performance and bandwidth savings. Fewer vertices means a faster transfer from your
computerís memory to your graphics card and fewer vertex transformations (see Chapter 4).
  */