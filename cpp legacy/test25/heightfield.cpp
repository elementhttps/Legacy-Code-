#include <stdio.h>
#include <glut.h>

#include "heightfield.h"

bool SwiftHeightField::Create(char *hFileName, const int hWidth, const int hHeight){	
	hmHeight = hHeight;
	hmWidth = hWidth;
	
	FILE *fp;

	fp = fopen(hFileName, "rb");

	fread(hHeightField, 1, hWidth * hHeight, fp);

	fclose(fp);

	return true;
}

void SwiftHeightField::Render(void)
{

	glBegin(GL_POINTS);//GL_POINTS 
		for (int hMapX = 0; hMapX < hmWidth; hMapX++)
		{
			for (int hMapZ = 0; hMapZ < hmHeight; hMapZ++)
			{
				
				glVertex3f(hMapX, hHeightField[hMapX][hMapZ], hMapZ);
					
			}
		}
	glEnd();

	
	/*
	glBegin(GL_POINTS);//GL_POINTS 
		for (int hMapX = 0; hMapX < hmWidth; hMapX++)
		{
			for (int hMapZ = 0; hMapZ < hmHeight; hMapZ++)
			{
				
				glVertex3f(hMapX, hHeightField[hMapX][hMapZ], hMapZ);
					
			}
		}
	glEnd();



	for (int hMapX = 0; hMapX < hmWidth; hMapX++)
	{
		for (int hMapZ = 0; hMapZ < hmHeight; hMapZ++)
		{
			glBegin(GL_TRIANGLE_STRIP);
				glVertex3f(hMapX, hHeightField[hMapX][hMapZ], hMapZ);	
				glVertex3f(hMapX, hHeightField[hMapX][hMapZ + 1], hMapZ + 1);	
				glVertex3f(hMapX + 1, hHeightField[hMapX + 1][hMapZ], hMapZ);	
				glVertex3f(hMapX + 1, hHeightField[hMapX + 1][hMapZ + 1], hMapZ + 1);
			glEnd();
		}
	}







	*/
}