/* clock example: countdown */
#include <stdio.h>
#include <time.h>



int main ()
{
  int n;
  time_t t;
  time(&t);
  printf(ctime(&t)); 
  printf ("Starting countdown...\n");
  for (n=0; n<60; n++)
  {
    printf ("%d\n",n);
    clock_t endwait;
  endwait = clock () + 1 * CLOCKS_PER_SEC ;
  while (clock() < endwait) {}
	if (n == 8){return 0;}
  }
  printf ("FIRE!!!\n");
  return 0;
}
