
#include <iostream>
#include <stdio.h>
#include <string>             // for STL string class
#include <windows.h>          // for HANDLE
#include <process.h>          // for _beginthread()
using namespace std;


class ThreadX
{
private:
  int loopStart;//isto sto i startvalue
  int loopEnd; // isto sto i end value 
  int dispFrequency;// isto sto i frequvency

public:
  string threadName; // isto sto i CString
  string mm;

  ThreadX( int startValue, int endValue, int frequency )
  {
    loopStart = startValue;
    loopEnd = endValue;
    dispFrequency = frequency;
  }


  static unsigned __stdcall ThreadStaticEntryPoint(void * pThis)//POCETAK THREDA 
  {
      ThreadX * pthX = (ThreadX*)pThis;   // the tricky cast //pointer na threadX pthX =ThreadStaticEntryPoint na void funkciju
      pthX->ThreadEntryPoint(); // now call the true entry-point-function //pozivam iz klase funkciju
     

      return 1;          // the thread exit code              //KRAJ THREDA 
  }

  void ThreadEntryPoint()
  {
     // This is the desired entry-point-function 

    for (int i = loopStart; i <= loopEnd; ++i)//ne moze da se ubace startValue,endValue jer se nalaze van void funkcije
    {
      if (i % dispFrequency == 0)//% operator koji zaokruzava 5/2 = 2 npr
      {
		 //Sleep(10);
          printf( "%s: i = %d\n", threadName.c_str(), i );//napisi naziv threada pomocu printf
		  printf( "%s ------- /%s /%s \n", threadName.c_str() );
	  }
    }
    
    printf( "%s thread terminating\n", threadName.c_str() );
  }
};


int main()
{
    // All processes get a primary thread automatically. 
//-------------------------------------------------------------------------------------
    ThreadX * o1 = new ThreadX( 0, 10000000, 2000 );

    HANDLE   hth1;// handle na thread 1 
    unsigned  uiThread1ID; //unsigned uzmi samo pozitivne vrednosti

	hth1 = (HANDLE)_beginthreadex( NULL,         // security
                                   0,            // stack size
                                   ThreadX::ThreadStaticEntryPoint,//uzmi iz funkcije vrednosti
                                   o1,           // arg list
                                   CREATE_SUSPENDED,  // so we can later call ResumeThread()
                                   &uiThread1ID );

    if ( hth1 == 0 )//ako je handle ekvivalentno nuli
        printf("Failed to create thread 1\n");

    DWORD   dwExitCode;

    GetExitCodeThread( hth1, &dwExitCode );  // should be STILL_ACTIVE = 0x00000103 = 259 // ubaci u exit code vrednost
    printf( "initial thread 1 exit code = %u\n", dwExitCode );

	

    o1->threadName = "t1";
//-------------------------------------------------------------------------------------

//-------------------------------------------------------------------------------------
    ThreadX * o2 = new ThreadX( -10000000, 0, 2000 );

    HANDLE   hth2;
    unsigned  uiThread2ID;

    hth2 = (HANDLE)_beginthreadex( NULL,         // security
                                   0,            // stack size
                                   ThreadX::ThreadStaticEntryPoint,//uzmi iz funkcije
                                   o2,           // arg list
                                   CREATE_SUSPENDED,  // so we can later call ResumeThread()
                                   &uiThread2ID );

    if ( hth2 == 0 )
        printf("Failed to create thread 2\n");

    GetExitCodeThread( hth2, &dwExitCode );  // should be STILL_ACTIVE = 0x00000103 = 259
    printf( "initial thread 2 exit code = %u\n", dwExitCode );

    o2->threadName = "t2";
//-------------------------------------------------------------------------------------

//-------------------------------------------------------------------------------------
    ThreadX * o3 = new ThreadX( -10000000,0 , 5000);
    HANDLE hth3;
	unsigned uiThread3ID;

     hth3 = (HANDLE)_beginthreadex( NULL,         // security
                                   0,            // stack size
                                   ThreadX::ThreadStaticEntryPoint,//uzmi iz funkcije
                                   o3,           // arg list
                                   CREATE_SUSPENDED,  // so we can later call ResumeThread()
                                   &uiThread3ID );
     if ( hth3 == 0 )
        printf("Failed to create thread 3\n");
     GetExitCodeThread( hth3, &dwExitCode );  // should be STILL_ACTIVE = 0x00000103 = 259
    
	 o3->threadName = "t3";
//--------------------------------------------------------------------------------------

   
    ResumeThread( hth1 );   
    Sleep(10);//10
    ResumeThread( hth2 );
	Sleep(10);//10
    ResumeThread( hth3 );
   
    WaitForSingleObject( hth1, INFINITE );
    WaitForSingleObject( hth2, INFINITE );
    WaitForSingleObject( hth3, INFINITE );


    GetExitCodeThread( hth1, &dwExitCode );
    printf( "thread 1 exited with code %u\n", dwExitCode );

    GetExitCodeThread( hth2, &dwExitCode );
    printf( "thread 2 exited with code %u\n", dwExitCode );

    GetExitCodeThread( hth3, &dwExitCode );
    printf( "thread 3 exited with code %u\n", dwExitCode );

    CloseHandle( hth1 );
    CloseHandle( hth2 );
	CloseHandle( hth3 );

    delete o1;//cisti thread1
    o1 = NULL;

    delete o2;//cisti thread2
    o2 = NULL;

	delete o3;//cisti thread3
    o3 = NULL;


     char f;
	 std::cin>>f;

    printf("Primary thread terminating.\n");
}
